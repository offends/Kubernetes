> 本文作者：丁辉

# Kubernetes容器启动报打开的文件数过多

**failed to create fsnotify watcher: too many open files**

> 系统中打开文件数目过多,导致无法创建更多的文件监视器

- 临时

  ```bash
  sysctl -w fs.inotify.max_user_instances=2099999999
  ```

- 永久

  ```bash
  echo "fs.inotify.max_user_instances = 2099999999" >> /etc/sysctl.conf
  sysctl -p
  ```

重启容器后恢复

> 清理慎用
>
> ```bash
> lsof | awk '{print $2}' | sort | uniq -c | sort -n | awk '{if ($1 > threshold) print $2}' | xargs -I{} kill {}
> ```

