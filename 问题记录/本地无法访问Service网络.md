> 本文作者：丁辉

# 本地无法访问Service网络

## 添加路由

> 引发问题场景：
>
> - 一主机多网卡情况下
> - 缺失路由导致无法通讯或主机默认路由网段错误

|   主机网关    | 是否为 Kubernetes 使用网络 |
| :-----------: | :------------------------: |
| 192.168.1.100 |             是             |
| 192.168.2.100 |             否             |

1. 查看默认路由

   ```bash
   ip route show default
   ```

   > 查看路由基础命令
   >
   > - 查看路由
   >
   >   ```bash
   >   route -n
   >   ```
   >
   > - 查看路由
   >
   >   ```bash
   >   ip route
   >   ```

2. 添加默认路由

   ```bash
   route add default gw 192.168.1.100 dev eth0
   ```

   > 或使用 `ip route` 命令
   >
   > ```
   > ip route add default via 192.168.1.100 dev eth0
   > ```

3. 刷新路由缓存

   ```bash
   ip route flush cache
   ```

4. 添加路由后，需要重启 Kubernetes 网络插件

## 遇到问题

> 如果已存在默认路由呢？

**删除默认路由**

```bash
route del default gw 192.168.2.100 dev eth0
```

或

```bash
ip route del default via 192.168.2.100 dev eth0
```

