> 本文作者：丁辉

# K3s-Helm部署报错

报错内容为

```bash
Error: INSTALLATION FAILED: Kubernetes cluster unreachable: Get "http://localhost:8080/version": dial tcp [::1]:8080: connect: connection refused
```

> 问题原因：本地没有 kube-config 导致 K3s 无法连接到 Kubernetes 集群引起的

执行命令解决

```bash
kubectl config view --raw > ~/.kube/config
```

