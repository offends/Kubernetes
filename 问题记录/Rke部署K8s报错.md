> 本文作者：丁辉

# Rke部署K8s报错

- Rke 部署的时候报错：Failed to set up SSH tunneling for host

  > 解决办法

  1. 改动一下 SSH 参数

     ```bash
     vim /etc/ssh/sshd_config
     ```

  2. 修改此参数

     ```bash
     AllowTcpForwarding yes
     ```

- RKE部署 K8s 其中 Nginx-proxy 容器缺失配置文件

  > 在初始化 RKE 的时候报错可能会是说检测失败

  问题原因是因为 Nginx-proxy arm 版 docker 镜像有问题，导致 Nginx 配置文件内容不正确，替换一下内容即可修复

  1. 编辑配置文件

     ```bash
     vi nginx.conf
     ```

     内容如下

     ```nginx
     error_log stderr notice;
     
     worker_processes auto;
     events {
       multi_accept on;
       use epoll;
       worker_connections 1024;
     }
     
     stream {
             upstream kube_apiserver {
                 
                 server 10.206.16.12:6443;
                 
             }
     
             server {
                 listen        6443;
                 proxy_pass    kube_apiserver;
                 proxy_timeout 10m;
                 proxy_connect_timeout 2s;
     
             }
     
     }
     ```

  2. 拷贝配置文件到容器内

     ```bash
     docker cp nginx.conf nginx-proxy:/etc/nginx/nginx.conf
     ```

  3. 重启容器

     ```bash
     docker restart nginx-proxy
     ```

     



