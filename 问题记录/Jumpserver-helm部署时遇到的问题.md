> 本文作者：丁辉

# Jumpserver-helm部署时遇到的问题

[jumpserver官网地址](https://docs.jumpserver.org/zh/master/install/setup_by_fast/?h=labels)

- 组件启动一直处于准备状态未启动	(原因是因为有一个 Svc 没有生成出来导致其他 Pod 无法连接)

  (这个问题有点久远了，当时写这份文档的时候没有截图报错哈哈)

  1. 部署 service

     ```bash
     vi jump-svc.yaml
     ```

     内容如下

     ```yaml
     apiVersion: v1
     kind: Service
     metadata:
       name: jump-service
     spec:
       selector:
         app.jumpserver.org/name: jms-web
       ports:
       - protocol: TCP
         port: 80
         targetPort: 80
         nodePort: 30000
       type: NodePort
     ```

  2. 部署

     ```bash
     kubectl apply -f jump-svc.yaml
     ```

- Jumpsevrer-helm部署遇到解析未自动生成问题

  单独给pod添加域名解析

  - 写法1

    ```yaml
    hostAliases:
      - ip: 127.0.0.1
        hostnames:
        - "$yuming"
    ```

  - 写法2

    ```yaml
    hostAliases:
    - hostnames:
      - $yuming
      ip: 127.0.0.1
    ```

    