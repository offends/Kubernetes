> 本文作者：丁辉

# Kubeadm安装K8s失败记录

- 在我部署 k8s 的时候，最近会遇到 flannel 一直重启的情况（报错 pod cidr not assigne）

  ```bash
  vim /etc/kubernetes/manifests/kube-controller-manager.yaml
  ```

  ```bash
  - --allocate-node-cidrs=true
  - --cluster-cidr=10.244.0.0/16
  ```

  ```bash
  systemctl restart kubelet
  ```

- k8s-kubeadm单节点搭建导致主节点污点禁止pod运行

  ```bash
  #查看污点
  kubectl describe nodes master | grep -A 3 Taints
  #删除
  kubectl taint nodes <node> <taints>-
  #一次删除多个
  kubectl taint nodes --all node-role.kubernetes.io/control-plane- node-role.kubernetes.io/master-
  ```

- k8s-kubeadm-containerd搭建网络通讯问题

  ```bash
  vim /etc/kubernetes/manifests/kube-controller-manager.yaml
  增加参数：

  - --allocate-node-cidrs=true
  - --cluster-cidr=10.244.0.0/16

  systemctl restart kubelet
  ```

- K8s "tab" 报错

  ```bash
  yum install bash-completion -y
  source /usr/share/bash-completion/bash_completion
  source <(kubectl completion bash)
  ```

- 安装 K8s 失败，发现缺少一下模块

  1. 添加模块

     ```bash
     cat > /etc/sysconfig/modules/ipvs.modules <<EOF    
     #!/bin/bash
     modprobe -- ip_vs
     modprobe -- ip_vs_rr
     modprobe -- ip_vs_wrr
     modprobe -- ip_vs_sh
     modprobe -- nf_conntrack
     EOF
     ```

  2. 加载

     ```bash
     modprobe br_netfilter
     lsmod | grep netfilter
     sysctl -p /etc/sysctl.d/k8s.conf
     sysctl --system
     ```

  3. 替换镜像源

     ```bash
     sed -i 's#sandbox_image = "k8s.gcr.io/pause:3.6"#sandbox_image = "registry.aliyuncs.com/google_containers/pause:3.6"#' /etc/containerd/config.toml
     ```

     
