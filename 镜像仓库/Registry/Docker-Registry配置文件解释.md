> 本文作者：丁辉

# Docker-Registry配置文件解释

[官网文档](https://docs.docker.com/registry/configuration/)

## 基础配置

```yaml
version: 0.1  # 配置文件的版本号

log:  # 日志相关配置
  fields:
    service: registry  # 指定日志服务的名称，这里是 registry

storage:  # 存储相关的配置
  cache:
    blobdescriptor: inmemory  # 配置 blob 描述符的缓存方式为内存中存储

  filesystem:
    rootdirectory: /var/lib/registry  # 文件系统存储的根目录

http:  # HTTP 服务相关配置
  addr: :5000  # 服务监听的地址和端口，这里是所有 IP 的 5000 端口
  headers:
    X-Content-Type-Options: [nosniff]  # HTTP 响应头配置，用于防止 MIME 类型嗅探

health:  # 健康检查相关配置
  storagedriver:
    enabled: true  # 是否启用存储驱动的健康检查
    interval: 10s  # 健康检查的间隔时间，这里设置为每10秒执行一次
    threshold: 3  # 健康检查失败的阈值，连续失败3次将认为是不健康状态
```

## 高级配置

### 维护模块配置

```yaml
maintenance:
  uploadpurging:
    enabled: true  # 启用或禁用上传清理。如果启用，Registry 会定期清理未完成的上传。
    age: 168h      # 定义上传存在的最长时间。超过这个时间的上传将被清理。默认为一周。
    interval: 24h  # 清理操作的执行间隔。默认每24小时执行一次。
    dryrun: false  # 如果设置为 true，则清理操作只会记录哪些文件会被删除，而不实际删除文件。
  readonly:
    enabled: false  # 设置为 true 以将 Registry 置于只读模式。在进行维护或升级时，这可能很有用。
```

### 管理重定向模块配置

```yaml
redirect:
  disable: false  # 设置为 true 可以禁用重定向功能。默认情况下，重定向是启用的。
  maxdepth: 5     # 设置重定向链的最大深度。这个参数可以帮助防止重定向循环或是太长的重定向链。
```

### 控制镜像删除模块配置

```bash
delete:
  enabled: true  # 启用或禁用删除功能。如果设置为 true，则可以从 Registry 中删除镜像或标签。
  purge: true    # 如果启用，删除镜像或标签时将自动清理与之相关的所有数据，如层和清单。
  purgeinterval: 24h # 自动清理操作的执行间隔。默认每24小时执行一次清理。
  dryrun: false  # 如果设置为 true，则删除操作只会记录哪些文件会被删除，而不实际删除文件。
```

### 存储类型模块配置

- 本地存储

  ```yaml
  storage:
    filesystem:
      rootdirectory: /var/lib/registry
  ```

- 配置s3类型存储

  ```yaml
  storage:
    s3:
      accesskey: YOUR_MINIO_ACCESS_KEY #MinIO上设置的访问密钥ID
      secretkey: YOUR_MINIO_SECRET_KEY #MinIO上设置的访问密钥
      region: local #MinIO存储桶所在的地域或区域
      regionendpoint: http://x.x.x.x:9000 #MinIO地址
      bucket: YOUR_MINIO_BUCKET_NAME #MinIO存储桶的名称
      encrypt: false #可选项，是否启用加密传输
      secure: true #可选项，是否使用 HTTPS 连接到 MinIO
      v4auth: true #可选项，使用 MinIO v4 版本认证
      chunksize: 5242880 #可选项，分块上传时的块大小，单位为字节
      rootdirectory: / #默认存储路径
  ```

- 配置Docker Registry的存储后端为阿里云对象存储服务 (OSS)

  ```yaml
  storage:
    oss:
      # 用于访问OSS服务的Access Key ID，是进行身份验证的一部分
      accesskeyid: 
      # 与Access Key ID 相对应的访问密钥，是进行身份验证的另一部分
      accesskeysecret: 
      # OSS存储桶所在的地域
      region: 
      # 用于存储Docker镜像的OSS存储桶的名称
      bucket: 
  ```

  