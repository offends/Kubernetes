> 本文作者：丁辉

# Docker-Registry

[官方文档](https://distribution.github.io/distribution/)

> 相关文档地址

- [Helm部署Docker-Registry](https://gitee.com/offends/Kubernetes/blob/main/Helm/Helm%E9%83%A8%E7%BD%B2Docker-Registry.md)
- [Helm部署Docker-Registry-UI](https://gitee.com/offends/Kubernetes/blob/main/Helm/Helm%E9%83%A8%E7%BD%B2Docker-Registry-UI.md)
