> 本文作者：丁辉

# 清理镜像仓库

1. 开启镜像删除功能, 修改镜像仓库容器添加变量

   ```bash
   spec:
     env:
     - name: REGISTRY_STORAGE_DELETE_ENABLED
       value: "true"
   ```

2. 查看镜像

   ```bash
   curl -u <用户>:<密码> http://<registry-url>/v2/_catalog
   ```

3. 查看镜像标签

   ```bash
   curl -u <用户>:<密码>  -X GET http://<registry-url>/v2/<image-name>/tags/list
   ```

4. 删除

   ```bash
   curl -I -XGET --header "Accept:application/vnd.docker.distribution.manifest.v2+json" \
   -u <用户>:<密码> http://<registry-url>/v2/<IMAGE_NAME>/manifests/<TAG>
   ```

   或

   ```bash
   curl -I -XDELETE -u <用户>:<密码> http://<registry-url>/v2/<IMAGE_NAME>/manifests/<TAG>
   ```

   
