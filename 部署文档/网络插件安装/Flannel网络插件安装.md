> 本文作者：丁辉

# Flannel网络插件安装

[Github软件包下载](https://github.com/flannel-io/flannel/releases)	[Github-Flannel-Yaml文件](https://github.com/flannel-io/flannel/blob/master/Documentation/kube-flannel.yml)

## 开始部署

1. 最新版下载

   ```bash
   wget https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
   ```

2. 部署

   ```bash
   kubectl apply -f kube-flannel.yml
   ```

## 其他参数配置(可选步骤)

1. 编辑 Yaml 文件

   ```bash
   vi kube-flannel.yml
   ```

2. 添加如下内容

   ```bash
   containers:
   - args:
     - --ip-masq
     - --kube-subnet-mgr
     - --iface=eth0 #此参数指定网卡
   ```

**参数解释**

|       参数        |                             解释                             |
| :---------------: | :----------------------------------------------------------: |
|     --ip-masq     | 启用 IP masquerade 功能，允许从内部网络发送的数据包经过 NAT 转换，以伪装成外部网络的数据包，确保 Kubernetes Pod 可以与集群外部通信。 |
| --kube-subnet-mgr | 启用 Kubernetes 子网管理器，负责动态分配每个节点的 Pod CIDR（Pod的IP地址范围），以确保在整个集群中不会发生 IP 地址冲突。 |
|   --iface=eth0    | 指定 flannel 使用的网络接口，这里设置为 eth0。flannel 将通过指定的网络接口与其他节点通信。您可能需要根据实际情况更改此参数以匹配正确的网络接口。 |
