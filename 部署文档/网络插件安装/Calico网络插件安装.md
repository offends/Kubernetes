> 本文作者：丁辉

# Calico网络插件安装

[Github官网](https://github.com/projectcalico/calico)	[Github-Calico-Yaml文件](https://github.com/projectcalico/calico/blob/master/manifests/calico.yaml)

1. 最新版下载

   ```bash
   wget https://docs.projectcalico.org/manifests/calico.yaml
   ```

2. 部署

   ```bash
   kubectl apply -f calico.yaml
   ```

   
