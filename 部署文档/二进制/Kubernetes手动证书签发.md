> 本文作者：丁辉

# Kubernetes手动证书签发

## 环境准备

> 安装 CFSSL

1. 创建证书目录

   ```bash
   mkdir /root/k8s
   cd /root/k8s
   ```

2. 下载 CFSSL 文件

   [Github下载地址](https://github.com/cloudflare/cfssl/releases)

   ```bash
   wget https://github.com/cloudflare/cfssl/releases/download/v1.6.4/cfssl_1.6.4_linux_amd64
   wget https://github.com/cloudflare/cfssl/releases/download/v1.6.4/cfssljson_1.6.4_linux_amd64
   wget https://github.com/cloudflare/cfssl/releases/download/v1.6.4/cfssl-certinfo_1.6.4_linux_amd64
   ```

3. 授权并移动到可执行目录下

   ```bash
   chmod +x cfssl_*_linux_amd64 cfssljson_*_linux_amd64 cfssl-certinfo_*_linux_amd64
   mv cfssl_*_linux_amd64 /usr/local/bin/cfssl
   mv cfssljson_*_linux_amd64 /usr/local/bin/cfssljson
   mv cfssl-certinfo_*_linux_amd64 /usr/bin/cfssl-certinfo
   ```

## 开始签发证书

1. 创建 CA 证书签名请求

   ```bash
   cat > ca-csr.json <<EOF
   {
       "CN": "kubernetes",
       "key": {
         "algo": "rsa",
         "size": 2048
       },
       "names": [
         {
           "C": "CN",
           "ST": "BeiJing",
           "L": "BeiJing",
           "O": "k8s",
           "OU": "System"
         }
       ],
         "ca": {
            "expiry": "87600h"
         }
     }
   EOF
   ```

2. 生成 CA 证书和私钥

   ```bash
   cfssl gencert -initca ca-csr.json | cfssljson -bare ca
   ```

3. 创建 CA 配置文件

   ```bash
   cat > ca-config.json <<EOF
   {
     "signing": {
       "default": {
         "expiry": "87600h"
       },
       "profiles": {
         "kubernetes": {
           "usages": [
               "signing",
               "key encipherment",
               "server auth",
               "client auth"
           ],
           "expiry": "87600h"
         }
       }
     }
   }
   EOF
   ```

4. 创建 kubernetes 证书

   ```bash
   cat > kubernetes-csr.json <<EOF
   {
       "CN": "kubernetes",
       "hosts": [
         "127.0.0.1",
         "192.168.1.10",
         "192.168.1.20",
         "192.168.1.30",
         "10.244.0.1",
         "kubernetes",
         "kubernetes.default",
         "kubernetes.default.svc",
         "kubernetes.default.svc.cluster",
         "kubernetes.default.svc.cluster.local"
       ],
       "key": {
           "algo": "rsa",
           "size": 2048
       },
       "names": [
           {
               "C": "CN",
               "ST": "BeiJing",
               "L": "BeiJing",
               "O": "k8s",
               "OU": "System"
           }
       ]
   }
   EOF
   ```

5. 生成 kubernetes 证书和私钥

   ```bash
   cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=kubernetes kubernetes-csr.json | cfssljson -bare kubernetes
   ```

6. 创建 admin 证书

   ```bash
   cat > admin-csr.json <<EOF
   {
     "CN": "admin",
     "hosts": [],
     "key": {
       "algo": "rsa",
       "size": 2048
     },
     "names": [
       {
         "C": "CN",
         "ST": "BeiJing",
         "L": "BeiJing",
         "O": "system:masters",
         "OU": "System"
       }
     ]
   }
   EOF
   ```

7. 生成 admin 证书和私钥

   ```bash
   cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=kubernetes admin-csr.json | cfssljson -bare admin
   ```

8. 创建 kube-proxy 证书

   ```bash
   cat > kube-proxy-csr.json <<EOF
   {
     "CN": "system:kube-proxy",
     "hosts": [],
     "key": {
       "algo": "rsa",
       "size": 2048
     },
     "names": [
       {
         "C": "CN",
         "ST": "BeiJing",
         "L": "BeiJing",
         "O": "k8s",
         "OU": "System"
       }
     ]
   }
   EOF
   ```

9. 生成 kube-proxy 客户端证书和私钥

   ```bash
   cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=kubernetes  kube-proxy-csr.json | cfssljson -bare kube-proxy
   ```

10. 创建 kube-controller-manager 证书

    ```bash
    cat > kube-controller-manager-csr.json <<EOF
    {
        "CN": "system:kube-controller-manager",
        "hosts": [
          "127.0.0.1",
          "192.168.1.10",
          "192.168.1.20", 
          "192.168.1.30"
        ],
        "key": {
            "algo": "rsa",
            "size": 2048
        },
        "names": [
          {
            "C": "CN",
            "ST": "BeiJing",
            "L": "BeiJing",
            "O": "system:kube-controller-manager",
            "OU": "System"
          }
        ]
    }
    EOF
    ```

11. 生成 kube-scheduler 客户端证书和私钥

    ```bash
    cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=kubernetes kube-controller-manager-csr.json | cfssljson -bare kube-controller-manager
    ```

12. 创建 kube-scheduler 证书

    ```bash
    cat > kube-scheduler-csr.json <<EOF
    {
        "CN": "system:kube-scheduler",
        "hosts": [
          "127.0.0.1",
          "192.168.1.10",
          "192.168.1.20", 
          "192.168.1.30"
        ],
        "key": {
            "algo": "rsa",
            "size": 2048
        },
        "names": [
          {
            "C": "CN",
            "ST": "BeiJing",
            "L": "BeiJing",
            "O": "system:kube-scheduler",
            "OU": "System"
          }
        ]
    }
    EOF
    ```

13. 生成 kube-scheduler 客户端证书和私钥

    ```bash
    cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=kubernetes kube-scheduler-csr.json | cfssljson -bare kube-scheduler
    ```

14. 校验证书

    - openssl 校验

      ```bash
      openssl x509  -noout -text -in  kubernetes.pem
      ```

    - cfssl-certinfo 校验

      ```bash
      cfssl-certinfo -cert kubernetes.pem
      ```

15. 分发证书

    ```bash
    mkdir -p /etc/kubernetes/pki
    cp *.pem /etc/kubernetes/pki
    ```

    **使用证书的组件如下**

    - etcd：使用 kubernetes-key.pem、kubernetes.pem
    - kube-apiserver：使用 kubernetes-key.pem、kubernetes.pem
    - kubelet：使用 ca.pem
    - kube-proxy：使用 kube-proxy-key.pem、kube-proxy.pem
    - kubectl：使用 dmin-key.pem、admin.pem
    - kube-controller-manager：使用 kube-controller-manager-key.pem、kube-controller-manager.pem
    - kube-scheduler ：使用 kube-scheduler-key.pem、kube-scheduler.pem
