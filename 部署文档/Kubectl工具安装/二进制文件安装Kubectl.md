> 本文作者：丁辉

# 二进制文件安装Kubectl(推荐)

[官方文档](https://kubernetes.io/zh-cn/docs/tasks/tools/install-kubectl-linux/#install-kubectl-binary-with-curl-on-linux)

## 下载最新版Kubectl

- X86-64

  ```bash
  curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
  ```

  > 下载指定版本
  >
  > ```bash
  > curl -LO https://dl.k8s.io/release/v1.29.2/bin/linux/amd64/kubectl
  > ```

- ARM64

  ```bash
  curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/arm64/kubectl"
  ```

  > 下载指定版本
  >
  > ```bash
  > curl -LO https://dl.k8s.io/release/v1.29.2/bin/linux/arm64/kubectl
  > ```

## 验证该可执行文件(可选步骤)

- X86-64

  ```bash
  curl -LO "https://dl.k8s.io/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl.sha256"
  ```

- ARM64

  ```bash
  curl -LO "https://dl.k8s.io/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/arm64/kubectl.sha256"
  ```

**基于校验和文件，验证 kubectl 的可执行文件**

```bash
echo "$(cat kubectl.sha256)  kubectl" | sha256sum --check
```

> 验证通过时，输出为 `kubectl: OK`

## 安装

```bash
install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
```

## 验证

- 查看版本

  ```bash
  kubectl version --client
  ```

- 查看版本详细信息

  ```bash
  kubectl version --client --output=yaml
  ```

