> 本文作者：丁辉

# Kubectl安装Convert插件

> 一个 Kubernetes 命令行工具 `kubectl` 的插件，允许你将清单在不同 API 版本间转换。 这对于将清单迁移到新的 Kubernetes 发行版上未被废弃的 API 版本时尤其有帮助。

[官方文档](https://kubernetes.io/zh-cn/docs/tasks/tools/install-kubectl-linux/#install-kubectl-convert-plugin)

## 安装Convert

- X86-64

  ```bash
  curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl-convert"
  ```

- ARM64

  ```bash
   curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/arm64/kubectl-convert"
  ```

## 验证该可执行文件(可选步骤)

- X86-64

  ```bash
  curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl-convert.sha256"
  ```

- ARM64

  ```bash
  curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/arm64/kubectl-convert.sha256"
  ```

**基于校验和文件，验证 kubectl 的可执行文件**

```bash
echo "$(cat kubectl-convert.sha256) kubectl-convert" | sha256sum --check
```

> 验证通过时，输出为 `kubectl: OK`

## 安装

```bash
install -o root -g root -m 0755 kubectl-convert /usr/local/bin/kubectl-convert
```

## 验证

```bash
kubectl convert --help
```