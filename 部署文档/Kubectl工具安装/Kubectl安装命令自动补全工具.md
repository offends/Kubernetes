> 本文作者：丁辉

# Kubectl安装命令自动补全工具

[官方文档](https://kubernetes.io/zh-cn/docs/tasks/tools/install-kubectl-linux/#optional-kubectl-configurations)

## 安装 bash-completion

- Ubuntu

  ```bash
  apt-get install bash-completion
  ```

- Centos

  ```bash
  yum install bash-completion
  ```

## 启动 kubectl 自动补全功能

```bash
echo 'source <(kubectl completion bash)' >>~/.bashrc
source ~/.bashrc
```