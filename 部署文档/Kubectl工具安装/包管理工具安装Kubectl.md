> 本文作者：丁辉

# 包管理工具安装Kubectl

[官方文档](https://kubernetes.io/zh-cn/docs/tasks/tools/install-kubectl-linux/#install-using-native-package-management)

## Centos安装

1. 添加 Kubernetes 的 `yum` 仓库

   > 如果你想使用 v1.29 之外的 Kubernetes 版本， 将下面命令中的 v1.29 替换为所需的次要版本。

   ```bash
   cat <<EOF | tee /etc/yum.repos.d/kubernetes.repo
   [kubernetes]
   name=Kubernetes
   baseurl=https://pkgs.k8s.io/core:/stable:/v1.29/rpm/
   enabled=1
   gpgcheck=1
   gpgkey=https://pkgs.k8s.io/core:/stable:/v1.29/rpm/repodata/repomd.xml.key
   EOF
   ```

2. 更新仓库

   ```bash
   yum update
   ```

3. 安装 kubectl

   ```bash
   yum install -y kubectl
   ```

## Ubuntu安装

1. 创建目录

   ```bash
   mkdir -p -m 755 /etc/apt/keyrings
   ```

2. 下载 Kubernetes 软件包仓库的公共签名密钥。 同一个签名密钥适用于所有仓库，因此你可以忽略 URL 中的版本信息。

   ```bash
   curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.29/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
   ```

3. 添加 Kubernetes 的 `apt` 仓库

   > 如果你想用 v1.29 之外的 Kubernetes 版本， 请将下面命令中的 v1.29 替换为所需的次要版本。

   ```bash
   echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.29/deb/ /' | tee /etc/apt/sources.list.d/kubernetes.list
   ```

4. 更新 `apt` 包索引

   ```bash
   apt-get update
   ```

5. 安装 kubectl

   ```bash
   apt-get install -y kubectl
   ```

## 验证

- 查看版本

  ```bash
  kubectl version --client
  ```

- 查看版本详细信息

  ```bash
  kubectl version --client --output=yaml
  ```
