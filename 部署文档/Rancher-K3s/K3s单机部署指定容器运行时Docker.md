> 本文作者：丁辉

# K3s单机部署指定容器运行时Docker

> K3s默认容器运行时为: Containerd, 本文教你如何使用 Docker 容器运行时

[官方文档](https://docs.k3s.io/zh/advanced#%E4%BD%BF%E7%94%A8-docker-%E4%BD%9C%E4%B8%BA%E5%AE%B9%E5%99%A8%E8%BF%90%E8%A1%8C%E6%97%B6)

## 部署Docker

1. Docker安装

   ```bash
   curl https://releases.rancher.com/install-docker/20.10.sh | sh
   ```

   > 传递参数使用国内源
   >
   > ```bash
   > curl -fsSL https://releases.rancher.com/install-docker/20.10.sh | sh -s -- --mirror Aliyun
   > ```

2. 启动 Docker

   ```bash
   systemctl enable docker
   systemctl start docker
   ```

## 部署K3s

> 使用 `--docker` 参数指定 K3s 要使用的运行时

- 国外源

  ```bash
  curl -sfL https://get.k3s.io | \
  K3S_KUBECONFIG_OUTPUT=~/.kube/config \
  K3S_KUBECONFIG_MODE=644 \
  K3S_NODE_NAME=k3s-master \
  sh -s - --docker
  ```
  
- 国内源

  ```bash
  curl -sfL https://rancher-mirror.rancher.cn/k3s/k3s-install.sh | INSTALL_K3S_MIRROR=cn \
  K3S_SYSTEM_DEFAULT_REGISTRY=registry.cn-hangzhou.aliyuncs.com \
  K3S_KUBECONFIG_OUTPUT=~/.kube/config \
  K3S_KUBECONFIG_MODE=644 \
  K3S_NODE_NAME=k3s-master \
  sh -s - --docker
  ```

**参数解释**

|            参数             |                             解释                             |
| :-------------------------: | :----------------------------------------------------------: |
|     INSTALL_K3S_MIRROR      |  该参数用于指定 k3s 安装过程中下载软件包和镜像的镜像站点。   |
| K3S_SYSTEM_DEFAULT_REGISTRY | 该参数用于指定 k3s 系统组件（如 kube-apiserver、kube-controller-manager 等）使用的默认镜像仓库地址。 |
|    K3S_KUBECONFIG_OUTPUT    | 指定 K3s 安装完成后生成的 kubeconfig 文件的输出路径。在这个例子中，设置为 `~/.kube/config`，表示 kubeconfig 文件将保存在当前用户的主目录下的 `.kube` 目录中，文件名为 `config`。 |
|     K3S_KUBECONFIG_MODE     | 指定生成的 kubeconfig 文件的权限模式。在这个例子中，设置为 `644`，表示生成的 kubeconfig 文件权限模式为 `-rw-r--r--`，即当前用户有读写权限，其他用户只有读权限。 |
|        K3S_NODE_NAME        | 指定 K3s 安装节点的名称。在这个例子中，设置为 `k3s-master`，表示安装的节点将被命名为 `k3s-master`。 |

## 卸载

1. 停止 K3s

   ```bash
   k3s-killall.sh
   ```

2. 卸载 K3s

   ```bash
   k3s-uninstall.sh
   ```

3. 停止 Docker

   ```bash
   systemctl stop docker
   ```

4. 卸载 Docker

   - Centos

     ```bash
     yum remove docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin docker-ce-rootless-extras -y
     ```

   - Ubuntu

     ```bash
     sudo apt-get purge docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin docker-ce-rootless-extras
     ```

5. 删除 Docker 数据残留

   ```bash
   rm -rf /var/lib/docker
   rm -rf /var/lib/containerd
   ```

   

   
