> 本文作者：丁辉

# K3s自动部署清单

> 在 `/var/lib/rancher/k3s/server/manifests` 中找到的任何文件都会以类似 `kubectl apply` 的方式自动部署到 Kubernetes，在启动和在磁盘上更改文件时都是如此。从该目录中删除文件不会从集群中删除相应的资源。

[官方文档](https://docs.k3s.io/zh/installation/packaged-components)

