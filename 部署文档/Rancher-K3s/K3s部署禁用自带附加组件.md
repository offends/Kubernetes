> 本文作者：丁辉

# K3s部署禁用自带附加组件

[官方文档](https://docs.k3s.io/zh/installation/packaged-components)

**附加组件介绍**

|     可选项     |                             描述                             |
| :------------: | :----------------------------------------------------------: |
|    coredns     |         用于 Kubernetes 集群中 DNS 服务的核心组件。          |
|   servicelb    |       提供了服务负载均衡的组件，用于在集群中分发流量。       |
|    traefik     | 一个流行的反向代理和负载均衡工具，用于管理应用程序的入口流量。 |
| local-storage  |   本地存储提供了将本地磁盘挂载到 Kubernetes Pod 中的功能。   |
| metrics-server |  用于收集和暴露 Kubernetes 集群中各种对象的度量数据的组件。  |

## 部署K3s

> 使用 `--disable` 禁用自带附加组件安装

- 国外源

  ```bash
  curl -sfL https://get.k3s.io | \
  K3S_KUBECONFIG_OUTPUT=~/.kube/config \
  K3S_KUBECONFIG_MODE=644 \
  K3S_NODE_NAME=k3s-master \
  sh -s - --disable traefik,metrics-server,local-storage,servicelb
  ```

- 国内源

  ```bash
  curl -sfL https://rancher-mirror.rancher.cn/k3s/k3s-install.sh | INSTALL_K3S_MIRROR=cn \
  K3S_SYSTEM_DEFAULT_REGISTRY=registry.cn-hangzhou.aliyuncs.com \
  K3S_KUBECONFIG_OUTPUT=~/.kube/config \
  K3S_KUBECONFIG_MODE=644 \
  K3S_NODE_NAME=k3s-master \
  sh -s - --disable traefik,metrics-server,local-storage,servicelb
  ```

**参数解释**

| 参数                        | 解释                                                         |
| --------------------------- | ------------------------------------------------------------ |
| INSTALL_K3S_MIRROR          | 该参数用于指定 k3s 安装过程中下载软件包和镜像的镜像站点。    |
| K3S_SYSTEM_DEFAULT_REGISTRY | 该参数用于指定 k3s 系统组件（如 kube-apiserver、kube-controller-manager 等）使用的默认镜像仓库地址。 |
| K3S_KUBECONFIG_OUTPUT       | 指定 K3s 安装完成后生成的 kubeconfig 文件的输出路径。在这个例子中，设置为 `~/.kube/config`，表示 kubeconfig 文件将保存在当前用户的主目录下的 `.kube` 目录中，文件名为 `config`。 |
| K3S_KUBECONFIG_MODE         | 指定生成的 kubeconfig 文件的权限模式。在这个例子中，设置为 `644`，表示生成的 kubeconfig 文件权限模式为 `-rw-r--r--`，即当前用户有读写权限，其他用户只有读权限。 |
| K3S_NODE_NAME               | 指定 K3s 安装节点的名称。在这个例子中，设置为 `k3s-master`，表示安装的节点将被命名为 `k3s-master`。 |

## 卸载

1. 停止

   ```bash
   k3s-killall.sh
   ```

2. 卸载

   ```bash
   k3s-uninstall.sh
   ```
