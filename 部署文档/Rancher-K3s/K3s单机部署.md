> 本文作者：丁辉

# K3s单机部署

[Github仓库](https://github.com/k3s-io/k3s/)

[中文官方文档](https://docs.k3s.io/zh/)

## 开始部署

[K3s基础环境准备官方文档](https://docs.k3s.io/zh/installation/requirements)

|  节点名称  |      IP      |         角色         |
| :--------: | :----------: | :------------------: |
| k3s-master | 192.168.1.10 | control-plane,master |

[所有变量参数解释](https://docs.k3s.io/zh/cli/server)

- 国外源

  ```bash
  curl -sfL https://get.k3s.io | \
  K3S_KUBECONFIG_OUTPUT=~/.kube/config \
  K3S_KUBECONFIG_MODE=644 \
  K3S_NODE_NAME=k3s-master \
  sh -
  ```

- 国内源

  ```bash
  curl -sfL https://rancher-mirror.rancher.cn/k3s/k3s-install.sh | INSTALL_K3S_MIRROR=cn \
  K3S_SYSTEM_DEFAULT_REGISTRY=registry.cn-hangzhou.aliyuncs.com \
  K3S_KUBECONFIG_OUTPUT=~/.kube/config \
  K3S_KUBECONFIG_MODE=644 \
  K3S_NODE_NAME=k3s-master \
  sh -
  ```

**参数解释**

|            参数             |                             解释                             |
| :-------------------------: | :----------------------------------------------------------: |
|     INSTALL_K3S_MIRROR      |  该参数用于指定 k3s 安装过程中下载软件包和镜像的镜像站点。   |
| K3S_SYSTEM_DEFAULT_REGISTRY | 该参数用于指定 k3s 系统组件（如 kube-apiserver、kube-controller-manager 等）使用的默认镜像仓库地址。 |
|    K3S_KUBECONFIG_OUTPUT    | 指定 K3s 安装完成后生成的 kubeconfig 文件的输出路径。在这个例子中，设置为 `~/.kube/config`，表示 kubeconfig 文件将保存在当前用户的主目录下的 `.kube` 目录中，文件名为 `config`。 |
|     K3S_KUBECONFIG_MODE     | 指定生成的 kubeconfig 文件的权限模式。在这个例子中，设置为 `644`，表示生成的 kubeconfig 文件权限模式为 `-rw-r--r--`，即当前用户有读写权限，其他用户只有读权限。 |
|        K3S_NODE_NAME        | 指定 K3s 安装节点的名称。在这个例子中，设置为 `k3s-master`，表示安装的节点将被命名为 `k3s-master`。 |

## 卸载

1. 停止

   ```bash
   k3s-killall.sh
   ```

2. 卸载

   ```bash
   k3s-uninstall.sh
   ```

