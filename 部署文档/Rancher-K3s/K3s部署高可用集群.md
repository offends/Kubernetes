> 本文作者：丁辉

# K3s部署高可用集群

[Github仓库](https://github.com/k3s-io/k3s/)

[中文官方文档](https://docs.k3s.io/zh/)

[K3s基础环境准备官方文档](https://docs.k3s.io/zh/installation/requirements)

|   节点名称   |      IP      |           角色            |
| :----------: | :----------: | :-----------------------: |
| k3s-master-1 | 192.168.1.10 | control-plane,etcd,master |
| k3s-master-2 | 192.168.1.20 | control-plane,etcd,master |
| k3s-master-3 | 192.168.1.30 | control-plane,etcd,master |
| k3s-worker-1 | 192.168.1.40 |          worker           |

> Master节点VIP: 192.168.1.100

[集群数据存储配置](https://docs.k3s.io/zh/datastore)

[高可用嵌入式 etcd](https://docs.k3s.io/zh/datastore/ha-embedded)

## 开始部署管理节点

[所有变量参数解释](https://docs.k3s.io/zh/cli/server)

[集群负载均衡器](https://docs.k3s.io/zh/datastore/cluster-loadbalancer)

- 国外源

  ```bash
  curl -sfL https://get.k3s.io | \
  K3S_NODE_NAME=k3s-master-1 \
  K3S_KUBECONFIG_OUTPUT=~/.kube/config \
  K3S_KUBECONFIG_MODE=644 \
  K3S_TOKEN=SECRET \
  INSTALL_K3S_EXEC="--node-taint k3s-controlplane=true:NoExecute" \
  sh -s - server \
  --cluster-init \
  --tls-san 192.168.1.100
  ```

- 国内源

  ```bash
  curl -sfL https://rancher-mirror.rancher.cn/k3s/k3s-install.sh | INSTALL_K3S_MIRROR=cn \
  K3S_NODE_NAME=k3s-master-1 \
  K3S_KUBECONFIG_OUTPUT=~/.kube/config \
  K3S_KUBECONFIG_MODE=644 \
  K3S_TOKEN=SECRET \
  INSTALL_K3S_EXEC="--node-taint k3s-controlplane=true:NoExecute" \
  sh -s - server \
  --cluster-init \
  --tls-san 192.168.1.100
  ```
  
  >该参数用于指定 k3s 系统组件（如 kube-apiserver、kube-controller-manager 等）使用的默认镜像仓库地址。(阿里同步较慢有时会获取不到最新镜像)
  >
  >```bash
  >K3S_SYSTEM_DEFAULT_REGISTRY=registry.cn-hangzhou.aliyuncs.com
  >```

**参数解释**

|                             参数                             |                             解释                             |
| :----------------------------------------------------------: | :----------------------------------------------------------: |
|                        K3S_NODE_NAME                         | 指定 K3s 安装节点的名称。在这个例子中，设置为 `k3s-master-1`，表示安装的节点将被命名为 `k3s-master-1`。 |
|                    K3S_KUBECONFIG_OUTPUT                     | 指定 K3s 安装完成后生成的 kubeconfig 文件的输出路径。在这个例子中，设置为 `~/.kube/config`，表示 kubeconfig 文件将保存在当前用户的主目录下的 `.kube` 目录中，文件名为 `config`。 |
|                     K3S_KUBECONFIG_MODE                      | 指定生成的 kubeconfig 文件的权限模式。在这个例子中，设置为 `644`，表示生成的 kubeconfig 文件权限模式为 `-rw-r--r--`，即当前用户有读写权限，其他用户只有读权限。 |
|                          K3S_TOKEN                           |                       配置共享 secret                        |
| INSTALL_K3S_EXEC="--node-taint k3s-controlplane=true:NoExecute" |                   controlplane节点禁止调度                   |
|                        --cluster-init                        |                 使用嵌入式 Etcd 初始化新集群                 |
|                          --tls-san                           | 在 TLS 证书上添加其他主机名或 IPv4/IPv6 地址作为 Subject Alternative Name |
|                      INSTALL_K3S_MIRROR                      |  该参数用于指定 k3s 安装过程中下载软件包和镜像的镜像站点。   |

## 添加管理节点[2,3节点同理]

- 国外源

  ```bash
  curl -sfL https://get.k3s.io | \
  K3S_NODE_NAME=k3s-master-2 \
  K3S_KUBECONFIG_OUTPUT=~/.kube/config \
  K3S_KUBECONFIG_MODE=644 \
  K3S_TOKEN=SECRET \
  INSTALL_K3S_EXEC="--node-taint k3s-controlplane=true:NoExecute" \
  sh -s - server \
  --server https://192.168.1.100:6443 \
  --tls-san 192.168.1.100
  ```

- 国内源

  ```bash
  curl -sfL https://rancher-mirror.rancher.cn/k3s/k3s-install.sh | INSTALL_K3S_MIRROR=cn \
  K3S_NODE_NAME=k3s-master-2 \
  K3S_KUBECONFIG_OUTPUT=~/.kube/config \
  K3S_KUBECONFIG_MODE=644 \
  K3S_TOKEN=SECRET \
  INSTALL_K3S_EXEC="--node-taint k3s-controlplane=true:NoExecute" \
  sh -s - server \
  --server https://192.168.1.100:6443 \
  --tls-san 192.168.1.100
  ```

## 添加计算节点

> 在管理节点获取 ${node-token}
>
> ```bash
> cat /var/lib/rancher/k3s/server/node-token
> ```

- 国外源

  ```bash
  curl -sfL https://get.k3s.io | \
  K3S_NODE_NAME=k3s-worker-1 \
  K3S_URL=https://192.168.1.100:6443 \
  K3S_TOKEN=${node-token} \
  sh -s - agent
  ```

- 国内源

  ```bash
  curl -sfL https://rancher-mirror.rancher.cn/k3s/k3s-install.sh | INSTALL_K3S_MIRROR=cn \
  K3S_NODE_NAME=k3s-worker-1 \
  K3S_URL=https://192.168.1.100:6443 \
  K3S_TOKEN=${node-token} \
  sh -s - agent
  ```

**计算节点添加角色标签**

```bash
kubectl label node ${node} node-role.kubernetes.io/worker=true --overwrite
```

> `--overwrite` 强制覆盖

## 卸载

### 卸载 Server

1. 删除 Server 节点

   ```bash
   kubectl delete node ${node}
   ```

2. 停止 Server

   ```bash
   k3s-killall.sh
   ```

3. 卸载 Server

   ```bash
   k3s-uninstall.sh
   ```

### 卸载 Agent

1. 删除 Agent 节点

   ```bash
   kubectl delete node ${node}
   ```

2. 停止 K3s-Agent

   ```bash
   k3s-killall.sh
   ```

3. 卸载 K3s-Agent

   ```bash
   k3s-agent-uninstall.sh
   ```

