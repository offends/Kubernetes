> 本文作者：丁辉

# Kubernetes基础环境准备

## 时间同步

1. 安装 ntpdate

   ```bash
   yum -y install ntpdate
   ```

2. 配置时区并同步

   ```bash
   ln -sf /usr/share/zoneinfo/Asia/Shanghai  /etc/localtime
   ntpdate -u ntp.aliyun.com && date
   ```

> 离线环境使用此篇文档同步
>
> [NTP时间同步](https://gitee.com/offends/Linux/blob/main/Docs/NTP%E6%97%B6%E9%97%B4%E5%90%8C%E6%AD%A5.md)

## Linux环境配置

1. 关闭防火墙

   ```bash
   systemctl stop firewalld && systemctl disable firewalld
   ```

2. 禁用 selinux

   ```bash
   sudo setenforce 0
   sudo sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config
   ```

3. 禁用 swap

   ```bash
   swapoff -a
   sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab
   ```

**解释**

在 Kubernetes 集群部署过程中，禁用 Swap 和 SELinux 是出于一些安全和性能方面的考虑。

- 禁用 Swap：Swap（交换空间）是一种可以将内存中的数据暂时存储到硬盘上的机制。在 Kubernetes 中，默认情况下是禁用 Swap 的。禁用 Swap 可以避免因为将数据交换到磁盘导致的性能下降，同时也避免了节点上的内存使用不一致性问题。
- 禁用 SELinux：SELinux（Security-Enhanced Linux）是为了增强 Linux 系统的安全性而提供的一种安全子系统。尽管 SELinux 提供了强大的安全功能，但它更适合于传统的单机环境，而不是容器化环境。在 Kubernetes 中，禁用 SELinux 可以简化集群的配置和管理，并且减少可能由于权限配置不正确而导致的问题。

当然，禁用 Swap 和 SELinux 并不是绝对必需的，这取决于你的具体场景和需求。如果你有特定的安全需求或者对性能有较高的要求，禁用 Swap 和 SELinux 可能是一个好的选择。但在某些情况下，可能需要根据实际情况来进行配置和调整。

## 主机配置

| 主机名 |      IP      |
| :----: | :----------: |
| k8s-1  | 192.168.1.10 |
| k8s-2  | 192.168.1.20 |
| k8s-3  | 192.168.1.30 |

1. 配置主机名

   - k8s-1

     ```bash
     hostnamectl set-hostname k8s-1
     ```

   - k8s-2

     ```bash
     hostnamectl set-hostname k8s-2
     ```

   - k8s-3

     ```bash
     hostnamectl set-hostname k8s-3
     ```

2. 添加 hosts

   ```bash
   vi /etc/hosts
   ```

   添加内容如下

   ```bash
   192.168.1.10 k8s-1
   192.168.1.20 k8s-2
   192.168.1.30 k8s-3
   ```

## 修改内核参数

1. 加载 br_netfilter 网络过滤器模块

   ```bash
   cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
   overlay
   br_netfilter
   EOF
   ```

   加载

   ```bash
   modprobe overlay
   modprobe br_netfilter
   ```

2. 设置所需的 sysctl 参数

   > 更多完整参数配置请查看	[参数配置](https://gitee.com/offends/Kubernetes/blob/main/File/Conf/k8s.conf)

   ```bash
   cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
   net.bridge.bridge-nf-call-ip6tables = 1
   net.bridge.bridge-nf-call-iptables = 1
   net.ipv4.ip_forward = 1
   vm.swappiness = 0
   EOF
   ```

   加载

   ```bash
   sysctl --system
   ```

## NetworkManager配置

Network Manager 是一个能够动态控制和配置网络的守护进程，NetworkManager 管理除 lo(环回)设备以外的所有设备。但是，在容器环境下很多网络设备是由网络驱动创建，NetworkManager 对容器网络设备的控制可能导致集群网络通信异常。因为需要将容器相关的设备设置为 unmanaged，以使 NetworkManager 忽略这些设备。

- 直接关闭 NetworkManager

  ```bash
  systemctl stop NetworkManager && systemctl disable NetworkManager
  ```

- 临时 unmanaged

  ```bash
  nmcli device set xxx managed no
  ```

- 永久 unmanaged

  1. 启用插件

     ```bash
     vi /etc/NetworkManager/NetworkManager.conf
     ```

     添加如下内容

     ```bash
     [main]
     plugins=keyfile
     ```

  2. 编写配置文件

     ```bash
     cat <<EOF > /etc/NetworkManager/conf.d/99-unmanaged-devices.conf
     [keyfile]
     unmanaged-devices=interface-name:eth*,except:interface-name:eth0;interface-name:docker0;interface-name:flannel*;interface-name:cali*;interface-name:cni0;mac:66:77:88:99:00:aa
     EOF
     ```

     **参数解释**

     - `interface-name:eth*`：表示匹配以"eth"开头的接口名称。
     - `except:interface-name:eth0`：表示排除名为"eth0"的接口。
     - `interface-name:docker0`：表示匹配名为"docker0"的接口。
     - `interface-name:flannel*`：表示匹配以"flannel"开头的接口名称。
     - `interface-name:cali*`：表示匹配以"flannel"开头的接口名称。
     - `interface-name:cni0`：表示匹配名为"cni0"的接口。
     - `mac:66:77:88:99:00:aa`：表示匹配MAC地址为"66:77:88:99:00:aa"的接口。

     总结起来，这段代码的作用是配置网络接口的命名规则，包括匹配以"eth"开头的接口名称（除了"eth0"），匹配名为"docker0"、"flannel"开头的接口名称以及名为"cni0"的接口，并且排除MAC地址为"66:77:88:99:00:aa"的接口。

  3. 重新加载配置

     ```bash
     systemctl reload NetworkManager
     ```

### 问题记录

在 centos 8.x 环境中，默认没有安装 `network-scripts`，禁止 Network Manager 服务后则无法重启网络，可以通过手动安装。

```bash
yum -y install network-scripts
```

