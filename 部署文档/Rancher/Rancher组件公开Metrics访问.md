> 本文作者：丁辉

# Rancher组件公开Metrics访问

## RKE1添加Arg

```bash
vi cluster.yml
```

修改如下内容(当前只有这俩默认不对外开放)

```yaml
services:
  etcd:
    image: ""
    extra_args:
      listen-metrics-urls: "http://0.0.0.0:2381"
...
kubeproxy:
    image: ""
    extra_args:
      metrics-bind-address: "0.0.0.0:10249"
```