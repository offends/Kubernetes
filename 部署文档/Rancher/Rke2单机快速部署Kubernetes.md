> 本文作者：丁辉

[Rke2文档](https://docs.rke2.io/)

[Rancher中文文档](https://docs.rancher.cn/)

# Rke2单机快速部署Kubernetes

|   节点名称   |      IP      |               Kubernetes角色               |
| :----------: | :----------: | :----------------------------------------: |
| k8s-master-1 | 192.168.1.10 | Controlplane,etcd,worker,keepalived-master |

## 环境准备

> !!!每次部署都写挺麻烦的索性都放在一个文件内了请查看 [Kubernetes基础环境准备](https://gitee.com/offends/Kubernetes/blob/main/%E9%83%A8%E7%BD%B2%E6%96%87%E6%A1%A3/Kubernetes%E5%9F%BA%E7%A1%80%E7%8E%AF%E5%A2%83%E5%87%86%E5%A4%87.md) ,请按照此文档初始化环境

### 所有节点执行

1. 更改主机名

   ```bash
   hostnamectl set-hostname k8s-master-1 && bash
   ```
   
3. 在三台节点上配置 NetworkManager

   - 配置 cali 和 flannel 的网卡不被 NetworkManager 管理

     ```bash
     mkdir -p /etc/NetworkManager/conf.d
     ```

     内容如下

     ```bash
     cat <<EOF > /etc/NetworkManager/conf.d/rke2-canal.conf
     [keyfile]
     unmanaged-devices=interface-name:cali*;interface-name:flannel*
     EOF
     ```

   - 重启 NetworkManager

     ```bash
     systemctl daemon-reload
     systemctl restart NetworkManager
     ```

### 开始部署

[Rke2-Github-releases](https://github.com/rancher/rke2/releases)

1. 安装 RKE2

   ```bash
   curl -sfL https://get.rke2.io | sh -
   ```

   > - 使用国内源
   >
   >   ```bash
   >   curl -sfL http://rancher-mirror.rancher.cn/rke2/install.sh | INSTALL_RKE2_MIRROR=cn INSTALL_RKE2_TYPE="server" sh -
   >   ```
   >
   > - 指定版本 
   >
   >   ```bash
   >   curl -sfL https://rancher-mirror.rancher.cn/rke2/install.sh | INSTALL_RKE2_MIRROR=cn INSTALL_RKE2_TYPE="server" INSTALL_RKE2_VERSION="v1.29.3+rke2r1" sh -
   >   ```

2. 开始部署主节点

   ```bash
   systemctl enable rke2-server.service
   systemctl start rke2-server.service
   ```

   > 启动失败查看日志
   >
   > ```bash
   > rke2 server --config /etc/rancher/rke2/config.yaml --debug
   > ```

3. 配置 RKE2 可执行文件加入到系统的 PATH 中

   ```bash
   echo "export PATH=$PATH:/var/lib/rancher/rke2/bin" >> /etc/profile && source /etc/profile
   ```

4. 配置 config 文件

   ```bash
   mkdir ~/.kube && cp /etc/rancher/rke2/rke2.yaml ~/.kube/config
   ```

5. 验证

   ```bash
   kubectl get node
   ```

6. 配置 crictl 软链接

   ```bash
   ln -s /var/lib/rancher/rke2/agent/etc/crictl.yaml /etc/crictl.yaml
   ```

7. 验证

   ```bash
   crictl ps
   ```

## 卸载节点

1. 停止 Rke2

   ```bash
   rke2-killall.sh
   ```

2. 卸载 Rke2

   ```bash
   rke2-uninstall.sh
   ```