> 本文作者：丁辉

# Kubeasz部署Kubernetes集群

[Github官方文档](https://github.com/easzlab/kubeasz)

[Github安装文档](https://github.com/easzlab/kubeasz/blob/master/docs/setup/quickStart.md)

|         节点名称         |      IP      |  Kubernetes角色  |
| :----------------------: | :----------: | :--------------: |
| k8s-master-1,Kubeasz管理 | 192.168.1.10 | master,etcd,node |
|       k8s-master-2       | 192.168.1.20 | master,etcd,node |
|       k8s-master-3       | 192.168.1.30 | master,etcd,node |

## 环境准备

1. 配置免密

   ```bash
   ssh-keygen -t rsa -N "" -f ~/.ssh/id_rsa -q
   ```

2. 配置免密登录

   ```bash
   ssh-copy-id root@192.168.1.10
   ssh-copy-id root@192.168.1.20
   ssh-copy-id root@192.168.1.30
   ```

3. 更改主机名

   - 192.168.1.10

     ```bash
     hostnamectl set-hostname k8s-master-1 && bash
     ```

   - 192.168.1.20

     ```bash
     hostnamectl set-hostname k8s-master-2 && bash
     ```

   - 192.168.1.30

     ```bash
     hostnamectl set-hostname k8s-master-3 && bash
     ```

4. 编辑 /etc/hosts 文件

   ```bash
   vi /etc/hosts
   ```

   添加如下内容

   ```bash
   192.168.1.10 k8s-master-1
   192.168.1.20 k8s-master-2
   192.168.1.30 k8s-master-3
   ```

## 开始部署 Kubeasz

[Github Releases](https://github.com/easzlab/kubeasz/releases)

1. 下载 ezdown

   ```bash
   wget https://github.com/easzlab/kubeasz/releases/download/3.6.2/ezdown && chmod 777 ezdown
   ```

   > 下载支持 Docker 的版本
   >
   > ```bash
   > wget https://github.com/easzlab/kubeasz/releases/download/3.2.0/ezdown && chmod 777 ezdown
   > ```

2. 下载kubeasz代码、二进制、默认容器镜像

   - 国内环境

     ```bash
     ./ezdown -D
     ```

     > 下载 flannel 镜像
     >
     > ```bash
     > ./ezdown -X flannel
     > ```

   - 海外环境

     ```bash
     ./ezdown -D -m standard
     ```

     > 下载 flannel 镜像
     >
     > ```bash
     > ./ezdown -D -m standard -X flannel
     > ```

3. 运行 Kubeasz

   ```bash
   ./ezdown -S
   ```

   > 配置快捷命令
   >
   > ```bash
   > echo "alias dk='docker exec -it kubeasz'" >> ~/.bashrc
   > source ~/.bashrc
   > ```
   >
   > 配置后执行 `dk` 即可

4. 创建新集群

   ```bash
   dk ezctl new k8s-01
   ```

5. 编辑 hosts 文件

   ```bash
   vi /etc/kubeasz/clusters/k8s-01/hosts
   ```

   内容如下

   ```bash
   # 'etcd' cluster should have odd member(s) (1,3,5,...)
   [etcd]
   192.168.1.10
   192.168.1.20
   192.168.1.30
   
   # master node(s)
   [kube_master]
   192.168.1.10 k8s_nodename='k8s-master-1'
   192.168.1.20 k8s_nodename='k8s-master-2'
   192.168.1.30 k8s_nodename='k8s-master-3'
   
   # work node(s)
   [kube_node]
   192.168.1.10 k8s_nodename='k8s-master-1'
   192.168.1.20 k8s_nodename='k8s-master-2'
   192.168.1.30 k8s_nodename='k8s-master-3'
   
   # VIP
   [ex_lb]
   192.168.1.10 LB_ROLE=master EX_APISERVER_VIP=192.168.1.100 EX_APISERVER_PORT=8443
   192.168.1.20 LB_ROLE=backup EX_APISERVER_VIP=192.168.1.100 EX_APISERVER_PORT=8443
   192.168.1.30 LB_ROLE=backup EX_APISERVER_VIP=192.168.1.100 EX_APISERVER_PORT=8443
   
   ...#此处省略
   # Cluster container-runtime supported: docker, containerd
   CONTAINER_RUNTIME="containerd"
   
   # Network plugins supported: calico, flannel, kube-router, cilium, kube-ovn
   CLUSTER_NETWORK="flannel"
   
   ...#此处省略
   ```

6. 修改 config.yml 文件

   ```bash
   vi /etc/kubeasz/clusters/k8s-01/config.yml
   ```

   内容如下

   ```yml
   ############################
   # role:kube-master
   ############################
   # k8s 集群 master 节点证书配置，可以添加多个ip和域名（比如增加公网ip和域名）尽量预留一些IP使用
   MASTER_CERT_HOSTS:
     - "192.168.1.10"
     - "192.168.1.20"
     - "192.168.1.30"
     - "192.168.1.100"
   ```


## 开始安装 Kubernetes

1. 安装

   ```bash
   dk ezctl setup k8s-01 all
   ```

   > 或一步一步安装
   >
   > ```bash
   > # 初始化集群配置
   > dk ezctl setup k8s-01 01
   > # 安装 etcd
   > dk ezctl setup k8s-01 02
   > ...#此处省略
   > ```

2. 加载新配置

   ```bash
   source ~/.bashrc
   ```

3. 查看节点状态

   ```bash
   kubectl get node
   ```

## 添加节点

[官方文档](https://github.com/easzlab/kubeasz/blob/master/docs/op/op-node.md)

- 添加 worker

  ```bash
  dk ezctl add-node k8s-01 192.168.1.40 k8s_nodename='k8s-master-4'
  ```

  > SSH 非 22 端口
  >
  > ```bash
  > dk ezctl add-node k8s-01 192.168.1.40 k8s_nodename='k8s-master-4' ansible_ssh_port=10022
  > ```

- 添加 master

  ```bash
  dk ezctl add-master k8s-01 192.168.1.40 k8s_nodename='k8s-master-4'
  ```

- 添加 etcd

  ```bash
  dk ezctl add-etcd k8s-01 192.168.1.40
  ```

### 删除节点

- 删除 worker

  ```bash
  dk ezctl del-node k8s-01 192.168.1.40
  ```

- 删除 master

  ```bash
  dk ezctl del-master k8s-01 192.168.1.40
  ```

- 删除 etcd

  ```bash
  dk ezctl del-etcd k8s-01 192.168.1.40
  ```
