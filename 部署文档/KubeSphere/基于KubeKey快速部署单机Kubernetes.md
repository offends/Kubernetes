> 本文作者：丁辉

# 基于KubeKey快速部署单机Kubernetes

|         节点名称         |      IP      |      Kubernetes角色       |
| :----------------------: | :----------: | :-----------------------: |
| k8s-master-0,KubeKey管理 | 192.168.1.10 | control-plane,etcd,worker |

## 基础准备

[官方主页](https://kubesphere.io/zh/)

1. 更改主机名

   ```bash
   hostnamectl set-hostname k8s-master-0 && bash
   ```

2. 编辑 /etc/hosts 文件

   ```bash
   echo "192.168.1.10 k8s-master-0" >> /etc/hosts
   ```

3. 安装依赖

   - Centos

     ```bash
     yum install conntrack-tools socat -y
     ```

   - Ubuntu

     ```bash
     apt install conntrack socat -y
     ```

## 开始安装 KubeKey

[KubeKey-Github-Releases](https://github.com/kubesphere/kubekey/releases)

1. 下载最新版 KubeKey

   > 配置国内加速
   >
   > ```bash
   > export KKZONE=cn
   > ```

   ```bash
   curl -sfL https://get-kk.kubesphere.io | sh -
   ```

   > 指定版本
   >
   > ```bash
   > curl -sfL https://get-kk.kubesphere.io | VERSION=v3.0.13 sh -
   > ```

2. 为 `kk` 添加可执行权限

   ```bash
   chmod +x kk && mv kk /usr/local/bin/
   ```


## 开始安装 Kubernetes

**执行命令**

```bash
kk create cluster -y
```

> 默认 Kubernetes 版本为 v1.23.10
>
> 容器运行时为 Docker

### 指定版本

- 安装 Kubernetes

  ```bash
  --with-kubernetes v1.22.12
  ```

  > 查看 KubeKey 支持的 Kubernetes 版本列表
  >
  > ```bash
  > kk version --show-supported-k8s
  > ```

- 安装 Kubesphere

  [Kubesphere-Github-Releases](https://github.com/kubesphere/kubesphere/releases)
  
  ```bash
  --with-kubesphere v3.4.1
  ```

### 命令参数


|            标志             |                             说明                             |
| :-------------------------: | :----------------------------------------------------------: |
|       -a, --artifact        | 指定 KubeKey artifact（工件）的路径。artifact 是一个包含了所需二进制文件和其他资源的 tarball 文件，用于在离线环境中部署 Kubernetes。 |
|     --container-manager     | 指定容器运行时：docker、crio、containerd 或 isula。默认为 "docker"。 |
|           --debug           |                        打印详细信息。                        |
|    --download-cmd string    | 用户定义的下载所需二进制文件的命令。第一个参数 '%s' 是输出路径，第二个参数 '%s' 是 URL。默认为 "curl -L -o %s %s"。 |
|       -f, --filename        |                     指定配置文件的路径。                     |
|        --ignore-err         |      忽略错误信息，删除报告错误的主机，并强制继续执行。      |
|         --namespace         |    指定 KubeKey 使用的命名空间。默认为 "kubekey-system"。    |
|     --skip-pull-images      |                       跳过预拉取镜像。                       |
|     --skip-push-images      |                       跳过预推送镜像。                       |
|      --with-kubernetes      |                 指定支持的 Kubernetes 版本。                 |
|      --with-kubesphere      |          部署指定版本的 KubeSphere。默认为 v3.4.1。          |
|    --with-local-storage     |            部署本地 PV provisioner（本地存储）。             |
|       --with-packages       |              通过 artifact 安装操作系统软件包。              |
| --with-security-enhancement |                          安全增强。                          |
|          -y, --yes          |                        跳过确认检查。                        |

## 卸载集群

```bash
kk delete cluster
```

