> 本文作者：丁辉

# 基于KubeKey部署Kubernetes

|         节点名称         |      IP      |      Kubernetes角色       |
| :----------------------: | :----------: | :-----------------------: |
| k8s-master-1,KubeKey管理 | 192.168.1.10 | control-plane,etcd,worker |
|       k8s-master-2       | 192.168.1.20 | control-plane,etcd,worker |
|       k8s-master-3       | 192.168.1.30 | control-plane,etcd,worker |

## 基础准备

[官方主页](https://kubesphere.io/zh/)

### 所有节点执行

1. 创建用户

   ```bash
   useradd -m -s /bin/bash kubekey
   ```

2. 配置用户密码

   > 根据自己的密码修改 "123456"

   ```bash
   echo "kubekey:123456" | chpasswd
   ```

3. 配置 kubekey 免密使用 sudo 权限

   ```bash
   cat <<EOF | sudo tee /etc/sudoers.d/kubekey
   # User rules for kubekey
   kubekey ALL=(ALL) NOPASSWD: ALL
   EOF
   ```

4. 更改主机名

   - 192.168.1.10

     ```bash
     hostnamectl set-hostname k8s-master-1 && bash
     ```

   - 192.168.1.20

     ```bash
     hostnamectl set-hostname k8s-master-2 && bash
     ```

   - 192.168.1.30

     ```bash
     hostnamectl set-hostname k8s-master-3 && bash
     ```

5. 编辑 /etc/hosts 文件

   ```bash
   vi /etc/hosts
   ```

   添加如下内容

   ```bash
   192.168.1.10 k8s-master-1
   192.168.1.20 k8s-master-2
   192.168.1.30 k8s-master-3
   ```

6. 安装依赖

   - Centos

     ```bash
     yum install conntrack-tools socat -y
     ```

   - Ubuntu

     ```bash
     apt install conntrack socat -y
     ```

### KubeKey管理节点执行

1. 切换用户

   ```bash
   su - kubekey
   ```

2. 创建密钥

   ```bash
   sudo ssh-keygen -t rsa -N "" -f /root/.ssh/id_rsa -q
   ```

3. 配置免密登录

   ```bash
   sudo ssh-copy-id kubekey@192.168.1.10
   sudo ssh-copy-id kubekey@192.168.1.20
   sudo ssh-copy-id kubekey@192.168.1.30
   ```

## 开始安装 KubeKey

[KubeKey-Github-Releases](https://github.com/kubesphere/kubekey/releases)

### KubeKey管理节点执行

1. 下载最新版 KubeKey

   > 配置国内加速
   >
   > ```bash
   > export KKZONE=cn
   > ```

   ```bash
   curl -sfL https://get-kk.kubesphere.io | sh -
   ```

   > 指定版本
   >
   > ```bash
   > curl -sfL https://get-kk.kubesphere.io | VERSION=v3.0.13 sh -
   > ```

2. 为 `kk` 添加可执行权限

   ```bash
   sudo chmod +x kk
   sudo mv kk /usr/local/bin/
   ```

3. 生成配置文件

   ```bash
   kk create config -f kubesphere.yaml
   ```

   > 安装 Kubesphere 指定参数
   >
   > [Kubesphere-Github-Releases](https://github.com/kubesphere/kubesphere/releases)
   >
   > ```bash
   > --with-kubesphere v3.4.1
   > ```
   
   修改如下内容
   
   ```yaml
   apiVersion: kubekey.kubesphere.io/v1alpha2
   kind: Cluster
   metadata:
     name: sample
   spec:
     hosts:
     - {name: k8s-master-1, address: 192.168.100.10, internalAddress: 192.168.100.10, user: kubekey, password: "123456"}
     - {name: k8s-master-2, address: 192.168.100.20, internalAddress: 192.168.100.20, user: kubekey, password: "123456"}
     - {name: k8s-master-3, address: 192.168.100.30, internalAddress: 192.168.100.30, user: kubekey, password: "123456"}
     roleGroups:
       etcd:
       - k8s-master-1
       - k8s-master-2
       - k8s-master-3
       control-plane: 
       - k8s-master-1
       - k8s-master-2
       - k8s-master-3
       worker:
       - k8s-master-1
       - k8s-master-2
       - k8s-master-3
     controlPlaneEndpoint:
       # 启用内置的 HAProxy 负载均衡器
       internalLoadbalancer: haproxy
       domain: lb.kubesphere.local
       address: ""
       port: 6443
     kubernetes:
       # 指定 Kubernetes 版本, 可使用 kk version --show-supported-k8s 命令查询当前 KubeKey 支持的版本
       version: v1.23.10
       clusterName: cluster.local
       autoRenewCerts: true
       # 指定容器运行时
       containerManager: docker
     etcd:
       type: kubekey
     network:
       plugin: calico
       kubePodsCIDR: 10.233.64.0/18
       kubeServiceCIDR: 10.233.0.0/18
       multusCNI:
         enabled: false
     registry:
       privateRegistry: ""
       namespaceOverride: ""
       registryMirrors: []
       insecureRegistries: []
     addons: []
   ```

## 开始安装 Kubernetes

```bash
kk create cluster -f kubesphere.yaml -y
```

扩容节点

```bash
kk add nodes -f kubesphere.yaml -y
```

## 卸载集群

```bash
kk delete cluster -f kubesphere.yaml
```
