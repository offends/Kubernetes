> 本文作者：丁辉

# 基于KubeSphereLuban搭建开发环境

[官方文档](https://dev-guide.kubesphere.io/extension-dev-guide/zh/quickstart/prepare-development-environment/)

1. 准备 Kubernetes 集群

   ```bash
   curl -sfL https://get-kk.kubesphere.io | sh -
   ./kk create cluster --with-local-storage  --with-kubernetes v1.25.4 --container-manager containerd  -y
   ```

2. 准备 Helm 工具

   ```bash
   curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
   ```

3. 安装 KubeSphere Luban Helm Chart

   ```bash
   helm upgrade --install -n kubesphere-system --create-namespace ks-core  https://charts.kubesphere.io/main/ks-core-0.4.0.tgz --set apiserver.nodePort=30881 --debug --wait
   ```

   > 更多配置参数，请参考[KubeSphere Helm Chart 配置](https://docs.kubesphere.com.cn/v4.0/03-install-and-uninstall/01-install-ks-core/#_高级配置)