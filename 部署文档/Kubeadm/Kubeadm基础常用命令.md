> 本文作者：丁辉

# Kubeadm基础常用命令

- 打印出 `kubeadm` 工具初始化 Kubernetes 集群时的默认配置

  ```bash
  kubeadm config print init-defaults > kubeadm.yaml
  ```

- 列出所有容器镜像

  ```bash
  kubeadm config images list --config kubeadm.yaml
  ```