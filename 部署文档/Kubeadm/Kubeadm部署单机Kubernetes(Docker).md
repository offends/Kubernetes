> 本文作者：丁辉

# Kubeadm部署单机Kubernetes(Docker)

>  Kubernetes v1.24 以后需要额外安装 cri-dockerd , Kubernetes 就正常识别到 Docker
>

| 节点名 |      IP      |
| :----: | :----------: |
| master | 192.168.1.10 |

## 环境准备

> !!!每次部署都写挺麻烦的索性都放在一个文件内了请查看 [Kubernetes基础环境准备](https://gitee.com/offends/Kubernetes/blob/main/部署文档/Kubernetes基础环境准备.md) ,请按照此文档初始化环境

1. 配置主机名

   ```bash
   hostnamectl set-hostname master && bash
   ```

2. 配置主机 hosts

   ```bash
   vi /etc/hosts
   ```

   添加如下内容

   ```bash
   192.168.1.10 master
   ```

## 安装Docker

请跳转此文档	[Docker网络安装](https://gitee.com/offends/Kubernetes/blob/main/Docker/Docs/Centos%E5%AE%89%E8%A3%85Docker.md)

## 安装Cri-dockerd

[Github软件包下载](https://github.com/Mirantis/cri-dockerd/releases)

1. 下载

   ```bash
   wget https://github.com/Mirantis/cri-dockerd/releases/download/v0.3.8/cri-dockerd-0.3.8-3.el7.x86_64.rpm
   ```

2. 安装

   ```bash
   rpm -ivh *.rpm --force --nodeps
   ```

3. 配置国内源

   ```bash
   vi /usr/lib/systemd/system/cri-docker.service
   ```

   修改

   ```bash
   ExecStart=/usr/bin/cri-dockerd --network-plugin=cni --pod-infra-container-image=registry.aliyuncs.com/google_containers/pause:3.9
   ```

4. 启动

   ```bash
   systemctl daemon-reload
   systemctl enable cri-docker.socket cri-docker
   systemctl start cri-docker.socket cri-docker
   systemctl status cri-docker.socket
   systemctl status cri-docker
   ```

## 安装Runc

[Github软件包下载](https://github.com/opencontainers/runc/releases)

1. 下载

   ```bash
   wget https://github.com/opencontainers/runc/releases/download/v1.1.10/runc.amd64
   ```

2. 安装

   ```bash
   install -m 755 runc.amd64 /usr/local/bin/runc
   ```

## 安装Kubeadm

请跳转此文档	[安装Kubeadm](https://gitee.com/offends/Kubernetes/blob/main/部署文档/Kubeadm/安装Kubeadm.md)

## 安装Kubernetes

1. Kubernetes 安装

   ```bash
   kubeadm init \
   --node-name=master \
   --image-repository=registry.aliyuncs.com/google_containers \
   --cri-socket=unix:///var/run/cri-dockerd.sock \
   --apiserver-advertise-address=192.168.1.10 \
   --pod-network-cidr=10.244.0.0/16 \
   --service-cidr=10.96.0.0/12
   ```

   **参数解释**

   |               参数               |                             说明                             |
   | :------------------------------: | :----------------------------------------------------------: |
   |       `--node-name=master`       |                  指定节点的名称为“master”。                  |
   |      `--image-repository=`       | 指定容器镜像仓库地址，此处指定了镜像仓库为registry.aliyuncs.com/google_containers。 |
   |         `--cri-socket=`          | 指定容器运行时接口（CRI）的Unix套接字文件路径，用于与容器运行时通信。此处设置为`unix:///var/run/cri-dockerd.sock`，与CRI-Dockerd通信。 |
   | `--apiserver-advertise-address=` | 指定API服务器公告地址，即API服务器将会公布的地址。在此设置为`192.168.1.10`。 |
   |      `--pod-network-cidr=`       | 指定Pod网络的CIDR地址段。这个CIDR地址段用于分配给Pod。在此设置为`10.244.0.0/16`。 |
   |        `--service-cidr=`         | 指定Service的CIDR地址段。这个CIDR地址段用于分配给Service。在此设置为`10.96.0.0/12`。 |

2. 根据提示配置文件

   ```bash
   mkdir -p $HOME/.kube
   sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
   sudo chown $(id -u):$(id -g) $HOME/.kube/config
   export KUBECONFIG=/etc/kubernetes/admin.conf
   ```

   > 永久生效
   >
   > ```bash
   > echo "export KUBECONFIG=/etc/kubernetes/admin.conf" >>  ~/.bash_profile
   > source ~/.bash_profile
   > ```

## 安装网络插件

请跳转此文档

- [Flannel网络插件安装](https://gitee.com/offends/Kubernetes/blob/main/部署文档/网络插件安装/Flannel网络插件安装.md)
- [Calico网络插件安装](https://gitee.com/offends/Kubernetes/blob/main/部署文档/网络插件安装/Calico网络插件安装.md)
