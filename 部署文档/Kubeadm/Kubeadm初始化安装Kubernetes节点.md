> 本文作者：丁辉

## Kubeadm初始化安装Kubernetes节点

1. Kubernetes安装

   ```bash
   kubeadm init \
   --image-repository=registry.aliyuncs.com/google_containers \
   --apiserver-advertise-address=192.168.1.10 \
   --pod-network-cidr=10.244.0.0/16 \
   --service-cidr=10.96.0.0/12
   ```

2. 根据提示配置文件

   ```bash
   mkdir -p $HOME/.kube
   sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
   sudo chown $(id -u):$(id -g) $HOME/.kube/config
   export KUBECONFIG=/etc/kubernetes/admin.conf
   ```

   > 永久生效
   >
   > ```bash
   > echo "export KUBECONFIG=/etc/kubernetes/admin.conf" >>  ~/.bash_profile
   > source ~/.bash_profile
   > ```

3. 安装网络插件

   请跳转此文档

   - [Flannel网络插件安装](https://gitee.com/offends/Kubernetes/blob/main/部署文档/网络插件安装/Flannel网络插件安装.md)
   - [Calico网络插件安装](https://gitee.com/offends/Kubernetes/blob/main/部署文档/网络插件安装/Calico网络插件安装.md)