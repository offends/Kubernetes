> 本文作者：丁辉

# 安装Kubeadm

[阿里源配置文件](https://developer.aliyun.com/mirror/kubernetes)

[官方安装文档](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#installing-kubeadm-kubelet-and-kubectl)

## YUM安装

1. 添加网络源

   - 官方源

     ```bash
     cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
     [kubernetes]
     name=Kubernetes
     baseurl=https://pkgs.k8s.io/core:/stable:/v1.30/rpm/
     enabled=1
     gpgcheck=1
     gpgkey=https://pkgs.k8s.io/core:/stable:/v1.30/rpm/repodata/repomd.xml.key
     exclude=kubelet kubeadm kubectl cri-tools kubernetes-cni
     EOF
     ```

   - 阿里源

     - 新版

       ```bash
       cat <<EOF | tee /etc/yum.repos.d/kubernetes.repo
       [kubernetes]
       name=Kubernetes
       baseurl=https://mirrors.aliyun.com/kubernetes-new/core/stable/v1.28/rpm/
       enabled=1
       gpgcheck=1
       gpgkey=https://mirrors.aliyun.com/kubernetes-new/core/stable/v1.28/rpm/repodata/repomd.xml.key
       EOF
       ```

     - 旧版

       ```bash
       cat <<EOF > /etc/yum.repos.d/kubernetes.repo
       [kubernetes]
       name=Kubernetes
       baseurl=https://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64/
       enabled=1
       gpgcheck=1
       repo_gpgcheck=1
       gpgkey=https://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg https://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
       EOF
       ```

2. 安装

   - 官方源

     ```bash
     yum install -y kubelet kubeadm kubectl --disableexcludes=kubernetes
     ```

   - 阿里源

     > 由于官网未开放同步方式, 可能会有索引gpg检查失败的情况, 这时请用 `yum install -y --nogpgcheck kubelet kubeadm kubectl` 安装

     ```bash
     yum install -y --nogpgcheck kubelet kubeadm kubectl
     ```

3. 启动

   ```bash
   systemctl enable --now kubelet
   ```


## APT安装

- 官方源

  1. 更新`apt`软件包索引并安装使用 Kubernetes`apt`存储库所需的软件包

     ```bash
     sudo apt-get update
     sudo apt-get install -y apt-transport-https ca-certificates curl gpg
     ```

  2. 下载 Kubernetes 软件包存储库的公共签名密钥。所有存储库都使用相同的签名密钥，因此您可以忽略 URL 中的版本

     ```bash
     curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.30/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
     ```

  3. 添加适当的 Kubernetes`apt`存储库。请注意，此存储库仅包含适用于 Kubernetes 1.30 的软件包；对于其他 Kubernetes 次要版本，您需要更改 URL 中的 Kubernetes 次要版本以匹配所需的次要版本（您还应该检查您正在阅读的文档是否适用于您计划安装的 Kubernetes 版本）。

     ```bash
     echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.30/deb/ /' | sudo tee /etc/apt/sources.list.d/kubernetes.list
     ```

  4. 更新`apt`软件包索引，安装 kubelet、kubeadm 和 kubectl，并固定其版本

     ```bash
     sudo apt-get update
     sudo apt-get install -y kubelet kubeadm kubectl
     sudo apt-mark hold kubelet kubeadm kubectl
     ```

  5. 运行 kubeadm 之前启用 kubelet 服务

     ```bash
     systemctl enable --now kubelet
     ```

- 阿里源

  - 新版

    1. 更新`apt`软件包索引并安装使用 Kubernetes`apt`存储库所需的软件包

       ```bash
       apt-get update && apt-get install -y apt-transport-https
       ```

    2. 下载 Kubernetes 软件包存储库的公共签名密钥。

       ```bash
       curl -fsSL https://mirrors.aliyun.com/kubernetes-new/core/stable/v1.28/deb/Release.key |
           gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
       ```

    3. 添加适当的 Kubernetes`apt`存储库。

       ```bash
       echo "deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://mirrors.aliyun.com/kubernetes-new/core/stable/v1.28/deb/ /" |
           tee /etc/apt/sources.list.d/kubernetes.list
       ```

    4. 更新`apt`软件包索引，安装 kubelet、kubeadm 和 kubectl，并固定其版本

       ```bash
       apt-get update
       apt-get install -y kubelet kubeadm kubectl
       ```

    5. 运行 kubeadm 之前启用 kubelet 服务

       ```bash
       systemctl enable --now kubelet
       ```

  - 旧版

    1. 更新`apt`软件包索引并安装使用 Kubernetes`apt`存储库所需的软件包

       ```bash
       apt-get update && apt-get install -y apt-transport-https
       ```

    2. 下载 Kubernetes 软件包存储库的公共签名密钥。

       ```bash
       curl https://mirrors.aliyun.com/kubernetes/apt/doc/apt-key.gpg | apt-key add - 
       ```

    3. 添加适当的 Kubernetes`apt`存储库。

       ```bash
       cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
       deb https://mirrors.aliyun.com/kubernetes/apt/ kubernetes-xenial main
       EOF
       ```

    4. 更新`apt`软件包索引，安装 kubelet、kubeadm 和 kubectl，并固定其版本

       ```bash
       apt-get update
       apt-get install -y kubelet kubeadm kubectl
       ```

    5. 运行 kubeadm 之前启用 kubelet 服务

       ```bash
       systemctl enable --now kubelet
       ```

       