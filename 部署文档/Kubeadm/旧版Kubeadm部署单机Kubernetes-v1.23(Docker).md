> 本文作者：丁辉

# 旧版Kubeadm部署单机Kubernetes-v1.23(Docker)

[Github-v1.23版本情况](https://github.com/kubernetes/kubernetes/blob/master/CHANGELOG/CHANGELOG-1.23.md)

> 指定 v1.23 版本安装
>

| 节点名 |      IP      |
| :----: | :----------: |
| master | 192.168.1.10 |

## 环境准备

> !!!每次部署都写挺麻烦的索性都放在一个文件内了请查看 [Kubernetes基础环境准备](https://gitee.com/offends/Kubernetes/blob/main/部署文档/Kubernetes基础环境准备.md) ,请按照此文档初始化环境

1. 配置主机名

   ```bash
   hostnamectl set-hostname master && bash
   ```

2. 配置主机 hosts

   ```bash
   vi /etc/hosts
   ```

   添加如下内容

   ```bash
   192.168.1.10 master
   ```

## 安装 Docker

- 设置存储库

  [阿里源配置文件](https://developer.aliyun.com/mirror/docker-ce?spm=a2c6h.13651102.0.0.4eac1b11shXBpr)

  ```bash
  yum install -y yum-utils
  yum-config-manager --add-repo https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
  ```

- 安装(特定版本)

  ```bash
  yum install docker-ce-20.10.24-3.el7 docker-ce-cli-20.10.24-3.el7 -y
  ```

- 编辑配置文件

  ```bash
  vi /etc/docker/daemon.json
  ```

  ```bash
  {
  "exec-opts": ["native.cgroupdriver=systemd"]
  }
  ```

- 启动

  ```bash
  systemctl start docker
  systemctl enable docker
  systemctl status docker
  ```

## 安装Kubeadm

[阿里源配置文件](https://developer.aliyun.com/mirror/kubernetes?spm=a2c6h.13651102.0.0.4eac1b11shXBpr)

- 添加网络源

  ```bash
  cat <<EOF > /etc/yum.repos.d/kubernetes.repo
  [kubernetes]
  name=Kubernetes
  baseurl=https://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64/
  enabled=1
  gpgcheck=1
  repo_gpgcheck=1
  gpgkey=https://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg https://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
  EOF
  ```

- 安装(特定版本)

  ```bash
  yum install kubelet-1.23.17 kubeadm-1.23.17 kubectl-1.23.17 -y
  ```

- 启动

  ```bash
  systemctl enable --now kubelet
  ```

## 安装Kubernetes

- master 安装

  ```bash
  kubeadm init \
  --kubernetes-version=v1.23.17 \
  --pod-network-cidr=10.244.0.0/16 \
  --service-cidr=10.96.0.0/12 \
  --apiserver-advertise-address=192.168.1.10 --image-repository=registry.aliyuncs.com/google_containers
  ```

  **参数**

- 根据提示配置文件

  ```bash
  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config
  export KUBECONFIG=/etc/kubernetes/admin.conf
  ```

  > 永久生效
  >
  > ```bash
  > echo "export KUBECONFIG=/etc/kubernetes/admin.conf" >>  ~/.bash_profile
  > source ~/.bash_profile
  > ```

## 安装网络插件

请跳转此文档

- [Flannel网络插件安装](https://gitee.com/offends/Kubernetes/blob/main/部署文档/网络插件安装/Flannel网络插件安装.md)
- [Calico网络插件安装](https://gitee.com/offends/Kubernetes/blob/main/部署文档/网络插件安装/Calico网络插件安装.md)
