> 本文作者：丁辉

# Kubeadm部署单机Kubernetes(Containerd)

| 节点名 |      IP      |
| :----: | :----------: |
| master | 192.168.1.10 |

## 环境准备

> !!!每次部署都写挺麻烦的索性都放在一个文件内了请查看 [Kubernetes基础环境准备](https://gitee.com/offends/Kubernetes/blob/main/部署文档/Kubernetes基础环境准备.md) ,请按照此文档初始化环境

1. 配置主机名

   ```bash
   hostnamectl set-hostname master && bash
   ```

2. 配置主机 hosts

   ```bash
   vi /etc/hosts
   ```

   添加如下内容

   ```bash
   192.168.1.10 master
   ```

## 安装Containerd

请跳转此文档	[网络源安装Containerd](https://gitee.com/offends/Kubernetes/blob/main/Containerd/Docs/%E7%BD%91%E7%BB%9C%E6%BA%90%E5%AE%89%E8%A3%85Containerd.md)

## 安装Kubeadm

请跳转此文档	[安装Kubeadm](https://gitee.com/offends/Kubernetes/blob/main/%E9%83%A8%E7%BD%B2%E6%96%87%E6%A1%A3/Kubeadm/%E5%AE%89%E8%A3%85Kubeadm.md)

## 安装 Kubernetes

请跳转此文档	[Kubeadm初始化安装Kubernetes节点](https://gitee.com/offends/Kubernetes/tree/main/%E9%83%A8%E7%BD%B2%E6%96%87%E6%A1%A3/Kubeadm/Kubeadm%E5%88%9D%E5%A7%8B%E5%8C%96%E5%AE%89%E8%A3%85Kubernetes%E8%8A%82%E7%82%B9.md)
