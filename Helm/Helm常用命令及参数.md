> 本文作者：丁辉

# Helm常用命令及参数

- **添加仓库**:

  ```bash
  repo add ${repo_name} ${repository_url} 
  ```

- **更新仓库**:

  ```bash
  repo update
  ```

- **在 Helm 仓库中搜索 chart (Helm 软件包)**

  ```bash
  helm search repo ${repo_name}
  ```

  搜索包括开发版本(development versions)

  ```bash
  helm search repo ${repo_name} --devel
  ```

- **拉取仓库文件(默认为压缩包)**:

  ```bash
  helm pull ${repo_name}/${chart_name}
  ```

  拉取仓库文件(源文件)

  ```bash
  helm pull ${repo_name}/${chart_name} --untar
  ```

- **安装(本地目录)**:

  ```bash
  helm install ${release_name} ./${local_chart_dir}
  ```

- **安装(指定网络源)**:

  ```bash
  helm install ${release_name} ${repo_name}/${chart_name}
  ```

- **更新**:

  ```bash
  helm upgrade ${release_name} ${repo_name}/${chart_name}
  ```

- **卸载**:

  ```bash
  helm uninstall ${release_name}
  ```

# 其他参数

- `--create-namespace` **用于在安装时创建指定的命名空间(如果尚不存在)**

  ```bash
  helm install ${release_name} ${repo_name}/${chart_name} \
    --namespace ${namespace_name} \
    --create-namespace
  ```

  