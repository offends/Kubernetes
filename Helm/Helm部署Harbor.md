> 本文作者：丁辉

# Helm部署Harbor

[官网地址](https://goharbor.io/docs/2.6.0/install-config/harbor-ha-helm/)	[Github下载地址](https://github.com/goharbor/harbor/releases)

## 开始部署

1. 添加 Helm 仓库

   ```bash
   helm repo add harbor https://helm.goharbor.io
   helm repo update
   ```

2. 编辑 values.yaml

   ```bash
   vi harbor-values.yaml
   ```

   内容如下

   ```yaml
   expose:
     type: ingress
     tls:
       enabled: true
       certSource: secret
       secret:
         secretName: "harbor-tls"
     ingress:
       hosts:
         core: # 域名
   
   # 对外访问地址
   externalURL: http://域名
   
   # 配置 Harbor密码
   harborAdminPassword: "Harbor12345"
   
   # 持久化存储配置部分
   persistence:
     enabled: true  
   
   # 是否启用监控组件
   metrics:
     enabled: true  
   ```

3. 安装

   ```bash
   helm install harbor \
     --namespace harbor \
     --create-namespace \
     harbor/harbor \
     -f harbor-values.yaml
   ```

## 卸载

1. 卸载 Harbor

   ```bash
   helm uninstall harbor -n harbor
   ```

2. 删除 Harbor 命名空间

   ```bash
   kubectl delete ns harbor
   ```

   

