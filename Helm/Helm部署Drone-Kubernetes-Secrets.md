> 本文作者：丁辉

# Helm部署Drone-Kubernetes-Secrets

[使用文档](https://docs.drone.io/secret/external/kubernetes/)

## 介绍

**Drone-Kubernetes-Secrets 是一个用于管理 Drone 与 Kubernetes 之间 Secrets 交互的组件**。它允许用户在 Drone CI/CD 流程中使用 Kubernetes 集群中的 Secrets，以便更安全地访问敏感数据，例如密码、令牌或 SSH 密钥。

## 开始部署

1. 添加 Drone Helm Chart 存储库

   ```bash
   helm repo add drone https://charts.drone.io
   helm repo update
   ```

2. 创建命名空间

   ```bash
   kubectl create namespace drone
   ```

3. 生成密钥

   ```bash
   openssl rand -hex 16
   ```

4. 编写模版文件

   ```bash
   vi drone-kubernetes-secrets-values.yaml
   ```

   内容如下

   ```yaml
   rbac:
     secretNamespace: drone
   env:
     SECRET_KEY: 填入密钥
     KUBERNETES_NAMESPACE: drone
   ```

5. 启动

   ```bash
   helm install drone-kubernetes-secrets drone/drone-kubernetes-secrets -f drone-runner-kube-values.yaml -n drone
   ```

## 修改Runner-Kube配置

1. 编辑 `drone-runner-kube-values.yaml` 文件

   ```bash
   vi drone-runner-kube-values.yaml
   ```

   env 下添加

   ```yaml
   env:
     DRONE_SECRET_PLUGIN_ENDPOINT: http://drone-kubernetes-secrets:3000
     DRONE_SECRET_PLUGIN_TOKEN: 此处跟SECRET_KEY一致
   # 如有需要开启 DEBUG 调试
     # DRONE_DEBUG: true
   ```

2. 更新 drone-runner-kube

   ```bash
   helm upgrade drone-runner-kube drone/drone-runner-kube -f drone-runner-kube -n drone
   ```

## 卸载

1. 卸载 drone-kubernetes-secrets

   ```bash
   helm uninstall drone-kubernetes-secrets -n drone
   ```

2. 删除命名空间

   ```bash
   kubectl delete namespace drone
   ```

# 使用方法

1. 创建 Secret

   ```bash
   vi drone-secret.yaml
   ```

   内容如下

   ```yaml
   apiVersion: v1
   kind: Secret
   type: Opaque
   data:
     username: YWRtaW4K
     password: YWRtaW4K
   metadata:
     name: build-secret
     namespace: drone
   ```

   部署

   ```bash
   kubectl apply -f drone-secret.yaml
   ```

2. 编写 `.drone.yml`

   ```yaml
   kind: pipeline
   type: kubernetes
   name: secret-demo
   
   steps:
   - name: hello
     image: busybox
     # 环境变量
     environment:
       USERNAME:
         from_secret: USERNAME
       PASSWORD:
         from_secret: PASSWORD
     # 执行命令
     commands:
     # 判断是否存在环境变量,存在则输出成功,不存在则输出失败
     - if [ -n "$USERNAME" ]; then echo "USERNAME exists"; else echo "USERNAME does not exist"; fi
     - if [ -n "$PASSWORD" ]; then echo "PASSWORD exists"; else echo "PASSWORD does not exist"; fi
   ---
   kind: secret
   name: USERNAME
   get:
     path: build-secret
     name: username
   ---
   kind: secret
   name: PASSWORD
   get:
     path: build-secret
     name: password
   ```

3. 构建后查看结果
