> 本文作者：丁辉

# Helm部署OpenEBS存储

## 介绍

**OpenEBS是一种开源云原生存储解决方案，托管于CNCF基金会**。OpenEBS 管理每个 Kubernetes 节点上可用的存储，并使用该存储为有状态工作负载提供[本地](https://openebs.io/docs#local-volumes)或[分布式（也称为复制）](https://openebs.io/docs#replicated-volumes)持久卷。

## 安装 OpenEBS

[官方主页](https://openebs.io/)

[Github仓库](https://github.com/openebs/charts)

[Helm安装文档](https://openebs.github.io/charts/)

1. 添加仓库

   ```bash
   helm repo add openebs https://openebs.github.io/charts
   helm repo update
   ```

2. 编辑 values.yaml 文件

   ```bash
   vi openebs-values.yaml
   ```

   内容如下

   ```yaml
   localprovisioner:
     enableDeviceClass: false
   ```

3. 安装

   [Github-Charts参数文档](https://github.com/openebs/charts/tree/d-master/charts/openebs)

   ```bash
   helm install openebs --namespace openebs openebs/openebs --create-namespace -f openebs-values.yaml
   ```

4. 检查 storageclass

   ```bash
   kubectl get sc
   ```

   > 存在 openebs-hostpath 则👌

5. 运行容器使用 openebs-hostpath 测试

   [openebs-hostpath官方文档](https://openebs.io/docs/user-guides/localpv-hostpath)

   ```bash
   kubectl apply -f https://gitee.com/offends/Kubernetes/raw/main/File/Yaml/openebs-pod.yaml
   ```

   > 容器启动后查看结果
   >
   > ```bash
   > kubectl exec hello-local-hostpath-pod -- cat /mnt/store/greet.txt
   > ```
   >
   > 卸载测试容器
   >
   > ```bash
   > kubectl delete -f https://gitee.com/offends/Kubernetes/raw/main/File/Yaml/openebs-pod.yaml
   > ```

## 设置 openebs-hostpath 为默认存储类

```bash
kubectl patch storageclass openebs-hostpath  -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}' 
```

> 取消 openebs-hostpath 为默认存储类
>
> ```bash
> kubectl patch storageclass openebs-hostpath -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"false"}}}'
> ```

## 卸载 OpenEBS

```bash
helm uninstall openebs -n openebs
```

# 问题记录

MountVolume.NewMounter initialization failed for volume "pvc-某某" : path "/var/openebs/local/pvc-某某" does not exist

> 在将创建本地 PV 主机路径的节点上设置目录, 该目录将被称为 `BasePath` 默认位置是 `/var/openebs/local`

- Rke1 集群配置

  修改 `cluster.yml` 文件,  后更新 rke 集群

  ```yml
  services:
    kubelet:
      extra_binds:
        - /var/openebs/local:/var/openebs/local
  ```

  

