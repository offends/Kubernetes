> 本文作者：丁辉

# Helm部署Drone-Runner-Docker

## 介绍

**Drone-Runner-Docker 是一个用于在 Docker 容器中运行 Drone 构建步骤的插件**。它允许用户在隔离的容器环境中执行构建任务，以确保构建过程的一致性和可重现性。

## 开始部署

[官方部署文档](https://github.com/drone/charts/blob/master/charts/drone-runner-docker/docs/install.md)

> Docker 需要开启 2375 端口

1. 添加 Drone Helm Chart 存储库

   ```bash
   helm repo add drone https://charts.drone.io
   helm repo update
   ```

2. 创建命名空间

   ```bash
   kubectl create namespace drone
   ```

3. 创建 secret 文件

   ```bash
   kubectl create secret generic runner-drone-secret \
     --from-literal=DRONE_RUNNER_CAPACITY=2 \
     --from-literal=DRONE_RUNNER_NAME=runner \
     --from-literal=DRONE_RPC_SECRET=填入密钥 \
     --from-literal=DRONE_RPC_HOST=填入drone域名 \
     --from-literal=DRONE_RPC_PROTO=https \
     -n drone
   ```

   > Runner 添加标签
   >
   > ```bash
   > --from-literal=DRONE_RUNNER_LABELS=标签:值
   > ```

4. 编写模版文件

   ```bash
   vi drone-runner-docker-values.yaml
   ```

   内容如下

   ```yaml
   extraSecretNamesForEnvFrom:
     - runner-drone-secret
     
   # 本地 Docker 开启 2375 后配置(后续构建将使用宿主机本地 Docker 服务)
   env:
     DOCKER_HOST: "tcp://<节点IP>:2375"
   ```

   > 查看 MTU 值, 如果 mtu 小于 1500 则需要传递额外参数
   >
   > ```bash
   > ip link show
   > ```
   >
   > 添加额外参数
   >
   > ```yaml
   > dind:
   >   commandArgs:
   >     - "--host"
   >     - "tcp://localhost:2375"
   >     - "--mtu=12345"
   > ```

5. 启动

   ```bash
   helm install drone-runner-docker drone/drone-runner-docker \
     -n drone \
     -f drone-runner-docker-values.yaml
   ```

## 卸载

1. 卸载 drone-runner-docker

   ```bash
   helm uninstall drone-runner-docker -n drone
   ```

2. 删除 secret

   ```bash
   kubectl delete secret runner-drone-secret -n drone
   ```

3. 删除命名空间

   ```bash
   kubectl delete namespace drone
   ```



