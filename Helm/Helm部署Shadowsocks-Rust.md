> 本文作者：丁辉

# Helm部署Shadowsocks-Rust

[Github仓库](https://github.com/shadowsocks/shadowsocks-rust)	[官网](https://shadowsocks.org/)

## 介绍

`shadowsocks-rust` 是一个使用 Rust 编程语言实现的 Shadowsocks 协议，它提供了网络代理服务。与原始的 Shadowsocks 或基于 C 语言的 shadowsocks-libev 相比，shadowsocks-rust 的目标是提供更好的性能和更高的安全性。

- **使用 Rust 语言**：Rust 提供了内存安全性保证，这意味着使用 Rust 编写的程序在编译时就能排除很多可能的安全隐患，如缓冲区溢出等问题。

- **兼容性**：shadowsocks-rust 完全兼容原始 Shadowsocks 协议，这意味着它可以与其他使用 Shadowsocks 协议的客户端和服务器互操作。

- **支持多种加密算法**：它支持多种加密方式，包括但不限于 AES-256-GCM、ChaCha20-Poly1305 等，这些加密算法旨在确保传输的安全性。

- **跨平台支持**：shadowsocks-rust 可以运行在多种平台上，包括 Linux、macOS 和 Windows，这使得它在不同环境中都可以部署和使用。

- **性能**：由于 Rust 语言的高效性，shadowsocks-rust 在运行时通常表现出更低的延迟和更高的吞吐量。

- **开源项目**：作为一个开源项目，shadowsocks-rust 在 GitHub 上维护，允许社区贡献代码，同时用户可以自由地下载、修改和分发。

## 对外端口详解

- TCP端口

  **TCP（Transmission Control Protocol）**：这是最常见的协议类型，用于大多数网络通信。Shadowsocks 默认使用 TCP 进行数据传输。大部分网络请求，如网页浏览、文件下载、邮件传输等，都使用 TCP 协议。

- UDP端口

  **UDP（User Datagram Protocol）**：这是另一种协议，通常用于需要低延迟的应用，如视频流、实时游戏、语音通话等。UDP 不像 TCP 那样进行严格的数据校验和排序，因此在某些场景下可以提供更快的数据传输速率。

## 开始安装

1. 克隆代码

   ```bash
   git clone https://github.com/shadowsocks/shadowsocks-rust.git
   cd shadowsocks-rust
   ```

2. 编写 values.yaml 文件

   ```bash
   vi shadowsocks-rust-values.yaml
   ```

   内容如下

   ```yaml
   servers:
     - server: "0.0.0.0" # 对外允许访问地址
       server_port: 8388
       service_port: 80
       password: ""  # 填写你的密码
       method: "aes-256-gcm"  # 加密方法
       timeout: 60
       fast_open: true  # 允许数据在 TCP 三次握手的过程中开始传输
       mode: "tcp_and_udp"  # 同时支持 TCP 和 UDP
   
   # 使用本地网络
   hostPort: true
   ```

3. 安装

   ```bash
   helm install \
     shadowsocks-rust ./k8s/chart \
     --namespace shadowsocks \
     --create-namespace \
     -f shadowsocks-rust-values.yaml
   ```

4. 客户端安装

   [Github](https://github.com/shadowsocks/ShadowsocksX-NG)

   打开软件 > 点击服务器 > 服务器设置 > 点击左下角 + 号 > 添加服务器信息并确定

## 卸载

1. 卸载 shadowsocks-rust

   ```bash
   helm uninstall shadowsocks-rust -n shadowsocks
   ```

2. 删除命名空间

   ```bash
   kubectl delete ns shadowsocks
   ```

# Linux客户端安装

> Linux客户端安装可以利用 shadowsocks-rust chart 稍微修改一些参数即可

| 需要连接的服务端IP(假设) | 连接密码(假设) |
| :----------------------: | :------------: |
|       192.168.1.10       |     123456     |

1. 创建命名空间

   ```bash
   kubectl create namespace shadowsocks
   ```

2. 下载 Yaml 文件

   ```bash
   wget https://gitee.com/offends/Kubernetes/raw/main/File/Yaml/shadowsocks-rust-client.yaml
   ```

3. 修改 Yaml 文件

   ```bash
   vi shadowsocks-rust-client.yaml
   ```

   修改如下内容

   ```yaml
   apiVersion: v1
   kind: ConfigMap
   metadata:
     name: shadowsocks-rust-client
     namespace: shadowsocks
   data:
     config.json: |
       {
         "server": "192.168.1.10",
         "server_port": 8388,
         "password": "123456",
         "local_address": "0.0.0.0",
         "local_port": 1080,
         "timeout": 300,
         "method": "aes-256-gcm"
       }
   ```

4. 安装

   ```bash
   kubectl apply -f shadowsocks-rust-client.yaml
   ```

5. 访问地址为

   ```bash
   shadowsocks-rust-client.shadowsocks.svc.cluster.local:1080
   ```

## 卸载

1. 卸载 shadowsocks-rust

   ```bash
   kubectl delete -f shadowsocks-rust-client.yaml
   ```

2. 删除命名空间

   ```bash
   kubectl delete ns shadowsocks
   ```