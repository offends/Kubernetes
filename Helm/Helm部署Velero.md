> 本文作者：丁辉

# Helm部署Velero

[官方文档](https://velero.io/docs/)	[Velero客户端下载](https://github.com/vmware-tanzu/velero/releases)	[Helm仓库介绍页](https://vmware-tanzu.github.io/helm-charts/)	[Helm-Github仓库](https://github.com/vmware-tanzu/helm-charts/blob/main/charts/velero/README.md)

## 开始部署

> 准备 Minio 存储
>
> 示例：
>
> - 存储地址为：192.168.1.10
> - 账户/密码：minioadmin/minioadmin
> - 存储桶：backup

1. 添加 Helm 仓库

   ```bash
   helm repo add vmware-tanzu https://vmware-tanzu.github.io/helm-charts
   helm repo update
   ```

2. 编辑 values.yaml

   ```bash
   vi velero-values.yaml
   ```

   内容如下

   ```yaml
   cleanUpCRDs: true
   
   snapshotsEnabled: false
   
   initContainers:
     - name: velero-plugin-for-aws
       image: velero/velero-plugin-for-aws:latest
       imagePullPolicy: IfNotPresent
       volumeMounts:
         - mountPath: /target
           name: plugins
   
   configuration:
     backupStorageLocation:
     - name: default
       provider: aws
       bucket: backup
       accessMode: ReadWrite
       config:
         region: minio-region
         s3ForcePathStyle: true
         s3Url: http://192.168.1.10:9000
         publicUrl: http://192.168.1.10:9000
   
   credentials:
     useSecret: true
     secretContents:
       cloud: |
         [default]
         aws_access_key_id=minioadmin
         aws_secret_access_key=minioadmin
     
   # 配置定时备份任务 disabled 改为 false 即可启动定时任务(默认关闭)
   # 特别参数解释: 
   # useOwnerReferencesInBackup: 设置为 false，表示在备份时不使用资源的 owner references 来确定哪些关联资源应该包括在备份中。如果设置为 true，Velero 会根据 Kubernetes 的 owner references 备份会自动包括关联资源。
   # ttl: 设置备份的生存时间(Time To Live)。
   # storageLocation: 指定备份数据存储的位置。
   # includedNamespaces: 列出要包括在备份中的命名空间。
   # includedResources: 指定备份中要包括的资源类型。
   # excludedResources: 指定不包括在备份中的资源类型。
   schedules:
     backup:
       disabled: true
       schedule: "*/1 * * * *"
       useOwnerReferencesInBackup: false
       template:
         ttl: "240h"
         storageLocation: default
         includedNamespaces:
         - default
         includedResources:
         - pv,pvc
         excludedResources:
         - pod
   ```

3. 安装

   ```bash
   helm install velero vmware-tanzu/velero \
     --namespace velero --create-namespace \
     -f velero-values.yaml
   ```

4. 下载客户端

   ```bash
   wget https://github.com/vmware-tanzu/velero/releases/download/v1.14.0/velero-v1.14.0-linux-amd64.tar.gz
   ```

5. 解压文件

   ```bash
   tar -zxvf velero-v*-linux-amd64.tar.gz
   ```

6. 移动客户端二进制文件到可执行目录

   ```bash
   mv velero-v*-linux-amd64/velero /usr/local/bin/
   ```

## 卸载

1. 卸载 velero

   ```bash
   helm uninstall velero -n velero
   ```

2. 删除命名空间

   ```bash
   kubectl delete ns velero
   ```

# 基础命令

- 查看所有备份

  ```bash
  velero get backups
  ```

- 备份整个集群

  ```bash
  velero backup create <backup-name>
  ```

  **基础参数**

  - 备份特定的命名空间 `--include-namespaces <namespace>`
  - 备份特定的资源 `--include-resources <resources>`
  - 备份多个特定的资源 `--include-resources pv,pvc`
  - 排除特定资源 `--exclude-resources <resources>`

- 恢复整个备份

  ```
  velero restore create --from-backup <backup-name>
  ```

  - 恢复特定资源 `--include-resources <resources>`

  - 恢复到特定命名空间 `--namespace-mappings old-namespace:<new-namespace>`

  - 恢复时指定名称

    > 这个名称有助于你后续追踪和管理这次特定的恢复操作

    ```bash
    velero restore create <restore-name> --from-backup <backup-name>
    ```

- 删除备份

  ```bash
  velero backup delete <backup-name>
  ```

  > 删除操作时不想被提示确认，可以添加 `--confirm` 参数来直接删除

- 创建定时备份

  > 使用 Cron 表达式(可用参数和 backup 一致)

  ```bash
  velero schedule create <schedule-name> --schedule="* * * * *"
  ```

  - 每 24 小时备份一次 `--schedule="@every 24h"`
  - 每一分钟执行一次 `--schedule="1 * * * *"`
  
- 查看全部定时备份

  ```bash
  velero get schedule
  ```

- 删除定时备份

  ```bash
  velero schedule delete <schedule-name>
  ```

  > 删除操作时不想被提示确认，可以添加 `--confirm` 参数来直接删除

  
