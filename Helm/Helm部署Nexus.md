> 本文作者：丁辉

# Helm部署Nexus

[官网](https://www.sonatype.com/products/sonatype-nexus-repository)

[Helm-Chart](https://github.com/sonatype/helm3-charts)

[GitHub文档](https://github.com/sonatype/nxrm3-ha-repository/blob/main/nxrm-ha/README.md)

## 介绍

**Nexus 是一个强大的仓库管理器，主要用于代理远程仓库及部署第三方构件**。它极大地简化了本地内部仓库的维护和外部仓库的访问。

> 目前推荐使用的是 **sonatype/nxrm-ha**, 用于部署高可用性的 Nexus Repository Manager。其最新版本确保了高可靠性和高可用性, 是企业级生产环境的理想选择。

## 开始部署(旧版)

1. 添加仓库

   ```bash
   helm repo add sonatype https://sonatype.github.io/helm3-charts/
   helm repo update
   ```

2. 编写 values.yaml

   ```bash
   vi nexus-values.yaml
   ```

   内容如下

   ```yaml
   # 对外访问地址
   ingress:
     enabled: true
     ingressClassName: # 指定 ingress 控制器, 不指定则需要集群内存在默认的 ingress 控制器
     annotations:
       nginx.ingress.kubernetes.io/proxy-body-size: "0"
     hostPath: /
     hostRepo: # 域名
     tls:
       - secretName: nexus-tls
         hosts:
           - # 域名
   
   # 修改镜像 TAG
   image:
     repository: sonatype/nexus3
     tag: latest
     
   # 配置存储
   persistence:
     enabled: true
     accessMode: ReadWriteOnce
     storageClass: ""
     storageSize: 20Gi
   ```

3. 安装

   ```bash
   helm install \
     nexus sonatype/nexus-repository-manager \
     --namespace nexus \
     --create-namespace \
     -f nexus-values.yaml
   ```

4. 查看密码

   ```bash
   kubectl exec `kubectl get pod -nnexus|grep nexus-nexus-repository-manager|grep Running|awk '{print $1}'` -nnexus cat /nexus-data/admin.password
   ```

5. 登录

   > admin/密码

   登录后修改密码

## 卸载

1. 卸载 Nexus

   ```bash
   helm uninstall nexus -n nexus
   ```

2. 删除命名空间

   ```bash
   kubectl delete ns nexus
   ```


## 开始部署(新版)

> 新版应该是：`sonatype/nxrm-ha` 仓库，但是我验证时提示需要企业授权🤷所以先放一放吧。

**临时测试 values.yaml**

```yaml
# 配置默认安装命名空间
namespaces:
  nexusNs:
    enabled: true
    name: "nexusrepo"

# 调整资源限额
statefulset:
  replicaCount: 3
  container:
    resources:
      requests:
        cpu: 8
        memory: "8Gi"
      limits:
        cpu: 16
        memory: "16Gi"

secret:
  # 存储数据库机密
  dbSecret:
    enabled: true
  db:
    user: # 填写外部 Postgres 用户
    password: # 填写外部 Postgres 密码
    host: # 填写外部 Postgres 连接地址
  
  # 存储初始 Nexus Repository 管理员密码机密
  nexusAdminSecret:
    enabled: true
    adminPassword: "" # 填写管理员密码
   
  # 存储您的 Nexus Repository Pro 许可证
  license:
    licenseSecret:
      enabled: false
      file: ./nx-license-file.lic
      
# 配置存储
storageClass:
  enabled: false
  name: "" # 指定存储卷, 不指定则需要集群内存在默认的存储卷
pvc:
  accessModes: ReadWriteOnce
  storage: 20Gi
  volumeClaimTemplate:
    enabled: true
```

