> 本文作者：丁辉

# Helm部署Haproxy

## 介绍

**HAProxy是一个功能强大的开源软件，专门用于提供高可用性、负载均衡以及基于TCP和HTTP应用的代理服务**。

## 开始部署

| 节点名称 |      IP      |
| :------: | :----------: |
|   web1   | 192.168.1.10 |
|   web2   | 192.168.1.20 |

[Github仓库](https://github.com/haproxytech/helm-charts)

1. 添加 Helm 仓库

   ```bash
   helm repo add haproxytech https://haproxytech.github.io/helm-charts
   helm repo update
   ```

2. 创建命名空间

   ```bash
   kubectl create namespace haproxy
   ```

3. 编写 values.yaml

   ```bash
   vi haproxy-values.yaml
   ```

   内容如下

   ```yaml
   config: |
     global
       log stdout format raw local0
       maxconn 1024
   
     defaults
       log global
       timeout client 60s
       timeout connect 60s
       timeout server 60s
   
     frontend fe_main
       bind :80
       default_backend be_main
   
     backend be_main
       server web1 192.168.1.10:80 check
       server web2 192.168.1.20:80 check
       
   ingress:
     enabled: true
     servicePort: 80
     className: "" # 指定 ingress 控制器, 不指定则需要集群内存在默认的 ingress 控制器
     hosts:
       - host: # 域名
         paths:
           - path: /
             pathType: Prefix
     tls:
       - secretName: haproxy-tls
         hosts:
           - # 域名
   ```

   **参数解释**

   | 配置部分 |      参数       |                             解释                             |
   | :------: | :-------------: | :----------------------------------------------------------: |
   |  global  |       log       | 应用全局日志配置，将日志输出到标准输出，并使用原始格式进行日志记录，日志级别为 local0。 |
   |          |     maxconn     |                   设置最大连接数为 1024。                    |
   | defaults |       log       | 应用全局日志配置，将日志输出到标准输出，并使用原始格式进行日志记录，日志级别为 local0。 |
   |          | timeout client  | 设置客户端超时时间为 60 秒，即客户端连接到 HAProxy 但没有发送请求的最大时间。 |
   |          | timeout connect |   设置连接超时时间为 60 秒，即连接到后端服务器的最大时间。   |
   |          | timeout server  | 设置服务器超时时间为 60 秒，即后端服务器响应客户端请求的最大时间。 |
   | frontend |      bind       |          在端口 80 上绑定，监听所有 IP 地址的流量。          |
   |          | default_backend |       将所有来自前端的请求转发到名为 be_main 的后端。        |
   | backend  |     server      | 定义两个后端服务器，分别为 web1 和 web2，它们的 IP 地址分别为 192.168.1.10 和 192.168.1.20，监听端口为 80，并且 HAProxy 会定期检查它们的健康状态。 |

4. 创建Nginx证书secret

   > cert为.pem和.crt文件都可以

   ```bash
   kubectl create secret tls haproxy-tls --key nginx.key --cert nginx.pem -n haproxy
   ```

5. 部署

   ```bash
   helm install haproxy haproxytech/haproxy -f haproxy-values.yaml -n haproxy
   ```


6. 查看访问地址

   ```bash
   kubectl get svc -n haproxy
   ```

## 卸载

1. 卸载 haproxy

   ```bash
   helm uninstall haproxy -n haproxy
   ```

2. 删除命名空间

   ```bash
   kubectl delete namespace haproxy
   ```
