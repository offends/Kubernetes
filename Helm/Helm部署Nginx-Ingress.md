> 本文作者：丁辉
>

# Helm 部署 Nginx-Ingress

## 介绍

**Nginx-Ingress 是一个基于 Nginx 的 Ingress 控制器，用于管理外部访问 Kubernetes 集群内部服务的路由规则**。它实现了一个配置有规则的反向代理负载均衡器，能够将入站 HTTP 和 HTTPS 请求路由到集群内的相应服务上。

## 开始部署

[官方文档](https://kubernetes.github.io/ingress-nginx/deploy/)

1. 添加仓库

   ```bash
   helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
   helm repo update
   ```

2. 编写模版文件

   ```bash
   vi ingress-values.yaml
   ```

   内容如下

   ```yaml
   controller:
     # 开启监控
     metrics:
       enabled: true
       serviceMonitor:
         enabled: true
         
     # 配置镜像加速
     image:
       registry: k8s.dockerproxy.com
       
     # 使用主机网络时，利用 Kubernetes 集群内的 DNS 解析服务
     dnsPolicy: ClusterFirstWithHostNet
     
     # 使用本地网络
     hostNetwork: true
     
     # Pod 使用 DaemonSet 方式运行
     kind: DaemonSet
     
     # 只允许调度到具有 ingress="true" 的节点上,[ kubectl label node xxx ingress="true" ]
     nodeSelector:
       kubernetes.io/os: linux
       ingress: "true"
     
     # 禁用后状态字段会报告 Ingress 控制器 Pod 所在节点的 IP 地址或节点列表的 IP 地址
     publishService:
       enabled: false
     
     # 启用 Kubernetes Service
     service:
       enabled: false
       
     # 配置镜像加速
     admissionWebhooks:
       patch:
         image:
           registry: k8s.dockerproxy.com
     
     # 设置为集群默认 ingress 控制器
     ingressClassResource:
       default: true
   ```

3. 配置节点标签

   ```bash
   kubectl label node ${node} ingress="true"
   ```

4. 部署

   ```bash
   helm install ingress-nginx \
     ingress-nginx/ingress-nginx \
     -f ingress-values.yaml 
   ```

## 卸载

```bash
helm uninstall ingress-nginx
```

