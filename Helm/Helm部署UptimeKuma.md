> 本文作者：丁辉

# Helm部署UptimeKuma

## 介绍

UptimeKuma是一个开源的、自托管的监控工具，专为追踪网站、应用程序和网络服务的可用性而设计。它提供了一个简洁的用户界面，使用户能够轻松地监控他们的在线服务的状态，并接收关于其状态变化的实时更新。

## 开始部署

[Github仓库](https://github.com/louislam/uptime-kuma)

官方推荐(非官方)：[Helm仓库](https://github.com/k3rnelpan1c-dev/uptime-kuma-helm)

1. 添加 Helm 仓库

   ```bash
   helm repo add k3 https://k3rnelpan1c-dev.github.io/uptime-kuma-helm/
   helm repo update
   ```

2. 编写 values.yaml 文件

   ```bash
   vi uptime-kuma-values.yaml
   ```

   内容如下

   ```yaml
   ingress:
     enabled: true
     className: "" # 指定 ingress 控制器, 不指定则需要集群内存在默认的 ingress 控制器
     hosts:
       - host: # 域名
         paths:
           - path: /
             pathType: ImplementationSpecific
     tls:
      - secretName: uptime-kuma-tls
        hosts:
          - # 域名
          
   # 配置持久化存储
   persistence:
     enabled: true
     sizeLimit: 4Gi
     storageClass: "" # 指定存储卷, 不指定则需要集群内存在默认的存储卷
   ```

3. 创建Nginx证书secret

   > cert为.pem和.crt文件都可以

   ```bash
   kubectl create secret tls uptime-kuma-tls --key nginx.key --cert nginx.pem -n monitor
   ```

4. 安装

   ```bash
   helm install uptime-kuma k3/uptime-kuma \
     -n monitor \
     -f uptime-kuma-values.yaml
   ```

## 卸载

```bash
helm uninstall uptime-kuma -n monitor
```

