> 本文作者：丁辉

# Helm部署HertzBeat

## 介绍

**HertzBeat是一个开源实时监控系统，具有无需Agent、性能集群、兼容Prometheus等特点**。

## 开始部署

[Github仓库](https://github.com/apache/hertzbeat/tree/master)

[中文官方文档](https://hertzbeat.apache.org/zh-cn/docs/)

1. 添加仓库

   ```bash
   helm repo add hertzbeat https://charts.hertzbeat.com/
   helm repo update
   ```

2. 创建命名空间

   ```bash
   kubectl create namespace hertzbeat
   ```

3. 编写 values.yaml 文件

   ```bash
   vi hertzbeat-values.yaml
   ```

   内容如下

   ```yaml
   expose:
     type: ingress
     ingress:
       enabled: true
       host: "" # 域名
       tls:
         enabled: true
         tls:
           - secretName: hertzbeat-tls
   ```

4. 创建Nginx证书secret

   > cert为.pem和.crt文件都可以

   ```bash
   kubectl create secret tls hertzbeat-tls --key nginx.key --cert nginx.pem -n hertzbeat
   ```

5. 安装

   ```bash
   helm install hertzbeat hertzbeat/hertzbeat \
     --namespace hertzbeat --create-namespace \
     -f hertzbeat-values.yaml
   ```

## 卸载

1. 卸载 hertzbeat

   ```bash
   helm uninstall hertzbeat -n hertzbeat
   ```

2. 删除 secret

   ```bash
   kubectl delete secret hertzbeat-tls -n hertzbeat
   ```

3. 删除命名空间

   ```bash
   kubectl delete namespace hertzbeat
   ```
