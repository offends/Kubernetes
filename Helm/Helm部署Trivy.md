> 本文作者：丁辉

# Helm部署Trivy

## 介绍

**Trivy 是一个简单而全面的容器漏洞扫描程序，适用于持续集成（CI）环境**。它能够检测操作系统包和应用程序依赖的漏洞，帮助开发人员确保镜像的安全性。

## 开始部署

[官网](https://trivy.dev/)	[Github仓库](https://github.com/aquasecurity/trivy-operator)

[Github-trivy-java-db](https://github.com/aquasecurity/trivy-java-db)	[Github-trivy-db](https://github.com/aquasecurity/trivy-db)	[数据同步ORAS软件](https://oras.land/docs/quickstart/)

[Trivy-db离线数据下载地址](https://github.com/aquasecurity/trivy/releases)	[Trivy-db使用介绍](https://github.com/aquasecurity/trivy-db/pkgs/container/trivy-db)

1. 添加仓库

   ```bash
   helm repo add aqua https://aquasecurity.github.io/helm-charts/
   helm repo update
   ```

2. 配置 values.yaml 文件

   ```bash
   vi trivy-operator-values.yaml
   ```

   内容如下

   ```yaml
   # 配置代理
   global:
     image:
       registry: "ghcr.dockerproxy.com"
       
   # 开启监控自动发现
   serviceMonitor:
     enabled: true
   
   # 配置不需要扫描的命令空间
   excludeNamespaces: "kube-system,trivy-system"
   
   operator:
     # 指定访问私有容器镜像仓库所需的认证信息
     privateRegistryScanSecretsNames:
       trivy-system: docker-registry-secret
     # 是否使用内置的 Trivy 服务器(要使用外部 Trivy 服务器则配置为"false")
     builtInTrivyServer: true
     
   trivy:
     # 配置镜像仓库检测
     insecureRegistries:
       rbdRegistry: index.docker.io # 填写镜像仓库地址,替换为自己的仓库地址
     storageClassName: "" # 指定存储卷, 不指定则需要集群内存在默认的存储卷
     mode: ClientServer # 指定了 Trivy 的运行模式为客户端模式
     serverURL: "https://trivy.trivy:4975" # 指定了 Trivy 服务器的访问 URL
     # 配置国内 Trivy 数据库
     dbRegistry: "ccr.ccs.tencentyun.com"
     dbRepository: "inative/trivy-db"
     javaDbRegistry: "ccr.ccs.tencentyun.com"
     javaDbRepository: "inative/trivy-java-db"
   ```

3. 创建镜像仓库 Secret

   ```bash
   kubectl create secret docker-registry docker-registry-secret \
     --docker-server=index.docker.io \
     --docker-username=YOUR_USERNAME \
     --docker-password=YOUR_PASSWORD \
     --namespace=trivy-system
   ```

4. 部署

   ```bash
   helm install trivy-operator aqua/trivy-operator \
     --namespace trivy-system \
     --create-namespace \
     -f trivy-operator-values.yaml
   ```

5. 验证查看报告

   - 查询漏洞报告

     > 报告主要关注的是集群中的容器镜像或其他资源是否包含已知的安全漏洞。

     ```bash
     kubectl get vulnerabilityreports -o wide
     ```

   - 查询配置审计报告

     > 报告专注于集群资源的配置安全，检查 Kubernetes 资源的配置设置是否遵守安全最佳实践。

     ```bash
     kubectl get configauditreports -o wide
     ```

## 卸载

1. 卸载 trivy-operator

   ```bash
   helm uninstall trivy-operator -n trivy-system
   ```

2. 删除命名空间

   ```bash
   kubectl delete ns trivy-system
   ```

   
