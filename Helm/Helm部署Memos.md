> 本文作者：丁辉

# Helm部署Memos

## 介绍

**Memos是一个开源且免费的自托管知识库，它允许用户自由写作并使用SQLite数据库文件进行数据存储**。

## 开始部署

[Github仓库](https://github.com/usememos/memos)	[Github-Helm仓库](https://github.com/usememos/helm)

1. 拉取 Helm chart

   ```bash
   git clone https://github.com/usememos/helm.git
   ```

2. 创建命名空间

   ```bash
   kubectl create namespace memos
   ```

3. 编辑 values.yaml

   ```bash
   vi memos-values.yaml
   ```

   内容如下

   ```yaml
   # 配置镜像加速
   image:
     repo: ghcr.dockerproxy.com
   
   # 开启持久化存储
   persistence:
     enabled: true
     storageClass: "" # 指定存储卷, 不指定则需要集群内存在默认的存储卷
   
   # 开启 ingress 对外访问
   ingress:
     enabled: true
     className: "" # 指定 ingress 控制器, 不指定则需要集群内存在默认的 ingress 控制器
     hosts:
       - host: # 域名
         paths:
           - path: /
             pathType: Prefix
     tls:
       - hosts:
         - # 域名
         secretName: memos-tls
   ```

4. 创建Nginx证书secret

   > cert为.pem和.crt文件都可以

   ```bash
   kubectl create secret tls memos-tls --key nginx.key --cert nginx.pem -n memos
   ```

5. 部署

   ```bash
   helm install memos ./helm -f memos-values.yaml -n memos
   ```

## 卸载

1. 卸载 memos

   ```bash
   helm uninstall memos -n memos
   ```

2. 删除 secret

   ```bash
   kubectl delete secret memos-tls -n memos
   ```

3. 删除命名空间

   ```bash
   kubectl delete namespace memos
   ```

   

