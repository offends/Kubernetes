> 本文作者：丁辉

# Helm部署

[官方文档](https://helm.sh/docs/intro/install/)

## 官方脚本

```bash
curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
```

## 国内源获取

> 考虑到文件在外网可能会拉取非常慢，所以选择国内源获取

```bash
curl https://gitee.com/offends/Kubernetes/raw/main/File/Shell/helm-install.sh | bash
```

## 卸载 Helm

```bash
rm -rf $(which helm)
```

