> 本文作者：丁辉

# Helm部署Traffic-Manager

## 介绍

**Traffic Manager是一种流量分配系统，用于在网络中管理和控制数据流**。Traffic Manager常被用于监控网络状态、优化数据流向以及确保高效可靠的数据传输。在多种应用场景中，例如内容分发网络(CDN)、云服务负载均衡、企业网络管理等，Traffic Manager扮演着至关重要的角色。

## 开始部署

[官方主页](https://www.getambassador.io/docs)	[官方文档](https://www.getambassador.io/docs/telepresence/latest/install/manager#install-the-traffic-manager-with-helm)

1. 添加仓库

   ```bash
   helm repo add datawire https://app.getambassador.io
   helm repo update
   ```

2. 创建命名空间

   ```bash
   kubectl create namespace ambassador
   ```

3. 安装

   ```bash
   helm install traffic-manager --namespace ambassador datawire/telepresence
   ```

4. 本地开启链接验证

   ```bash
   telepresence connect
   ```

   > **测试 Ping 通任何一个容器 Service 代表成功**
   >
   > 取消链接
   >
   > ```bash
   > telepresence quit
   > ```

## 卸载

1. 卸载 traffic-manager

   ```bash
   helm uninstall traffic-manager -n ambassador
   ```

2. 删除命名空间

   ```bash
   kubectl delete namespace ambassador
   ```

   