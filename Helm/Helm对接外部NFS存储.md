> 本文作者：丁辉
>

# Helm对接外部NFS存储

> **NFS subdir 外部配置程序**是一个自动配置程序，它使用现有且已配置的NFS 服务器来支持通过持久卷声明动态配置 Kubernetes 持久卷。

## 部署

[Github仓库](https://github.com/kubernetes-sigs/nfs-subdir-external-provisioner/tree/master)

[官网介绍](https://github.com/kubernetes-sigs/nfs-subdir-external-provisioner/blob/master/charts/nfs-subdir-external-provisioner/README.md)

1. 添加仓库

   ```bash
   helm repo add nfs-subdir-external-provisioner https://kubernetes-sigs.github.io/nfs-subdir-external-provisioner/
   helm repo update
   ```

2. 安装

   ```bash
   helm install nfs-subdir-external-provisioner nfs-subdir-external-provisioner/nfs-subdir-external-provisioner \
   --set nfs.server=127.0.0.1 \
   --set nfs.path=/data
   ```

   > 国内无法拉取到此镜像 x86 架构的我已经同步到国内了，使用此命令安装
   >
   > ```bash
   > helm install nfs-subdir-external-provisioner \
   > nfs-subdir-external-provisioner/nfs-subdir-external-provisioner \
   > --set image.repository=registry.cn-hangzhou.aliyuncs.com/offends/nfs-subdir-external-provisioner \
   > --set image.tag=v4.0.2 \
   > --set nfs.server=127.0.0.1 \
   > --set nfs.path=/data
   > ```

3. 查看

   ```bash
   kubectl get pod -l app=nfs-subdir-external-provisioner
   ```

4. 检查 storageclass

   ```bash
   kubectl get sc
   ```

    > 存在 nfs-client 则👌

## 配置 nfs-client 为默认存储类

```bash
kubectl patch storageclass nfs-client -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'
```

> 取消 nfs-client 为默认存储类
>
> ```bash
> kubectl patch storageclass nfs-client -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"false"}}}'
> ```

## 指定 NFS vers 版本

> 遇到一个非常难解的问题，在 centos7.9 系统版本中容器挂载使用 NFS 存储当作持久化卷，数据库这一种类型的容器无法启动，修改 vers 版本为3.0解决问题

**解决方案**

- 第一种修改 values.yaml 配置文件

  ```bash
  vi nfs-subdir-external-provisioner/values.yaml
  ```

  内容如下

  ```bash
  nfs:
    server:
    path: /data
    mountOptions:
    - nfsvers=3.0
  ```

- 第二种添加 install 参数

```bash
--set nfs.mountOptions[0]=nfsvers=3.0
```

## 老版本部署(已废弃)

[Github仓库](https://github.com/kubernetes-retired/external-storage/tree/master/nfs-client)

1. 拉取代码

   ```bash
   helm repo add stable https://charts.helm.sh/stable
   helm repo update
   ```

2. 部署

   ```bash
   helm install nfs-storageclass stable/nfs-client-provisioner --set nfs.server=127.0.0.1 --set nfs.path=/data
   ```

3. 修改 apiserver 参数

   ```bash
   vi /etc/kubernetes/manifests/kube-apiserver.yaml
   ```

   添加如下参数

   ```bash
   - --feature-gates=RemoveSelfLink=false
   ```


## 卸载

```bash
helm uninstall nfs-subdir-external-provisioner
```

