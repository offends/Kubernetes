> 本文作者：丁辉

# Helm部署KongGateway

[官方安装文档](https://docs.konghq.com/gateway/latest/install/kubernetes/proxy/)	[Github仓库](https://github.com/pantsel/konga)

## 介绍

Kong Gateway是一个**基于Nginx和OpenResty实现的云原生分布式API网关，具有高性能、高可用特点**。

## 开始部署

1. 添加 Helm 仓库

   ```bash
   helm repo add kong https://charts.konghq.com
   helm repo update
   ```

2. 编辑 values.yaml

   ```bash
   vi kong-gateway-values.yaml
   ```

   内容如下

   ```yaml
   ingressController:
     enabled: true
     
   # 安装模式配置为 daemonset
   deployment:
     daemonset: true
     hostNetwork: false
     
   # 更改 service type
   proxy:
     enabled: true
     type: ClusterIP
     http:
       hostPort: 80
     tls:
       hostPort: 443
   manager:
     enabled: true
     type: ClusterIP
   admin:
     enabled: true
     type: ClusterIP
     # 启用管理员API
     http:
       enabled: true
     tls:
       enabled: false
     
   # 配置标签
   nodeSelector:
     kong: "true"
     
   # 开启监控
   serviceMonitor:
     enabled: true
   ```

3. 配置节点标签

   ```bash
   kubectl label node ${node} kong="true"
   ```

4. 安装

   ```bash
   helm install kong kong/kong \
     --namespace kong \
     --create-namespace \
     -f kong-gateway-values.yaml
   ```


## 启用 Postgres 数据库

要启用 Postgres 数据库的话需要在 values.yaml 内添加如下内容:

```yaml
# 对接外部数据库默认为不对接 postgres
# 官方 postgres 参数文档: https://docs.konghq.com/gateway/3.7.x/reference/configuration/#datastore-section
# 提示 pg_host 参数使用 svc 配置后组件启动可能会报错解析错误, 切换为 svc IP即可解决。
env:  
  database: "postgres"
  pg_host: "kong-postgresql.kong.svc.cluster.local"
  pg_port: "5432"
  pg_user: kong
  pg_password: kong
  pg_database: kong
  pg_ssl: false
  pg_ssl_verify: false
  
  router_flavor: "traditional"
  nginx_worker_processes: "2"
  proxy_access_log: /dev/stdout
  admin_access_log: /dev/stdout
  admin_gui_access_log: /dev/stdout
  portal_api_access_log: /dev/stdout
  proxy_error_log: /dev/stderr
  admin_error_log: /dev/stderr
  admin_gui_error_log: /dev/stderr
  portal_api_error_log: /dev/stderr
  prefix: /kong_prefix/

# 开启 postgresql
postgresql:
  enabled: true
  auth:
    username: "kong"
    password: "kong"
    database: "kong"
```

## 卸载

1. 卸载

   ```bash
   helm uninstall kong -n kong
   ```

2. 删除 PVC

   ```bash
   kubectl delete pvc data-kong-postgresql-0 -n kong
   ```

3. 删除命名空间

   ```bash
   kubectl delete namespace kong
   ```

   
