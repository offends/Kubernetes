> 本文作者：丁辉

# Helm部署Metrics-Server

## 介绍

**Metrics-Server 是一个 Kubernetes 插件，用于聚合和收集集群中与资源使用情况相关的指标数据**。它通过 Kubelet 的 API 获取各节点和容器的资源使用情况，为 Kubernetes 的自动资源管理和水平 Pod 自动扩展提供数据支持。

## 开始部署

[官方文档](https://kubernetes-sigs.github.io/metrics-server/)

1. 添加 Metrics-Server Helm 仓库

   ```bash
   helm repo add metrics-server https://kubernetes-sigs.github.io/metrics-server/
   helm repo update
   ```

2. 编辑模版文件

   ```bash
   vi metrics-server-values.yaml
   ```

   内容如下

   ```yaml
   # 配置镜像加速
   image:
     repository: registry.aliyuncs.com/google_containers/metrics-server
   args:
   - --kubelet-insecure-tls
   
   # 开启 ServiceMonitor
   metrics:
     enabled: true
   serviceMonitor:
     enabled: true
   ```

    **参数解释**

   |                             参数                             |                            描述                            |
   | :----------------------------------------------------------: | :--------------------------------------------------------: |
   |                      `--cert-dir=/tmp`                       |              Metrics Server 使用的证书目录。               |
   |                     `--secure-port=4443`                     |             Metrics Server 监听的安全端口号。              |
   |                   `--kubelet-insecure-tls`                   |         是否跳过与 kubelet 通信时的 TLS 证书验证。         |
   | `--kubelet-preferred-address-types=InternalIP,ExternalIP,Hostname` |    Metrics Server 与 kubelet 通信时首选的节点地址类型。    |
   |                  `--metric-resolution=15s`                   | 生成度量数据的分辨率（间隔），这里设置为每 15 秒生成一次。 |

3. 部署

   ```bash
   helm install metrics-server metrics-server/metrics-server \
     --namespace monitor -f metrics-server-values.yaml
   ```


## 卸载

```bash
helm uninstall metrics-server -n monitor
```

