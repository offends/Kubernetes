> 本文作者：丁辉

# Helm部署Docker-Registry-UI

[Github仓库](https://github.com/Joxit/helm-charts/tree/main)

[Helm-Chart仓库](https://github.com/Joxit/helm-charts/tree/main/charts/docker-registry-ui)

## 介绍

**Docker-Registry-UI 是一个基于Web的可视化管理工具，用于简化Docker Registry的使用和管理**。

## 开始部署

1. 添加仓库

   ```bash
   helm repo add joxit https://helm.joxit.dev
   helm repo update
   ```

2. 创建命名空间

   ```bash
   kubectl create namespace hub
   ```

3. 编写 values.yaml 文件

   ```bash
   vi docker-registry-ui-values.yaml
   ```

   内容如下

   ```yaml
   ui:
     image: joxit/docker-registry-ui:latest
     # 如下配置对应官方部署文档: https://github.com/Joxit/docker-registry-ui#recommended-docker-registry-usage
     singleRegistry: true
     title: "Docker registry UI"
     deleteImages: true
     showContentDigest: true
     # 开启 proxy 填写 Docker Registry 的访问地址
     proxy: true
     dockerRegistryUrl: http://docker-registry.hub.svc.cluster.local:5000
     showCatalogNbTags: true
     catalogMinBranches: 1
     catalogMaxBranches: 1
     taglistPageSize: 100
     registrySecured: false
     catalogElementsLimit: 1000
     
     # UI对外访问
     ingress:
       enabled: true
       host: #域名
       ingressClassName: nginx
       tls:
         - hosts:
           - #域名
           secretName: docker-registry-ui-tls
   ```

4. 创建Nginx证书secret

   > cert为.pem和.crt文件都可以

   ```bash
   kubectl create secret tls docker-registry-ui-tls --key nginx.key --cert nginx.pem -n hub
   ```

5. 安装

   ```bash
   helm install docker-registry-ui joxit/docker-registry-ui \
     -f docker-registry-ui-values.yaml \
     --namespace hub
   ```

## 卸载

1. 卸载 gitea

   ```bash
   helm uninstall docker-registry-ui -n hub
   ```

2. 删除 secret

   ```bash
   kubectl delete secret docker-registry-ui-tls -n hub
   ```

3. 删除命名空间

   ```bash
   kubectl delete namespace hub
   ```