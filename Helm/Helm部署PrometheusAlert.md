> 本文作者：丁辉

# Helm部署PrometheusAlert

## 介绍

**PrometheusAlert 是一个开源的运维告警中心消息转发系统，它能够支持多种主流的监控系统、日志系统以及数据可视化系统**。PrometheusAlert 的设计宗旨是为了解决不同系统之间预警消息的有效传递问题，确保关键信息能够及时通知到相关人员。

## 开始部署

[官方文档](https://github.com/feiyu563/PrometheusAlert/blob/master/doc/readme/base-install.md)

> 准备好 Mysql 数据库
>
> 安装可查看如下文档
>
> [Helm部署Mysql](https://gitee.com/offends/Kubernetes/blob/main/Helm/Helm%E9%83%A8%E7%BD%B2Mysql.md)

|     服务名      |                  IP地址:端口                   |            账户密码             |
| :-------------: | :--------------------------------------------: | :-----------------------------: |
|      Mysql      |               192.168.1.10:3306                |         root/Root123456         |
| PrometheusAlert | prometheusalert.monitor.svc.cluster.local:8080 | prometheusalert/prometheusalert |

1. 拉取代码

   ```bash
   git clone https://github.com/feiyu563/PrometheusAlert.git
   cd PrometheusAlert/example/helm
   ```

2. Mysql创建数据库

   ```bash
   CREATE DATABASE prometheusalert CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
   ```

3. 修改 app.conf 文件

   ```bash
   vi prometheusalert/config/app.conf
   ```

   修改内容如下

   ```bash
   db_driver=mysql
   db_host=mysql.mysql.svc.cluster.local
   db_port=3306
   db_user=root
   db_password=Root123456
   db_name=prometheusalert
   # 开启飞书告警通道
   open-feishu=1
   ```

   > 参考	[app.conf文件配置](https://gitee.com/offends/Kubernetes/blob/main/File/Conf/PrometheusAlert-App.conf)

4. 编写 values.yaml 文件

   ```bash
   vi prometheusalert-values.yaml
   ```

   内容如下

   ```yaml
   ingress:
     enabled: false
   ```

5. 安装

   ```bash
   helm install prometheusalert ./prometheusalert \
     --namespace monitor --create-namespace \
     -f prometheusalert-values.yaml
   ```

## 卸载

```bash
helm uninstall prometheusalert -n monitor
```

# 配置飞书告警模版测试

打开PrometheusAlert web页面，进入菜单模版管理-->自定义模板-->添加模版

- **模版名称**：prometheus-fs

- **模版类型**：飞书

- **模版用途**：Prometheus

- **模版内容:**

  ```bash
  {{- range $k, $v := .alerts -}}
    {{- if eq $v.status "resolved" -}}
      <font color="green">**告警恢复信息**</font>
      事件名称: **{{ if $v.annotations.summary }}{{ $v.annotations.summary }}{{ else }}{{ $v.labels.alertname }}{{ end }}**
      {{ if $v.status }}告警类型: {{$v.status}}{{ end }}
      {{ if $v.labels.level }}告警级别: {{$v.labels.level}}{{ end }}
      开始时间: {{GetCSTtime $v.startsAt}}
      恢复时间: {{GetCSTtime $v.endsAt}}
      {{ if $v.labels.instance }}主机地址: {{$v.labels.instance}}{{ end }}
      {{ if $v.annotations.value }}当前值: {{$v.annotations.value}}{{ end }}
      <font color="green">**事件回顾: {{$v.annotations.description}}**</font>
    {{- else -}}
      <font color="red">**告警信息**</font>
      事件名称: **{{ if $v.annotations.summary }}{{ $v.annotations.summary }}{{ else }}{{ $v.labels.alertname }}{{ end }}**
      {{ if $v.status }}告警类型: {{$v.status}}{{- end }}
      {{ if $v.labels.level }}告警级别: {{$v.labels.level}}{{ end }}
      开始时间: {{GetCSTtime $v.startsAt}}
      {{ if $v.labels.instance }}主机地址: {{$v.labels.instance}}{{ end }}
      {{ if $v.annotations.value }}触发值: {{$v.annotations.value}}{{ end }}
      <font color="red">**事件详情: {{$v.annotations.description}}**</font>
    {{- end -}}
  {{- end -}}
  ```
  
- 保存模版
