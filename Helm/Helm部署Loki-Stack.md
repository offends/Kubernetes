> 本文作者：丁辉

# Helm部署Loki-Stack

## 介绍

**Loki-Stack 是一个日志聚合和分析平台，它包括了 Loki、Promtail、Grafana 等组件**。Loki-Stack 旨在为 Kubernetes 环境提供一套完整的日志管理系统，通过这些组件的协同工作，可以实现对容器日志的收集、存储、查询和可视化。

## 开始部署

[Github仓库](https://github.com/grafana/helm-charts/tree/main/charts/loki-stack)

1. 添加仓库

   ```bash
   helm repo add grafana https://grafana.github.io/helm-charts
   helm repo update
   ```

2. 编辑 values.yaml

   ```bash
   vi loki-values.yaml
   ```

   内容如下

   ```yaml
   test_pod:
     enabled: false
    
   # 开启监控
   loki:
     serviceMonitor:
       enabled: true
   ```

3. 部署

   ```bash
   helm install loki-stack grafana/loki-stack \
     -n monitor --create-namespace \
     -f loki-values.yaml
   ```


## 卸载

1. 卸载 loki-stack

   ```bash
   helm uninstall loki-stack -n monitor
   ```

2. 删除命名空间

   ```bash
   kubectl delete namespace monitor
   ```
