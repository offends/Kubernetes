> 本文作者：丁辉
>

# Helm部署Gitea

## 介绍

**Gitea是一个轻量级的DevOps平台软件，支持Git托管、代码审查、团队协作、软件包注册和CI/CD等功能**。

## 开始部署

[Chart仓库](https://dl.gitea.com/charts/)

1. 添加 Helm 仓库

   ```bash
   helm repo add gitea https://dl.gitea.io/charts
   helm repo update
   ```

2. 创建命名空间

   ```bash
   kubectl create namespace gitea
   ```

3. 生成secret

   ```bash
   kubectl create secret generic gitea-secret \
     --from-literal=username=设定仓库账号 \
     --from-literal=password=设定仓库密码 \
     -n gitea
   ```

4. 创建Nginx证书secret

   > cert为.pem和.crt文件都可以

   ```bash
   kubectl create secret tls gitea-tls --key nginx.key --cert nginx.pem -n gitea
   ```

5. 编写模版文件

   ```bash
   vi gitea-values.yaml
   ```

   内容如下

   ```yaml
   # 开启 ingress 对外访问
   ingress:
     enabled: true
     className: # 指定 ingress 控制器, 不指定则需要集群内存在默认的 ingress 控制器
     hosts:
       - host: # 域名
         paths:
           - path: /
             pathType: Prefix
     tls:
       - hosts:
         - # 域名
         secretName: gitea-tls
       
   # 配置持久化存储
   global:
     storageClass: # 指定存储卷, 不指定则需要集群内存在默认的存储卷
     
   # 配置 ssh 模式下对外访问端口
   service:
     ssh:
       type: NodePort
       port: 22
       nodePort: 30000
     
   # 配置管理员账号和密码
   gitea:
     admin:
       existingSecret: gitea-secret
       email: "gitea@gitea.com" # 配置仓库默认用户邮箱
       
     config:
       APP_NAME: "Gitea" # 配置 Gitea 默认主页面展示名称
     
       server:
         SSH_DOMAIN: "gitea.com"
         DOMAIN: "gitea.com"
         SSH_LISTEN_PORT: "22"
         SSH_PORT: "30000"
     
   # 关闭 redis 集群
   redis-cluster:
     enabled: false
     
   # 关闭 postgresql 集群  
   postgresql-ha:
     enabled: false
     
   # 启用 postgresql
   postgresql:
     enabled: true
   ```

6. 部署

   ```bash
   helm install gitea --namespace gitea -f gitea-values.yaml gitea/gitea
   ```

## 卸载

1. 卸载 gitea

   ```bash
   helm uninstall gitea -n gitea
   ```

2. 删除 secret

   ```bash
   kubectl delete secret gitea-tls gitea-secret -n gitea
   ```

3. 删除命名空间

   ```bash
   kubectl delete namespace gitea
   ```

   
