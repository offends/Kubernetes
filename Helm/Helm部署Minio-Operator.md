> 本文作者：丁辉

# Helm部署Minio-Operator

> 文档写于 2024年08月02日 ，因为 Minio-Operator 官方近期在改动 Helm 部署的内容，可能未来就无法使用此文档来部署了，特此记录提示。

## 介绍

MinIO 是一种高性能的分布式对象存储服务，可以在多种环境中部署，包括私有云、公有云以及混合云。MinIO 提供了与 Amazon S3 兼容的 API，这使其成为在各种云原生应用中处理大规模数据的理想选择。在 Kubernetes 上部署 MinIO 时，通常会使用 MinIO Operator 和 MinIO Tenant 来简化和自动化管理过程。



**MinIO Operator** 是一个 Kubernetes 自定义控制器，用于自动化 MinIO 实例的部署和管理。它使用 Kubernetes 自定义资源定义（CRD）来管理 MinIO 集群的生命周期。Operator 模式允许开发者将运维逻辑封装成代码，这样就可以自动处理例如部署、扩展、升级、备份等任务。

**主要特性包括：**

- **自动化部署**：自动化部署 MinIO 集群，包括设置网络、存储、节点分配等。
- **高可用性**：确保部署的 MinIO 集群具有高可用性配置，包括跨区域或跨数据中心的数据复制。
- **缩放和升级**：支持无缝的缩放和升级操作，不中断服务地增加存储容量或更新 MinIO 版本。
- **监控和日志**：与 Kubernetes 监控系统（如 Prometheus）和日志系统（如 Fluentd）集成，提供实时监控和日志收集。



**MinIO Tenant ** 是MinIO 在 Kubernetes 中的部署实例，代表一个逻辑上隔离的 MinIO 存储集群。在 MinIO Operator 的管理下，每个 Tenant 作为一个独立的 MinIO 集群运行，拥有自己的存储、用户和策略管理。

**主要特性包括：**

- **资源隔离**：每个 Tenant 可以配置独立的资源（CPU、内存、存储），保证不同 Tenant 之间的操作不会相互影响。
- **独立管理**：每个 Tenant 都可以独立管理，包括用户权限、存储桶和数据访问策略。
- **安全性**：支持与 Kubernetes 的安全特性集成，如 RBAC、TLS 加密通信和秘密管理。
- **灵活配置**：可以根据需要配置数据的复制和冗余策略，优化性能和成本。



**结合使用 MinIO Operator 和 MinIO Tenant**

将 MinIO Operator 和 MinIO Tenant 结合使用，可以在 Kubernetes 上有效地部署、管理和扩展企业级的对象存储解决方案。Operator 负责管理和维护 MinIO 集群的基础设施，而 Tenant 提供了操作的逻辑隔离和资源封装。这种模式特别适合那些需要在 Kubernetes 环境中部署大规模、高可用性和高安全性对象存储服务的企业和应用。通过自动化的管理和监控，企业可以确保其数据存储服务既高效又稳定，同时减少人工干预和操作错误的风险。

[Github仓库](https://github.com/minio/operator/tree/master)

[官方Minio-Operator部署文档](https://min.io/docs/minio/kubernetes/upstream/operations/install-deploy-manage/deploy-operator-helm.html#overview)	[官方Minio-Tenant部署文档](https://min.io/docs/minio/kubernetes/upstream/operations/install-deploy-manage/deploy-minio-tenant-helm.html#overview)

## 开始部署

|     主机     |  访问地址(示例)   | 对应内部Service地址 |
| :----------: | :---------------: | :-----------------: |
| Minio-Tenant |   minio-hl.com    |    minio-hl:9000    |
| Minio-Tenant | minio-console.com | minio-console:9090  |

1. 添加 Helm 仓库

   ```bash
   helm repo add minio-operator https://operator.min.io/
   helm repo update
   ```

2. 安装 Operator

   ```bash
   helm install minio-operator \
     --namespace minio-operator \
     --create-namespace \
     --set operator.replicaCount=2 \
     minio-operator/operator
   ```

3. 编辑 Tenant values.yaml

   ```bash
   vi minio-tenant-values.yaml
   ```

   内容如下

   ```yaml
   ingress:
     api:
       enabled: true
       tls:
         - hosts:
         - minio-hl.com
         secretName: minio-hl-tls
       host: minio-hl.com
     console:
       enabled: true
       tls:
         - hosts:
         - minio-console.com
         secretName: minio-console-tls
       host: minio-console.com
   
   secrets:
     name: minio-env-configuration
     accessKey: minio 
     secretKey: minio123
     existingSecret:
       name: enabled
     
   tenant:
     name: minio
     storageClassName:
     configuration:
       name: minio-env-configuration
     certificate:
       requestAutoCert: false
     env:
       - name: MINIO_SERVER_URL
         value: "http://minio-hl.com"
       - name: MINIO_STORAGE_CLASS_STANDARD
         value: "EC:4"
     pools:
       - servers: 4
         name: pool-0
         volumesPerServer: 4
         size: 10Gi
   ```

   **变量解释**

   - `MINIO_SERVER_URL`：用于指定MinIO Share功能反馈的URL中的地址。可以通过此地址下载MinIO中的文件。
   - `MINIO_STORAGE_CLASS_STANDARD`：默认值EC:4，此参数可不修改。如果为了提高数据可用性，可以提高EC的值。但是同样会减少实际可用空间。简单来说这就是设置数据冗余的比例。如果需要配置 Pools-Servers为1，则不配置次变量。[官方文档-纠删码基础知识](https://min.io/docs/minio/linux/operations/concepts/erasure-coding.html)

4. 安装 Tenant

   ```bash
   helm install minio-tenant \
     --namespace minio-tenant \
     --create-namespace \
     minio-operator/tenant \
     -f minio-tenant-values.yaml
   ```

5. 访问

   > 地址：minio-console.com
   >
   > 账户密码：minio/minio123

## 卸载

1. 卸载 Minio-Tenant

   ```bash
   helm uninstall minio-tenant -n minio-tenant
   ```

2. 卸载 Minio-Operator

   ```bash
   helm uninstall minio-operator -n minio-operator
   ```

3. 删除 Minio-Tenant 命名空间

   ```bash
   kubectl delete ns minio-tenant
   ```

4. 删除 Minio-Operator 命名空间

   ```bash
   kubectl delete ns minio-operator
   ```

   
