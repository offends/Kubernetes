> 本文作者：丁辉

# Helm部署Mysql

## 介绍

**MySQL是一个流行的开源关系型数据库管理系统（RDBMS），由瑞典公司MySQL AB开发，现属于Oracle公司的一款产品**。它以高性能、易用性和对多种语言的支持而闻名，是构建Web应用和服务的首选数据库之一。

## 开始部署

1. 添加仓库

   ```bash
   helm repo add bitnami https://charts.bitnami.com/bitnami
   helm repo update
   ```

2. 编写 values.yaml 文件

   ```bash
   vi mysql-values.yaml
   ```

   内容如下

   ```yaml
   global:
     storageClass: "" # 指定存储卷, 不指定则需要集群内存在默认的存储卷
   auth:
     rootPassword: "Root123456"
     defaultAuthenticationPlugin: "mysql_native_password"
   # 开启监控
   metrics:
     enabled: true
     serviceMonitor:
       enabled: true
   ```

3. 开始部署

   ```bash
   helm install mysql bitnami/mysql \
     --namespace mysql --create-namespace \
     -f mysql-values.yaml
   ```

## 卸载

```bash
helm uninstall mysql -n mysql
```

