> 本文作者：丁辉

# Helm部署JuiceFS-CSI对接对象存储

## 介绍

**JuiceFS CSI是一个遵循CSI规范的驱动程序，它实现了容器编排系统与JuiceFS文件系统之间的接口，使得Kubernetes集群能够以持久卷的形式提供给Pod使用**。

## 开始部署

> 提前准备 Minio 和 Mysql 数据库

| 服务名 |  服务器地址:端口  | 存储库名 |         账户/密码         |
| :----: | :---------------: | :------: | :-----------------------: |
| Mysql  | 192.168.1.10:3306 | juicefs  |         root:root         |
| Minio  | 192.168.1.20:9000 | juicefs  | ${accessKey}/${secretKey} |

[官方Chart仓库](https://github.com/juicedata/charts/tree/main)	[官方文档](https://juicefs.com/docs/zh/csi/introduction)

1. 添加仓库

   ```bash
   helm repo add juicefs https://juicedata.github.io/charts/
   helm repo update
   ```

2. 编辑 values.yaml 文件

   ```bash
   vi juicefs-values.yaml
   ```

   内容如下

   ```yaml
   # 检查 kubelet 根目录 ps -ef | grep kubelet | grep root-dir
   kubeletDir: /var/lib/kubelet
   
   # 配置存储
   storageClasses:
   - name: juicefs-sc
     enabled: true
     reclaimPolicy: Delete
     allowVolumeExpansion: true
     backend:
       name: "rainbond"
       metaurl: "mysql://root:root@(192.168.1.10:3306)/juicefs"
       storage: "s3"
       # 创建 Access Keys 填写
       accessKey: "${accessKey}"
       secretKey: "${secretKey}"
       bucket: "http://192.168.1.20:9000/juicefs?tls-insecure-skip-verify=true"
       envs: "{TZ: Asia/Shanghai}"
   
   # 关闭面板
   dashboard:
     enabled: false
   
   # 配置镜像加速
   sidecars:
     livenessProbeImage:
       repository: registry.aliyuncs.com/google_containers/livenessprobe
     nodeDriverRegistrarImage:
       repository: registry.aliyuncs.com/google_containers/csi-node-driver-registrar
     csiProvisionerImage:
       repository: registry.aliyuncs.com/google_containers/csi-provisioner
     csiResizerImage:
       repository: registry.aliyuncs.com/google_containers/csi-resizer
   ```

3. 部署

   ```bash
   helm install juicefs juicefs/juicefs-csi-driver \
     --namespace juicefs --create-namespace \
      -f juicefs-values.yaml
   ```

## 卸载

```bash
helm uninstall juicefs -n juicefs
```

## 动态配置

> 如果想加入动态配置可在安装之前编辑此文件添加

```bash
vi juicefs-csi-driver/templates/storageclass.yaml
```

> 此处添加：juicefs/clean-cache: "true"
>

```yaml
apiVersion: storage.k8s.io/v1
...
parameters:
  juicefs/clean-cache: "true"
```

## 问题记录

> /var/lib/juicefs 是 juicefs 的缓存目录，请单独挂载磁盘(在本盘写满后才会清理，所以会导致 / 写满)
