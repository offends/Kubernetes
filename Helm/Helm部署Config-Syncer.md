> 本文作者：丁辉

# Helm部署Config-Syncer

## 介绍

Config-Syncer 是一个用于同步配置文件的工具。它通常用于在分布式系统中保持各个节点的配置文件一致。通过使用 Config-Syncer，您可以确保当一个节点上的配置文件发生更改时，其他节点上的相应文件也会被更新，从而保持整个系统的一致性和稳定性。Config Syncer 可以 保持 ConfigMaps 和 Secrets 在命名空间和集群之间同步。

## 开始部署

[Github仓库](https://github.com/config-syncer/config-syncer)	[Github-Chart仓库](https://github.com/appscode/charts)

1. 添加 Helm 仓库

   ```bash
   helm repo add appscode https://charts.appscode.com/stable/
   helm repo update
   ```

2. 编辑 values.yaml

   ```bash
   vi config-syncer-values.yaml
   ```

   内容如下

   ```yaml
   config:
     # 通过: kubectl config current-context 可获取当前上下文的集群名
     clusterName: local
     # 如果设置，只有来自这个命名空间的 configmaps 和 secrets 会被同步
     configSourceNamespace: ""
     # kubeconfig 文件内容，用于 configmap 和 secret 同步器
     kubeconfigContent: ""
   ```

3. 部署

   ```bash
   helm install config-syncer appscode/kubed \
     -n kubed --create-namespace \
     -f config-syncer-values.yaml
   ```

## 卸载

1. 卸载 config-syncer

   ```bash
   helm uninstall config-syncer -n kubed
   ```

2. 删除命名空间

   ```bash
   kubectl delete namespace kubed
   ```

# 使用

## ConfigMap/Secret

[官方文档](https://config-syncer.com/docs/v0.15.1/guides/config-syncer/intra-cluster/)

> 以 ConfigMap 示例, Secret 同理。
>
> 同步 `default` 命名空间下名为 `demo` 的ConfigMap。

1. 开始同步

   - 同步到所有命名空间

     ```bash
     kubectl annotate configmap demo kubed.appscode.com/sync="" -n default
     ```

   - 同步到指定命名空间

     1. 给命名空间添加标签

        ```bash
        kubectl label namespace kubed app=kubed
        ```

     2. 添加注解

        ```bash
        kubectl annotate configmap demo kubed.appscode.com/sync="app=kubed" -n default --overwrite
        ```

2. 检查同步状况

   ```bash
   kubectl get configmaps --all-namespaces | grep demo
   ```

3. 移除注解

   ```bash
   kubectl annotate configmap demo kubed.appscode.com/sync- -n default
   ```