> 本文作者：丁辉

# Helm部署Docker-Registry

[官方文档](https://distribution.github.io/distribution/)

[Github-Docker-Registry](https://github.com/distribution/distribution)

[Helm-Chart仓库](https://github.com/helm/charts/tree/master/stable/docker-registry)

## 介绍

**Docker Registry 是一个用于存储、管理和分发 Docker 镜像的服务器应用程序**。

## 开始部署

1. 添加仓库

   ```bash
   helm repo add stable https://charts.helm.sh/stable
   helm repo update
   ```

2. 创建命名空间

   ```bash
   kubectl create namespace hub
   ```

3. 使用 Htpasswd 生成镜像仓库账户密码

   1. 安装

      - Centos安装

        ```bash
        BASHyum -y install httpd-tools
        ```

      - Ubuntu安装

        ```bash
        apt-get -y install apache2-utils
        ```

   2. 生成密码

      ```bash
      htpasswd -Bbn admin 123456
      ```

      **参数解释**

      - `-B`: 使用 bcrypt 算法进行密码加密。这是推荐使用的安全加密方式。
      - `-b`: 表示将用户名和密码作为命令行参数提供，而不是通过交互式输入。
      - `-n`: 表示不更新文件，而是将加密后的用户名和密码输出到标准输出（通常是终端或屏幕）。
      - `admin`: 是要创建或更新的用户名。
      - `123456`: 是该用户名的密码。

4. 编写 values.yaml 文件

   ```bash
   vi docker-registry-values.yaml
   ```

   内容如下

   [Docker-Registry配置文件解释](https://gitee.com/offends/Kubernetes/blob/main/%E9%95%9C%E5%83%8F%E4%BB%93%E5%BA%93/Registry/Docker-Registry%E9%85%8D%E7%BD%AE%E6%96%87%E4%BB%B6%E8%A7%A3%E9%87%8A.md)

   ```yaml
   image:
     repository: registry
     tag: latest
     
   # 填入 htpasswd 生成的密码
   secrets:
     htpasswd: "admin:$2y$05$Fx9LJWaWzrgvHRm9wwrBl.V254BIoqnH/KA6wWnOMxMtmRqVbWq4O"
     
   # Docker-Registry配置文件
   configData:
     version: 0.1
     log:
       fields:
         service: registry
     storage:
       delete:
         enabled: true
       cache:
         blobdescriptor: inmemory
       filesystem:
         rootdirectory: /var/lib/registry
     http:
       addr: :5000
       headers:
         X-Content-Type-Options: [nosniff]
         Access-Control-Allow-Origin: ['*']
     health:
       storagedriver:
         enabled: true
         interval: 10s
         threshold: 3
         
   # 设置存储类型: filesystem 或 s3
   storage: filesystem
   persistence:
     enabled: true
     size: 50Gi
     storageClass: # 指定存储卷, 不指定则需要集群内存在默认的存储卷
   ```

5. 创建Nginx证书secret

   > cert为.pem和.crt文件都可以

   ```bash
   kubectl create secret tls registry-tls --key nginx.key --cert nginx.pem -n hub
   ```

6. 部署

   ```bash
   helm install docker-registry stable/docker-registry \
     -f docker-registry-values.yaml \
     --namespace hub
   ```

7. 部署 Ingress 对外访问

   > 因 Helm 官方 stable 仓库在 2022 年就停止维护了, Chart 有些参数版本较老, 索性自己创建 Ingress

   ```bash
   cat <<EOF | kubectl apply -f -
   apiVersion: networking.k8s.io/v1
   kind: Ingress
   metadata:
     name: docker-registry
     namespace: hub
     annotations:
       # 不限制文件上传大小
       nginx.ingress.kubernetes.io/proxy-body-size: "0"
     labels:
       app: docker-registry
       release: docker-registry
   spec:
     rules:
       - host: #域名
         http:
           paths:
             - pathType: Prefix
               backend:
                 service:
                   name: docker-registry
                   port:
                     number: 5000
               path: /
     tls:
       - hosts:
         - #域名
         secretName: registry-tls
   EOF
   ```

## 卸载

1. 卸载 gitea

   ```bash
   helm uninstall docker-registry -n hub
   ```

2. 删除 secret

   ```bash
   kubectl delete secret registry-tls -n hub
   ```

3. 删除命名空间

   ```bash
   kubectl delete namespace hub
   ```

