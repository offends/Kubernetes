> 本文作者：丁辉

# Helm部署Nightingale

## 介绍

**Nightingale 是一个开源的监控工具，主要用于监控和分析电子病历系统（EMR）中的数据**。

## 开始部署

[官方文档](https://flashcat.cloud/docs/)

[项目仓库](https://github.com/ccfos/nightingale)

[Helm仓库地址](https://github.com/flashcatcloud/n9e-helm/tree/master)

1. 克隆 Helm 仓库

   ```bash
   git clone https://github.com/flashcatcloud/n9e-helm.git
   ```

2. 编写 values.yaml

   ```bash
   vi n9e-values.yaml
   ```

   内容如下

   ```yaml
   expose:
     type: clusterIP # 使用 clusterIP
   
   externalURL: https://填写域名 # 改为自己的外部服务访问地址
   
   # 指定存储卷, 不指定则需要集群内存在默认的存储卷
   persistence:
     enabled: true
     persistentVolumeClaim:
       database:
         storageClass: ""
       redis:
         storageClass: ""
       prometheus:
         storageClass: ""
   
   categraf:
     internal:
       docker_socket: unix:///var/run/docker.sock # 如果您的kubernetes运行时是容器或其他，则清空此变量。
       
   n9e:
     internal:
       image:
         repository: flashcatcloud/nightingale
         tag: latest # 使用最新版镜像
   ```

3. 部署

   ```bash
   helm install nightingale ./n9e-helm -n monitor -f n9e-values.yaml --create-namespace
   ```

4. 创建 Nginx 证书 secret

   > cert为.pem和.crt文件都可以

   ```bash
   kubectl create secret tls n9e-tls --key nginx.key --cert nginx.pem -n monitor
   ```

5. 编写 ingress 文件

   ```bash
   vi n9e-ingress.yaml
   ```

   内容如下

   ```yaml
   apiVersion: networking.k8s.io/v1
   kind: Ingress
   metadata:
     name: n9e-ingress
     namespace: monitor
   spec:
     ingressClassName: nginx
     rules:
       - host: # 域名
         http:
           paths:
             - pathType: Prefix
               backend:
                 service:
                   name: nightingale-center
                   port:
                     number: 80
               path: /
     tls:
       - hosts:
         - # 域名
         secretName: n9e-tls
   ```

6. 部署 ingress

   ```bash
   kubectl apply -f n9e-ingress.yaml
   ```

7. 访问验证

   - 访问地址：https://hello.n9e.info
   - 账户密码：root/root.2020

## 卸载

1. 删除 ingress

   ```bash
   kubectl delete -f n9e-ingress.yaml
   ```

2. 卸载 nightingale

   ```bash
   helm uninstall nightingale -n monitor
   ```

3. 删除 secret

   ```bash
   kubectl delete secret n9e-tls -n monitor
   ```

4. 删除命名空间

   ```bash
   kubectl delete namespace monitor
   ```

# 开始使用

添加自带的 prometheus 数据源

登录系统后点击 > 系统配置 > 数据源 > 添加

- 数据源名称

  ```bash
  nightingale-prometheus
  ```

- URL

  ```bash
  http://nightingale-prometheus:9090
  ```

# 如何配置外部数据库

## 使用外部数据库

> 仅为示例

更改 values.yaml 内 database 配置

```bash
vi n9e-values.yaml
```

内容如下

```yml
database:
  type: external # 改为 external
  external:
    host: "192.168.1.10"
    port: "3306"
    name: "n9e_v6"
    username: "root"
    password: "root"
    sslmode: "disable"
```

## 配置外部 Reids 启动

> 指定 Reids 运行模式为独立模式或哨兵模式
>
> 仅为示例

更改 values.yaml 内 redis 配置

```bash
vi n9e-values.yaml
```

内容如下

```yml
redis:
  type: external
  external:
    addr: "192.168.1.10:6379"
    sentinelMasterSet: ""
    password: ""
    mode: "standalone" # standalone/sentinel
```

# 问题记录

- 部署后 prometheus 无法抓取 n9e 监控目标

  原因是因为：

  1. prometheus 配置 svc 地址错误
  2. helm 模版格式错误

  解决方法：

  [本人提交记录](https://github.com/flashcatcloud/n9e-helm/pull/109)
