> 本文作者：丁辉

# Helm部署Drone

[官方文档](https://docs.drone.io/)

[官方中文文档](https://drone.cool/)

## 介绍

**Drone 是一个开源的持续集成和持续部署（CI/CD）平台，广泛用于自动化代码构建、测试和发布流程**。

## 开始部署

1. 添加 Drone Helm Chart 存储库

   ```bash
   helm repo add drone https://charts.drone.io
   helm repo update
   ```

2. 创建命名空间

   ```bash
   kubectl create namespace drone
   ```

3. 部署 Postgres

   ```bash
   kubectl apply -f https://gitee.com/offends/Kubernetes/raw/main/File/Yaml/drone-postgres.yaml
   ```

4. 生成密钥

   ```bash
   openssl rand -hex 16
   ```

5. 生成 Secret

   > 对接 Gitea 参数替换如下：
   >
   > DRONE_GITEA_CLIENT_ID
   >
   > DRONE_GITEA_CLIENT_SECRET

   ```bash
   kubectl create secret generic drone-secret \
     --from-literal=DRONE_RPC_SECRET=填入密钥 \
     --from-literal=DRONE_GITHUB_CLIENT_ID=填入Github-ID \
     --from-literal=DRONE_GITHUB_CLIENT_SECRET=填入Github-SECRET \
     --from-literal=DRONE_GIT_USERNAME=配置Github用户名 \
     --from-literal=DRONE_GIT_PASSWORD=配置Github密码 \
     --from-literal=DRONE_USER_CREATE=username:填入管理员用户名,admin:true \
     -n drone
   ```

   **参数解释**

   | 参数                          | 描述                                                         |
   | ----------------------------- | ------------------------------------------------------------ |
   | `DRONE_RPC_SECRET=`           | 将名为`DRONE_RPC_SECRET`的密钥添加到Secret中，用于Drone CI/CD工具的RPC通信和验证。 |
   | `DRONE_GITHUB_CLIENT_ID=`     | 将GitHub OAuth应用程序的客户端ID添加到Secret中。             |
   | `DRONE_GITHUB_CLIENT_SECRET=` | 将GitHub OAuth应用程序的客户端密钥添加到Secret中。           |
   | `DRONE_GIT_USERNAME=`         | 将GitHub用户名添加到Secret中，用于访问GitHub仓库。           |
   | `DRONE_GIT_PASSWORD=`         | 将GitHub密码添加到Secret中，用于访问GitHub仓库。             |
   | `DRONE_USER_CREATE=username:` | 指定在Drone启动时创建的用户信息，包括用户名和角色（管理员）。 |

6. 编写模版文件

   > 对接 Gitea 参数替换如下：
   >
   > DRONE_GITEA_SERVER

   ```bash
   vi drone-values.yaml
   ```

   内容如下

   ```yaml
   # 开启 ingress 对外访问
   ingress:
     enabled: true
     className: "" # 指定 ingress 控制器, 不指定则需要集群内存在默认的 ingress 控制器
     hosts:
       - host: # 域名
         paths:
           - path: /
             pathType: Prefix
     tls:
       - secretName: drone-tls
         hosts:
           - # 域名
     
   env:
     DRONE_GITHUB_SERVER: https://github.com #仓库地址
     DRONE_SERVER_HOST: #域名
     DRONE_SERVER_PROTO: https
     DRONE_DATABASE_DRIVER: postgres
     DRONE_DATABASE_DATASOURCE: postgres://postgres:postgres@drone-db:5432/drone?sslmode=disable
   extraSecretNamesForEnvFrom:
     - drone-secret
   persistentVolume:
     enabled: false
   ```

   > 其他参数
   >
   > ```bash
   > # oauth会验证gitlab证书，如果验证不过，需要打开
   > DRONE_GITLAB_SKIP_VERIFY: true
   > ```

7. 安装

   ```bash
   helm install drone drone/drone -f drone-values.yaml -n drone
   ```

## 卸载

1. 卸载 drone

   ```bash
   helm uninstall drone -n drone
   ```

2. 删除 secret

   ```bash
   kubectl delete secret drone-secret -n drone
   ```

3. 删除命名空间

   ```bash
   kubectl delete namespace drone
   ```

