> 本文作者：丁辉

# Helm部署Drone-Runner-Kube

## 介绍

**Drone-Runner-Kube 是一个插件，它允许 Drone 在 Kubernetes 集群中运行构建步骤**。这个插件利用 Kubernetes 的资源管理能力，为 Drone 提供了在容器化环境中执行构建、测试和部署的能力。

## 开始部署

1. 添加 Drone Helm Chart 存储库

   ```bash
   helm repo add drone https://charts.drone.io
   helm repo update
   ```

2. 创建命名空间

   ```bash
   kubectl create namespace drone
   ```

3. 创建 secret 文件

   ```bash
   kubectl create secret generic runner-drone-secret \
     --from-literal=DRONE_RUNNER_CAPACITY=2 \
     --from-literal=DRONE_RUNNER_NAME=runner \
     --from-literal=DRONE_RPC_SECRET=填入密钥 \
     --from-literal=DRONE_RPC_HOST=填入drone域名 \
     --from-literal=DRONE_RPC_PROTO=https \
     -n drone
   ```

   > Runner 添加标签
   >
   > ```bash
   > --from-literal=DRONE_RUNNER_LABELS=标签:值
   > ```

4. 编写模版文件

   ```bash
   vi drone-runner-kube-values.yaml
   ```

   内容如下

   ```yaml
   extraSecretNamesForEnvFrom:
     - runner-drone-secret
   rbac:
     buildNamespaces:
       - drone
   env:
     DRONE_NAMESPACE_DEFAULT: drone
   ```

5. 启动

   ```bash
   helm install drone-runner-kube drone/drone-runner-kube -f drone-runner-kube-values.yaml -n drone
   ```

## 卸载

1. 卸载 drone-runner-kube

   ```bash
   helm uninstall drone-runner-kube -n drone
   ```

2. 删除 secret

   ```bash
   kubectl delete secret runner-drone-secret -n drone
   ```

3. 删除命名空间

   ```bash
   kubectl delete namespace drone
   ```
