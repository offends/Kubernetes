> 本文作者：丁辉

# Helm 部署 Minio

## 介绍

**Minio 是一个高性能、开源的云存储和对象存储服务器，适用于任何规模的应用**。

## 开始部署

[官方仓库](https://github.com/minio/minio/tree/master/helm/minio)

1. 添加仓库

   ```bash
   helm repo add minio https://charts.min.io/
   helm repo update
   ```

2. 创建命名空间

   ```bash
   kubectl create namespace minio
   ```

3. 编写 Yaml 文件

   ```bash
   vi minio-values.yaml
   ```

   内容如下

   ```yaml
   # 开启 ingress 对外访问
   consoleIngress:
     enabled: true
     ingressClassName: # 指定 ingress 控制器, 不指定则需要集群内存在默认的 ingress 控制器
     annotations:
       nginx.ingress.kubernetes.io/proxy-body-size: "1024m" # 调整文件上传允许传输大小
     path: /
     hosts:
       - # 域名
     tls:
      - secretName: minio-tls
        hosts:
          - # 域名
   
   # 配置镜像加速
   image:
     repository: quay.io/minio/minio
     tag: latest
     pullPolicy: IfNotPresent
     
   # 配置 Minio 用户密码
   rootUser: "填写账户"
   rootPassword: "填写密码"
   replicas: 1
   
   # 开启持久化存储
   persistence:
     enabled: true
     storageClass: "" # 指定存储卷, 不指定则需要集群内存在默认的存储卷
   
   # 独立部署模式 
   mode: standalone
   resources:
     requests:
       memory: 512Mi
       
   # 指定分享访问地址
   environment:
     MINIO_SERVER_URL: "https://域名:9000"
   ```

4. 创建 Nginx 证书 secret

   > cert为.pem和.crt文件都可以

   ```bash
   kubectl create secret tls minio-tls --key nginx.key --cert nginx.pem -n minio
   ```

5. 安装

   ```bash
   helm install --namespace minio minio minio/minio -f minio-values.yaml
   ```

6. 部署 Nginx 代理

   ```bash
   vi default.conf
   ```

   内容如下

   ```nginx
   server {
       listen       9000 ssl;
       server_name  localhost; # 这里替换自己的域名
   
       client_max_body_size 1024m; # 限制上传文件大小
   
       ssl_certificate     /etc/nginx/conf.d/cert/tls.crt; 
       ssl_certificate_key /etc/nginx/conf.d/cert/tls.key;
   
       location / {
                       proxy_set_header    X-FORWARDED-FOR $remote_addr;
                       proxy_set_header    X-FORWARDED-PROTO $scheme;
                       proxy_set_header    Host   $http_host;
                       proxy_pass          http://minio:9000;
       }
   }
   ```

7. 编辑 Dockerfile

   ```bash
   vi Dockerfile
   ```

   内容如下

   ```dockerfile
   FROM nginx:alpine-slim
   
   COPY ./default.conf /etc/nginx/conf.d/default.conf
   
   EXPOSE 9000
   ```

8. 构建镜像

   ```bash
   docker build -t minio-gateway:v1.0 .
   ```

9. 查看 Minio SVC IP

   ```bash
   kubectl get svc -n minio | grep 9000 | awk '{print $3}'
   ```

10. 编辑 Yaml

    ```bash
    vi minio-gateway.yaml
    ```

    内容如下

    ```yaml
    apiVersion: apps/v1
    kind: Deployment
    metadata:
      namespace: minio
      name: minio-gateway
      labels:
        app: minio-gateway
    spec:
      selector:
        matchLabels:
          app: minio-gateway
      template:
        metadata:
          labels:
            app: minio-gateway
        spec:
          hostNetwork: true
          hostAliases:
          - ip: "" #填入 Minio SVC IP
            hostnames:
            - "minio"
          containers:
            - name: minio-gateway
              image: minio-gateway:v1.0
              imagePullPolicy: IfNotPresent
              ports:
                - containerPort: 9000
                  protocol: TCP
              readinessProbe:
                failureThreshold: 3
                initialDelaySeconds: 5
                periodSeconds: 3
                successThreshold: 1
                tcpSocket:
                  port: 9000
                timeoutSeconds: 10
              resources:
                limits:
                  memory: 128Mi
              volumeMounts:
                - name: ssl
                  mountPath: "/etc/nginx/conf.d/cert/"
          volumes:
            - name: ssl
              secret:
                secretName: minio-ssl
    ```

11. 部署

    ```bash
    kubectl apply -f minio-gateway.yaml
    ```

## 卸载

1. 卸载网关

   ```bash
   kubectl delete -f minio-gateway.yaml
   ```

2. 卸载 minio

   ```bash
   helm uninstall minio -n minio
   ```

3. 删除 secret

   ```bash
   kubectl delete secret minio-tls -n minio
   ```

4. 删除命名空间

   ```bash
   kubectl delete namespace minio
   ```

