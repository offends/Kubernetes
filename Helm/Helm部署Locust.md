> 本文作者：丁辉

# Helm部署Locust

[官方文档](https://locust.io/)

[Github仓库](https://github.com/locustio/locust)

[官方推荐的HelmChart仓库](https://github.com/deliveryhero/helm-charts/tree/master/stable/locust)

## 介绍

Locust 是一个开源的性能测试工具，它主要用于测试网站或网络应用程序的负载能力和性能。与其他性能测试工具不同，Locust 使用 Python 语言进行测试脚本的编写，这使得它更灵活和易于使用。

## 开始部署

1. 添加 Helm 仓库

   ```bash
   helm repo add deliveryhero https://charts.deliveryhero.io/
   helm repo update
   ```

2. 编写 values.yaml 文件

   ```bash
   vi locust-values.yaml
   ```

   内容如下

   ```yaml
   ingress:
     enabled: true
     className: ""
     hosts:
       - host: # 域名
         pathType: ImplementationSpecific
         path: /
     tls:
       - secretName: locust-tls
         hosts:
           - # 域名
   
   image:
     repository: locustio/locust
     tag: latest
   ```

3. 安装

   ```bash
   helm install locust \
     --namespace locust \
     --create-namespace \
     deliveryhero/locust \
     -f locust-values.yaml
   ```

## 卸载

1. 卸载 Locust

   ```bash
   helm uninstall locust -n locust
   ```

2. 删除命名空间

   ```bash
   kubectl delete ns locust
   ```

   