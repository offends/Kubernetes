> 本文作者：丁辉

# Helm部署Coder

[Github仓库](https://github.com/coder/code-server)

## 介绍

**code-server是一个将Visual Studio Code（VS Code）部署到服务器上，使用户能够通过浏览器进行远程代码编辑和开发的项目**。它不是官方微软的产品，但提供了类似于官方vscode.dev的网页版体验。

## 开始部署

1. 拉取仓库文件到本地

   ```bash
   git clone https://github.com/coder/code-server && cd code-server/ci
   ```

2. 创建命名空间

   ```bash
   kubectl create namespace code-server
   ```

3. 编辑模版文件

   ```bash
   vi code-server-values.yaml
   ```

   内容如下

   ```yaml
   persistence:
     enabled: true
     storageClass: "" # 指定存储卷, 不指定则需要集群内存在默认的存储卷
     accessMode: ReadWriteOnce
     
   ingress:
     enabled: true
     hosts:
       - host: # 域名
         paths:
           - /
     ingressClassName: "" # 指定 ingress 控制器, 不指定则需要集群内存在默认的 ingress 控制器
     tls:
       - secretName: code-server-tls
         hosts:
           - # 域名
   ```

4. 创建Nginx证书secret

   > cert为.pem和.crt文件都可以

   ```bash
   kubectl create secret tls code-server-tls --key nginx.key --cert nginx.pem -n code-server
   ```

5. 安装

   ```bash
   helm install code-server ./helm-chart --namespace code-server -f code-server-values.yaml
   ```

6. 查看密码登录

   ```bash
   echo $(kubectl get secret --namespace code-server code-server -o jsonpath="{.data.password}" | base64 --decode)
   ```

## 卸载

1. 卸载 code-server

   ```bash
   helm uninstall code-server -n code-server
   ```

2. 删除 secret

   ```bash
   kubectl delete secret code-server-tls -n code-server
   ```

3. 删除命名空间

   ```bash
   kubectl delete namespace code-server
   ```
