#!/bin/bash

#############################################################################################
# 用途: 初始化文档脚本
# 作者: 丁辉
# 编写时间: 2024-07-11
#############################################################################################

# 定义函数信息
RED='\033[0;31m'
NC='\033[0m'
GREEN='\033[32m'
YELLOW='\033[33m'
TIME="+%Y-%m-%d %H:%M:%S"

function SEND_INFO() {
    info=$1
    echo -e "${GREEN}$(date "$TIME") INFO: $info${NC}"
}
function SEND_WARN() {
    warn=$1
    echo -e "${YELLOW}$(date "$TIME") WARN: $warn${NC}"
}
function SEND_ERROR() {
    error=$1
    echo -e "${RED}$(date "$TIME") ERROR: $error${NC}"
}

# 整理文档
function Clean_File() {
    SEND_INFO "------------ 开始清理仓库内无关文件 ------------"
    SEND_INFO "------------ Git 文件清理 ------------"
    rm -rf .git
    rm -rf .gitignore
    rm -rf .drone.yml
    rm -rf ./README.md
    SEND_INFO "------------ 特殊文件/目录清理 ------------"
    find . -type f ! \( -name "*.md" -o -name "builder.sh" \) -exec rm {} +
    SEND_INFO "------------ 开始初始化文档 ------------"
    SEND_INFO "------------ README.md 文件初始化 ------------"
    # 清理第一行带有 * 号文件
    for i in $(find ./ -type f -name "*.md"); do
        if head -n 1 ${i} | grep -q '^*'; then
            rm ${i}
        fi
    done
    # 更改剩下的文件名称
    for i in $(find ./ -type f -name "README.md"); do
        path=$(echo $i | awk -F'/README.md' '{print $1}')
        name=$(sed -n '3p' $i | awk '{print $2}')
        mv $i ${path}/${name}.md
    done
}

# 初始化 MD 文件
function Init_Docs() {
    SEND_INFO "------------ 初始化文档正文 ------------"    
    for i in $(find ./ -type f -name "*.md" | sed 's|^./||'); do
        DNAME=$(echo $i | awk -F '/' '{print "Kubernetes-"$1}')
        MDNAME=$(echo $i | awk -F '/' '{print $(NF)}' | awk -F '.' '{print $1}')
    sed -i "1i---\n\
title:\n\
keywords: admin-template,vue,element,后台模板\n\
cover: /img/block.png\n\
sticky:\n\
banner:\n\
  type: img\n\
  bgurl: /img/demo.png\n\
  bannerText: \n\
author: Offends\n\
categories:\n\
toc: true\n\
single_column: true\n\
---\n" $i
    sed -i "s#title:#title: $MDNAME#g" $i
    sed -i "s#categories:#categories: $DNAME#g" $i
    done
}

function Replace_Url() {
    SEND_INFO "------------ 开始替换 Gitee 链接为 Blog 链接 ------------"
    for i in $(grep -r 'https://gitee.com/offends/' . | awk -F ':' '{print $1}'); do
        sed -i 's|https://gitee.com/offends/\(.*\)blob/main/\(.*\.md\)|https://blog.offends.cn/\1\2|g' $i || true
        sed -i 's|\.md$|.html|g' $i || true
        sed -i 's|\.md)$|.html)|g' $i || true
    done
}

function All() {
    SEND_INFO "开始构建......"
    Clean_File
    Init_Docs
    Replace_Url
    SEND_INFO "构建完成"
}

All