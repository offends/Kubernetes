> 本文作者：丁辉

# Ingress问题记录

> 重启 ingress 控制器后报错

```bash
-------------------------------------------------------------------------------
NGINX Ingress controller
  Release:       v1.10.1
  Build:         4fb5aac1dd3669daa3a14d9de3e3cdb371b4c518
  Repository:    https://github.com/kubernetes/ingress-nginx
  nginx version: nginx/1.25.3

-------------------------------------------------------------------------------

F0504 11:14:47.852320       7 main.go:64] port 80 is already in use. Please check the flag --http-port
```

**解决方法**

> 允许从端口 1 开始的所有端口都可以被非特权用户使用

- 临时解决

  ```bash
  sysctl -w net.ipv4.ip_unprivileged_port_start=1
  ```

- 永久

  ```bash
  echo 'net.ipv4.ip_unprivileged_port_start = 1' > /etc/sysctl.d/ip_unprivileged_port_start.conf
  ```

**总结**

这根本就是权限问题, 而官方的报错存在误导性。很让人生气啊, 硬生生看了一下午。