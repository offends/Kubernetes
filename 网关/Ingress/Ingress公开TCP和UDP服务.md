> 本文作者：丁辉

# Ingress公开TCP和UDP服务

[官方文档](https://kubernetes.github.io/ingress-nginx/user-guide/exposing-tcp-udp-services/)

1. 添加 Ingress 启动参数

   ```yaml
   - --tcp-services-configmap=$(POD_NAMESPACE)/ingress-nginx-tcp
   - --udp-services-configmap=$(POD_NAMESPACE)/ingress-nginx-udp
   ```

2. 创建 Configmap

   > 格式为：<service port>:<namespace/service name>:[PROXY]:[PROXY]

   - TCP

     ```yaml
     cat <<EOF | kubectl apply -f -
     apiVersion: v1
     kind: ConfigMap
     metadata:
       name: ingress-nginx-tcp
       namespace: default
     data:
       9000: "default/example-go:8080"
     EOF
     ```

   - UDP

     ```yaml
     cat <<EOF | kubectl apply -f -
     apiVersion: v1
     kind: ConfigMap
     metadata:
       name: ingress-nginx-udp
       namespace: default
     data:
       53: "kube-system/kube-dns:53"
     EOF
     ```

3. 创建完 Configmap 后即可测试访问对应端口