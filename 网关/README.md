> 本文作者：丁辉

# 网关介绍

> Ingress和Gateway API都是Kubernetes集群中用于处理外部流量进入集群的资源类型，但它们有不同的设计目标和功能特点。

## Ingress

**Ingress** 是Kubernetes中用来管理进入集群的HTTP和HTTPS流量的资源。它通过定义一些规则来控制服务如何对外暴露，并且通常与Ingress控制器一起使用，后者负责实际处理和路由流量。

### 主要特点：

1. **规则定义**：通过定义主机名、路径等规则来路由流量到不同的服务。
2. **TLS支持**：可以配置TLS证书以支持HTTPS。
3. **负载均衡**：提供基础的负载均衡功能。
4. **扩展性**：通过注解支持一些扩展功能，但这些功能通常是特定于某个Ingress控制器的。

### 常见Ingress控制器：

- NGINX Ingress Controller
- Traefik
- HAProxy
- Kong

## Gateway API

**Gateway API** 是Kubernetes社区中一个相对较新的项目，它提供了一组面向现代服务的标准化API，旨在替代和扩展现有的Ingress API。Gateway API更加灵活和强大，能够支持更复杂的路由和流量管理需求。

### 主要特点：

1. **分离角色**：将Gateway（流量入口）与Route（路由规则）分开，允许不同团队分别管理。
2. **增强的路由能力**：支持HTTP、TCP、TLS等多种协议，并且能够定义更复杂的路由规则。
3. **统一的配置模型**：提供了一致的配置模型，便于扩展和定制。
4. **多租户支持**：通过支持Namespace和GatewayClass，方便在多租户环境中使用。

### 主要资源：

- **Gateway**：定义网关的配置，如监听端口、协议等。
- **GatewayClass**：定义网关的实现类型。
- **HTTPRoute**、**TCPRoute**、**TLSRoute**等：定义具体的路由规则。

## Kubernetes官方对Ingress和Gateway API的态度

### **Ingress API**

- **稳定性**：Ingress API已经在Kubernetes中稳定运行多年，是许多集群中用于管理外部流量的关键组件。
- **维护**：虽然Ingress API依然会继续维护，但其功能和扩展性已经接近饱和，未来的开发重点将更多地转向Gateway API。

### **Gateway API**

- **现代化设计**：Gateway API是一个更现代化的流量管理解决方案，设计上更加模块化、灵活，并且具备更强的可扩展性。
- **功能丰富**：相比Ingress API，Gateway API支持更多的协议（如TCP、UDP、TLS等），并且可以定义更复杂的路由和流量管理规则。
- **未来发展方向**：Kubernetes社区将把主要的开发和改进精力投入到Gateway API上，以满足未来更加复杂和多样化的流量管理需求。
