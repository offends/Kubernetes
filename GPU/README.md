> 本文作者：丁辉

# GPU的使用

> 相关文档地址

- [Linux下载并安装GPU驱动](https://gitee.com/offends/Linux/blob/main/Docs/Linux%E4%B8%8B%E8%BD%BD%E5%B9%B6%E5%AE%89%E8%A3%85GPU%E9%A9%B1%E5%8A%A8.md)
- [GPU容器化基础环境准备](https://gitee.com/offends/Kubernetes/blob/main/Docker/Docs/Docker%E4%BD%BF%E7%94%A8GPU.md)
- [Helm部署NVIDIA-K8s-Device-Plugin插件](https://gitee.com/offends/Kubernetes/blob/main/Helm/Helm%E9%83%A8%E7%BD%B2NVIDIA-K8s-Device-Plugin.md)
- [Kubernetes-NVIDIA之阿里云插件Gpushare-Device-Plugin](https://gitee.com/offends/Kubernetes/blob/main/GPU/Kubernetes-NVIDIA%E4%B9%8B%E9%98%BF%E9%87%8C%E4%BA%91%E6%8F%92%E4%BB%B6Gpushare-Device-Plugin.md)
