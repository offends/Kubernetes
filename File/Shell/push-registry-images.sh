#!/bin/bash

#############################################################################################
# 用途: 上传 Registry 镜像脚本
# 作者: 丁辉
# 更新时间: 2024-06-29
#############################################################################################

# 镜像仓库基础信息配置
REGISTRY_URL=registry.cn-hangzhou.aliyuncs.com
REGISTRY_USER=admin
REGISTRY_PASSWD=password
PULL_IMAGE=true # 是否开启拉取最新镜像

# 定义函数信息
RED='\033[0;31m'
NC='\033[0m'
GREEN='\033[32m'
YELLOW='\033[33m'
TIME="+%Y-%m-%d %H:%M:%S"

function SEND_INFO() {
    info=$1
    echo -e "${GREEN}$(date "$TIME") INFO: $info${NC}"
}
function SEND_WARN() {
    warn=$1
    echo -e "${YELLOW}$(date "$TIME") WARN: $warn${NC}"
}
function SEND_ERROR() {
    error=$1
    echo -e "${RED}$(date "$TIME") ERROR: $error${NC}"
}
if [ -z "$1" ]; then
    echo "请输入要上传的镜像名称,格式为: push-registry-images.sh NAME:TAG NAME:TAG"
    exit 1
fi

SEND_INFO "正在登录镜像仓库 $REGISTRY_URL"
docker login $REGISTRY_URL -u $REGISTRY_USER -p $REGISTRY_PASSWD > /dev/null 2>&1
if [ $? -eq 0 ]; then
    SEND_INFO "登录镜像仓库成功"
else
    SEND_ERROR "登录镜像仓库失败"
    exit 1
fi

IMAGES_NAME=$@

for IMAGE_NAME in ${IMAGES_NAME[@]}; do
    if [[ $PULL_IMAGE == "true" ]]; then
        SEND_INFO "正在拉取镜像: $IMAGE_NAME"
        docker pull $IMAGE_NAME
        if [ $? -ne 0 ]; then
            SEND_ERROR "拉取镜像 $IMAGE_NAME 失败"
            exit 1
        fi
    fi
    docker tag $IMAGE_NAME $REGISTRY_URL/$IMAGE_NAME
    docker push $REGISTRY_URL/$IMAGE_NAME
    if [ $? -eq 0 ]; then
        SEND_INFO "上传镜像 $IMAGE_NAME 成功"
    else
        SEND_ERROR "上传镜像 $IMAGE_NAME 失败"
    fi
done