#!/bin/bash

#############################################################################################
# 用途: 恢复 rkestate 状态文件脚本
# 作者: 丁辉
# 更新时间: 2024-03-27
#############################################################################################

# 检测当前是否为 Master 节点
if [ ! -f /etc/kubernetes/ssl/kubecfg-kube-node.yaml ]; then
  echo "未检测到 /etc/kubernetes/ssl/kubecfg-kube-node.yaml 文件, 请登录 Master 节点执行脚本"
  exit 1
fi

# 找回文件
docker run --rm --net=host \
-v $(docker inspect kubelet --format '{{ range .Mounts }}{{ if eq .Destination "/etc/kubernetes" }}{{ .Source }}{{ end }}{{ end }}')/ssl:/etc/kubernetes/ssl:ro \
--entrypoint bash \
rancher/rancher-agent:v2.2.2 \
-c 'kubectl --kubeconfig /etc/kubernetes/ssl/kubecfg-kube-node.yaml get configmap \
-n kube-system full-cluster-state -o json | jq -r .data.\"full-cluster-state\" | jq -r .' > cluster.rkestate