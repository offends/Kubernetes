> 本文作者：丁辉

# Ceph创建文件系统

## 开始创建

[官方文档](https://docs.ceph.com/en/reef/cephfs/createfs/)

1. 创建存储池

   > 一个 ceph 文件系统需要至少两个 RADOS 存储池，一个用于存储数据，一个用于存储元数据

   ```bash
   ceph osd pool create cephfs_data
   ceph osd pool create cephfs_metadata
   ```

   检查

   ```bash
   ceph osd pool ls
   ```

2. 创建 cephfs

   ```bash
   ceph fs new cephfs cephfs_metadata cephfs_data
   ```

   检查

   ```bash
   ceph fs ls
   ```

3. 设置可用的元数据服务器 MDS 数量

   查看 MDS 状态

   ```bash
   ceph mds stat
   ```

   > 结果为 `cephfs-1/1/1 up  {0=rainbond=up:active}`

   设置 MDS 数量

   ```bash
   ceph fs set cephfs max_mds 1
   ```

4. 配置认证用户 cephfs

   ```bash
   ceph auth get-or-create client.cephfs mon 'allow r' mds 'allow rw' osd 'allow rw pool=cephfs_data, allow rw pool=cephfs_metadata'
   ```

5. 查看 cephfs 用户密钥

   ```bash
   ceph auth get client.cephfs
   ```

6. 本地挂载测试

   请查看此篇文章	[Ceph挂载文件系统](https://gitee.com/offends/Kubernetes/blob/main/%E5%AD%98%E5%82%A8/Ceph/Ceph%E6%8C%82%E8%BD%BD%E6%96%87%E4%BB%B6%E7%B3%BB%E7%BB%9F.md)	尝试挂载
