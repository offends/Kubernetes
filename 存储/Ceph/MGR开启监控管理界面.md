> 本文作者：丁辉

# MGR开启监控管理界面

1. 开启 dashboard

   ```bash
   docker exec ceph-mgr ceph mgr module enable dashboard
   ```

2. 创建证书

   ```bash
   docker exec ceph-mgr ceph dashboard create-self-signed-cert
   ```

3. 创建登陆用户名和密码

   ```bash
   docker exec ceph-mgr sh -c 'echo "admin" > /password.txt'
   docker exec ceph-mgr ceph dashboard set-login-credentials admin -i /password.txt
   ```

4. 配置外部访问端口

   ```bash
   docker exec ceph-mgr ceph config set mgr mgr/dashboard/server_port 7000
   ```

5. 配置外部访问

   ```bash
   docker exec ceph-mgr ceph config set mgr mgr/dashboard/server_addr 192.168.1.10
   ```

6. 关闭 HTTPS

   ```bash
   docker exec ceph-mgr ceph config set mgr mgr/dashboard/ssl false
   ```

7. 重启 Mgr

   ```bash
   docker exec ceph-mgr ceph mgr module disable dashboard
   docker exec ceph-mgr ceph mgr module enable dashboard
   ```

访问地址: `http://127.0.0.1:7000/`

账户密码:  `admin/admin`