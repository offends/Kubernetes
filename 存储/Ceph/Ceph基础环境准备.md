> 本文作者：丁辉

# Ceph基础环境准备

|  节点名称   |      IP      |
| :---------: | :----------: |
| ceph-node-1 | 192.168.1.10 |
| ceph-node-2 | 192.168.1.20 |
| ceph-node-3 | 192.168.1.30 |

1. 所有节点时间同步(必做)

   [请查看此文章](https://gitee.com/offends/Linux/blob/main/Docs/NTP%E6%97%B6%E9%97%B4%E5%90%8C%E6%AD%A5.md)

2. 更改主机名

   - ceph-node-1

     ```bash
     hostnamectl set-hostname ceph-node-1 && bash
     ```

   - ceph-node-2

     ```bash
     hostnamectl set-hostname ceph-node-2 && bash
     ```

   - ceph-node-3

     ```bash
     hostnamectl set-hostname ceph-node-3 && bash
     ```

3. 配置 Hosts 文件

   ```bash
   cat >> /etc/hosts <<EOF
   192.168.1.10 ceph-node-1
   192.168.1.20 ceph-node-2
   192.168.1.30 ceph-node-3
   EOF
   ```

4. 禁用 selinux

   ```bash
   sudo setenforce 0
   sudo sed -i 's/^SELINUX=enforcing$/SELINUX=disabled/' /etc/selinux/config
   ```

   或如果启用了 SELinux, 请运行以下命令

   ```bash
   mkdir {/etc/ceph,/var/lib/ceph}
   chcon -Rt svirt_sandbox_file_t /etc/ceph
   chcon -Rt svirt_sandbox_file_t /var/lib/ceph
   ```

5. 关闭防火墙

   ```bash
   systemctl stop firewalld
   systemctl disable firewalld
   ```

6. 销毁磁盘上的分区表(用于清理旧盘)

   > 如果遇到设备资源繁忙可查看此文档	[设备或资源繁忙问题解决](https://gitee.com/offends/Linux/blob/main/%E5%AD%98%E5%82%A8/%E9%97%AE%E9%A2%98%E8%AE%B0%E5%BD%95/%E8%AE%BE%E5%A4%87%E6%88%96%E8%B5%84%E6%BA%90%E7%B9%81%E5%BF%99.md)

   - 官方清理方法

     ```bash
     docker run --rm --privileged=true \
     -v /dev/:/dev/ \
     -e OSD_DEVICE=/dev/sdb \
     quay.io/ceph/daemon:latest zap_device
     ```

   - [磁盘清理](https://gitee.com/offends/Linux/blob/main/%E5%AD%98%E5%82%A8/%E7%A3%81%E7%9B%98%E6%B8%85%E7%90%86.md)

   - 下载脚本清理

     ```bash
     wget https://gitee.com/offends/Kubernetes/raw/main/File/Shell/clean-disk-ceph.sh
     ```

     > 通过修改 `DISK=""` 字段实现选择磁盘

7. 配置快捷命令

   ```bash
   echo 'alias ceph="docker exec ceph-mon ceph"' >> /etc/profile
   source /etc/profile
   ```
