> 本文作者：丁辉

# Ceph挂载文件系统

> 本文拿默认 cephfs 举例

1. 本地安装 ceph 客户端

   - Centos

     ```bash
     yum install -y ceph-common
     ```

   - Ubuntu

     ```bash
     apt install -y ceph-common
     ```
     

2. 查看 cephfs 用户密钥

   ```bash
   ceph auth get client.cephfs
   ```

3. 挂载文件系统到本地 /mnt/ 下测试

   ```bash
   mount -t ceph ceph-node-1:6789:/ /mnt/ -o name=cephfs,secret=AQAHjRdmdrPAAxAAuCtGNkM/W5XAVU2rWkUz+w==
   ```