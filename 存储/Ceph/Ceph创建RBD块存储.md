> 本文作者：丁辉

# Ceph创建RBD块存储

## 开始创建

1. 创建 Pool

   ```bash
   ceph osd pool create kubernetes
   ```

2. 初始化 RBD

   ```bash
   rbd pool init kubernetes
   ```

3. 创建访问块设备的认证用户

   ```bash
   ceph auth get-or-create client.kubernetes mon 'profile rbd' osd 'profile rbd pool=kubernetes'
   ```


## 清理删除

1. 查找池中所有 RBD 镜像

   ```bash
   rbd ls -l kubernetes
   ```

2. 删除 RBD 镜像

   ```bash
   rbd rm kubernetes/csi-vol-5a69b8e4-e8b5-4e74-8a41-1c7725f4c5f0
   ```

3. 删除 Pool

   ```bash
   ceph osd pool rm kubernetes kubernetes --yes-i-really-really-mean-it
   ```

4. 删除认证用户

   ```bash
   ceph auth del client.kubernetes
   ```

   