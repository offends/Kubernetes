> 本文作者：丁辉

# CEPH对接Kubernetes-StorageClass

> 相关文档地址

- [Helm对接外部Ceph](https://gitee.com/offends/Kubernetes/blob/main/Helm/Helm%E5%AF%B9%E6%8E%A5%E5%A4%96%E9%83%A8Ceph.md)
- [Helm部署Rook-Ceph](https://gitee.com/offends/Kubernetes/blob/main/Helm/Helm%E9%83%A8%E7%BD%B2Rook-Ceph.md)
