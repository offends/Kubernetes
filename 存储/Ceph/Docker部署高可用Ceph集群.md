> 本文作者：丁辉

# Docker部署高可用Ceph集群

[CEPH版本](https://docs.ceph.com/en/latest/releases/)	[CEPH下载](http://download.ceph.com/)

[DockerHub仓库(已废弃)](https://hub.docker.com/r/ceph/daemon)	[Quay仓库](https://quay.io/repository/ceph/daemon)

|  节点名称   |      IP      |  存储盘  |        角色         |
| :---------: | :----------: | :------: | :-----------------: |
| ceph-node-1 | 192.168.1.10 | /dev/sdb | mon,mgr,osd,mds,rgw |
| ceph-node-2 | 192.168.1.20 | /dev/sdb | mon,mgr,osd,mds,rgw |
| ceph-node-3 | 192.168.1.30 | /dev/sdb | mon,mgr,osd,mds,rgw |

**组件介绍**

- OSD（Object Storage Daemon）：负责管理磁盘上的数据块，执行数据的读写操作，并确保集群的高可用性。通常至少需要部署三个节点以保证系统的稳定运行。
- MON（Monitor）：负责维护 Ceph 集群的状态、配置和映射信息，保障集群元数据的一致性，协调节点间数据的分布和恢复。为确保集群的高可用性，一般需要至少部署三个节点。
- MDS（Metadata Server）：负责管理文件系统的目录结构和文件/目录的元数据信息，为 CephFS（Ceph 的分布式文件系统）提供元数据服务。需要注意的是，块存储和对象存储不需要部署 MDS。
- MGR（Manager）：负责收集 Ceph 集群的状态信息，包括 OSD、MON、MDS 的性能指标和健康状况，并提供可视化的仪表板（Ceph Dashboard）供用户查看。为确保集群的高可用性，通常至少需要部署两个节点。
- RGW（Rados Gateway）：提供了 RESTful API，允许用户通过 HTTP/HTTPS 请求访问和管理存储在 Ceph 集群中的数据。支持 Amazon S3 API 和 OpenStack Swift API。

**使用镜像介绍**

- `ceph/daemon` 是一个 Docker 镜像，它用于运行 Ceph 集群中的各种守护进程，如监控节点（MON）、管理节点（MGR）、元数据服务器（MDS）、对象存储守护进程（OSD）以及 RADOS 网关（RGW）。这个镜像包含了运行一个完整 Ceph 存储集群所需的所有组件，使得部署和管理变得更加容易和灵活，特别是在容器化环境中。

- `ceph/daemon` 镜像的优点是可以简化 Ceph 的部署和运维过程，因为它预配置了 Ceph 的各种服务和依赖，用户只需通过设置适当的环境变量和参数即可快速启动各种 Ceph 组件。这使得它在动态的云环境中特别有用，可以轻松地扩展或更新集群。

- 此外，使用 Docker 运行 Ceph 服务还有助于隔离不同服务的运行环境，提高系统的安全性和稳定性。

## 基础环境准备

[请查看此文章](https://gitee.com/offends/Kubernetes/blob/main/存储/Ceph/Ceph基础环境准备.md)

## 开始部署

### 部署监视器 MON

1. 节点一启动 mon

   ```bash
   docker run -d --net=host \
     --name ceph-mon \
     --restart=always \
     --privileged=true \
     -v /dev:/dev \
     -v /etc/ceph:/etc/ceph \
     -v /var/lib/ceph/:/var/lib/ceph/ \
     -v /var/log/ceph/:/var/log/ceph/ \
     -v /etc/localtime:/etc/localtime:ro \
     -e MON_IP=192.168.1.10 \
     -e CEPH_PUBLIC_NETWORK=192.168.1.0/24 \
     ceph/daemon:latest mon
   ```

2. 禁用不安全认证

   > 这个参数控制是否允许在集群中通过全局 ID 回收机制（global ID reclaim）来认证用户身份

   ```bash
   ceph config set mon auth_allow_insecure_global_id_reclaim false
   ```

3. 创建 OSD 秘钥

   ```bash
   ceph auth get client.bootstrap-osd -o /var/lib/ceph/bootstrap-osd/ceph.keyring
   ```

5. 拷贝节点一 ceph 目录, 传输至节点二和节点三, 传输完后按照第一步启动容器

   > 注意修改 MON_IP

   ```bash
   # 节点二
   scp -r /etc/ceph/ root@ceph-node-2:/etc/
   scp -r /var/lib/ceph root@ceph-node-2:/var/lib/ceph
   # 节点三
   scp -r /etc/ceph root@ceph-node-3:/etc/ceph
   scp -r /var/lib/ceph root@ceph-node-3:/var/lib/ceph
   ```

### 部署监控 MGR

> 为了确保集群的高可用性, 通常至少要部署两个节点

```bash
docker run -d --net=host \
  --name ceph-mgr \
  --restart=always \
  -v /etc/ceph:/etc/ceph \
  -v /var/lib/ceph/:/var/lib/ceph/ \
  -v /var/log/ceph/:/var/log/ceph/ \
  -v /etc/localtime:/etc/localtime:ro \
  ceph/daemon:latest mgr
```

### 部署存储 OSD

1. 初始化块存储

   ```bash
   docker exec ceph-mon ceph-volume lvm prepare --data /dev/sdb
   ```

   ceph-volume **参数解释**

   | 参数                   | 解释                                                         |
   | ---------------------- | ------------------------------------------------------------ |
   | `--bluestore`          | 指定使用 Bluestore 存储引擎。Bluestore 是 Ceph 的一种存储格式，不依赖于文件系统，直接在裸设备上操作数据。 |
   | `--filestore`          | 指定使用 Filestore 存储引擎。Filestore 是较旧的存储格式，它在底层文件系统（如 XFS）之上操作数据。 |
   | `--no-systemd`         | 在创建时禁止生成 systemd 单元文件。通常用于不使用 systemd 的系统或者在特定的管理脚本中需要手动管理服务启动。 |
   | `--data`               | 指定用于数据存储的设备或分区。                               |
   | `--db`                 | 仅用于 Bluestore。指定用于存放数据库的设备或分区，可以提升性能。 |
   | `--wal`                | 仅用于 Bluestore。指定用于写入前日志（Write-Ahead Log）的设备或分区，用于提升写入性能。 |
   | `--journal`            | 仅用于 Filestore。指定日志存放的设备或分区，用于提升日志处理性能。 |
   | `--prepare`            | 准备磁盘用于 Ceph 使用，但不启动 OSD。                       |
   | `--activate`           | 激活之前已准备好的 OSD。                                     |
   | `--crush-device-class` | 指定设备的 CRUSH 类别，用于 Ceph 集群的数据分布和复制策略中。 |

2. 部署 OSD

   > 注意：根据自己情况修改 OSD_ID

   ```bash
   docker run -d --net=host --name=ceph-osd \
     --privileged=true \
     -e OSD_ID=0 \
     -v /dev/:/dev/ \
     -v /etc/ceph:/etc/ceph \
     -v /var/lib/ceph/:/var/lib/ceph/ \
     -v /var/log/ceph/:/var/log/ceph/ \
     -v /etc/localtime:/etc/localtime:ro \
     ceph/daemon:latest osd_ceph_volume_activate
   ```

   >使用本地目录启动 OSD
   >
   >```bash
   >docker run -d --net=host --name=ceph-osd \
   >  --privileged=true \
   >  -v /etc/ceph:/etc/ceph \
   >  -v /var/lib/ceph/:/var/lib/ceph/ \
   >  -v /var/log/ceph/:/var/log/ceph/ \
   >  -v /var/lib/ceph/osd/osd-0:/var/lib/ceph/osd \
   >  -v /etc/localtime:/etc/localtime:ro \
   >  ceph/daemon:latest osd_directory
   >```

### 部署文件系统 MDS

```bash
docker run -d --net=host \
  --name ceph-mds \
  --restart=always \
  -v /etc/ceph:/etc/ceph \
  -v /var/lib/ceph/:/var/lib/ceph/ \
  -v /var/log/ceph/:/var/log/ceph/ \
  -v /etc/localtime:/etc/localtime:ro \
  -e CEPHFS_CREATE=1 \
  -e CEPHFS_NAME=cephfs \
  -e CEPHFS_DATA_POOL=cephfs_data \
  -e CEPHFS_DATA_POOL_PG=128 \
  -e CEPHFS_METADATA_POOL=cephfs_metadata \
  -e CEPHFS_METADATA_POOL_PG=64 \
  ceph/daemon:latest mds
```

### 部署对象存储接 RGW

1. 创建 Rgw 密钥

   ```bash
   docker exec ceph-mon ceph auth get client.bootstrap-rgw -o /var/lib/ceph/bootstrap-rgw/ceph.keyring
   ```

2. 启动

   ```bash
   docker run -d --net=host \
     --name=ceph-rgw \
     --restart=always \
     -v /etc/ceph:/etc/ceph \
     -v /var/lib/ceph/:/var/lib/ceph/ \
     -v /var/log/ceph/:/var/log/ceph/ \
     -v /etc/localtime:/etc/localtime:ro \
     ceph/daemon rgw
   ```

### 开启管理界面

查看此文档 [MGR开启监控管理界面](https://gitee.com/offends/Kubernetes/blob/main/存储/Ceph/MGR开启监控管理界面.md)

## 卸载

1. 删除容器

   ```bash
   docker rm -f ceph-mon
   docker rm -f ceph-mgr
   docker rm -f ceph-osd
   docker rm -f ceph-mds
   docker rm -f ceph-rgw
   ```

2. 删除持久化目录

   ```bash
   rm -rf {/etc/ceph,/var/lib/ceph,/var/log/ceph}
   ```
