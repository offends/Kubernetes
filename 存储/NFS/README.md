> 本文作者：丁辉

# 对接NFS相关文档

> 相关文档地址

- [Linux部署NFS存储](https://gitee.com/offends/Linux/blob/main/%E5%AD%98%E5%82%A8/NFS/Linux%E9%83%A8%E7%BD%B2NFS%E5%AD%98%E5%82%A8.md)
- [Rsync常用参数解释](https://gitee.com/offends/Linux/blob/main/%E5%AD%98%E5%82%A8/NFS/Rsync%E5%B8%B8%E7%94%A8%E5%8F%82%E6%95%B0%E8%A7%A3%E9%87%8A.md)
- [NFS服务优化](https://gitee.com/offends/Linux/blob/main/%E5%AD%98%E5%82%A8/NFS/NFS%E6%9C%8D%E5%8A%A1%E4%BC%98%E5%8C%96.md)
- [Nfs高可用实现Rsync+Inotify](https://gitee.com/offends/Linux/blob/main/%E5%AD%98%E5%82%A8/NFS/Nfs%E9%AB%98%E5%8F%AF%E7%94%A8%E5%AE%9E%E7%8E%B0Rsync+Inotify.md)
- [Nfs高可用实现Rsync+Sersync2](https://gitee.com/offends/Linux/blob/main/%E5%AD%98%E5%82%A8/NFS/Nfs%E9%AB%98%E5%8F%AF%E7%94%A8%E5%AE%9E%E7%8E%B0Rsync+Sersync2.md)
- [Helm对接外部NFS存储](https://gitee.com/offends/Kubernetes/blob/main/Helm/Helm%E5%AF%B9%E6%8E%A5%E5%A4%96%E9%83%A8NFS%E5%AD%98%E5%82%A8.md)

