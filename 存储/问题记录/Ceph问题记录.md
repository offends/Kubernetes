> 本文作者：丁辉

# Ceph问题记录

> 重启 Mon 后 `/opt/ceph-container/bin/entrypoint.sh: Existing mon, trying to rejoin cluster...` 

- 该问题通过以下方式"解决"： 在 Mon 持久化目录中 `rm -rf /var/lib/ceph/mon/<MONID>` , 其他 mon 将需要重复同样的修复方式。

  或

- 修改启动文件修复

  1. 将 mon 容器内的启动脚本复制到宿主机中。

     ```bash
     docker cp ceph-mon:/opt/ceph-container/bin/start_mon.sh .
     ```

  2. 修改启动脚本

     ```bash
     # 找到如下代码，注释掉
     v2v1=$(ceph-conf -c /etc/ceph/"${CLUSTER}".conf 'mon host' | tr ',' '\n' | grep -c "${MON_IP_NO_BRACKETS}")
     # 替换为如下代码
     v2v1=2
     ```

  3. 将修改后的启动脚本再复制到容器中

     ```bash
     docker cp start_mon.sh ceph-mon:/opt/ceph-container/bin/start_mon.sh 
     ```

  4. 重新启动 mon 容器

     ```bash
     docker restart ceph-mon
     ```

  5. 再重启其他两个 mon 节点即可恢复