> 本文作者：丁辉

# 安装Containerd

| 支持系统 |  支持架构  |
| :------: | :--------: |
| Centos*  | x86、arm64 |
| Ubuntu*  | x86、arm64 |

## 极简模式

> 直接二进制安装最新版 Containerd

```bash
curl -sfL https://gitee.com/offends/Kubernetes/raw/main/Containerd/install.sh | bash -s all_curl
```

## 多功能模式

```bash
git clone https://gitee.com/offends/Kubernetes.git && cd Kubernetes/Containerd
```

**参数解释**

| 脚本参数       | 作用                           | 用法                        |
| -------------- | ------------------------------ | --------------------------- |
| binary         | 二进制安装 Containerd 最新版本 | ./install.sh binary         |
| packagemanager | 网络安装 Containerd 最新版本   | ./install.sh packagemanager |

**包管理器安装切换网络源加速 ("binary" 和 "packagemanager" 均可使用以下参数)**

- 使用官方加速

  ```bash
  export INSTALL_NETWORK=default
  ```
- 使用阿里国加速 [默认]

  ```bash
  export INSTALL_NETWORK=aliyun
  ```

**二进制文件下载地址替换 ("binary" 可使用以下参数)**

> 文件格式需要为 `cri-containerd-*-linux-*.tar.gz` 类似于此格式 * 部分可使用版本和架构补充。

- 指定版本下载 (默认从 Github 仓库下载, 默认版本为: "1.7.18")

  ```bash
  export CONTAINERD_VERSION="1.7.18"
  ```
  
- 指定二进制文件包下载地址

  ```bash
  export OFFENDS_URL=""
  ```
  
  > 示例
  >
  > ```bash
  > export OFFENDS_URL="https://minio.com/cri-containerd-*-linux-amd64.tar.gz"
  > ```
  
  
