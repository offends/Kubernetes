> 本文作者：丁辉

# Nerdctl工具

[Github](https://github.com/containerd/nerdctl)

## 开始安装

1. 下载软件

   ```bash
   wget https://github.com/containerd/nerdctl/releases/download/v1.7.6/nerdctl-1.7.6-linux-amd64.tar.gz
   ```

2. 解压文件

   ```bash
   tar -zxvf nerdctl-*-linux-amd64.tar.gz
   ```

3. 安装

   ```bash
   install -o root -g root -m 0755 nerdctl /usr/local/bin/nerdctl
   ```

## 基本使用

- 查看 Containerd 镜像

  ```bash
  nerdctl -n k8s.io images 
  ```

- 登录镜像仓库

  ```bash
  nerdctl -n k8s.io login <仓库地址> -u <账号> -p<密码>
  ```

- 退出镜像仓库登录

  ```bash
  nerdctl -n k8s.io logout <仓库地址>
  ```

- 拉取镜像

  ```bash
  nerdctl -n k8s.io pull nginx:latest
  ```

- 更改镜像 TAG

  ```bash
  nerdctl -n k8s.io tag nginx:v1 nginx:v2
  ```

- 推送镜像

  ```bash
  nerdctl -n k8s.io push nginx:latest --insecure-registry
  ```

  > `--insecure-registry` 表示目标仓库是一个不安全的私有仓库，不需要进行TLS证书验证