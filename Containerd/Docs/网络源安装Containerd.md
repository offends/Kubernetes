> 本文作者：丁辉

[Github安装文档](https://github.com/containerd/containerd/blob/main/docs/getting-started.md)

[Containerd软件包](https://github.com/containerd/containerd/releases)	[Runc软件包](https://github.com/opencontainers/runc/releases)	[Cni插件包](https://github.com/containernetworking/plugins/releases)

## 网络源安装Containerd

1. 设置存储库

   [阿里源配置文件](https://developer.aliyun.com/mirror/docker-ce?spm=a2c6h.13651102.0.0.4eac1b11shXBpr)

   ```bash
   yum install -y yum-utils
   yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
   ```

   > 国内
   >
   > ```bash
   > yum install -y yum-utils
   > yum-config-manager --add-repo https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
   > ```

2. 安装

   ```bash
   yum install -y containerd.io
   ```

3. 备份配置文件

   ```bash
   mv /etc/containerd/config.toml /etc/containerd/config.toml.bak
   containerd config default > /etc/containerd/config.toml
   ```

4. 修改驱动和镜像地址

   ```bash
   vi /etc/containerd/config.toml
   ```

   内容如下

   ```toml
   [plugins."io.containerd.grpc.v1.cri".containerd.runtimes.runc]
     ...
     [plugins."io.containerd.grpc.v1.cri".containerd.runtimes.runc.options]
       SystemdCgroup = true #修改这里
   
   [plugins."io.containerd.grpc.v1.cri"]
     sandbox_image = "registry.aliyuncs.com/google_containers/pause:3.9"
   ```

5. 配置 crictl 配置文件

   ```bash
   curl -so /etc/crictl.yaml https://gitee.com/offends/Kubernetes/raw/main/Containerd/Files/crictl.yaml
   ```

6. 启动

   ```bash
   systemctl enable containerd
   systemctl start containerd
   systemctl status containerd
   ```

