> 本文作者：丁辉

# 二进制安装Containerd

## 安装 Containerd

[containerd软件包](https://github.com/containerd/containerd/releases)

[cni插件包](https://github.com/containernetworking/plugins/releases)

1. 下载二进制文件

   ```bash
   wget https://github.com/containerd/containerd/releases/download/v1.6.33/cri-containerd-cni-1.6.33-linux-amd64.tar.gz
   ```

2. 解压

   ```bash
   mkdir containerd
   tar -zxvf cri-containerd-cni-*-linux-amd64.tar.gz -C containerd
   cd containerd
   ```

3. 移动文件至安装目录

   ```bash
   \cp usr/local/bin/* /usr/local/bin/
   \cp etc/systemd/system/containerd.service /usr/lib/systemd/system/containerd.service
   mkdir /opt/cni/bin -p
   \cp /root/containerd/opt/cni/bin/* /opt/cni/bin/
   ```

4. 备份配置文件

   ```bash
   mkdir /etc/containerd
   containerd config default > /etc/containerd/config.toml
   ```

5. 修改驱动和镜像地址

   ```bash
   vi /etc/containerd/config.toml
   ```

   内容如下

   ```toml
   [plugins."io.containerd.grpc.v1.cri".containerd.runtimes.runc]
     ...
     [plugins."io.containerd.grpc.v1.cri".containerd.runtimes.runc.options]
       SystemdCgroup = true #修改这里
   
   [plugins."io.containerd.grpc.v1.cri"]
     sandbox_image = "registry.aliyuncs.com/google_containers/pause:3.9"
   ```

6. 配置 crictl 配置文件

   ```bash
   curl -so /etc/crictl.yaml https://gitee.com/offends/Kubernetes/raw/main/Containerd/Files/crictl.yaml
   ```

7. 启动

   ```bash
   systemctl enable containerd
   systemctl start containerd
   systemctl status containerd
   ```

## 安装 RunC

[Github软件包下载](https://github.com/opencontainers/runc/releases)

1. 下载

   ```bash
   wget https://github.com/opencontainers/runc/releases/download/v1.1.12/runc.amd64
   ```

2. 安装

   ```bash
   install -m 755 runc.amd64 /usr/local/bin/runc
   ```
