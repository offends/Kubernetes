> 本文作者：丁辉

# Ctr命令使用

- 查看镜像

  ```bash
  ctr -n k8s.io images ls
  ```

- 更改镜像 TAG

  ```bash
  ctr -n k8s.io image tag nginx:v1 nginx:v2
  ```

- 拉取镜像

  ```bash
  ctr -n k8s.io images pull docker.io/library/nginx:latest
  ```

  **更多参数**

  - `--hosts-dir "/etc/containerd/certs.d"`：指定了包含镜像仓库证书的目录的路径。

  - `-k`：忽略 TLS 验证过程中的证书错误。
  - `--plain-http=true`：此选项指明在拉取镜像时使用未加密的 HTTP 协议，而不是加密的 HTTPS。

- 推送镜像

  ```bash
  ctr -n k8s.io image push -u <账户>:<密码> docker.io/library/nginx:latest
  ```

  **更多参数**

  - `--plain-http=true`：此选项指明在拉取镜像时使用未加密的 HTTP 协议，而不是加密的 HTTPS。