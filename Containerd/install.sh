#!/bin/bash

#############################################################################################
# 用途: 多功能部署 Containerd 脚本
# 作者: 丁辉
# 编写时间: 2023-12-29
# 更新时间: 2024-01-26
#############################################################################################

# 加载检测脚本
source <(curl -sS https://gitee.com/offends/Linux/raw/main/File/Shell/Check_command.sh)
CHECK_CPU
# 变量
SCRIPT_VERSION="1.0.0"
INSTALL_NETWORK=${INSTALL_NETWORK:-aliyun}
# Containerd 版本
CONTAINERD_VERSION=${CONTAINERD_VERSION:-1.7.18}
# Github下载加速
GIT_PROXY="https://mirror.ghproxy.com"
URL="https://github.com/containerd/containerd/releases/download/v${CONTAINERD_VERSION}/cri-containerd-cni-${CONTAINERD_VERSION}-linux-${ARCH_TYPE_2}.tar.gz"

# --help 帮助信息
function HELP(){
    echo "Usage: script_name [OPTIONS] [ARGUMENTS]"
    echo ""
    echo "Description:"
    echo "  Offends"
    echo ""
    echo "Options:"
    echo "  -h, --help                  显示此帮助信息"
    echo "  -v, --version               显示当前脚本版本号"
    echo "  all_curl                    省心网络安装,所有文件都会从网络 curl 下载"
    echo "  binary                      二进制安装 Containerd 最新版本"
    echo "  packagemanager              网络安装 Containerd 最新版本"
    echo ""
    echo "Examples:"
    echo "  示例 1: ./install.sh all_curl"
    echo "  示例 1: ./install.sh binary"
    echo "  示例 2: ./install.sh packagemanager"
    exit 0
}

# 在线安装
function INSTALL_CONTAINERD(){
    if [ $# -eq 1 ]; then
        while true; do
            case $1 in
                all_curl)
                    INSTALL_0
                    break
                ;;
                binary)
                    SEND_INFO "您选择的安装方式为: 二进制安装 Containerd 最新版本"
                    INSTALL_1
                    break
                    ;;
                packagemanager)
                    SEND_INFO "您选择的安装方式为: 网络安装 Containerd 最新版本"
                    INSTALL_2
                    break
                    ;;
                --help|-h)
                    HELP
                    ;;
                --version|-v)
                    SEND_INFO "当前脚本版本号为: $SCRIPT_VERSION"
                    break
                    ;;
                *)
                    SEND_ERROR "参数错误"
                    HELP
                    ;;
            esac
        done
    else
        SEND_ERROR "参数错误"
        HELP
    fi
}

function INSTALL_0(){
    IF_LINUX
    INIT_CONTAINERD
    CURL_FILE
}


function INSTALL_1(){
    CHECK_INSTALL wget
    SEND_INFO "正在下载 Containerd 二进制文件包[Github],网络较慢请稍后..."
    if [ ${INSTALL_NETWORK} = "default" ]; then
        SEND_INFO "您选择的网络为: 默认网络"
        BINARY_URL=${OFFENDS_URL:-$URL}
    elif [ ${INSTALL_NETWORK} = "aliyun" ]; then
        SEND_INFO "您选择的网络为: 阿里云网络加速"
        BINARY_URL=${OFFENDS_URL:-$GIT_PROXY/$URL}
    fi
    wget ${BINARY_URL}
    if [ $? -ne 0 ]; then
        SEND_ERROR "下载 Containerd 二进制文件包失败,请检查网络"
        exit 1
    fi
    CHECK_DIR "./containerd"
    CHECK_COMMAND_NULL tar -zxvf cri-containerd-*-linux-*.tar.gz -C containerd
    BASE_FILES
}

function INSTALL_2(){
    IF_LINUX
    INIT_CONTAINERD
    CP_FILE
}

function BASE_FILES(){
    SEND_INFO "正在初始化文件"
    CHECK_COMMAND_NULL \\cp containerd/usr/local/bin/* /usr/local/bin/
    CHECK_COMMAND_NULL \\cp containerd/etc/systemd/system/containerd.service /usr/lib/systemd/system/containerd.service
    CHECK_DIR "/opt/cni/bin"
    # CHECK_COMMAND_NULL \\cp containerd/opt/cni/bin/* /opt/cni/bin/
    CHECK_DIR "/etc/containerd"
    CHECK_COMMAND_NULL rm -rf ./containerd
    CHECK_COMMAND_NULL rm -rf ./cri-containerd-*-linux-*.tar.gz
    INIT_CONTAINERD
    CP_FILE
}

function IF_LINUX(){
    log_file="/tmp/check_install_log"
    if command -v yum >/dev/null 2>&1; then
        SEND_INFO "正在准备包,根据网络情况可能需要几分钟,请稍后..."
        yum install -y yum-utils >>"$log_file" 2>&1
        if [ $? -ne 0 ]; then
            SEND_ERROR "安装 yum-utils 失败，请查看日志"
            tail -n 10 "$log_file"  # 输出最后10行日志
            rm -rf $log_file
            exit 1
        fi
        if [ ${INSTALL_NETWORK} = "default" ]; then
            SEND_INFO "您选择的网络为: 默认网络"
            CHECK_COMMAND_NULL yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
            yum install containerd.io -y
        elif [ ${INSTALL_NETWORK} = "aliyun" ]; then
            SEND_INFO "您选择的网络为: 阿里云网络"
            CHECK_COMMAND_NULL yum-config-manager --add-repo https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
            yum install containerd.io -y
        fi
    elif command -v apt >/dev/null 2>&1; then
        SEND_INFO "正在准备包,根据网络情况可能需要几分钟,请稍后..."
        if [ ${INSTALL_NETWORK} = "default" ]; then
            SEND_INFO "您选择的网络为: 默认网络"
            curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
            CHECK_COMMAND_NULL chmod a+r /etc/apt/keyrings/docker.gpg

            echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
            $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
            sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

            CHECK_COMMAND_NULL apt-get update
            apt-get install containerd.io -y
        elif [ ${INSTALL_NETWORK} = "aliyun" ]; then
            SEND_INFO "您选择的网络为: 阿里云网络"
            curl -fsSL https://mirrors.aliyun.com/docker-ce/linux/ubuntu/gpg | sudo apt-key add -
            # add-apt-repository "deb [arch=amd64] https://mirrors.aliyun.com/docker-ce/linux/ubuntu $(lsb_release -cs) stable"
            # 设置 Docker 源地址
            docker_repo="deb [arch=amd64] https://mirrors.aliyun.com/docker-ce/linux/ubuntu $(lsb_release -cs) stable"
            # 添加 Docker 源并自动回车确认
            echo -e "\n" | sudo add-apt-repository "$docker_repo"
            CHECK_COMMAND_NULL apt-get update
            apt-get install containerd.io -y
        fi
    else
        SEND_ERROR "无法识别的系统软件包管理工具"
        exit 1
    fi
}


function INIT_CONTAINERD(){
    SEND_INFO "正在初始化 Containerd"
    CHECK_COMMAND_NULL containerd config default \> /etc/containerd/config.toml
    sed -i 's/SystemdCgroup = false/SystemdCgroup = true/g' /etc/containerd/config.toml
    sed -i 's|sandbox_image[[:space:]]*=[[:space:]]*".*"|sandbox_image = "registry.aliyuncs.com/google_containers/pause:3.9"|g' /etc/containerd/config.toml
}

function CP_FILE(){
    CHECK_COMMAND_NULL \\cp ./Files/crictl.yaml /etc/crictl.yaml
}

function CURL_FILE(){
    curl -so /etc/crictl.yaml https://gitee.com/offends/Kubernetes/raw/main/Containerd/Files/crictl.yaml
}

function START_CONTAINERD(){
    SEND_INFO "正在启动 Containerd 服务"
    CHECK_COMMAND_NULL systemctl daemon-reload
    CHECK_COMMAND_NULL systemctl enable containerd
    CHECK_COMMAND_NULL systemctl start containerd
    SEND_INFO "Containerd 服务启动成功"
}

# 检测某个systemd服务是否存在
function CHECK_SYSTEMD(){
    if ! command -v containerd >/dev/null 2>&1; then
        INSTALL_CONTAINERD $@
        START_CONTAINERD
    else
        SEND_INFO "Containerd 服务已安装,版本为: $(containerd --version | awk '{print $3}')"
        if ! systemctl status containerd >/dev/null 2>&1; then
            SEND_WARN "Containerd 服务未启动,正在启动 Containerd 服务"
            if ! systemctl start containerd >/dev/null 2>&1; then
                SEND_ERROR "Containerd 服务启动失败,请检查日志排查错误"
                exit 1
            else
                SEND_INFO "Containerd 服务启动成功"
            fi
        fi
    fi
}

CHECK_SYSTEMD $@