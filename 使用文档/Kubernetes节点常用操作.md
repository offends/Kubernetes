> 本文作者：丁辉

# Kubernetes节点常用操作

## 允许节点加入集群

1. 查看待加入节点

   ```bash
   kubectl get csr
   ```

2. 查看节点信息

   ```bash
   kubectl describe csr $name
   ```

3. 批准加入

   ```bash
   kubectl certificate approve $name
   ```

4. 查看结果

   ```bash
   kubectl get csr
   #CONDITION=Approved,Issued
   ```

## 删除节点

> 在删除节点前，要先排空节点上POD

1. 排空POD

   ```bash
   kubectl drain <node name> --delete-local-data --force --ignore-daemonsets
   ```

2. 删除节点

   ```bash
   kubectl delete node <node name>
   ```

## 给节点打标签

- `Master` 打标签

  ```bash
  kubectl label node 192.168.1.10 node-role.kubernetes.io/master='k8s-master1'
  ```

- `Node` 打标签

  ```bash
  kubectl label node ${node} node-role.kubernetes.io/worker=true
  ```

