> 本文作者：丁辉

# Kubernetes常用命令

- 查看资源定义都有哪些字段

  ```bash
  kubectl explain deployments.spec
  ```

- 查看最近的事件

  ```bash
  kubectl get events --sort-by='.lastTimestamp'
  ```

- 列出 Kubernetes 集群中可用的 API 资源

  ```bash
  kubectl api-resources
  ```

- 设置 Kubernetes 默认命名空间

  > 这样在执行命令的时候就可以不用指定命名空间啦
  
  ```bash
  kubectl config set-context --current --namespace=命名空间名称
  ```
  
- 将 Kubernetes 集群的配置信息（包括集群、用户、凭据等）导出到 `~/.kube/config` 文件中

  ```bash
  kubectl config view --raw > ~/.kube/config
  ```

- 获取 svc 的 clusterIP

  ```bash
  kubectl get svc kubernetes -o jsonpath='{.spec.clusterIP}'
  ```

- 获取 Kubernetes 集群内部域名后缀

  ```bash
  kubectl -n kube-system get configmap/coredns -o jsonpath='{.data.Corefile}' | grep 'kubernetes' | sed 's/{//'
  ```

- 显示有关 Kubernetes 集群的基本信息，包括控制平面组件的地址、服务的端点等

  ```bash
  kubectl cluster-info
  ```

- 用于生成有关 Kubernetes 集群的详尽信息的完整转储

  ```bash
  kubectl cluster-info dump
  ```

- 获取集群 Name

  ```bash
  kubectl config view --minify -o "jsonpath={.clusters[0].name}"
  ```

- 获取集群用户

  ```bash
  kubectl config view --minify -o "jsonpath={.users[*].name}"
  ```

- 获取集群组

  ```bash
  kubectl config view --minify -o "jsonpath={.contexts[*].context.user}"
  ```

  

