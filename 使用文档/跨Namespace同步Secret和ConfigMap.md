> 本文作者：丁辉

# 跨Namespace同步Secret和ConfigMap

> 示例：同步 `test` 命名空间下的 `demo-secret` 到 `default` 命名空间下

## JQ实现

1. 准备依赖 `jq`

   - Centos

     ```bash
     yum install jq -y
     ```

   - Ubuntu

     ```bash
     apt install jq -y
     ```

2. 执行命令

   ```bash
   kubectl get secret demo-secret -n test -o json \
    | jq 'del(.metadata["namespace","creationTimestamp","resourceVersion","selfLink","uid"])' \
    | kubectl apply -n default -f -
   ```

## SED实现

```bash
kubectl get secret demo-secret -n test -o json \
 | jq 'del(.metadata["namespace","creationTimestamp","resourceVersion","selfLink","uid"])' \
 | kubectl apply -n default -f -
```

## Helm部署Config-Syncer实现自动同步

请查看此文档：[Helm部署Config-Syncer](https://gitee.com/offends/Kubernetes/blob/main/Helm/Helm%E9%83%A8%E7%BD%B2Config-Syncer.md)

