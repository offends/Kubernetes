> 本文作者：丁辉

# Kubernetes强制删除资源

## 卸载 Mount 挂载

```bash
umount $(mount | grep /var/lib/kubelet/pods | awk '{print $3}')
```

## 强制删除 Pod

**指定你的 Pod 名称**

```bash
POD_NAME=
```

- 强制删除 Terminating Pod

  ```bash
  kubectl delete pod ${POD_NAME} --force
  ```

- 立刻终止并强制删除 Terminating Pod

  ```bash
  kubectl delete pod ${POD_NAME} --grace-period=0 --force
  ```

- 通过修改系统参数删除 Terminating Pod(仅Centos)

  > 通过设置 `fs.may_detach_mounts=1` , Linux内核可以允许卸载这些挂载点，即使它们仍然被一些进程占用。

  ```bash
  sysctl -w fs.may_detach_mounts=1
  ```

- 通过修改 finalizers 删除 Terminating Pod

  > 当你删除一个资源（比如 Pod）时，Kubernetes 会将该资源的 finalizers 字段设置为一个非空的数组，这些 finalizers 是用来执行删除操作的一系列步骤。一旦这些步骤全部完成，Kubernetes 就会将资源完全删除。但是，有时候 Pod 可能会被卡在 Terminating 状态，无法正常删除，这可能是因为某些 finalizers 的执行未能成功完成，从而导致 Pod 无法被删除。

  ```bash
  kubectl patch pod ${POD_NAME} -p '{"metadata":{"finalizers":null}}'
  ```

## 强制删除当前 Namespace 下所有 Pvc Pv

- 设置变量

  ```bash
  export YOURNAMESPACE=你的名称空间
  ```

- 删除 Pvc

  ```bash
  for line in  $(kubectl get pvc -n $YOURNAMESPACE | awk ' NR > 1{print $1}') ;do  kubectl delete pvc  $line  -n $YOURNAMESPACE ; echo $line; done
  ```

- 删除 Pv

  ```bash
  for line in  $(kubectl get pv -n $YOURNAMESPACE | awk ' NR > 1{print $1}') ;do  kubectl delete pv  $line  -n $YOURNAMESPACE ; echo $line; done
  ```

- 若发现 Pv 还是删除不了

  ```bash
  for line in  $(kubectl get pv -n $YOURNAMESPACE | awk ' NR > 1{print $1}') ;do  kubectl patch pv $line -p '{"metadata":{"finalizers":null}}' -n $YOURNAMESPACE ; kubectl delete pv  $line  -n $YOURNAMESPACE ;echo $line; done
  ```

## 强制删除 Namespace

**方法一**

- 先手动强制删除试试

  ```bash
  kubectl delete ns $YOURNAMESPACE --force --grace-period=0
  ```

**方法二**

- 导出 JSON 文件

  ```bash
  kubectl get namespace $YOURNAMESPACE -o json > ns.json
  ```

- 编辑 ns.josn 删除 finalizers 字段的值

  ```yaml
  "spec": {
      "finalizers": []
    },
  ```

- 开启proxy

  ```bash
  kubectl proxy --port=8081
  ```

- 删除

  ```bash
  curl -k -H "Content-Type:application/json" -X PUT --data-binary @ns.json http://127.0.0.1:8081/api/v1/namespaces/$YOURNAMESPACE/finalize
  ```

## 删除Evicted pod

```bash
kubectl get pod -A | grep Evicted | awk '{print $2" -n "$1}' | xargs kubectl delete pod
```

