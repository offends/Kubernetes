> 本文作者：丁辉
>

# 挂载Configmap配置文件

> 解决Kubernetes ConfigMap挂载导致容器目录覆盖的问题

- 创建 Configmap

  ```bash
  kubectl create configmap demo-config --from-file=./config.js
  ```

- 编辑 POD 容器

  ```yaml
    volumeMounts:
    - name: file
      mountPath: /opt/config.js #挂载到容器内的路径
      subPath: config.js #目录内其他内容不会被清理掉
  volumes:
  - name: file
    configMap:
      name: demo-config
  ```

