> 本文作者：丁辉

# Kubernetes无状态容器副本管理

- 更新版本

  ```bash
  kubectl rollout restart deployment  $deployment
  ```

- 查看更新状态

  ```bash
  kubectl rollout status  deployment/$deployment
  ```

- 查看版本信息

  ```bash
  kubectl rollout history deployment/$deployment
  ```

- 撤消当前上线并回滚到以前的修订版本

  ```bash
  kubectl rollout undo deployment/$deployment
  ```

- 回滚到指定版本

  ```bash
  kubectl rollout undo deployment/$deployment --to-revision=2
  ```

