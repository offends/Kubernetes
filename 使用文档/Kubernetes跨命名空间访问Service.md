> 本文作者：丁辉

# Kubernetes跨命名空间访问Service

## 使用完全限定域名 (FQDN)

> 在 Kubernetes 中，跨命名空间访问服务可以通过使用服务的完全限定域名 (FQDN) 或者创建服务的别名（Service Alias）来实现。下面是一些具体的方法和步骤。

**格式**

```bash
<service-name>.<namespace>.svc.cluster.local
```

**示例**

```bash
kubernetes.default.svc.cluster.local:443
```

**解释**

- **kubernetes**：这是服务 Service 的名称。
- **default**：这指的是服务所在的命名空间。
- **svc**：这表示这是一个服务（Service）。在 Kubernetes 中，服务是定义一组逻辑上相关的 Pod 访问规则的抽象，它允许外部访问或集群内部的负载均衡和服务发现。
- **cluster.local**：这是集群的默认域名。在 Kubernetes 集群中，所有的内部服务都默认位于这个域。
- **443**：这是端口号。

## 创建服务别名（Service Alias）

1. **创建一个 `Service` 对象**：在目标命名空间中创建一个新的服务对象，该对象会将流量转发到原始服务。

   ```bash
   vi service-alias.yaml
   ```

   内容如下

   ```yaml
   apiVersion: v1
   kind: Service
   metadata:
     name: kubernetes-service-alias
     namespace: default
   spec:
     type: ExternalName
     externalName: kubernetes.default.svc.cluster.local
   ```

2. 部署 Yaml

   ```bash
   kubectl apply -f service-alias.yaml
   ```

3. **使用别名访问服务**

   现在，`default` 命名空间中的 Pod 可以通过 `kubernetes-service-alias` 这个服务名来访问 `kubernetes` 服务。

# 总结

通过以上方法，你可以在 Kubernetes 集群中实现跨命名空间访问服务。使用 FQDN 是最直接的方法，而使用服务别名则提供了更大的灵活性。如果需要更严格的网络控制，可以结合 NetworkPolicy 来管理跨命名空间的访问。根据你的具体需求选择合适的方法。