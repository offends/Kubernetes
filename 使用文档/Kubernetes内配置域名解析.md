>本文作者：丁辉

# Kubernetes内配置域名解析

## 更改 Coredns 配置

> 不同部署方式的集群可能 Coredns 配置文件的名称也不同, 需要按照自身集群情况修改
>
> ```bash
> kubectl get cm -n kube-system | grep coredns
> ```

1. 备份原配置

   ```bash
   kubectl get cm coredns -n kube-system -o yaml > /root/coredns.yaml
   ```

2. 编辑 Yaml 文件

   ```bash
   kubectl edit cm -n kube-system coredns
   ```

   添加如下内容

   ```yaml
   apiVersion: v1
   data:
     Corefile: |
       .:53 {
           errors
           health {
             lameduck 5s
           }
           ready
           kubernetes cluster.local in-addr.arpa ip6.arpa {
             pods insecure
             fallthrough in-addr.arpa ip6.arpa
           }
           
           #加这一段
           #---------------------------
           hosts {
             192.168.1.10 www.demo.com
             fallthrough
           }
           #---------------------------
           
           prometheus :9153
           #forward . 192.168.1.10 #或者在这里添加DNS
           forward . "/etc/resolv.conf"
           cache 30
           loop
           reload
           loadbalance
       } # STUBDOMAINS - Rancher specific change
   kind: ConfigMap
   ···
   ```

3. 重启集群内所有 Coredns 立即生效

   > 当然这在生产并不可取, 尽量让他自行生效, 但过程较慢

   - Rke集群

     ```bash
     kubectl delete pod -l k8s-app=kube-dns -n kube-system
     ```

   - 其他集群待写

