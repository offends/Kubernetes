> 本文作者：丁辉

# Kubernetes 拷贝文件

## 拷贝容器内文件到本地

### 方法一

**使用 kubectl cp 拷贝**

```bash
kubectl -n 命名空间 cp 容器名:/容器内文件路径 ./拷贝到本地文件名
```

> 示例：
>
> ```bash
> kubectl -n test cp nginx-6db97db958-zrb7r:etc/nginx/nginx.conf ./nginx.conf
> ```
>
> **执行命令提示**
>
> ```bash
> tar: Removing leading `/' from member names
> ```
>
> 这是在提示你在 `kubectl -n 命名空间 cp 容器名:<这里开头不用加 "/" >`

### 方法二

**寻找到本地 Docker 持久化存储 拷贝文件到本地**

- 获取容器 ID

  ```bash
    CONTAINER_ID=$(kubectl -n 命名空间 describe  pod 容器名 | grep "Container ID:" | awk -F '/' '{print $3}')
  ```

  > 示例：
  >
  > ```bash
  > CONTAINER_ID=$(kubectl -n test describe  pod nginx-6db97db958-zrb7r | grep "Container ID:" | awk -F '/' '{print $3}')
  > ```

- 获取存储路径

  ```bash
  docker inspect -f '{{.GraphDriver.Data.UpperDir}}' $CONTAINER_ID
  ```


### 方法三

- 获取容器名称

  ```bash
  kubectl -n 命名空间 describe  pod 容器名 | grep "Containers:" -A 1
  ```

  > 示例：
  >
  > ```
  > kubectl -n test describe  pod nginx-6db97db958-zrb7r | grep "Containers:" -A 1
  > ```

- 寻找 Docker 容器

  ```bash
  docker ps | grep 容器名称
  ```

- 拷贝容器内文件

  ```bash
  docker cp 容器名称:/容器内路径 ./本地路径
  ```


## 拷贝本地文件到容器内

**使用 kubectl cp 拷贝**

```bash
kubectl -n 命名空间 cp ./本地文件名 容器名:/容器内路径
```

> 示例：
>
> ```bash
> kubectl -n test cp ./default.conf nginx-6db97db958-zrb7r:/etc/nginx/conf.d/
> ```



