> 本文作者：丁辉

# 查看Kubernetes内所有特权容器

1. 安装 JQ

   - Centos

     ```bash
     yum -y install jq
     ```

   - Ubuntu

     ```bash
     apt -y install jq
     ```

2. 查找特权容器

   - 查找特权容器并输出命名空间、容器名

     ```bash
     kubectl get pods --all-namespaces -o json | jq '.items[] | select(.spec.containers[].securityContext.privileged==true) | {namespace: .metadata.namespace, pod: .metadata.name}'
     ```

   - 查找特权容器并输出命名空间、容器名、容器端口

     ```bash
     kubectl get pods --all-namespaces -o json | jq '.items[] | select(.spec.containers[].securityContext.privileged==true) | {namespace: .metadata.namespace, pod: .metadata.name, ports: .spec.containers[].ports}'
     ```

     

