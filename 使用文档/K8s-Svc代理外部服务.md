> 本文作者：丁辉
>

# K8s-Svc代理外部服务

> 本文以 Mysql 为例

1. 编辑文件

   ```bash
   vi mysql-svc.yaml
   ```

   内容如下

   ```yaml
   apiVersion: v1
   kind: Endpoints
   metadata:
     name: mysql-proxy
     namespace: default
   subsets:
     - addresses:
         - ip: 192.168.1.1
         - ip: 192.168.1.2
       ports:
         - port: 3306
           protocol: TCP
   ---
   apiVersion: v1
   kind: Service
   metadata:
     name: mysql-proxy
     namespace: default
   spec:
     ports:
       - port: 3306
         targetPort: 3306
     type: ClusterIP
   ```

2. 部署 svc

   ```bash
   kubectl apply -f mysql-svc.yaml
   ```