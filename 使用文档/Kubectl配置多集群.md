> 本文作者：丁辉

# Kubectl配置多集群

## 基础命令

- 查看和显示当前配置的Kubernetes集群、用户和上下文等信息

  ```bash
  kubectl config view
  ```

- 查看当前上下文（context）的名称

  ```bash
  kubectl config current-context
  ```

- 切换当前 `kubectl` 上下文（context）

  ```bash
  kubectl config use-context $cluster-name
  ```

## 配置Kubectl多集群

> 前提条件：
>
> 使用 `kubectl config view` 命令查看
>
> 1. 集群 `cluster-name` 不同(现有：cluster1 和 cluster2)
> 2. 集群 `cluster-user` 不同(现有：kube-admin-cluster1 和 kube-admin-cluster2)s备份config文件

1. cluster1 和 cluster2 备份config文件

   ```bash
   cp ~/.kube/config ~/.kube/config.bak
   ```

2. 拷贝 cluster2 config 文件到 cluster1 节点上

   ```bash
   scp ~/.kube/config root@cluster1:/root
   ```

3. 合并配置文件

   ```bash
   KUBECONFIG=~/.kube/config:/root/config kubectl config view --flatten > /tmp/config
   ```

4. 替换 cluster1 config 文件

   ```bash
   \mv /tmp/config ~/.kube/config
   ```

5. 测试，在 cluster1 上切换到 cluster2

   ```bash
   kubectl config use-context cluster2
   ```

   
