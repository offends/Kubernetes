> 本文作者：丁辉

# Prometheus监控自动发现服务

> 还在学习中记录一下一些常用参数

```yaml
global:
  scrape_interval: 15s  # 全局的抓取间隔时间

scrape_configs:
  - job_name: 'rbd-chaos'  # 作业名称，可自定义
    kubernetes_sd_configs:
      - role: endpoints  # 使用Kubernetes服务发现
    relabel_configs:
      - source_labels: [__meta_kubernetes_namespace, __meta_kubernetes_service_name]
        regex: rbd-system;rbd-chaos  # 匹配命名空间和服务名
        action: keep  # 仅保留匹配的端点
      - source_labels: [__address__]
        target_label: component
        replacement: builder  # 添加component标签为"builder"
      - source_labels: [__address__]
        target_label: service_name
        replacement: builder  # 添加service_name标签为"builder"
```

