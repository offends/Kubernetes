> 本文作者：丁辉

# 阿里云盘配合容器化客户端使用

[官方仓库](https://github.com/tickstep/aliyunpan)

阿里云盘同步备份功能，支持备份本地文件到云盘，备份云盘文件到本地，双向同步备份三种模式。支持JavaScript插件对备份文件进行过滤。

备份功能支持以下三种模式：

1. 备份本地文件，即上传本地文件到网盘，始终保持本地文件有一个完整的备份在网盘
2. 备份云盘文件，即下载网盘文件到本地，始终保持网盘的文件有一个完整的备份在本地
3. 双向备份，保持网盘文件和本地文件严格一致

备份功能一般用于NAS等系统，进行文件备份。比如备份照片，就可以使用这个功能定期备份照片到云盘，十分好用。

1. 创建命名空间

   ```bash
   kubectl create ns aliyunpan
   ```

2. 打开 [阿里云盘网页](https://www.aliyundrive.com/drive/file/backup) 右键打开检查,点击 Console, 输入此命令获取值

   ```bash
   JSON.parse(localStorage.getItem("token")).refresh_token
   ```

3. 创建 Secret

   ```bash
   kubectl create secret generic aliyunpan-token --from-literal=ALIYUNPAN_REFRESH_TOKEN=${值} -n aliyunpan
   ```

4. 部署

   ```bash
   kubectl create --edit -f https://gitee.com/offends/Kubernetes/raw/main/File/Yaml/aliyun-sync.yaml
   ```

   > 注意修改变量 `ALIYUNPAN_PAN_DIR`

**参数解释**

- ALIYUNPAN_PAN_DIR：云盘目录
- ALIYUNPAN_REFRESH_TOKEN：RefreshToken
- ALIYUNPAN_SYNC_MODE：备份模式，支持三种: upload(备份本地文件到云盘),download(备份云盘文件到本地),sync(双向同步备份)
