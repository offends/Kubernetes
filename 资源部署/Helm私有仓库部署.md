> 本文作者：丁辉

# Helm私有仓库部署

## 开始部署

1. 部署

   ```bash
   kubectl create -f https://gitee.com/offends/Kubernetes/raw/main/File/Yaml/chartmuseum.yaml
   ```

2. 配置内部域名

   ```bash
   echo $(kubectl get svc -n helm | grep chartmuseum | awk '{print $3}') chartmuseum.local.com  >> /etc/hosts
   ```

3. 检查

   ```bash
   curl http://chartmuseum.local.com:8080/api/charts
   ```

## 使用

1. 添加仓库

   ```bash
   helm repo add localrepo http://chartmuseum.local.com:8080
   ```

2. 创建 Helm 文件

   ```bash
   helm create demo
   ```

3. 打包

   - 打包：不指定版本 ， 默认生成 0.1.0 版本

     ```bash
     helm package demo/ 
     ```

   - 打包： 指定版本 0.1.1

     ```bash
     helm package demo/ --version 0.1.1
     ```

4. 查看详情

   ```bash
   helm show chart demo
   ```

## Helm push 插件操作命令

1. 添加公共仓库

   ```bash
   helm repo add stable http://mirror.azure.cn/kubernetes/charts   
   ```

   > 也可以换成微软的源，速度快，内容和官方同步的 
   >
   > ```bash
   > helm repo add aliyun https://kubernetes.oss-cn-hangzhou.aliyuncs.com/charts
   > ```

2. Helm安装 push 插件

   ```bash
   helm plugin install https://github.com/chartmuseum/helm-push
   ```

3. Cm-push chats包到Chartmuseum

   - 添加 cm-push 插件

     ```bash
     helm plugin install https://github.com/chartmuseum/helm-push
     ```

   - 使用 cm-push 上传 chat 包

     ```bash
     helm cm-push demo-0.1.0.tgz localrepo
     ```

   - 使用 cm-push 上传 chat 目录

     ```bash
     helm cm-push ./demo localrepo
     ```

4. 查看结果

   ```bash
   curl http://chartmuseum.local.com:8080/api/charts |jq
   ```

5. 更新

   ```bash
   helm repo update
   ```

6. 检查

   ```bash
   helm search repo demo
   ```

## 复用harbor仓库

>  harbor 自带 helm 私有仓库的功能，不需要再部署一个helm 私有仓库，在这里给大家介绍一下 helm 如何上传chart包到 harbor

1. 添加私有仓库harbor

   添加 repo

   ```bash
   helm repo add harbor https://harbor.com/chartrepo/library --username xxx --password xxx
   ```

2. 使用 cm-push 命令 上传 chat 包

   ```bash
   helm cm-push demo-0.1.0.tgz harbor
   ```

3. 上传 cm-push chat 目录

   ```bash
   helm cm-push ./demo harbor
   ```

4. push chats 包到 harbor 使用 oci 协议

   登录到注册中心

   ```bash
   helm registry login -u helmchart harbor.com -p xxxxxx
   ```

5. 使用 push 命令上传 chat 包

   ```bash
   helm push demo-0.1.0.tgz oci://harbor.com/helmchart
   ```

6. 使用 pull 下载包

   ```bash
   helm pull oci://harbor.com/helmchart/mychart --version 0.1.0
   ```

7. 查看 chat 信息

   ```bash
   helm show all oci://harbor.com/helmchart/mychart --version 0.1.0
   ```

## Chartmuseum和curl的使用

### 添加Chartmuseum到Helm repo

- 登录

  ```bash
  helm repo add chartmuseum http://chartmuseum.local.com:8080 --username  admin --password admin
  ```

- 上传

  ```bash
  curl -u admin:admin  --data-binary "@demo-0.1.0.tgz" http://chartmuseum.local.com:8080/api/charts
  ```

- 下载

  ```bash
  curl -O -u admin:admin http://chartmuseum.local.com:8080/charts/demo-0.1.0.tgz
  ```

### chartmuseum其他API

- `GET /index.yaml` 得到 chartmuseum 的全部 charts

  ```bash
  curl http://chartmuseum.local.com:8080/index.yaml -u admin:admin
  ```

- `GET /charts/demo-0.1.0.tgz` 下载 charts 中的 demo

  ```bash
  curl -O http://chartmuseum.local.com:8080/charts/demo-0.1.0.tgz -u admin:admin
  ```

- `POST /api/charts` 上传一个新的chart版本

  ```bash
  curl -X POST  --data-binary '@demo-0.2.0.tgz'  http://chartmuseum.local.com:8080/api/charts -u admin:admin
  ```

- `DELETE /api/charts/<name>/<version>` 删除一个 chart 版本

  ```bash
  curl  -s -X DELETE  http://chartmuseum.local.com:8080/api/charts/demo/0.2.0 -u admin:admin | jq
  ```

- `GET /api/charts` 列出所有的charts

  ```bash
  curl -s  http://chartmuseum.local.com:8080/api/charts -u admin:admin | jq
  ```

- `GET /api/chatts/<name>` 列出chart的所有版本

  ```bash
  curl -s  http://chartmuseum.local.com:8080/api/charts/demo -u admin:admin | jq
  ```

- `GET /api/charts/<name>/<version>` 对一个chart版本的描述

  ```bash
  curl -s  http://chartmuseum.local.com:8080/api/charts/demo/0.2.0 -u admin:admin | jq
  ```

- `GET /health` return 200 OK

  ```bash
  curl  http://chartmuseum.local.com:8080/health
  ```

  

