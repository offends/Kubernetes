> 本文作者：丁辉

# Kubernetes部署Registry镜像仓库缓存服务

1. 下载 YAMl 文件

   ```bash
   wget https://gitee.com/offends/Kubernetes/raw/main/File/Yaml/registry-proxy-ds.yaml
   wget https://gitee.com/offends/Kubernetes/raw/main/File/Yaml/registry-proxy-ingress.yaml
   ```

2. 修改 registry-proxy.yaml 文件

   - 修改 PersistentVolumeClaim storageClassName 字段
   - 修改 DaemonSet env 字段

3. 修改 ingress.yaml 文件

   - 修改 ingressClassName 字段

4. 部署服务

   ```bash
   kubectl apply -f registry-proxy-ds.yaml
   ```

5. 部署对外访问 Ingress

   > 请根据自己环境情况修改 `registry-proxy-ingress.yaml`

   ```bash
   kubectl apply -f registry-proxy-ingress.yaml
   ```

   