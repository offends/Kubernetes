> 本文作者：丁辉

# Chatgpt-Pandora

> 丝滑访问 Chatgpt 方法如下

1. 部署

   ```bash
   kubectl create -f https://gitee.com/offends/Kubernetes/raw/main/File/Yaml/chatgpt-pandora.yaml
   ```

2. 创建 Secret

   ```bash
   kubectl create secret tls chat-tls --key nginx.key --cert nginx.pem -n tools
   ```

3. 创建 Ingress

   ```bash
   vi chatgpt-pandora-ingress.yaml
   ```

   内容如下

   ```yaml
   apiVersion: networking.k8s.io/v1
   kind: Ingress
   metadata:
     name: chatgpt-ingress
     namespace: chatgpt
   spec:
     ingressClassName: nginx
     rules:
       - host: #域名
         http:
           paths:
             - pathType: Prefix
               backend:
                 service:
                   name: chatgpt-web
                   port:
                     number: 80
               path: /
     tls:
       - hosts:
         - #域名
         secretName: chat-ssl
   ```

4. 部署

   ```bash
   kubectl apply -f chatgpt-pandora-ingress.yaml
   ```

5. 获取

   > 在官方获取 accessToken、或登录账号访问 ChatGpt

   ```bash
   https://chat.openai.com/api/auth/session
   ```

   
