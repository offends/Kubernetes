> 本文作者：丁辉

# 部署Kubernetes仪表板

[官方文档](https://kubernetes.io/zh-cn/docs/tasks/access-application-cluster/web-ui-dashboard/)

[Github文档](https://github.com/kubernetes/dashboard/blob/master/docs/user/access-control/creating-sample-user.md)

1. 部署 Dashboard UI

   ```bash
   kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.7.0/aio/deploy/recommended.yaml
   ```

2. 编辑对外 svc 文件

   ```bash
   vi kubernetes-dashboard-svc.yaml
   ```

   内容如下

   ```yaml
   apiVersion: v1
   kind: Service
   metadata:
     name: kubernetes-dashboard-40000
     labels:
       k8s-app: kubernetes-dashboard
     namespace: kubernetes-dashboard
   spec:
     ports:
     - name: kubernetes-dashboard
       nodePort: 40000
       port: 443
       protocol: TCP
       targetPort: 8443
     selector:
       k8s-app: kubernetes-dashboard
     type: NodePort
   ```

3. 部署

   ```bash
   kubectl apply -f kubernetes-dashboard-svc.yaml
   ```

4. 查看状态

   ```bash
   kubectl get all -n kubernetes-dashboard
   ```

5. 获取长期令牌

   ```bash
   vi kubernetes-dashboard-token.yaml
   ```

   内容如下

   ```yaml
   apiVersion: v1
   kind: ServiceAccount
   metadata:
     name: admin-user
     namespace: kubernetes-dashboard
   ---
   apiVersion: rbac.authorization.k8s.io/v1
   kind: ClusterRoleBinding
   metadata:
     name: admin-user
   roleRef:
     apiGroup: rbac.authorization.k8s.io
     kind: ClusterRole
     name: cluster-admin
   subjects:
   - kind: ServiceAccount
     name: admin-user
     namespace: kubernetes-dashboard
   ---
   apiVersion: v1
   kind: Secret
   metadata:
     name: admin-user
     namespace: kubernetes-dashboard
     annotations:
       kubernetes.io/service-account.name: "admin-user"   
   type: kubernetes.io/service-account-token  
   ```

6. 创建

   ```bash
   kubectl apply -f kubernetes-dashboard-token.yaml
   ```

7. 查看 token

   ```bash
   kubectl get secret admin-user -n kubernetes-dashboard -o jsonpath={".data.token"} | base64 -d
   ```

   