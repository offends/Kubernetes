> 本文作者：丁辉

# Kubernetes部署Simplex服务器SMP/XFTP服务

## 开始部署

1. 创建命名空间

   ```bash
   kubectl create namespace simplex
   ```

2. 创建 Secret

   ```bash
   kubectl create secret generic simplex-secret --from-literal=ADDR=${值} --from-literal=PASS=${值} -n simplex
   ```

   **`ADDR`**:	用于指定服务绑定的 IP 地址或域名。这通常是服务对外公开访问的地址。

   **`PASS`**:	这是用于设置某种形式的密码或访问密钥的变量。它通常用于身份验证或授权，以保护服务不被未经授权的访问。

3. 部署 SMP 服务

   ```bash
   kubectl apply -f https://gitee.com/offends/Kubernetes/raw/main/File/Yaml/simplex-smp.yaml
   ```

4. 部署 XFTP 服务

   ```bash
   kubectl apply -f https://gitee.com/offends/Kubernetes/raw/main/File/Yaml/simplex-xftp.yaml
   ```

5. 部署对外访问端口

   - SMP：5223
   - XFTP：5233

## 卸载

1. 卸载

   ```bash
   kubectl delete -f https://gitee.com/offends/Kubernetes/raw/main/File/Yaml/simplex-xftp.yaml
   ```

2. 卸载

   ```bash
   kubectl delete -f https://gitee.com/offends/Kubernetes/raw/main/File/Yaml/simplex-smp.yaml
   ```

3. 删除 Secret

   ```bash
   kubectl delete secret simplex-secret -n simplex
   ```

   