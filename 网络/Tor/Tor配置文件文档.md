> 本文作者：丁辉

# Tor配置文件文档

- Tor连接VPN使用

  **代理工具**：[Helm部署Shadowsocks-Rust](https://gitee.com/offends/Kubernetes/blob/main/Helm/Helm%E9%83%A8%E7%BD%B2Shadowsocks-Rust.md)

  ```bash
  # vi /etc/tor/torrc
  Log notice file /var/log/tor/notices.log
  SOCKSPort 0
  HiddenServiceNonAnonymousMode 1
  HiddenServiceSingleHopMode 1
  # 配置代理
  Socks5Proxy sslocal-rust:1080
  HiddenServiceDir /var/lib/tor/nginx
  HiddenServicePort 80 192.168.1.10:80
  ```

- 