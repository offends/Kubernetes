> 本文作者：丁辉

# Docker+IPSEC+FRP实现内网穿透

| 服务器名称 |      IP      |
| :--------: | :----------: |
| 公网服务器 | 192.168.1.10 |
| 内网服务器 | 192.168.1.20 |

## 部署IPSEC

[IPSEC官方文档](https://github.com/hwdsl2/docker-ipsec-vpn-server/blob/master/README-zh.md)

[高级参数配置](https://github.com/hwdsl2/docker-ipsec-vpn-server/blob/master/docs/advanced-usage-zh.md)

1. 配置用户密码以及密钥

   ```bash
   mkdir -p /opt/vpn
   ```

   ```bash
   cat > /opt/vpn/vpn.env << EOF
   VPN_IPSEC_PSK=demo
   VPN_USER=demo
   VPN_PASSWORD=demo
   EOF
   ```

   > 将 demo 替换成自己的 KEY、USER、PSSWORD

2. 创建网络

   ```bash
   docker network create vpn
   ```

3. 启动容器

   ```bash
   docker run \
     --network=vpn \
     --name ipsec-vpn-server \
     --restart=always \
     -v "/opt/vpn/vpn.env:/opt/src/env/vpn.env:ro" \
     -v /lib/modules:/lib/modules:ro \
     -d --privileged \
     hwdsl2/ipsec-vpn-server
   ```

   > 国内镜像地址
   >
   > ```bash
   > registry.cn-hangzhou.aliyuncs.com/offends/frp:ipsec-vpn-server
   > ```

## 部署FRP

### 本地客户端配置 Frpc

1. 编写配置文件

   ```bash
   cat > /opt/vpn/frpc.ini << EOF
   [common]
   server_addr = 192.168.1.10 #填写frps的IP
   server_port = 7000 #填写frps的port
   
   [ipsec-500]
   type = udp
   local_ip = ipsec-vpn-server
   local_port = 500
   remote_port = 500
   
   [ipsec-4500]
   type = udp
   local_ip = ipsec-vpn-server
   local_port = 4500
   remote_port = 4500
   EOF
   ```

2. 启动容器

   ```bash
   docker run --name frpc --restart=always \
     --net=vpn \
     -v "/opt/vpn/frpc.ini:/frp/frpc.ini:ro" \
     -d registry.cn-hangzhou.aliyuncs.com/offends/frp:frpc
   ```

### 公网服务端配置 Frps Server

1. 编辑配置文件

   ```bash
   mkdir /opt/vpn
   ```

   ```bash
   cat > /opt/vpn/frps.ini << EOF
   [common]
   bind_port = 7000
   EOF
   ```

2. 启动容器

   ```bash
   docker run --name frps --restart=always \
       -p 7000:7000 \
       -p 500:500/udp \
       -p 4500:4500/udp \
       -v "/opt/vpn/frps.ini:/frp/frps.ini:ro" \
       -d registry.cn-hangzhou.aliyuncs.com/offends/frp:frps
   ```

## 卸载清理

1. 本地客户端清理

   ```bash
   docker rm -f frpc
   docker rm -f ipsec-vpn-server
   docker network rm vpn
   rm -rf /opt/vpn
   ```

2. 公网服务端清理

   ```bash
   docker rm -f frps
   rm -rf /opt/vpn
   ```

## 电脑连接

- MAC电脑配

  - 置打开 VPN 配置，添加使用 L2TP/IPSec 协议

    <img src="https://minio.offends.cn:9000/offends/images/image-20230909220520651.png" style="zoom: 30%;" />

  - 要记得勾选通过VPN连接发送所有流量

    <img src="https://minio.offends.cn:9000/offends/images/image-20240210160920630.png" style="zoom: 30%;" />

- Windows电脑配置

  - 打开设置，添加 VPN 连接，使用 L2TP/IPSec 协议

    <img src="https://minio.offends.cn:9000/offends/images/image-20230909221041694.png" style="zoom: 30%;" />

