#!/bin/bash

# 填写服务器密码
PASSWORD=""

# 获取需要免密服务器IP地址, 自动排除 hosts 文件内 # [] 行
IP_ADDR=$(
    grep -vE '^\[|\]' /etc/ansible/hosts | grep -v '#' | sed '/^$/d'
)

. /etc/init.d/functions
# 一键生成密钥
if ! [ -f ~/.ssh/id_rsa.pub ];then
    ssh-keygen -t rsa -P '' -f ~/.ssh/id_rsa >/dev/null 2>&1  
    echo -e "\033[32m======Local=========\033[0m"
    action "Generate the key!"  /bin/true
fi

# 批量发送密钥
   for i in $IP_ADDR;do
      sshpass -p$PASSWORD ssh-copy-id -i /root/.ssh/id_rsa.pub -o StrictHostKeyChecking=no ${i} >/dev/null 2>&1

      if [ $? == 0 ];then
          echo -e "\033[32m=========`ssh $i hostname`==========\033[0m"
          action  "发送成功!!!" /bin/true
      else
          echo -e "\033[31m======$i=======\033[0m"
          action  "发送失败!!!" /bin/false
      fi
   done