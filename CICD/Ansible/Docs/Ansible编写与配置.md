> 本文作者：丁辉

# Ansible编写与配置

- 第一步禁用SSH主机密钥检查

  ```bash
  vi /etc/ansible/ansible.cfg
  ```

  解除 `host_key_checking` 注释

  ```bash
  [defaults]
  host_key_checking = False
  ```

  > 通常，当你连接到一个新的SSH主机时，SSH客户端会询问你是否要接受该主机的公钥。这个公钥将被保存到`known_hosts`文件中，以便将来的连接可以验证主机的身份。将`host_key_checking`设置为`False`会关闭这个验证步骤。

