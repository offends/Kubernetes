> 本文作者：丁辉

# Ansible 加密

- 创建加密文件

  ```bash
  ansible-vault create password.yml
  ```

  > 示例
  >
  > ```bash
  > [root@offends]# ansible-vault create password.yml
  > New Vault password: # 输入加密密码
  > Confirm New Vault password: # 二次输入加密密码
  > ```

- 指定文件加密

  ```bash
  ansible-vault encrypt /etc/ansible/hosts
  ```

- 加密字符串

  ```bash
  ansible-vault encrypt_string 123456
  ```

- 编辑加密文件

  ```bash
  ansible-vault edit password.yml
  ```

- 重新加密文件

  ```bash
  ansible-vault rekey password.yml
  ```

- 文件解密

  ```bash
  ansible-vault decrypt password.yml
  ```

- 查看加密数据文件原文

  ```bash
  ansible-vault view password.yml
  ```

# 剧本的使用

- 编写一份 `demo.yml` 剧本文件

  ```bash
  vi demo.yml
  ```

  ```yml
  ---
  - hosts: node1
    # 定义变量
    vars:
      - user_password: !vault |
            $ANSIBLE_VAULT;1.1;AES256
            36616162626462323130626563393433663637383166613262333433313534386561666531633837
            3663636662663363303463313662333064326537343563340a653566346636333633383163623662
            37386432626437636464386339316366346665383935336564623630333238353661666566343036
            3338613861393061320a626464306230626265656163613730303035613161616235373539613333
            6164
  
    tasks:
      - name: display variable from encryption variable 
        ansible.builtin.debug:
          msg: The user password is {{ user_password }}
  ```

- 通过询问口令执行剧本

  ```bash
  ansible-playbook demo.yml -v --ask-vault-pass
  ```

- 从密码文件中读取口令执行剧本

  ```bash
  echo '密钥密码' > .pwdfile && chmod 600 .pwdfile
  ```

  ```bash
  ansible-playbook demo.yml -v --vault-id .pwdfile
  ```

# 加密用户密码

- 创建变量文件

  ```bash
  mkdir vars -p
  vi vars/user_list.yml
  ```

  ```yml
  user_hosts:
    - all
  user_info:
    - user: demo
      # 密码需要用引号括起来,避免纯数字密码被解析成int类型数字
      password: "123456"
      # 备注信息可以使用中文，但尽量不用中文
      comment: "hello"
  ```

- 创建剧本文件

  ```bash
  vi user.yml
  ```

  ```yml
  - hosts: "{{ user_hosts }}"
    vars_files:
      - demo.yml
    tasks:
      - name: display variable from variable list
        ansible.builtin.debug:
          msg: |
            The username is "{{ item.user }}",
            the password is "{{ item.password }}",
            the comment is "{{ item.comment }}".
        loop: "{{ user_info }}"
      - name: create users
        ansible.builtin.user:
          name:  "{{ item.user }}"
          password: "{{ item.password|password_hash('sha512') }}"
          comment: "{{ item.comment }}"
          state: present
        loop: "{{ user_info }}"
        become: yes
  ```

- 加密变量文件

  ```bash
  ansible-vault encrypt vars/user_list.yml
  ```

- 执行剧本

  ```bash
  ansible-playbook user.yml -v --ask-vault-pass
  ```

- 查看是否创建用户

  ```bash
  tail -n 1 /etc/passwd
  ```

  
