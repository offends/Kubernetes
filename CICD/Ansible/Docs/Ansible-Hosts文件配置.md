> 本文作者：丁辉

# Ansible-Hosts文件配置

**编写 `/etc/ansible/hosts` 文件**

- 普通写法

  ```bash
  [group1] # 主机组
  
  192.168.1.10
  192.168.1.20
  
  [group2] # 主机组
  
  192.168.1.30
  192.168.1.40
  ```

- 为主机配置变量参数

  ```bash
  [group1] # 主机组
  
  192.168.1.10 ansible_ssh_port="22" ansible_ssh_user="root" ansible_ssh_pass=""
  192.168.1.20 ansible_ssh_port="22" ansible_ssh_user="root" ansible_ssh_pass=""
  
  [group2] # 主机组
  
  192.168.1.30 ansible_ssh_port="22" ansible_ssh_user="root" ansible_ssh_pass=""
  192.168.1.40 ansible_ssh_port="22" ansible_ssh_user="root" ansible_ssh_pass=""
  ```

- 为主机组配置变量参数

  ```bash
  [group1] # 主机组
  
  192.168.1.10
  192.168.1.20
  
  [group2] # 主机组
  
  192.168.1.30
  192.168.1.40
  
  [group1:vars]
  
  ansible_ssh_port="22" # 填写服务器端口
  
  ansible_ssh_user="root" # 填写服务器用户
  
  ansible_ssh_pass="" # 填写服务器密码
  
  [group2:vars]
  
  ansible_ssh_port="22" # 填写服务器端口
  
  ansible_ssh_user="root" # 填写服务器用户
  
  ansible_ssh_pass="" # 填写服务器密码
  ```

- 为所有主机组配置变量参数

  ```bash
  [group1] # 主机组
  
  192.168.1.10
  192.168.1.20
  
  [group2] # 主机组
  
  192.168.1.30
  192.168.1.40
  
  [all:vars] # 指定所有主机组
  
  ansible_ssh_port="22" # 填写服务器端口
  
  ansible_ssh_user="root" # 填写服务器用户
  
  ansible_ssh_pass="" # 填写服务器密码
  ```

  

