> 本文作者：丁辉

# Ansible 配置免密

**配置主机**

```bash
vi /etc/ansible/hosts
```

```bash
[ssh]

192.168.1.10
```

## SHELL配置免密

- 执行脚本

  ```bash
  bash /etc/ansible/ssh-copy.sh
  ```

  > 脚本会自动获取 `/etc/ansible/hosts` 内地址信息进行免密

## YML 配置服务器免密登录

- 编辑 SSH 主机配置文件填写服务器信息

  ```bash
  vi /etc/ansible/ssh-hosts
  ```

  > 内容如下
  >
  > ```yml
  > [ssh] # 主机组
  > 
  > # 192.168.1.[10:11] # 批量
  > # 192.168.1.10 # 单台
  > 192.168.1.10
  > 
  > [ssh:vars] # 指定主机组中的共享变量
  > 
  > ansible_ssh_user="root" # 填写服务器用户
  > 
  > ansible_ssh_pass="" # 填写服务器密码
  > ```

- 执行免密

  ```bash
  ansible-playbook -i /etc/ansible/ssh-hosts /etc/ansible/key.yml
  ```

- 验证

  ```bash
  ansible all -a ls
  ```

