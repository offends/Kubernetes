> 本文作者：丁辉

# Ansible模块参数解释以及常用命令

> 太多了，只记少数吧，累～

## Ansible 基础命令

- 查看 Ansible 的可用模块

  ```bash
  ansible-doc -l
  ```

- 测试主机的连通性

  ```bash
  ansible all -m ping
  ```

- 查看组下所有的IP

  ```bash
  ansible all --list
  ```
  
> 使用 -i 指定 hosts 文件位置
>
> ```bash
> -i /etc/ansible/hosts 
> ```

## Ansible-Playbook 命令

- 执行 Playbook

  ```bash
  ansible-playbook demo.yml
  ```

- 指定任务执行 Playbook

  ```bash
  ansible-playbook demo.yml --start-at-task="指定任务"
  ```

## Ansible 常用模块 Command

**常用参数**

| 参数              | 解释                                                |
| ----------------- | --------------------------------------------------- |
| command           | 要在远程主机上执行的命令。                          |
| creates           | 如果指定的文件或目录存在，则命令不会被执行。        |
| removes           | 如果指定的文件或目录不存在，则命令不会被执行。      |
| chdir             | 在执行命令之前，将当前工作目录更改为指定的目录。    |
| executable        | 用于执行命令的可执行文件的路径。                    |
| warn              | 控制命令失败时的错误消息显示方式。                  |
| stdin             | 将输入发送到命令的标准输入。                        |
| stdin_add_newline | 在发送到标准输入之前是否添加新行。                  |
| strip_empty_ends  | 控制在输出中删除空行的方式。                        |
| free_form         | 允许直接传递要执行的命令，无需使用`command`关键字。 |

- 先切换目录，执行命令

  ```bash
  ansible all -m command -a 'chdir=/mnt ls -la'
  ```

- 检查节点的内存情况

  ```bash
  ansible -m command -a "free -m " 'all'
  ```

## Ansible 常用模块 Shell

**常用参数**

| 参数              | 解释                                             |
| ----------------- | ------------------------------------------------ |
| cmd               | 要在远程主机上执行的命令。                       |
| creates           | 如果指定的文件或目录存在，则命令不会被执行。     |
| removes           | 如果指定的文件或目录不存在，则命令不会被执行。   |
| chdir             | 在执行命令之前，将当前工作目录更改为指定的目录。 |
| executable        | 用于执行命令的可执行文件的路径。                 |
| warn              | 控制命令失败时的错误消息显示方式。               |
| stdin             | 将输入发送到命令的标准输入。                     |
| stdin_add_newline | 在发送到标准输入之前是否添加新行。               |
| strip_empty_ends  | 控制在输出中删除空行的方式。                     |
| free_form         | 允许直接传递要执行的命令，无需使用`cmd`关键字。  |
| executable        | 用于执行命令的可执行文件的路径。                 |
| creates           | 如果指定的文件或目录存在，则命令不会被执行。     |
| removes           | 如果指定的文件或目录不存在，则命令不会被执行。   |

- 使用 /bin/bash 执行命令

  ```bash
  ansible all -m shell -a 'executable=/bin/bash ls -la /mnt'
  ```

## Ansible 常用模块 Script

**常用参数**

| 参数              | 解释                                                   |
| ----------------- | ------------------------------------------------------ |
| script            | 要在远程主机上执行的脚本文件的路径。                   |
| creates           | 如果指定的文件或目录存在，则脚本不会被执行。           |
| removes           | 如果指定的文件或目录不存在，则脚本不会被执行。         |
| chdir             | 在执行脚本之前，将当前工作目录更改为指定的目录。       |
| warn              | 控制脚本失败时的错误消息显示方式。                     |
| executable        | 用于执行脚本的解释器的路径。                           |
| stdin             | 将输入发送到脚本的标准输入。                           |
| stdin_add_newline | 在发送到标准输入之前是否添加新行。                     |
| strip_empty_ends  | 控制在输出中删除空行的方式。                           |
| free_form         | 允许直接传递要执行的脚本路径，无需使用`script`关键字。 |

- 编写一个脚本

  ```bash
  cat << EOF > demo.sh
  date
  EOF
  ```

- 执行脚本

  ```bash
  ansible all -m script -a "/etc/ansible/demo.sh"
  ```
  

## Ansible 常用模块 Copy

**常用参数**

| 参数           | 解释                                                         |
| -------------- | ------------------------------------------------------------ |
| src            | 源文件路径。                                                 |
| dest           | 目标文件路径。                                               |
| backup         | 是否备份目标文件。                                           |
| remote_src     | 如果为true，则从控制节点的文件系统复制文件到远程主机。       |
| checksum       | 控制是否使用校验和来判断文件是否需要复制。                   |
| content        | 如果为true，则在Ansible控制节点上生成文件内容并将其传输到远程主机上。 |
| directory_mode | 在创建目录时设置权限模式。                                   |
| force          | 强制复制文件，即使目标文件存在。                             |
| owner          | 设置目标文件的所有者。                                       |
| group          | 设置目标文件的所属组。                                       |
| mode           | 设置目标文件的权限模式。                                     |
| seuser         | 设置目标文件的SELinux用户。                                  |
| serole         | 设置目标文件的SELinux角色。                                  |
| setype         | 设置目标文件的SELinux类型。                                  |
| selevel        | 设置目标文件的SELinux等级。                                  |
| follow         | 是否跟随符号链接。                                           |


- 拷贝文件

  ```bash
  ansible all -m copy -a "src=/etc/ansible/demo.sh owner=root mode=777 dest=/mnt/demo.sh backup=yes"
  ```

## Ansible 常用模块 File

**常用参数**

| 参数           | 解释                                                         |
| -------------- | ------------------------------------------------------------ |
| path           | 文件或目录的路径。                                           |
| state          | 文件或目录的状态。可选值包括：file、directory、link、hard、absent。 |
| owner          | 设置文件或目录的所有者。                                     |
| group          | 设置文件或目录的所属组。                                     |
| mode           | 设置文件或目录的权限模式。                                   |
| dest           | 用于创建符号链接的目标文件路径。                             |
| src            | 用于创建硬链接的源文件路径。                                 |
| state          | 设置文件或目录的状态。                                       |
| follow         | 是否遵循符号链接。                                           |
| recurse        | 是否递归操作，用于目录。                                     |
| directory_mode | 用于设置目录的权限模式。                                     |
| force          | 是否强制执行。                                               |
| selevel        | 设置目标文件的SELinux等级。                                  |
| serole         | 设置目标文件的SELinux角色。                                  |
| setype         | 设置目标文件的SELinux类型。                                  |
| seuser         | 设置目标文件的SELinux用户。                                  |


- 创建文件

  ```bash
  ansible all -m file -a 'path=/mnt/demofile state=touch'
  ```

- 创建目录

  ```bash
  ansible all -m file -a 'path=/mnt/demodir state=directory'
  ```

- 修改文件的所有人

  ```bash
  ansible all -m file -a 'path=/mnt/demofile owner=docker'
  ```

- 删除文件或目录

  ```bash
  ansible all -m file -a 'path=/mnt/demofile state=absent'
  ansible all -m file -a 'path=/mnt/demodir state=absent'
  ```

## Ansible 常用模块 Archive

**常用参数**

| 参数        | 解释                                                   |
| ----------- | ------------------------------------------------------ |
| path        | 要打包的文件或目录的路径。                             |
| dest        | 打包文件的目标路径，包括文件名和扩展名。               |
| format      | 打包格式，可选值包括：tar、gz、bz2、tar.gz、tar.bz2。  |
| owner       | 设置打包文件的所有者。                                 |
| group       | 设置打包文件的所属组。                                 |
| mode        | 设置打包文件的权限模式。                               |
| remote_src  | 如果为true，则从控制节点的文件系统打包文件到远程主机。 |
| remove_path | 在归档中排除的路径的前缀。                             |
| exclude     | 排除指定的文件或目录。                                 |
| extra_opts  | 传递给归档命令的额外选项。                             |

- 打包文件

  ```bash
  ansible all -m archive -a "path=/mnt/demo.sh dest=/mnt/demo.gz format=gz"
  ```

  > 解压使用
  >
  > ```bash
  > gzip -d demo.gz
  > ```

## Ansible 常用模块 Cron

**常用参数**

| 参数         | 解释                                                         |
| ------------ | ------------------------------------------------------------ |
| name         | Cron任务的名称。                                             |
| minute       | 设置分钟字段。可接受数字（0-59）、星号（*，表示每分钟）或范围（例如：0-30，表示0到30分钟之间）。 |
| hour         | 设置小时字段。可接受数字（0-23）、星号（*，表示每小时）或范围。 |
| day          | 设置天字段。可接受数字（1-31）、星号（*，表示每天）或范围。  |
| month        | 设置月份字段。可接受数字（1-12）、星号（*，表示每月）或范围。 |
| weekday      | 设置星期字段。可接受数字（0-6，0代表星期日）、星号（*，表示每天）或范围。 |
| job          | Cron任务要执行的命令或脚本。                                 |
| state        | 设置Cron任务的状态。可选值包括：present（存在）或absent（不存在）。 |
| disabled     | 是否禁用Cron任务。如果设置为yes，则Cron任务将被禁用。        |
| user         | 设置Cron任务的所有者。                                       |
| minute       | 设置分钟字段。                                               |
| hour         | 设置小时字段。                                               |
| day          | 设置天字段。                                                 |
| month        | 设置月份字段。                                               |
| weekday      | 设置星期字段。                                               |
| special_time | 设置特殊时间值，如@reboot（重启时运行）或@daily（每天运行）。 |

- 生成定时任务

  ```bash
  ansible all -m cron -a 'job="echo hello" name=demo minute=*/2'
  ```

- 禁止该定时任务

  ```bash
  ansible all -m cron -a 'job="echo hello" name=demo minute=*/2 disabled=yes'
  ```

- 重新开启该定时任务

  ```bash
  ansible all -m cron -a 'job="echo hello" name=demo minute=*/2 disabled=no' 
  ```

- 删除定时任务

  ```bash
  ansible all -m cron -a 'job="echo hello" name=demo minute=*/2 state=absent'
  ```

## Ansible 常用模块 Yum

**常用参数**

| 参数              | 解释                                                    |
| ----------------- | ------------------------------------------------------- |
| name              | 指定要安装或删除的软件包名称。                          |
| state             | 指定软件包的状态。可选值包括：present、latest、absent。 |
| enablerepo        | 指定要启用的存储库。                                    |
| disablerepo       | 指定要禁用的存储库。                                    |
| disable_excludes  | 指定要禁用的软件包类型。                                |
| update_cache      | 是否更新缓存。                                          |
| disable_gpg_check | 是否禁用GPG检查。                                       |
| installroot       | 安装软件包的根目录。                                    |
| security          | 是否仅安装安全更新。                                    |
| bugfix            | 是否只安装修复错误的更新。                              |
| enhancement       | 是否只安装增强更新。                                    |
| ts                | 是否使用指定的事务ID。                                  |

- 安装软件

  ```bash
  ansible all -m yum -a "name=wget state=present"
  ```

- 卸载软件

  ```bash
  ansible all -m yum -a "name=wget state=absent"
  ```

## Ansible 常用模块 Dnf

**常用参数**

| 参数              | 解释                                                    |
| ----------------- | ------------------------------------------------------- |
| name              | 指定要安装或删除的软件包名称。                          |
| state             | 指定软件包的状态。可选值包括：present、latest、absent。 |
| enablerepo        | 指定要启用的存储库。                                    |
| disablerepo       | 指定要禁用的存储库。                                    |
| disable_excludes  | 指定要禁用的软件包类型。                                |
| update_cache      | 是否更新缓存。                                          |
| disable_gpg_check | 是否禁用GPG检查。                                       |
| installroot       | 安装软件包的根目录。                                    |
| security          | 是否仅安装安全更新。                                    |
| bugfix            | 是否只安装修复错误的更新。                              |
| enhancement       | 是否只安装增强更新。                                    |
| ts                | 是否使用指定的事务ID。                                  |

## Ansible 常用模块 Service

**常用参数**

| 参数          | 解释                                                         |
| ------------- | ------------------------------------------------------------ |
| name          | 指定要管理的服务名称。                                       |
| state         | 指定服务的状态。可选值包括：started、stopped、restarted、reloaded、enabled、disabled。 |
| enabled       | 是否设置服务开机自启动。                                     |
| pattern       | 用于匹配要操作的服务的模式。                                 |
| sleep         | 在重新启动服务之前等待的时间（以秒为单位）。                 |
| arguments     | 传递给服务启动脚本的额外参数。                               |
| runlevel      | 设置服务在特定运行级别下的启用状态。                         |
| daemon_reload | 在重启或重新加载服务之后是否重新加载守护程序。               |
| force         | 是否强制执行操作。                                           |

- 重启服务

  ```bash
  ansible all -m service -a "name=docker state=restarted"
  ```

## Ansible 常用模块 User

**常用参数**

| 参数            | 解释                                      |
| --------------- | ----------------------------------------- |
| name            | 用户名。                                  |
| state           | 用户的状态。可选值包括：present、absent。 |
| uid             | 用户的UID。                               |
| password        | 用户的密码。                              |
| group           | 用户所属的主组。                          |
| groups          | 用户所属的其他组。                        |
| append          | 是否追加用户到附加的组中。                |
| shell           | 用户的登录Shell。                         |
| home            | 用户的主目录。                            |
| move_home       | 是否移动用户的主目录。                    |
| createhome      | 是否创建用户主目录。                      |
| system          | 是否创建系统用户。                        |
| update_password | 是否更新密码。                            |
| expire          | 设置用户的账号过期日期。                  |
| remove          | 是否删除用户的主目录。                    |

- 创建用户

  ```bash
  ansible all -m user -a "name=demo state=present"
  ```

- 删除用户

  ```bash
  ansible all -m user -a "name=demo state=absent"
  ```

  

