> 本文作者：丁辉

# DronePlugins构建Docker镜像

## 准备环境

- 克隆仓库到本地

  ```bash
  git clone https://gitea.offends.cn/offends/Kubernetes.git && cd Kubernetes/CICD/Drone
  ```

- 修改 Dockerfile 并上传至自己的仓库

- 进入 Drone Web页面点击 SYNC 同步仓库

- 点击 ACTIVATE REPOSITORY 激活存储库

- 点击 Settings 配置 Secrets

  |     变量名      |       解释       |                          例                           |
  | :-------------: | :--------------: | :---------------------------------------------------: |
  |    REGISTRY     |   镜像仓库地址   |           registry.cn-hangzhou.aliyuncs.com           |
  | DOCKER_USERNAME | 镜像仓库登录用户 |                                                       |
  | DOCKER_PASSWORD | 镜像仓库登录密码 |                                                       |
  |      REPO       |  镜像的仓库名称  | registry.cn-hangzhou.aliyuncs.com/<命名空间>/<镜像名> |

## 插件 plugins/docker 参数

[官网文档](https://plugins.drone.io/plugins/docker)

| 属性                | 类型   | 可选/必选 | 描述                                                         | 默认值       |
| ------------------- | ------ | --------- | ------------------------------------------------------------ | ------------ |
| registry            | 字符串 | 可选      | 身份验证到该注册表的注册表                                   | 无           |
| username            | 字符串 | 可选      | 使用此用户名进行身份验证                                     | 无           |
| password            | 字符串 | 可选      | 使用此密码进行身份验证                                       | 推荐使用密钥 |
| repo                | 字符串 | 可选      | 图像的仓库名称                                               | 无           |
| tags                | 数组   | 可选      | 图像的仓库标签                                               | 无           |
| secret              | 字符串 | 可选      | 使用buildkit将秘密传递给dockerbuild。例如`id=mysecret，src=secret-file` | 无           |
| dockerfile          | 字符串 | 可选      | 要使用的Dockerfile                                           | Dockerfile   |
| dry_run             | 字符串 | 可选      | 如果不应在最后推送Docker镜像，则为布尔值                     | 无           |
| purge               | 布尔值 | 可选      | 如果应在最后清理Docker镜像，则为布尔值                       | true         |
| context             | 字符串 | 可选      | 要使用的上下文路径，默认为git存储库的根目录                  | 无           |
| target              | 字符串 | 可选      | 要使用的构建目标，必须在Docker文件中定义                     | 无           |
| force_tag           | 布尔值 | 可选      | 替换现有匹配的镜像标签                                       | false        |
| insecure            | 布尔值 | 可选      | 启用与此注册表的不安全通信                                   | false        |
| mirror              | 字符串 | 可选      | 使用镜像注册表而不是直接从中央Hub拉取图像                    | 无           |
| bip                 | 布尔值 | 可选      | 用于传递桥接IP                                               | false        |
| custom_dns          | 字符串 | 可选      | 为容器设置自定义DNS服务器                                    | 无           |
| custom_dns_search   | 字符串 | 可选      | Docker守护程序DNS搜索域                                      | 无           |
| storage_driver      | 字符串 | 可选      | 支持`aufs`、`overlay`或`vfs`驱动器                           | 无           |
| storage_path        | 字符串 | 可选      | Docker守护程序存储路径                                       | 无           |
| build_args          | 字符串 | 可选      | 传递给docker build的自定义参数                               | 无           |
| build_args_from_env | 字符串 | 可选      | 将环境变量作为docker build的自定义参数传递                   | 无           |
| auto_tag            | 布尔值 | 可选      | 根据git分支和git标签自动生成标签名称                         | false        |
| auto_tag_suffix     | 字符串 | 可选      | 使用此后缀生成标签名称                                       | 无           |
| debug               | 布尔值 | 可选      | 以详细调试模式启动docker守护进程                             | false        |
| launch_debug        | 布尔值 | 可选      | 以详细调试模式启动docker守护进程                             | false        |
| mtu                 | 字符串 | 可选      | Docker守护程序自定义MTU设置                                  | 无           |
| ipv6                | 字符串 | 可选      | Docker守护程序IPv6网络                                       | 无           |
| experimental        | 布尔值 | 可选      | Docker守护程序实验模式                                       | false        |
| daemon_off          | 布尔值 | 可选      | 不启动docker守护进程                                         | false        |
| cache_from          | 字符串 | 可选      | 考虑作为缓存源的镜像                                         | 无           |
| squash              | 布尔值 | 可选      | 在构建时压缩层                                               | false        |
| pull_image          | 布尔值 | 可选      | 强制在构建时拉取基础镜像                                     | false        |
| compress            | 布尔值 | 可选      | 使用gzip压缩构建上下文                                       | false        |
| custom_labels       | 字符串 | 可选      | 附加的k=v标签                                                | 无           |
| label_schema        | 字符串 | 可选      | label-schema标签                                             | 无           |
| email               | 字符串 | 可选      | Docker电子邮件                                               | 无           |
| no_cache            | 字符串 | 可选      | 不使用缓存的中间容器                                         | 无           |
| add_host            | 字符串 | 可选      | 附加的主机:IP映射                                            | 无           |
| platform            | 字符串 | 可选      | 指定构建输出的目标平台，例如`linux/amd64`、`linux/arm64`或`darwin/amd64` | 无           |
| ssh-agent-key       | 字符串 | 可选      | 用于ssh直通的私钥，参见https://docs.docker.com/engine/reference/commandline/buildx_build/#ssh | 推荐使用密钥 |



# Drone Webhook 通知构建结果

## 准备环境

- 克隆仓库到本地

  ```bash
  git clone https://gitea.offends.cn/offends/Kubernetes.git && cd Kubernetes/CICD/Drone
  ```

- 修改 Dockerfile 并上传至自己的仓库

- 进入 Drone Web页面点击 SYNC 同步仓库

- 点击 ACTIVATE REPOSITORY 激活存储库

- 点击 Settings 修改 Configuration,修改后点击 SAVE CHANGES 保存

  ```bash
  .drone-build-webhook.yml
  ```

- 配置 Secrets

  |   变量名    |        解释         |                        例                        |
  | :---------: | :-----------------: | :----------------------------------------------: |
  | WEBHOOK_URL | 发送 Webhook 的 URL | https://open.feishu.cn/open-apis/bot/v2/hook/*** |

## 插件 plugins/webhook 参数

| 属性名       | 类型    | 必需性   | 描述                     | 默认值           |
| ------------ | ------- | -------- | ------------------------ | ---------------- |
| urls         | string  | required | 发送 Webhook 的 URL      | none             |
| username     | string  | optional | 用于基本身份验证的用户名 | none             |
| password     | string  | optional | 用于基本身份验证的密码   | none             |
| SECRET       | string  |          | 推荐使用的密钥           | none             |
| method       | string  | optional | 请求使用的 HTTP 方法     | POST             |
| content_type | string  | optional | Webhook 的内容类型       | application/json |
| template     | string  | optional | Webhook 的自定义模板     | none             |
| headers      | array   | optional | 自定义标头的映射         | none             |
| skip_verify  | boolean | optional | 跳过 SSL 验证            | false            |
| debug        | boolean | optional | 启用调试信息             | false            |



# Drone Dind 构建 Docker 镜像

## 准备环境

- 克隆仓库到本地

  ```bash
  git clone https://gitea.offends.cn/offends/Kubernetes.git && cd Kubernetes/CICD/Drone
  ```

- 修改 Dockerfile 并上传至自己的仓库

- 进入 Drone Web页面点击 SYNC 同步仓库

- 点击 ACTIVATE REPOSITORY 激活存储库

- 点击 Settings 开启 Trusted (未开启无法挂载Docker守护进程)

- 修改 Configuration,修改后点击 SAVE CHANGES 保存

  ```bash
  .drone-dind.yml
  ```

- 配置 Secrets

  |     变量名      |       解释       |                例                 |
  | :-------------: | :--------------: | :-------------------------------: |
  |    REGISTRY     |   镜像仓库地址   | registry.cn-hangzhou.aliyuncs.com |
  | DOCKER_USERNAME | 镜像仓库登录用户 |                                   |
  | DOCKER_PASSWORD | 镜像仓库登录密码 |                                   |
  |    NAMESPACE    |  镜像的仓库名称  |            <命名空间>             |
  |    IMAGENAME    |     镜像名称     |                                   |
  |    IMAGETAG     |     镜像标签     |                                   |



# Drone 构建多架构 Docker 镜像

## 准备环境

- 克隆仓库到本地

  ```bash
  git clone https://gitea.offends.cn/offends/Kubernetes.git && cd Kubernetes/CICD/Drone
  ```

- 修改 Dockerfile 并上传至自己的仓库

- 进入 Drone Web页面点击 SYNC 同步仓库

- 点击 ACTIVATE REPOSITORY 激活存储库

- 点击 Settings 下 General 修改 Configuration,修改后点击 SAVE CHANGES 保存

  ```bash
  .drone-buildx.yml
  ```

- 配置 Secrets

  |     变量名      |       解释       |              例               |
  | :-------------: | :--------------: | :---------------------------: |
  |    REGISTRY     |   镜像仓库地址   |           docker.io           |
  | DOCKER_USERNAME | 镜像仓库登录用户 |                               |
  | DOCKER_PASSWORD | 镜像仓库登录密码 |                               |
  |      REPO       |  镜像的仓库名称  | docker.io/<命名空间>/<镜像名> |



# Drone Scp 文件到服务器内

## 准备环境

- 克隆仓库到本地

  ```bash
  git clone https://gitea.offends.cn/offends/Kubernetes.git && cd Kubernetes/CICD/Drone
  ```

- 修改 Dockerfile 并上传至自己的仓库

- 进入 Drone Web页面点击 SYNC 同步仓库

- 点击 ACTIVATE REPOSITORY 激活存储库

- 点击 Settings 下 General 修改 Configuration,修改后点击 SAVE CHANGES 保存

  ```bash
  .drone-scp.yml
  ```

- 配置 Secrets

  |  变量名  |        解释        |      例      |
  | :------: | :----------------: | :----------: |
  |   HOST   |    目标主机机器    | 192.168.1.10 |
  |   USER   | 目标主机机器用户名 |     Root     |
  | PASSWORD |  目标主机机器密码  |              |
  |   PORT   |  目标主机机器端口  |      22      |

## 插件 appleboy/drone-scp 参数

[官方文档](https://plugins.drone.io/plugins/scp)

| 属性名               | 类型   | 是否必需 | 描述                         | 默认值 |
| -------------------- | ------ | -------- | ---------------------------- | ------ |
| host                 | 字符串 | 必需     | 目标主机机器。               | 无     |
| port                 | 数字   | 可选     | 目标主机机器端口。           | 22     |
| username             | 字符串 | 必需     | 目标主机机器用户名。         | 无     |
| password             | 字符串 | 必需     | 目标主机机器密码。           | 无     |
| key                  | 字符串 | 可选     | 目标主机机器私钥。建议保密。 | 无     |
| passphrase           | 字符串 | 可选     | 私钥密码。建议保密。         | 无     |
| target               | 字符串 | 必需     | 目标主机机器路径。           | 无     |
| source               | 字符串 | 必需     | 源文件路径。                 | 无     |
| rm                   | 布尔值 | 可选     | 复制后是否删除源文件。       | false  |
| timeout              | 数字   | 可选     | 超时时间（秒）。             | 30     |
| command_timeout      | 数字   | 可选     | 命令超时时间（分钟）。       | 10     |
| strip_components     | 数字   | 可选     | 从目标路径中去除的组件数。   | 0      |
| tar_tmp_path         | 字符串 | 可选     | 用于临时存储tar文件的路径。  | 无     |
| tar_exec             | 字符串 | 可选     | Tar命令。                    | 无     |
| overwrite            | 布尔值 | 可选     | 如果目标文件存在，是否覆盖。 | false  |
| proxy_host           | 字符串 | 可选     | 代理主机。                   | 无     |
| proxy_port           | 数字   | 可选     | 代理端口。                   | 0      |
| proxy_username       | 字符串 | 可选     | 代理用户名。                 | 无     |
| proxy_password       | 字符串 | 可选     | 代理密码。建议保密。         | 无     |
| proxy_key            | 字符串 | 可选     | 代理私钥。建议保密。         | 无     |
| proxy_key_path       | 字符串 | 可选     | 代理私钥路径。               | 无     |
| proxy_key_passphrase | 字符串 | 可选     | 代理私钥密码。建议保密。     | 无     |



# Drone Ssh 操作服务器

## 准备环境

- 克隆仓库到本地

  ```bash
  git clone https://gitea.offends.cn/offends/Kubernetes.git && cd Kubernetes/CICD/Drone
  ```

- 修改 Dockerfile 并上传至自己的仓库

- 进入 Drone Web页面点击 SYNC 同步仓库

- 点击 ACTIVATE REPOSITORY 激活存储库

- 点击 Settings 下 General 修改 Configuration,修改后点击 SAVE CHANGES 保存

  ```bash
  .drone-ssh.yml
  ```

- 配置 Secrets

  |  变量名  |        解释        |      例      |
  | :------: | :----------------: | :----------: |
  |   HOST   |    目标主机机器    | 192.168.1.10 |
  |   USER   | 目标主机机器用户名 |     Root     |
  | PASSWORD |  目标主机机器密码  |              |
  |   PORT   |  目标主机机器端口  |      22      |

## 插件 appleboy/drone-ssh 参数

[官方文档](https://plugins.drone.io/plugins/ssh)

| 属性名          | 类型   | 是否必须 | 描述                         | 默认值         |
| --------------- | ------ | -------- | ---------------------------- | -------------- |
| host            | 字符串 | 必须     | 服务器的主机名或IP地址。     | 无             |
| port            | 字符串 | 可选     | 服务器的端口。               | 22             |
| username        | 字符串 | 可选     | 目标主机用户的帐户。         | 无             |
| password        | 字符串 | 可选     | 登录到服务器的密码。         | 无（建议保密） |
| key             | 字符串 | 可选     | 用户私钥的明文。             | 无（建议保密） |
| key_path        | 字符串 | 可选     | 登录到服务器的私钥路径。     | 无             |
| envs            | 数组   | 可选     | 在脚本部分可用的自定义秘密。 | 无             |
| script          | 字符串 | 可选     | 在服务器上执行的命令。       | 无             |
| script_stop     | 布尔值 | 可选     | 在第一次失败后停止脚本执行。 | false          |
| timeout         | 字符串 | 可选     | SSH连接建立的最长时间。      | 30秒           |
| command_timeout | 字符串 | 可选     | 执行命令的最长时间。         | 10分钟         |
| proxy_host      | 字符串 | 可选     | 代理的主机名或IP地址。       | 无             |
| proxy_port      | 字符串 | 可选     | 代理端口。                   | 无             |
| proxy_username  | 字符串 | 可选     | 代理用户名。                 | 无             |
| proxy_password  | 字符串 | 可选     | 代理密码。                   | 无（建议保密） |
| proxy_key       | 字符串 | 可选     | 代理私钥的明文。             | 无（建议保密） |
| proxy_key_path  | 字符串 | 可选     | 登录到代理的私钥路径。       | 无             |



# Drone Git Push 代码

> 你不会，俺不会，大家一起都不会！

## 准备环境

- 克隆仓库到本地

  ```bash
  git clone https://gitea.offends.cn/offends/Kubernetes.git && cd Kubernetes/CICD/Drone
  ```

- 修改 Dockerfile 并上传至自己的仓库

- 进入 Drone Web页面点击 SYNC 同步仓库

- 点击 ACTIVATE REPOSITORY 激活存储库

- 点击 Settings 下 General 修改 Configuration,修改后点击 SAVE CHANGES 保存

  ```bash
  .drone-git-push.yml
  ```

- 配置 Secrets

  | 变量名  |         解释          |            例             |
  | :-----: | :-------------------: | :-----------------------: |
  | REMOTE  |     目标远程仓库      |     <仓库 clone 地址>     |
  | SSH_KEY | 远程机器的私有SSH密钥 | <应该是 .ssh/id_rsa 文件> |

## 插件 appleboy/drone-git-push 参数

[官方文档](https://plugins.drone.io/plugins/git-push)

| 属性名         | 类型   | 是否必须 | 描述                                   | 默认值                       |
| -------------- | ------ | -------- | -------------------------------------- | ---------------------------- |
| ssh_key        | 字符串 | 可选     | 远程机器的私有SSH密钥。                | 无                           |
| remote         | 字符串 | 必须     | 目标远程仓库（如果为空，则假定存在）。 | 无                           |
| remote_name    | 字符串 | 可选     | 用于本地操作的远程的名称。             | deploy                       |
| branch         | 字符串 | 可选     | 要推送到的目标分支。                   | master                       |
| local_branch   | 字符串 | 可选     | 从中推送的本地分支或引用。             | HEAD                         |
| path           | 字符串 | 可选     | 仓库的路径。                           | 当前仓库                     |
| force          | 布尔值 | 可选     | 使用 --force 标志进行强制推送。        | false                        |
| skip_verify    | 布尔值 | 可选     | 跳过HTTPS证书的验证。                  | false                        |
| commit         | 布尔值 | 可选     | 在推送之前添加并提交仓库的内容。       | false                        |
| commit_message | 字符串 | 可选     | 为提交添加自定义消息。                 | [skip ci] Commit dirty state |
| empty_commit   | 布尔值 | 可选     | 如果没有更改，则创建一个空的提交。     | false                        |
| author_name    | 字符串 | 可选     | 提交的作者姓名。                       | commiter name                |
| author_email   | 字符串 | 可选     | 提交的作者电子邮件。                   | commiter email               |
| followtags     | 布尔值 | 可选     | 使用 --follow-tags 选项推送。          | false                        |
