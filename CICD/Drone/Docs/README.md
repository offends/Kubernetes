> 本文作者：丁辉

# Drone的使用

## 快速启动 Runner 执行端

```bash
docker run --detach   \
    --volume=/var/run/docker.sock:/var/run/docker.sock  \
    --env=DRONE_RPC_PROTO=https  \
    --env=DRONE_RPC_HOST= \
    --env=DRONE_RPC_SECRET= \
    --env=DRONE_RUNNER_CAPACITY=4 \
    --env=DRONE_RUNNER_NAME=runner \
    --restart=always  \
    --name=runner \
    drone/drone-runner-docker
```

> Runner 添加标签
>
> ```bash
> --from-literal=DRONE_RUNNER_LABELS=<标签:值> \
> ```

## Kubernetes部署文档

- [Helm部署Drone](https://gitee.com/offends/Kubernetes/blob/main/Helm/Helm%E9%83%A8%E7%BD%B2Drone.md)
- [Helm部署Drone-Runner-Docker](https://gitee.com/offends/Kubernetes/blob/main/Helm/Helm%E9%83%A8%E7%BD%B2Drone-Runner-Docker.md)
- [Helm部署Drone-Runner-Kube](https://gitee.com/offends/Kubernetes/blob/main/Helm/Helm%E9%83%A8%E7%BD%B2Drone-Runner-Kube.md)
- [Helm部署Drone-Kubernetes-Secrets](https://gitee.com/offends/Kubernetes/blob/main/Helm/Helm%E9%83%A8%E7%BD%B2Drone-Kubernetes-Secrets.md)

