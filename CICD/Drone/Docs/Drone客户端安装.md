> 本文作者：丁辉

# Drone客户端安装

## 开始安装

[Homebrew官网](https://brew.sh/index_zh-cn)

- Linux 安装 Homebrew

  ```bash
  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
  ```

- MAC 使用 Homebrew 安装 Drone命令

  ```bash
  brew tap drone/drone
  brew install drone
  ```

## 使用命令触发 Drone 构建

[Drone 命令官网](https://0-8-0.docs.drone.io/zh/cli-installation/)

```bash
curl -X POST \
  -H "Authorization: Bearer $YOUR_API_TOKEN" \
  -H "Content-Type: application/json" \
  https://your.drone.instance/api/repos/用户/仓库名/builds
```

