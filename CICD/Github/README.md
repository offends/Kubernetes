> 本文作者：丁辉

# GithubAction学习

## 触发Action构建

```bash
curl -X POST https://api.github.com/repos/$用户/$仓库名/dispatches -H "Accept: application/vnd.github.everest-preview+json" -H "Authorization: token $YOUR_API_TOKEN" --data '{"event_type": "StartAction"}'
```

## GITHUB设置Secrets

网址为：`仓库地址/settings/secrets/actions`

## 构建示例

### 构建触发

```yaml
#定时任务触发构建
on:
  schedule:
    - cron: "0 0 * * *"

#通过接口触发构建
on:
  repository_dispatch:
    types:
      - StartAction

#通过 push 代码触发构建
on:
  push:
    branches:
      - master

# 当 push 到 master 分支，或者创建以 v 开头的 tag 时触发
on:
  push:
    branches:
      - master
    tags:
      - v*
```

### 本地执行命令类

```yaml
name: Build

#本地执行命令类
jobs:
  run-docker-command:
    runs-on: ubuntu-latest
    steps:
      - name: Checkout
        uses: actions/checkout@v3
      - name: Run Docker Command
        run: |
          docker run --name mysql \
          -e MYSQL_ROOT_PASSWORD=${{ secrets.PASSWORD }} \
          ${{ secrets.IMAGES }}
```

### 构建Docker镜像

```yaml
name: Build-Images

# Docker构建镜像并 push 到仓库内
jobs:
  Build-Images-One:
    runs-on: ubuntu-latest
    steps:
      -
        name: Checkout
        uses: actions/checkout@v3
      -
        name: Login to Docker Hub
        uses: docker/login-action@v2
        with:
          username: ${{ secrets.DOCKER_HUB_USERNAME }}
          password: ${{ secrets.DOCKER_HUB_ACCESS_TOKEN }}
      -
        name: Set up Docker Buildx
        uses: docker/setup-buildx-action@v2
      -
        name: Build and push image
        uses: docker/build-push-action@v3
        with:
          context: ./
          file: ./Dockerfile
          push: true
          tags: ${{ secrets.DOCKER_HUB_USERNAME }}/${{ secrets.DOCKER_HUB_STASH }}:${{ secrets.TAG }}
          
  Build--Images-Two:
    needs: Build-Images-One #等待 One 构建成功后开始执行
    runs-on: ubuntu-latest
    steps:
    -
      name: Check Out
      uses: actions/checkout@v3
    -
      name: Login to Docker Hub
      uses: docker/login-action@v2
      with:
        username: ${{ secrets.DOCKER_HUB_USERNAME }}
        password: ${{ secrets.DOCKER_HUB_ACCESS_TOKEN }}
    -
       name: Set up Docker Buildx
       uses: docker/setup-buildx-action@v2

    - name: Build and push
      id: docker_build
      uses: docker/build-push-action@v3
      with:
        context: ./demo/
        file: ./demo/Dockerfile
        push: true
        tags: ${{ secrets.DOCKER_HUB_USERNAME }}/${{ secrets.DOCKER_HUB_STASH }}:${{ secrets.TAG }}
```

### 构建多架构镜像

[官方Demo](https://docs.docker.com/build/ci/github-actions/multi-platform/)

```yaml
name: ci

on:
  push:
    branches:
      - "main"

jobs:
  docker:
    runs-on: ubuntu-latest
    steps:
      -
        name: Login to Docker Hub
        uses: docker/login-action@v2
        with:
            username: ${{ secrets.DOCKERHUB_USERNAME }}
            password: ${{ secrets.DOCKERHUB_TOKEN }}
      -
        name: Checkout
        uses: actions/checkout@v3
      -
        name: Set up QEMU
        uses: docker/setup-qemu-action@v2
      -
        name: Set up Docker Buildx
        uses: docker/setup-buildx-action@v2
      -
        name: Build and push
        uses: docker/build-push-action@v4
        with:
          context: .
          platforms: linux/amd64,linux/arm64
          #支持列表：https://github.com/docker-library/official-images#architectures-other-than-amd64
          #platforms: |
            #linux/arm64
            #linux/amd64
            #linux/arm/v5
            #linux/arm/v7
            #linux/386 #适用于 x86 32 位架构的 Docker 镜像
            #linux/mips64le #适用于 MIPS 64 位架构的 Docker 镜像
            #linux/ppc64le #适用于 IBM Power 架构的 Docker 镜像
            #linux/s390x #适用于 IBM Z 架构的 Docker 镜像
          push: true
          tags: ${{ secrets.DOCKERHUB_USERNAME }}/demo:latest
```

### Dependabot实现更新项目中的依赖项

当你在项目中使用很多第三方库（例如JavaScript项目中的npm包）时，这些库会不断更新，有时是为了添加新功能，有时是为了修复安全漏洞。手动跟踪和更新这些库可能既费时又容易出错。这就是Dependabot发挥作用的地方。

[官方文档](https://docs.github.com/en/code-security/dependabot/dependabot-version-updates/configuring-dependabot-version-updates)

```yaml
version: 2
updates:
- package-ecosystem: npm
  directory: "/"
  schedule:
    interval: daily
  open-pull-requests-limit: 20
```

