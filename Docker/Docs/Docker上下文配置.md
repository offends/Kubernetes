> 本文作者：丁辉

# Docker 上下文

## 基础命令

> 示例新上下文名为: docker-node2

- 查看当前上下文

  ```bahs
  docker context ls
  ```

- 详细查看上下文信息

  ```bash
  docker context inspect default
  ```

- 创建新的上下文

  ```bash
  docker context create docker-node2 --docker host=tcp://docker:2375
  ```

- 切换上下文

  ```bash
  docker context use docker-node2
  ```

  > 通过变量切换
  >
  > ```bash
  > export DOCKER_CONTEXT=docker-node2
  > ```
  >
  > 全局`--context`标志覆盖上下文
  >
  > ```bash
  > docker --context production container ls
  > ```

- 导出上下文

  ```bash
  docker context export docker-node2
  ```

- 导入上下文

  ```bash
  docker context import docker-node2 docker-node2.dockercontext
  ```

- 更新上下文

  ```bash
  docker context update docker-node2 --description "Test context"
  ```

  