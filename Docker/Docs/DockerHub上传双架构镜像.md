> 本文作者：丁辉

# DockerHub上传双架构镜像

## Docker manifest 推送双架构镜像

[官网文档](https://docs.docker.com/engine/reference/commandline/manifest/)

1. 构建并推送镜像

   - X85执行

     ```bash
     docker build -t offends/demo-x86:v1 .
     docker push offends/demo-x86:v1
     ```

   - arm执行

     ```bash
     docker build -t offends/demo-arm:v1 .
     docker push offends/demo-arm:v1
     ```

2. 创建 manifest

   ```bash
   docker manifest create offends/demo:v1  \
   offends/demo-x86:v1 \
   offends/demo-arm:v1
   ```

3. 为镜像指定架构

   ```bash
   docker manifest annotate offends/demo-x86:v1 \
   offends/demo-x86:v1 \
   --os linux --arch x86_64
         
   docker manifest annotate offends/demo-arm:v1 \
   offends/demo-arm:v1 \
   --os linux --arch arm64 --variant v8
   ```

4. 查看

   ```bash
   docker manifest inspect offends/demo:v1
   ```

5. 推送

   ```bash
   docker manifest push offends/demo:v1
   ```

## Docker Buildx 推送双架构镜像

[Buildx二进制文件下载](https://github.com/docker/buildx/releases)

[模拟仓库文档](https://github.com/tonistiigi/binfmt)

[官网文档](https://docs.docker.com/build/building/multi-platform/)

1. 安装 Buildx

   ```bash
   mkdir -p ~/.docker/cli-plugins
   mv buildx-v*.linux-amd64 ~/.docker/cli-plugins/docker-buildx
   chmod +x ~/.docker/cli-plugins/docker-buildx
   docker buildx version
   ```

2. 添加模拟仓库

   ```bash
   docker run --privileged --rm tonistiigi/binfmt --install all
   ```

   > 内核版本需要升级，如果过低无法添加成功

3. 查看

   ```bash
   docker buildx ls
   #一下是输出
   NAME/NODE DRIVER/ENDPOINT STATUS  BUILDKIT     PLATFORMS
   default * docker                               
     default default         running v0.8+unknown linux/amd64, linux/386, linux/arm64, linux/riscv64, linux/ppc64le, linux/s390x, linux/arm/v7, linux/arm/v6
   ```

4. 创建 builder 示例

   ```bash
   docker buildx create --name dmeo --use
   ```

5. 构建混合建构镜像

   ```bash
   docker buildx build --platform linux/amd64,linux/arm64/v8 -t demo:v1 --push .
   ```

## Docker Buildx使用私有仓库 推送双架构镜像

### 部署私有镜像仓库

[Docker hub文档](https://docs.docker.com/registry/)

[GitHub文档](https://github.com/distribution/distribution)

1. 启动镜像仓库

   ```bash
   docker run -d \
       --name docker-registry \
       --restart=always \
       -p 5000:5000 \
       -v /root/private-registry:/var/lib/registry \
       registry
   ```

2. 将本机Docker添加非安全仓库

   > Buildx 只允许 https 协议的镜像仓库使用，这里的方法之建议测试使用

   ```bash
   cat > /etc/docker/daemon.json <<EOF
   {
     "experimental": true,
     "insecure-registries": ["192.168.1.10:5000"]
   }
   EOF
   ```

3. 重启 docker 进程启用

   ```bash
   systemctl restart docker
   ```

4. 将 buildkit 镜像推送到私仓

   ```bash
   docker tag moby/buildkit:buildx-stable-1 192.168.1.10:5000/buildkit:buildx-stable-1
   docker push 192.168.1.10:5000/buildkit:buildx-stable-1
   ```

5. 新增 buildkit 私仓配置

   ```bash
   cat > /etc/buildkit/buildkitd.toml << EOF
   debug = true
   [registry."192.168.1.10:5000"]
     http = true
     insecure = true
   EOF
   ```

6. 创建 builder

   ```bash
   docker buildx create --use \
     --name builder \
     --driver-opt image=192.168.1.10:5000/buildkit:buildx-stable-1 \
     --config /etc/buildkit/buildkitd.toml
   ```

7. 构建混合建构镜像

   ```bash
   docker buildx build --platform linux/amd64,linux/arm64/v8 -t 192.168.1.10:5000/demo:v1 --push .
   ```

8. 查看

   ```bash
   curl  http://192.168.1.10:5000/v2/_catalog
   ```

### 清理

删除构建器实例

```bash
 docker buildx rm builder
```

