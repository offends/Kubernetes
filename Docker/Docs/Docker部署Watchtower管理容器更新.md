> 本文作者：丁辉

# Docker部署Watchtower管理容器更新

[Github仓库](https://github.com/containrrr/watchtower)

## 介绍

Watchtower 是一个开源的容器监控和自动更新工具，设计用于Docker容器环境。它可以监控正在运行的容器及其使用的镜像，当发现镜像有更新时，自动拉取新镜像并重新启动容器。这种自动化管理方式有助于确保部署的应用保持最新状态，从而减少安全风险和改进功能。

## 快速开始

```bash
docker run -d \
    --name watchtower \
    -v /var/run/docker.sock:/var/run/docker.sock \
    containrrr/watchtower
```

所有容器都会自动更新，也包括 Watch­tower 本身。

## 其他启动参数

- 自动清除旧镜像

  ```bash
  docker run -d \
      --name watchtower \
      --restart always \
      -v /var/run/docker.sock:/var/run/docker.sock \
      containrrr/watchtower \
      --cleanup
  ```

  > `--cleanup` 选项可以简写为 `-c`

  ```bash
  docker run -d \
      --name watchtower \
      --restart always \
      -v /var/run/docker.sock:/var/run/docker.sock \
      containrrr/watchtower -c
  ```

- 选择性自动更新

  ```bash
  docker run -d \
      --name watchtower \
      --restart always \
      -v /var/run/docker.sock:/var/run/docker.sock \
      containrrr/watchtower -c \
      nginx redis
  ```

- 配置容器更新列表

  ```bash
  vi ~/.watchtower.list
  ```

  内容如下

  ```bash
  nginx
  reidis
  ```

  启动 Watchtower 容器

  ```bash
  docker run -d \
      --name watchtower \
      --restart always \
      -v /var/run/docker.sock:/var/run/docker.sock \
      containrrr/watchtower -c \
      $(cat ~/.watchtower.list)
  ```

- 设置单个容器自动更新标签

  ```bash
  docker run -d \
      --name nginx \
      --restart always \
      --label com.centurylinklabs.watchtower.enable=true \
      nginx:latest
  ```

  启动 Watchtower 容器

  ```bash
  docker run -d \
      --name watchtower \
      --restart always \
      -v /var/run/docker.sock:/var/run/docker.sock \
      containrrr/watchtower -c \
      --label-enable
  ```

  > `--label-enable` 可以简写为 `-e`

  ```bash
  docker run -d \
      --name watchtower \
      --restart always \
      -v /var/run/docker.sock:/var/run/docker.sock \
      containrrr/watchtower -ce
  ```

- 设置自动更新检查频率

  - `--interval` 设置更新检测时间间隔单位为秒。

    ```bash
    docker run -d \
        --name watchtower \
        --restart always \
        -v /var/run/docker.sock:/var/run/docker.sock \
        containrrr/watchtower -c \
        --interval 3600
    ```

  - `--schedule`  设置定时检测更新时间。格式为 6 字段 Cron 表达式，而非传统的 5 字段，第一位是秒。

    > 比如每天凌晨 2 点检查一次更新

    ```bash
    docker run -d \
        --name watchtower \
        --restart always \
        -v /var/run/docker.sock:/var/run/docker.sock \
        containrrr/watchtower -c \
        --schedule "0 0 2 * * *"
    ```

- 手动更新

  > 检查 nginx 是否需要更新

  ```bash
  docker run --rm \
      -v /var/run/docker.sock:/var/run/docker.sock \
      containrrr/watchtower -c \
      --run-once \
      nginx
  ```

  > `--run-once` 可以简写为 `-R`

  ```bash
  docker run --rm \
      -v /var/run/docker.sock:/var/run/docker.sock \
      containrrr/watchtower -cR \
      nginx
  ```

  > 当容器设置过 `com.centurylinklabs.watchtower.enable=false` 参数则不会更新