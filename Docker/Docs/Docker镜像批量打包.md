> 本文作者：丁辉

# Docker镜像批量打包

- 第一种

  ```bash
  docker save $(docker images | grep -v REPOSITORY | awk 'BEGIN{OFS=":";ORS=" "}{print $1,$2}') -o k8s-master.tar
  ```

- 第二种

  > 将需要统一打包的镜像写在文件内

  ```bash
  cat > images.txt <<EOF
  nginx:alpine
  nginx:latest
  EOF
  ```

  打包

  ```bash
  docker save -o images.tar.gz $(cat images.txt) 
  ```

