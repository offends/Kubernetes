> 本文作者：丁辉

# Docker配置代理

## Docker服务代理配置

> 此方法适用于 `docker pull` 镜像配置代理

创建 Systemd 代理文件

```bash
mkdir -p /etc/systemd/system/docker.service.d
touch /etc/systemd/system/docker.service.d/proxy.conf
```

内容如下

```bash
[Service]
Environment="HTTP_PROXY=http://127.0.0.1:15777"
Environment="HTTPS_PROXY=http://127.0.0.1:15777"
Environment="NO_PROXY=localhost,127.0.0.1,example.com"
```

- `HTTP_PROXY=`：设置HTTP代理服务器
- `HTTPS_PROXY=`：设置HTTPS代理服务器
- `NO_PROXY=""`：设置不使用代理服务器的域名或IP地址列表

> 将 `http://127.0.0.1:15777` 换成可用的代理即可

重启生效

```bash
systemctl daemon-reload
systemctl restart docker
```

## 容器内部代理

> 在容器运行阶段，如果需要代理上网，则需要配置 `~/.docker/config.json`。

创建 Config.json 代理文件

```bash
mkdir ~/.docker/
vi ~/.docker/config.json
```

内容如下

```json
{
 "proxies":
 {
   "default":
   {
     "httpProxy": "http://192.168.1.100:15777",
     "httpsProxy": "http://192.168.1.100:15777",
     "noProxy": "localhost,127.0.0.1,example.com"
   }
 }
}
```

> 此外, 也可以直接在容器运行时通过注入 `http_proxy` 等环境变量进行代理

重启生效

```bash
systemctl daemon-reload
systemctl restart docker
```

## DockerBuild代理

```bash
docker build . \
    --build-arg "HTTP_PROXY=http://192.168.1.100:15777" \
    --build-arg "HTTPS_PROXY=http://192.168.1.100:15777" \
    --build-arg "NO_PROXY=localhost,127.0.0.1,example.com" \
    -t your/image:tag
```

