> 本文作者：丁辉

# Docker配置2375端口

## 方法一

1. 配置 `/etc/docker/daemon.json` 文件

   ```bash
   vi /etc/docker/daemon.json
   ```

   内容如下

   ```json
   {
     "hosts": ["tcp://0.0.0.0:2375", "unix:///var/run/docker.sock"]
   }
   ```

2. 重载并重启 Docker

   ```bash
   systemctl daemon-reload
   systemctl restart docker
   ```

## 方法二

1. 修改 `/usr/lib/systemd/system/docker.service` 文件

   ```bash
   vi /usr/lib/systemd/system/docker.service
   ```

   - 旧版 Docker

     ```bash
     ExecStart=/usr/local/bin/dockerd -H tcp://0.0.0.0:2375 -H unix://var/run/docker.sock
     ```

   - 新版 Docker

     ```bash
     ExecStart=/usr/bin/dockerd -H tcp://0.0.0.0:2375 -H fd:// --containerd=/run/containerd/containerd.sock
     ```

2. 重载并重启 Docker

   ```bash
   systemctl daemon-reload
   systemctl restart docker
   ```

## 验证

- 查看端口是否启动

  ```bash
  netstat -anput | grep 2375
  ```

- 测试

  ```bash
  docker -H tcp://192.168.1.10:2375 ps
  ```

# 配置证书访问

1. 可以使用本脚本生成证书

   ```bash
   curl -Os https://gitee.com/offends/Linux/raw/main/File/Shell/openssl-cert.sh && chmod 777 ./openssl-cert.sh
   ```

   > 修改
   >
   > ```bash
   > IP="127.0.0.1" # 本地 IP 地址
   > PASSWORD="123456" # 证书密码
   > VALIDITY_PERIOD=3650 # 证书有效时间
   > ```

2. 执行脚本

   ```bash
   ./openssl-cert.sh
   ```

3. 修改 `/usr/lib/systemd/system/docker.service` 文件

   ```bash
   vi /usr/lib/systemd/system/docker.service
   ```

   - 旧版 Docker

     ```bash
     ExecStart=/usr/bin/dockerd --tlsverify --tlscacert=/etc/docker/cert/2375/ca.pem --tlscert=/etc/docker/cert/2375/server-cert.pem --tlskey=/etc/docker/cert/2375/server-key.pem -H unix:///var/run/docker.sock
     ```

   - 新版 Docker

     ```bash
     ExecStart=/usr/bin/dockerd --tlsverify --tlscacert=/etc/docker/cert/2375/ca.pem --tlscert=/etc/docker/cert/2375/server-cert.pem --tlskey=/etc/docker/cert/2375/server-key.pem -H tcp://0.0.0.0:2375 -H fd:// --containerd=/run/containerd/containerd.sock
     ```

4. 重载并重启 Docker

   ```bash
   systemctl daemon-reload
   systemctl restart docker
   ```

5. 验证

   ```bash
   docker --tlsverify --tlscacert=/etc/docker/cert/2375/ca.pem --tlscert=/etc/docker/cert/2375/server-cert.pem --tlskey=/etc/docker/cert/2375/server-key.pem -H tcp://192.168.1.10:2375 ps
   ```

# 问题记录

> [方法一] 由于 Docker 在最近新版更换了容器引擎为 Containerd, daemon.json 文件如果配置原来的通信套接字文件路径是不行的,现在也没有更好的解决方案,本次我就记录一下从老外那里学来的方法。
>
> 嗯......为什么说这个方案也不是很对呢,因为他会替换你的 Docker 启动命令,那为什么不直接改 `docker.service` 呢?反正咱只是记录一下,通过修改 docker.service 方法我也放在 [方法二] 里了你们自己看吧

1. 配置 `/etc/docker/daemon.json` 文件

   ```bash
   vi /etc/docker/daemon.json
   ```

   内容如下

   ```json
   {
     "hosts": ["tcp://0.0.0.0:2375", "unix:///var/run/docker.sock"]
   }
   ```

2. 创建 `override.conf` 文件

   ```bash
   mkdir -p /etc/systemd/system/docker.service.d/
   vi /etc/systemd/system/docker.service.d/override.conf
   ```

   文件内容为

   ```bash
   [Service]
    ExecStart=
    ExecStart=/usr/bin/dockerd --config-file /etc/docker/daemon.json
   ```

3. 重载并重启 Docker

   ```bash
   systemctl daemon-reload
   systemctl restart docker
   ```

   

