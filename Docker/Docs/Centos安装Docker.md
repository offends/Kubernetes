> 本文作者：丁辉

# Docker网络安装

> 整体来说各系统安装方式都相差不大,那么咱们这里只举例 Centos 安装 Docker 形式
>
> [官网安装文档](https://docs.docker.com/engine/install/)

## 开始部署

1. 卸载就办 Docker

   ```bash
   sudo yum remove docker \
       docker-client \
       docker-client-latest \
       docker-common \
       docker-latest \
       docker-latest-logrotate \
       docker-logrotate \
       docker-engine
   ```

2. 设置存储库

   ```bash
   yum install -y yum-utils
   ```

   ```bash
   yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
   ```

   > 国内源
   >
   > ```bash
   > yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
   > ```

3. 安装最新版

   ```bash
   yum install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
   ```

   > 安装特定版本
   >
   > - 查看版本库
   >
   >   ```bash
   >   yum list docker-ce --showduplicates | sort -r
   >   ```
   >
   > - 安装
   >
   >   ```bash
   >   sudo yum install docker-ce-<VERSION_STRING> docker-ce-cli-<VERSION_STRING> containerd.io docker-buildx-plugin docker-compose-plugin
   >   ```

4. 启动

   ```bash
   systemctl enable docker
   systemctl start docker
   ```

## 卸载 Docker

1. 卸载软件包

   ```bash
   yum remove docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin docker-ce-rootless-extras
   ```

2. 清理文件

   ```bash
   rm -rf /var/lib/docker
   rm -rf /var/lib/containerd
   ```
