> 本文作者：丁辉

# Docker-fio磁盘读写测试

[官方文档](https://fio.readthedocs.io/en/latest/fio_doc.html)

1. 拉取测试工具镜像

   ```bash
   docker pull registry.cn-hangzhou.aliyuncs.com/offends/fio:latest
   ```

2. 启动并进入容器

   > 为了更好的测试磁盘读写速率我们挂载 /data 目录进行测试

   ```bash
   docker run --name disktest \
     -it --rm -v /data/disk_test:/data/disk_test \
     registry.cn-hangzhou.aliyuncs.com/offends/fio:latest \
     sh
   ```

3. 开始测试

   - 随机写

     ```bash
     fio --ioengine=libaio --runtime=300 --numjobs=2 --iodepth=64 --bs=4k --size=2G --rw=randwrite --filename=/data/disk_test --time_based=1 --direct=1 --name=test --group_reporting --cpus_allowed=3 --cpus_allowed_policy=split
     ```

   - 顺序写

     ```bash
     fio --ioengine=libaio -runtime=300  --numjobs=2 --iodepth=64 --bs=1024k --size=10G --rw=write --filename=/data/disk_test --time_based=1 --direct=1 --name=test --group_reporting --cpus_allowed=3 --cpus_allowed_policy=split
     ```

4. 查看结果

   > 随机写看（IOPS）
   >
   > 顺序写看（吞吐量BW）

# 命令参数

| 参数                 | 描述                                                         |
| -------------------- | ------------------------------------------------------------ |
| --debug=options      | 启用调试日志记录，可以选择启用不同类型的调试信息，比如进程、文件、IO等等。 |
| --parse-only         | 仅解析选项，不执行任何IO操作。                               |
| --output             | 将输出写入文件。                                             |
| --bandwidth-log      | 生成带宽日志。                                               |
| --minimal            | 生成最小化（简洁）的输出。                                   |
| --output-format=type | 指定输出格式，可以是简洁、JSON等。                           |
| --terse-version=type | 设置简洁版本输出格式。                                       |
| --version            | 打印版本信息并退出。                                         |
| --help               | 打印帮助信息。                                               |
| --cpuclock-test      | 执行CPU时钟的测试/验证。                                     |
| --crctest=[type]     | 测试校验和功能的速度。                                       |
| --cmdhelp=cmd        | 打印命令帮助，使用"all"可以查看所有命令。                    |
| --enghelp=engine     | 打印IO引擎的帮助信息，或者列出可用的IO引擎。                 |
| --enghelp=engine,cmd | 打印特定IO引擎命令的帮助信息。                               |
| --showcmd            | 将作业文件转换为命令行选项。                                 |
| --eta=when           | 指定何时打印ETA（预计完成时间）估计值。                      |
| --eta-newline=time   | 每个 'time' 时间段强制换行显示ETA。                          |
| --status-interval=t  | 每个 't' 时间段强制完整状态转储。                            |
| --readonly           | 打开安全只读检查，防止写入。                                 |
| --section=name       | 只运行作业文件中指定的部分，可以指定多个部分。               |
| --alloc-size=kb      | 将smalloc池的大小设置为指定的kb数（默认为16384）。           |
| --warnings-fatal     | Fio解析器警告变为致命错误。                                  |
| --max-jobs=nr        | 支持的最大线程/进程数。                                      |
| --server=args        | 启动后端fio服务器。                                          |
| --daemonize=pidfile  | 后台运行fio服务器，将PID写入文件。                           |
| --client=hostname    | 与远程后端fio服务器通信。                                    |
| --remote-config=file | 告诉fio服务器加载本地作业文件。                              |
| --idle-prof=option   | 报告系统或每CPU基础的CPU空闲情况或运行单位工作校准。         |
| --inflate-log=log    | 解压缩并输出压缩日志。                                       |
| --trigger-file=file  | 当文件存在时执行触发命令。                                   |
| --trigger-timeout=t  | 在指定的时间执行触发器。                                     |
| --trigger=cmd        | 将此命令设置为本地触发器。                                   |
| --trigger-remote=cmd | 将此命令设置为远程触发器。                                   |
| --aux-path=path      | 使用此路径作为fio生成文件的路径。                            |

