> 本文作者：丁辉

# Docker常用配置文件配置

## 更改IP池

- 添加如下参数

  ```bash
  vi /etc/docker/daemon.json
  ```

  ```json
  {
    "default-address-pools" : [
          {
             "base" : "192.168.0.0/16",
             "size" : 24
          }
      ]
  }
  ```

## 指定的 Cgroups 驱动程序

- 添加如下参数

  ```bash
  vi /etc/docker/daemon.json
  ```

  ```bash
  {
    "exec-opts": ["native.cgroupdriver=systemd"]
  }
  ```

- 验证

  ```bash
  docker info|grep "Cgroup Driver"
  ```

## Docker启用实验性CLI功能

**第一种**

- 客户端开启

  ```bash
  vi ~/.docker/config.json
  ```

  ```bash
  {
  	"experimental": "enabled"
  }
  ```

**第二种**

- 服务端开启

  ```bash
  vi /etc/docker/daemon.json
  ```

  ```bash
  {
  	"experimental": true
  }
  ```

  
