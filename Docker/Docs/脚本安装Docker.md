> 本文作者：丁辉

# 脚本安装Docker

- 官方源

  ```bash
  curl -fsSL https://get.docker.com | bash
  ```

- 阿里源

  ```bash
  curl -fsSL https://get.docker.com | bash -s docker --mirror Aliyun
  ```

  