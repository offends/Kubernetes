> 本文作者：丁辉

# Docker部署Gitlab

[企业版官网网址](https://docs.gitlab.com/ee/install/docker.html)

> 部署开源版
>
> 例：本地 IP 为 192.168.1.10

## 部署Gitlab

1. 启动容器

   > 这里也可以通过 `--hostname 192.168.1.10` 指定 Clone 地址，Gitlab会通过读取本地主机名作为默认的 Clone 地址

   ```bash
   docker run -itd \
     --restart always \
     -p 80:80 \
     -p 222:22 \
     -u root \
     -v $PWD/data/log:/var/log/gitlab \
     -v $PWD/data/opt:/var/opt/gitlab \
     -v $PWD/data/etc:/etc/gitlab \
     --privileged=true \
     --name=gitlab \
     gitlab/gitlab-ce:latest
   ```

2. 进入容器查看 root 初始密码

   ```bash
   docker exec -it gitlab bash
   ```

   ```bash
   cat /etc/gitlab/initial_root_password
   ```

   > 访问 192.168.1.10:80

3. 修改 SSH Clone 地址(进入容器内执行)

   ```bash
   cp /etc/gitlab/gitlab.rb /etc/gitlab/gitlab.rb.bak
   vi /etc/gitlab/gitlab.rb
   ```

   写入如下内容

   ```bash
   external_url "http://192.168.1.10:80" #http对外clone地址
   gitlab_rails["gitlab_ssh_host"] = "192.168.1.10" #ssh对外clone地址
   gitlab_rails["gitlab_shell_ssh_port"] = 222 #ssh对外clone端口
   ```

4. 重启服务

   ```bash
   gitlab-ctl reconfigure
   ```

## 安装Gitlab-runner

1. 启动容器

   ```bash
   docker run -itd --name gitlab-runner \
     --restart always \
     --privileged=true \
     -v $PWD/data/gitlab-runner-config:/etc/gitlab-runner \
     -v /var/run/docker.sock:/var/run/docker.sock \
     -v /usr/local/bin/docker:/usr/bin/docker \
     gitlab/gitlab-runner:latest
   ```

2. 创建 Runner 实例获取 Token

   点击Admin Area(管理员设置) > 点击CI/CD > 点击Runners > 点击New project runner > 选择Linux > 填写一个Tags > 点击Create runner

3. 进入容器

   ```bash
   docker exec -it gitlab-runner bash
   ```

4. 开始注册 Runner

   ```bash
   gitlab-ci-multi-runner register
   ```

   过程如下

   ```bash
   Runtime platform                                    arch=amd64 os=linux pid=106 revision=6e766faf version=16.4.0
   Running in system-mode.                            
                                                      
   Enter the GitLab instance URL (for example, https://gitlab.com/):
   http://192.168.1.10 #Gitlab地址
   Enter the registration token:
   ******** #刚刚获取到的Token
   Verifying runner... is valid                        runner=Te1gEas2d
   Enter a name for the runner. This is stored only in the local config.toml file:
   [f94c7a9b1272]: test #名称
   Enter an executor: docker+machine, instance, kubernetes, docker-windows, shell, virtualbox, docker-autoscaler, custom, docker, parallels, ssh:
   shell #输入一个执行器
   Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
    
   Configuration (with the authentication token) was saved in "/etc/gitlab-runner/config.toml"
   ```

5. 开始测试

   在测试仓库内创建 `.gitlab-ci.yml` 文件编写内容

   ```yml
   stages:
   - test
   build-test:
     stage: test
     tags:
       - test
     script:
      - echo "Hello world"
   ```

6. 查看结果

   进入测试仓库 > 点击Build > 点击Pipelines > 查看到 `passed` 即为成功可点击进去查看

## 忘记密码

1. 进入容器

   ```bash
   docker exec -it gitlab /bin/bash
   ```

2. 进⼊控制台(需要等待一段时间)

   ```bash
   gitlab-rails console -e production
   ```

3. 查询root⽤户

   ```bash
   user=User.where(id:1).first
   ```

4. 设置密码

   ```bash
   user.password='password'
   ```

5. 保存退出

   ```bash
   user.save!
   exit
   ```

   



