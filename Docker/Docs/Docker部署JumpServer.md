> 本文作者：丁辉

# Docker部署JumpServer

[官网](https://docs.jumpserver.org/zh/master/install/setup_by_fast/)	[JumpServer安装包](https://github.com/jumpserver/installer/releases)

1. 部署 Mysql 数据库

   ```bash
   docker run -itd --name jump-mysql \
     --restart=always -p 3306:3306 \
     -v /usr/local/jumpserver/data:/var/lib/mysql \
     -v /usr/local/jumpserver/logs:/var/log/mysql \
     -v /usr/local/jumpserver/conf:/etc/mysql/conf.d \
     -e MYSQL_ROOT_PASSWORD=jumpserver \
     -e MYSQL_DATABASE=jumpserver \
     mysql:5.7
   ```

2. 解压 JumpServer 安装包

   ```bash
   tar -xf jumpserver-installer-v*.tar.gz
   cd jumpserver-installer-v*
   ```

3. 修改 `config-example.txt` 文件配置参数, 并启动

   ```bash
   ./jmsctl.sh install
   ```
