> 本文作者：丁辉

# Docker部署Nacos

[官网文档](https://nacos.io/zh-cn/docs/v2/guide/user/auth.html)

1. 启动 MYSQL

   ```bash
   docker run --name nacos-mysql --restart=always \
     -p 3306:3306 \
     -v /data/mysql:/var/lib/mysql \
     -e MYSQL_USER=nacos \
     -e MYSQL_PASSWORD=nacos \
     -e MYSQL_DATABASE=nacos \
     -e MYSQL_ROOT_PASSWORD=root \
     -d mysql:latest
   ```

2. 启动 NACOS

   ```bash
   docker run --name nacos --restart=always \
     -p 8848:8848 \
     -e MYSQL_SERVICE_HOST=${MYSQL_HOST} \
     -e MYSQL_SERVICE_PORT=3306 \
     -e MYSQL_SERVICE_USER=nacos \
     -e MYSQL_SERVICE_PASSWORD=nacos \
     -e MYSQL_SERVICE_DB_NAME=nacos \
     -e MODE=standalone \
     -e PREFER_HOST_MODE=hostname \
     -d mysql:latest
   ```

3. 开启登录

   ```bash
   vi /data/nacos/application.properties
   ```

   内容如下

   ```bash
   # vi /data/nacos/application.properties
   nacos.core.auth.enabled=true
   nacos.core.auth.server.identity.key=nacos
   nacos.core.auth.server.identity.value=nacos
   
   nacos.core.auth.plugin.nacos.token.secret.key=SecretKey012345678901234567890123456789012345678901234567890123456789
   ```

4. 访问：http://localhost:8848/nacos/#/login

> 启动挂载配置文件
>
> ```bash
> -e 	/data/nacos/application.properties:/home/nacos/conf/application.properties
> ```