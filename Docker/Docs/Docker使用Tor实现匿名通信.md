> 本文作者：丁辉

# Docker使用Tor实现匿名通信

## 介绍

Tor（The Onion Router）是一种免费的开源软件，用于实现匿名通信。它通过全球范围内的一系列自愿维护的节点或“路由器”来转发和加密用户的互联网流量，从而隐藏用户的身份和位置。

[官方文档](https://community.torproject.org/onion-services/setup/install/)

## 开始部署

> 准备一个 Nginx 服务，部署 Tor 实现匿名通信 Nginx
>
> Nginx访问地址：192.168.1.10:80

1. 创建持久化目录

   ```bash
   mkdir -p $HOME/tor-data
   ```

2. 目录授权

   ```bash
   chmod 700 $HOME/tor-data
   chown 100:65533 $HOME/tor-data
   ```

3. 编写配置文件

   ```bash
   vi $HOME/tor-data/torrc
   ```

   内容如下

   ```bash
   Log notice file /var/log/tor/notices.log
   SOCKSPort 0
   HiddenServiceNonAnonymousMode 1
   HiddenServiceSingleHopMode 1
   # 配置代理
   #Socks5Proxy sslocal-rust:1080
   HiddenServiceDir /var/lib/tor/nginx
   HiddenServicePort 80 192.168.1.10:80
   ```

4. 启动容器

   ```bash
   docker run -itd \
     --restart always \
     -v $HOME/tor-data:/var/lib/tor \
     -v $HOME/tor-data:/etc/tor \
     --name=tor \
     osminogin/tor-simple:latest
   ```

5. 查看 hostname

   ```bash
   cat $HOME/tor-data/nginx/hostname 
   ```

6. 通过匿名浏览器访问

   > xxx.onion
