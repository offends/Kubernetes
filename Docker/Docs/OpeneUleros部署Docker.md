> 本文作者：丁辉

# OpeneUleros部署Docker

> OpeneUleros 为 dnf 包管理工具, 所以特意记录本次安装

## 基础配置

1. 修改chrony时间同步配置

   ```bash
   sed -i 's/pool pool.ntp.org iburst/pool ntp.aliyun.com/g' /etc/chrony.conf
   ```

2. 重启chrony服务, 并检查

   ```bash
   systemctl restart chronyd && chronyc sources
   ```

## 安装 Docker

1. 更新镜像源缓存

   ```bash
   dnf makecache
   ```

2. 安装

   ```bash
   dnf install docker
   ```

3. 启动

   ```bash
   systemctl start docker
   systemctl enable docker
   ```

   

