> 本文作者：丁辉

# Docker使用Caddy

[官网](https://caddyserver.com/)

1. 启动 Caddy 容器

   ```bash
   docker run -itd \
     --restart always \
     -p 80:80 \
     -v $PWD/data/caddy:/etc/caddy/ \
     --name=caddy \
     caddy:latest
   ```

2. 进入容器修改配置文件

   ```bash
   vi /etc/caddy/Caddyfile
   ```

   内容如下

   ```bash
   :80 {
   	root * /etc/caddy/www
   	file_server
   }
   ```

3. 重启容器

   ```bash
   docker restart caddy
   ```

4. 访问 IP:80