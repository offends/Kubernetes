> 本文作者：丁辉

# Docker快速部署LobeChat

[Github](https://github.com/lobehub/lobe-chat)	[官方文档](https://lobehub.com/zh/features)	[官方部署文档](https://lobehub.com/zh/docs/self-hosting/platform/docker)

[OpenAi-Api-keys页面](https://platform.openai.com/api-keys)

```
docker run -d -p 3210:3210 \
  -e OPENAI_API_KEY=sk-xxxx \
  -e ACCESS_CODE=lobe66 \
  --name lobe-chat \
  lobehub/lobe-chat
```

**参数解释**

- `OPENAI_API_KEY`：这是用于访问 OpenAI 服务的 API 密钥。这个密钥用于验证请求的身份，并确保请求是由授权用户发起的。

- `OPENAI_PROXY_URL`：这是一个代理服务器的 URL，用于将请求重定向到指定的地址。这可以用于在请求 OpenAI API 时绕过直接访问限制，或者用于增加安全性。

- `ACCESS_CODE`：这是一个访问代码，可能用于程序内部的身份验证或控制访问某些功能。
