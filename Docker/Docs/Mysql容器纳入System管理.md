> 本文作者：丁辉

# Mysql容器纳入System管理

1. 创建 Systemd Service 文件

   ```bash
   vi /usr/lib/systemd/system/mysql.service
   ```

   内容如下

   ```bash
   [Unit]
   Description=Mysql container
   Requires=docker.service
   After=docker.service
   [Service]
   RemainAfterExit=yes
   ExecStop=/usr/bin/docker stop mysql # 容器名
   ExecStart=/usr/bin/docker start mysql
   ExecReload=/usr/bin/docker restart mysql
   Restart=on-abnormal
   [Install]
   WantedBy=multi-user.target
   ```

2. 重载配置文件

   ```bash
   systemctl daemon-reload
   ```

3. 启动 Mysql

   ```bash
   systemctl start mysql
   ```

   

