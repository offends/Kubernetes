> 本文作者：丁辉

# Docker配置守护进程

> 通过修改 `/etc/docker/daemon.json` 配置守护进程
>
> [官方文档](https://docs.docker.com/engine/reference/commandline/dockerd/#daemon) [示例文件位置](https://docs.docker.com/engine/reference/commandline/dockerd/#daemon-configuration-file)

- 编写基础配置

  ```json
  {
    "registry-mirrors": [
      "https://dockerhub.azk8s.cn",
      "https://docker.mirrors.ustc.edu.cn",
      "http://hub-mirror.c.163.com"
    ],
    "insecure-registries": [],
    "max-concurrent-downloads": 10,
    "max-concurrent-uploads": 10,
    "log-driver": "json-file",
    "log-level": "warn",
    "log-opts": {
      "max-size": "10m",
      "max-file": "3"
      },
    "data-root": "/var/lib/docker"
  }
  ```

- 建立垃圾收集

  ```json
  {
    "builder": {
      "gc": {
        "enabled": true,
        "defaultKeepStorage": "10GB",
        "policy": [
          { "keepStorage": "10GB", "filter": ["unused-for=2200h"] },
          { "keepStorage": "50GB", "filter": ["unused-for=3300h"] },
          { "keepStorage": "100GB", "all": true }
        ]
      }
    }
  }
  ```

- 验证守护进程配置文件

  ```bash
  dockerd --validate --config-file=/etc/docker/daemon.json
  ```

- 重载 Docker

  ```bash
  systemctl reload docker
  ```

  

# 参数

| 参数                             | 用法和说明                                                   | 默认值/示例                                                  |
| -------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| allow-nondistributable-artifacts | 允许的非分发性文件列表（为空）                               | true/false                                                   |
| api-cors-header                  | API的跨源资源共享（CORS）头部，允许对API进行跨域请求         | "" （空字符串）或 * 或指定IP地址或域名                       |
| authorization-plugins            | 授权插件列表（为空）                                         |                                                              |
| bip                              | 容器网络的默认桥接接口的IP范围                               | "" （空字符串）                                              |
| bridge                           | 指定容器网络的默认桥接接口                                   | "" （空字符串）                                              |
| cgroup-parent                    | 指定容器的cgroup父目录                                       |                                                              |
| containerd                       | 容器运行时 containerd 的socket路径                           | "/run/containerd/containerd.sock"                            |
| containerd-namespace             | 容器运行时 containerd 的命名空间                             | "docker"                                                     |
| containerd-plugin-namespace      | 容器运行时 containerd 插件的命名空间                         | "docker-plugins"                                             |
| data-root                        | Docker 数据的根目录路径                                      |                                                              |
| debug                            | 是否启用调试模式                                             | true/false（根据具体实现或配置文件而定）                     |
| default-address-pools            | 默认的地址池设置列表，包括基础地址和子网大小                 | 示例：[]（空列表）                                           |
| default-cgroupns-mode            | 默认的cgroup命名空间模式（通常是私有）                       | "private"（私有模式）                                        |
| default-gateway                  | 默认网关设置                                                 |                                                              |
| default-gateway-v6               | 默认IPv6网关设置                                             |                                                              |
| default-network-opts             | 默认网络选项                                                 |                                                              |
| default-runtime                  | 默认容器运行时                                               | "runc"（具体容器运行时的名称）                               |
| default-shm-size                 | 默认的共享内存大小                                           | "64M"（64兆字节）                                            |
| default-ulimits                  | 默认的ulimit设置，指定文件描述符的硬限制和软限制             | 示例：{"nofile": {"Hard": 64000, "Name": "nofile", "Soft": 64000}} （文件描述符限制示例） |
| dns                              | DNS服务器列表                                                | 示例：[]（空列表）                                           |
| dns-opts                         | DNS选项列表                                                  | 示例：[]（空列表）                                           |
| dns-search                       | DNS搜索域列表                                                | 示例：[]（空列表）                                           |
| exec-opts                        | 容器执行参数列表                                             | 示例：[]（空列表）                                           |
| exec-root                        | 容器执行的根目录路径                                         | "" （空字符串）                                              |
| experimental                     | 是否启用实验性功能                                           | true/false（根据具体实现或配置文件而定）                     |
| features                         | Docker功能列表                                               |                                                              |
| fixed-cidr                       | 固定CIDR地址设置（通常用于设置 Docker 容器的 IP 地址）       | "" （空字符串）                                              |
| fixed-cidr-v6                    | 固定IPv6 CIDR地址设置（通常用于设置 Docker 容器的 IPv6 地址） | "" （空字符串）                                              |
| group                            | Docker进程的用户组                                           |                                                              |
| hosts                            | 主机名设置列表                                               | 示例：[]（空列表）                                           |
| proxies                          | 代理设置，包括HTTP代理、HTTPS代理和不使用代理的地址列表      |                                                              |
| icc                              | 是否启用容器间通信                                           | false （默认值为false）                                      |
| init                             | 是否启用自定义初始化进程                                     | false （默认值为false）                                      |
| init-path                        | 自定义初始化进程的路径                                       | "/usr/libexec/docker-init"                                   |
| insecure-registries              | 不安全的镜像仓库列表                                         | 示例：[]（空列表）                                           |
| ip                               | Docker守护进程监听的IP地址                                   | 0.0.0.0                                                      |
| ip-forward                       | 是否启用IP转发                                               | false （默认值为false）                                      |
| ip-masq                          | 是否启用IP伪装                                               | false （默认值为false）                                      |
| iptables                         | 是否启用iptables                                             | false （默认值为false）                                      |
| ip6tables                        | 是否启用ip6tables                                            | false （默认值为false）                                      |
| ipv6                             | 是否启用IPv6                                                 | true/false（根据具体实现或配置文件而定）                     |
| labels                           | 标签设置列表                                                 | 示例：[]（空列表）                                           |
| live-restore                     | 是否启用容器守护进程在宕机时自动恢复容器                     | true/false（根据具体实现或配置文件而定）                     |
| log-driver                       | 日志驱动设置（默认为json-file）                              | "json-file"（JSON文件）                                      |
| log-level                        | 日志级别设置                                                 | "" （空字符串）                                              |
| log-opts                         | 日志选项设置，包括缓存禁用、缓存大小、缓存最大文件数等       | 示例：{"max-size": "10m", "max-file": "5"}（最大大小为10兆字节，最大文件数为5） |
| max-concurrent-downloads         | 最大并发下载任务数                                           | 3（示例值）                                                  |
| max-concurrent-uploads           | 最大并发上传任务数                                           | 5（示例值）                                                  |
| max-download-attempts            | 最大下载尝试次数                                             | 5（示例值）                                                  |
| mtu                              | 最大传输单元设置                                             | 0（示例值）                                                  |
| no-new-privileges                | 是否禁用新特权                                               | false （默认值为false）                                      |
| node-generic-resources           | 节点通用资源列表，通常用于指定GPU等硬件资源                  | 示例：["NVIDIA-GPU=UUID1", "NVIDIA-GPU=UUID2"]（GPU资源示例） |
| oom-score-adjust                 | OOM分数调整设置                                              | 0（示例值）                                                  |
| pidfile                          | PID文件路径设置                                              |                                                              |
| raw-logs                         | 是否启用原始日志记录                                         | true/false（根据具体实现或配置文件而定）                     |
| registry-mirrors                 | 镜像仓库镜像设置列表                                         | 示例：[]（空列表）                                           |
| runtimes                         | 容器运行时设置，可以包括自定义运行时的路径和参数             |                                                              |
| seccomp-profile                  | 安全策略配置文件路径                                         | "" （空字符串）                                              |
| selinux-enabled                  | 是否启用SELinux                                              | true/false（根据具体实现或配置文件而定）                     |
| shutdown-timeout                 | 容器守护进程关闭超时设置                                     | 15（示例值）                                                 |
| storage-driver                   | 存储驱动设置                                                 |                                                              |
| storage-opts                     | 存储选项设置列表                                             | 示例：[]（空列表）                                           |
| swarm-default-advertise-addr     | Swarm模式下的默认广告地址设置                                |                                                              |
| tls                              | 是否启用TLS                                                  | true/false（根据具体实现或配置文件而定）                     |
| tlscacert                        | TLS CA证书路径                                               |                                                              |
| tlscert                          | TLS证书路径                                                  |                                                              |
| tlskey                           | TLS密钥路径                                                  |                                                              |
| tlsverify                        | 是否验证TLS                                                  | true/false（根据具体实现或配置文件而定）                     |
| userland-proxy                   | 是否使用用户空间代理                                         | true/false（根据具体实现或配置文件而定）                     |
| userland-proxy-path              | 用户空间代理的路径                                           | "/usr/libexec/docker-proxy"                                  |
| userns-remap                     | 用户命名空间重映射设置                                       |                                                              |
