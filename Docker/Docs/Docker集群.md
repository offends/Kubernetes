> 本文作者：丁辉

# Docker Swarm集群

[官方文档](https://docs.docker.com/engine/swarm/swarm-tutorial/deploy-service/)

|    节点IP    |  角色  |
| :----------: | :----: |
| 192.168.1.10 | MASTER |
| 192.168.1.20 | WORKER |

## 开始组建集群

1. 初始化 MASTER 节点

   ```bash
   docker swarm init
   ```

   > 指定 IP
   >
   > ```bash
   > docker swarm init --advertise-addr 192.168.1.10
   > ```
   >
   > 指定网段
   >
   > ```bash
   > --default-addr-pool 192.168.1.0/24
   > ```

2. WORKER 节点加入集群

   ```bash
   docker swarm join --token <token> 192.168.1.10:2377
   ```

3. 检查所有节点

   ```bash
   docker node ls
   ```

   > 删除节点命令为
   >
   > ```bash
   > docker swarm leave
   > ```
   >
   > 强制使用 ` --force` 参数

## 常用基础命令

- 查看加入 MASTER TOKEN

  ```bash
  docker swarm join-token manager
  ```

- 查看加入 WORKER TOKEN

  ```bash
  docker swarm join-token worker
  ```

- 查看所有节点

  ```bash
  docker node ls
  ```

- 查看节点详情

  ```bash
  docker node inspect <节点名称> --pretty
  ```


## Secrets 基础操作

- 创建 Secrets

  ```
  docker secret create <Secrets名称> ./<文件位置>
  ```

  > echo 创建 Secrets
  >
  > ```bash
  > echo "内容" | docker secret create <Secrets名称> -
  > ```
  >
  > openssl 创建 Secrets,生成一个随机的20个字符的密码,并将其作为密钥存储到Docker中
  >
  > ```bash
  > openssl rand -base64 20 | docker secret create mysql_password -
  > ```

- 查看 Secrets

  ```bash
  docker secret ls
  ```

- 检查 Secrets

  ```bash
  docker secret inspect <Secrets名称>
  ```

- 删除 Secrets

  ```bash
  docker secret rm <Secrets名称>
  ```

## Network 基础操作

- 创建 Network

  ```
  docker network create -d overlay <网络名称>
  ```

- 查看 Network

  ```bash
  docker network ls
  ```

- 检查 Network

  ```bash
  docker network inspect <网络名称>
  ```

- 删除 Network

  ```bash
  docker network rm <网络名称>
  ```

## Volume 基础操作

- 创建 Volume

  ```
  docker volume create -d overlay <存储名称>
  ```

- 查看 Volume

  ```bash
  docker volume ls
  ```

- 检查 Volume

  ```bash
  docker volume inspect <存储名称>
  ```

- 删除 Volume

  ```bash
  docker volume rm <存储名称>
  ```

## 启动容器测试

```bash
docker service create \
   --name mysql \
   --replicas 1 \
   --mount type=bind,source=/data/mysqld,destination=/var/lib/mysql \
   -e MYSQL_ROOT_PASSWORD="root" \
   mysql
```

**其他参数**

- 指定 Docker 节点

  ```bash
  --constraint 'node.hostname==节点名称'
  ```

- 指定对外端口

  ```bash
  --publish published=<容器对外端口>,target=<容器内部端口>
  ```

- 挂载 volume 存储

  ```bash
  --mount type=volume,source=<指定存储名称>,destination=<容器内部路径>
  ```

- 指定网络

  ```bash
  --network <网络名称>
  ```

- 挂载 secret

  ```bash
  --secret source=<secret名称>,target=<容器内路径>
  ```


**常见参数说明**

|             参数             |                 说明                  |
| :--------------------------: | :-----------------------------------: |
|           `--name`           |            指定服务的名称             |
|         `--replicas`         |           指定服务的副本数            |
|        `--constraint`        |      指定服务运行的节点约束条件       |
|         `--publish`          |     将容器的端口映射到主机的端口      |
|          `--mount`           |   将主机上的目录或文件挂载到容器内    |
|         `--network`          |     将服务连接到指定的Docker网络      |
|          `--secret`          | 将指定的Docker secret文件挂载到容器内 |
|             `-e`             |         指定容器内的环境变量          |
|         `--env-file`         |       指定容器内的环境变量文件        |
|    `--restart-condition`     |          指定容器的重启策略           |
|       `--update-delay`       |      指定服务更新之间的延迟时间       |
|    `--update-parallelism`    |       指定服务更新时的并行数量        |
|  `--update-failure-action`   |       指定服务更新失败后的操作        |
| `--update-max-failure-ratio` |      指定服务更新失败的最大比率       |
|      `--endpoint-mode`       |        指定服务的网络端点模式         |



