> 本文作者：丁辉

# Docker更改IP池

1. 更改配置文件

   ```bash
   vi /etc/docker/daemon.json
   ```

   内容如下

   ```bash
   {
     "default-address-pools" : [
           {
              "base" : "192.168.0.0/16",
              "size" : 24
           }
       ]
   }
   ```

2. 重启 Docker

   ```bash
   systemctl restart docker
   ```

   



