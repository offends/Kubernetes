> 本文作者：丁辉

# Docker配合Mysql-Secrets使用

1. 配置密码

   ```bash
   echo "root" > root-pass.txt
   echo "root" > offends-pass.txt
   ```

2. 创建 Secrets

   ```bash
   docker secret create mysql-root-pass ./root-pass.txt
   docker secret create mysql-offends-pass ./offends-pass.txt
   ```

3. 构建容器

   ```bash
   docker build -t mysql:v1 --file=./Dockerfile-secrets .
   ```

4. 创建持久化目录

   ```bash
   mkdir /data/mysqld
   ```

5. 启动容器

   ```bash
   docker service create \
      --name mysql \
      --replicas 1 \
      --publish published=3306,target=3306 \
      --mount type=bind,source=/data/mysqld,destination=/var/lib/mysql \
      --secret source=mysql-root-pass,target=/run/secrets/mysql-root-pass \
      --secret source=mysql-offends-pass,target=/run/secrets/mysql-offends-pass \
      mysql:v1
   ```

6. 查看

   ```bash
   docker service ps mysql
   ```

   > 查看完整事件
   >
   > ```bash
   > docker service ps mysql --no-trunc
   > ```

7. 停止容器

   ```bash
   docker service rm mysql
   ```

   

