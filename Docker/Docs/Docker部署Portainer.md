> 本文作者：丁辉
>

# Docker部署Portainer

[官方文档](https://docs.portainer.io/)

> 通过Portainer管理docker

## Docker部署

```bash
docker run -d -p 9000:9000 \
--name portainer --restart=always \
-v /var/run/docker.sock:/var/run/docker.sock \
-v /data/portainer:/data \
portainer/portainer-ce:latest
```

## 远程连接Docker

远程连接默认端口是2375	[Docker配置2375端口文档](https://gitee.com/offends/Kubernetes/blob/main/Docker/Docs/Docker%E9%85%8D%E7%BD%AE2375%E7%AB%AF%E5%8F%A3.md)

## 忘记密码

1. 下载新镜像

   ```bash
   docker pull portainer/helper-reset-password
   ```

2. 关闭容器

   ```bash
   docker stop portainer
   ```

3. 启动观看密码

   ```bash
   docker run --rm -v portainer_data:/data portainer/helper-reset-password
   ```

   

