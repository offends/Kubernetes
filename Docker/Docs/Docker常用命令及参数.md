> 本文作者：丁辉

# Docker基础命令

> 当然太简单的咱们就不记了,闭眼都会

## 基础命令

- 查看指定 Docker 镜像的历史记录,这个命令可以帮助你了解镜像是如何构建的

  ```bash
  docker image history app:v1
  ```

## Docker镜像批量打包

第一种

```bash
docker save $(docker images | grep -v REPOSITORY | awk 'BEGIN{OFS=":";ORS=" "}{print $1,$2}') -o images.tar
```

第二种

> 将需要统一打包的镜像写在文件内

```bash
cat > images.txt <<EOF
nginx:alpine
nginx:latest
EOF
```

打包

```bash
docker save -o images.tar.gz $(cat images.txt) 
```

## 清理资源命令

- 批量删除 Exited 容器

  ```bash
  docker rm $(docker ps -q -f status=exited)
  ```

- 移除所有没有使用的镜像 

  ```bash
  docker image prune -a
  ```

  > 跳过警告提示：`--force`或`-f`
  >
  > ```bash
  > docker image prune -f
  > ```
  >
  > 清理所有无用的镜像
  >
  > ```bash
  > docker image prune --all --force
  > ```
  >
  > 超过24小时创建的镜像
  >
  > ```bash
  > docker image prune -a --filter "until=24h"
  > ```

- 清理不再使用的移除容器

  ```bash
  docker container prune
  ```

- 移除卷

  ```bash
  docker volume prune
  ```

- 移除网络

  ```bash
  docker network prune
  ```

- 清理卷

  ```bash
  docker system prune --volumes
  ```

- 用于清理 Docker 系统中不再使用的资源，包括容器、镜像、网络和数据卷

  ```bash
  docker system prune -a
  ```

## 基本构建参数

| 参数                      | 描述                                                         | 用法示例                                            |
| ------------------------- | ------------------------------------------------------------ | --------------------------------------------------- |
| `--target`                | 选择构建过程中的目标阶段（Stage）。                          | `docker build --target my-stage .`                  |
| `--no-cache`              | 强制忽略缓存，每个指令都将重新执行。                         | `docker build --no-cache .`                         |
| `--build-arg`             | 设置构建过程中的参数变量。                                   | `docker build --build-arg MY_VAR=value .`           |
| `--squash`                | 合并镜像的历史记录以减小镜像层级数和总体积。                 | `docker build --squash -t myimage:latest .`         |
| `--disable-content-trust` | 在执行 `docker push` 和 `docker pull` 等命令时禁用内容信任。 | `docker build --disable-content-trust -t myimage .` |

## 启动参数

| 参数              | 描述                                   | 用法示例                             |
| ----------------- | -------------------------------------- | ------------------------------------ |
| `--cpus=2`        | 限制容器使用的 CPU 核心数量为 2 个。   | `docker run --cpus=2 myimage`        |
| `--memory="200m"` | 限制容器可用的内存为 200 兆字节 (MB)。 | `docker run --memory="200m" myimage` |