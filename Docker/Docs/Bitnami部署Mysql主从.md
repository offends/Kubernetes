> 本文作者：丁辉

# Bitnami部署Mysql主从

1. 创建持久化目录

   ```bash
   mkdir -p /opt/mysql/data
   chmod 777 /opt/mysql/data
   ```

2. 部署 Master 容器

   ```bash
   docker run --name mysql-master --restart=always \
     -p 3306:3306 \
     -v /opt/mysql/data:/bitnami/mysql/data \
     -e MYSQL_ROOT_PASSWORD=root \
     -e MYSQL_REPLICATION_MODE=master \
     -e MYSQL_REPLICATION_USER=slave \
     -e MYSQL_REPLICATION_PASSWORD=slave_password \
     -e MYSQL_AUTHENTICATION_PLUGIN=mysql_native_password \
     -d bitnami/mysql:latest
   ```

3. 部署 Slave 容器

   ```bash
   docker run --name mysql-slave --restart=always \
     -p 3306:3306 \
     -v /opt/mysql/data:/bitnami/mysql/data \
     -e MYSQL_MASTER_HOST=<MYSQL_MASTER_HOST> \
     -e MYSQL_MASTER_ROOT_PASSWORD=root \
     -e MYSQL_MASTER_PORT_NUMBER=3306 \
     -e MYSQL_REPLICATION_MODE=slave \
     -e MYSQL_REPLICATION_USER=slave \
     -e MYSQL_REPLICATION_PASSWORD=slave_password \
     -e MYSQL_AUTHENTICATION_PLUGIN=mysql_native_password \
     -d bitnami/mysql:latest
   ```

4. 进入 Slave 容器

   ```bash
   docker exec -it mysql-slave bash
   mysql -u root -proot
   ```

5. 查看同步状态

   ```bash
   show slave status\G;
   ```

> 切记请勿在主使用清空 GTID 信息命令，会使主从状态失效

