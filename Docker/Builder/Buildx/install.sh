#!/bin/bash

#############################################################################################
# 用途: 安装 Buildx 脚本
# 作者: 丁辉
# 编写时间: 2023-11-27
#############################################################################################

# 加载检测脚本
source <(curl -sS https://gitee.com/offends/Linux/raw/main/File/Shell/Check_command.sh)

function CHECK(){
    # 官方下载仓库: https://github.com/docker/buildx/releases
    VERSION="v0.12.0"
    DIR="/root/.docker/cli-plugins"

    URL="https://github.com/docker/buildx/releases/download/$VERSION/buildx-$VERSION.linux-$ARCH_TYPE_2"
    BIN_NAME="Buildx"
    CHECK_BIN "$DIR/docker-buildx"
}

# 引用 check_bin 变量不存在则安装
function CHECK_BIN_INSTALL(){
    if [ "$INSTALL_BIN" = false ]; then
        CHECK_INSTALL wget
        SEND_INFO "正在安装 $BIN_NAME,请稍后"
        CHECK_DIR "$DIR"
        CHECK_COMMAND_NULL wget $URL
        CHECK_COMMAND_NULL mv buildx-v*.linux-amd64 ~/.docker/cli-plugins/docker-buildx
        CHECK_COMMAND_NULL chmod +x ~/.docker/cli-plugins/docker-buildx
        SEND_INFO "Buildx 版本: $(docker buildx version | awk '{print $2}')"

    else 
        SEND_INFO "$BIN_NAME 已安装"
        SEND_INFO "Buildx 版本: $(docker buildx version | awk '{print $2}')"
    fi
}

# function CHECK_BIN_INSTALL_MAN(){
#     export DOCKER_BUILDKIT=1
#     docker build --platform=local -o . git://github.com/docker/buildx
# }

# 添加模拟架构
function RUN_BINFMT(){
    SEND_INFO "正在添加模拟架构"
    CHECK_COMMAND_NULL docker run --privileged --rm tonistiigi/binfmt --install all
    SEND_INFO "添加模拟架构完成,查看架构信息: docker buildx ls"
}

function ALL(){
    CHECK_SYSTEMD docker
    CHECK_CPU
    CHECK
    CHECK_BIN_INSTALL
    CHECK_CORE
    RUN_BINFMT
}

ALL