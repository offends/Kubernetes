#!/bin/bash

#############################################################################################
# 用途: 构建 Dockerfile 脚本
# 作者: 丁辉
# 编写时间: 2023-11-27
#############################################################################################

# 加载检测脚本
source <(curl -sS https://gitee.com/offends/Linux/raw/main/File/Shell/Check_command.sh)

# 创建 buildx
function CREATE_BUILDX(){
    # 国内镜像
    # --driver-opt dockerpracticesig/buildkit:master
    # --driver-opt dockerpracticesig/buildkit:master-tencent
    CHECK_COMMAND_NULL docker buildx create --use --name=buildx --driver docker-container --driver-opt image=moby/buildkit:buildx-stable-1
    CHECK_COMMAND_TRUE "创建 buildx 成功" "创建 buildx 失败,请根据错误信息检查"
}

# 开始构建镜像
function BUILD_IMAGE(){
    # 构建镜像
    CHECK_COMMAND_NULL docker buildx build --platform ${PLATFORM} -t ${IMAGE_NAME}:${IMAGE_TAG} . --push
    CHECK_COMMAND_TRUE "构建镜像成功" "构建镜像失败,请根据错误信息检查"
    NULL_TRUE docker buildx rm buildx
    GREEN_PRINTF "构建镜像完成,查看镜像架构信息: \n$(docker buildx imagetools inspect ${IMAGE_NAME}:${IMAGE_TAG} | grep Platform: | grep -v unknown/unknown | awk -F " " '{print $2}')"
}

function ALL(){
    # 检测是否安装 Docker
    CHECK_SYSTEMD docker
    CREATE_BUILDX
    BUILD_IMAGE
}

ALL