> 本文作者：丁辉

# BUILDX构建镜像

## 安装

1. 克隆代码

   ```bash
   git clone https://gitee.com/offends/Kubernetes.git
   cd Kubernetes/Docker/Builder/Buildx
   ```

2. 安装

   ```bash
   ./install.sh
   ```

## 手动构建

1. 编写 Dockerfile

   ```dockerfile
   # vi Dockerfile
   FROM --platform=$TARGETPLATFORM alpine
   
   RUN echo "Startup success" > /os.txt
   
   CMD tail -f /os.txt
   ```

2. 创建了一个名为 "buildx" 的构建器

   ```bash
   docker buildx create --use --name=buildx --driver docker-container --driver-opt image=moby/buildkit:buildx-stable-1
   ```

**参数解释**

| 参数                           | 说明                                                         |
| ------------------------------ | ------------------------------------------------------------ |
| `--use`                        | 将新构建器设置为当前活动的构建器(默认构建器)                 |
| `--name=buildx`                | 指定新构建器的名称为 "buildx"                                |
| `--driver docker`              | 指定使用的驱动程序为 "docker"                                |
| `--driver-opt image=...`       | 指定驱动程序选项，此处是指定 BuildKit 镜像的位置为 "moby/buildkit:buildx-stable-1" |
| `--config /etc/buildkitd.toml` | 指定配置文件                                                 |

3. 构建多架构镜像并推送镜像仓库

   **示例**

   > 举例仓库地址为 "offends"

   - 第一种方式(简单)

     ```bash
     docker buildx build --platform linux/amd64,linux/arm/v7 -t offends/app:v1 . --push
     ```

   - 第二种方式

     ```bash
     docker buildx build --platform linux/amd64,linux/arm/v7 -t offends/app:v1 --output type=registry,dest=offends .
     ```

     > 查看 buildx 当前可构建架构
     >
     > ```bash
     > docker buildx ls
     > ```

**参数解释**

| 参数                                         | 解释                                                         |
| -------------------------------------------- | ------------------------------------------------------------ |
| `docker buildx build`                        | 执行 Buildx 构建的命令                                       |
| `--platform linux/amd64,linux/arm/v6`        | 指定要构建的目标平台，这里包括 Linux AMD64 和 ARMv6          |
| `-t app:v1`                                  | 为构建的镜像设置标签为 "app:v1"                              |
| `-f ./Dockerfile`                            | 指定要使用的 Dockerfile 文件路径                             |
| `.`                                          | 构建上下文的路径，表示当前目录是构建上下文，其中包含了构建镜像所需的文件和指令 |
| `--output type=local,dest=.docker`           | 指定输出类型为本地，并将构建结果输出到名为 `.docker` 的目录中 |
| `--output type=oci,dest=<path>`              | 将构建结果输出为 OCI 格式的镜像包，并保存到指定路径          |
| `--output type=docker,dest=<path>`           | 将构建结果输出为 Docker 格式的镜像包，并保存到指定路径       |
| `--output type=image,name=<image_name>`      | 将构建的镜像推送到指定的镜像仓库，指定镜像名称               |
| `--output type=registry,dest=<registry_url>` | 将构建的镜像推送到指定的镜像仓库地址                         |
| `--push`                                     | 将构建的镜像推送到指定的镜像仓库                             |

### 常用命令

- 删除构建器

  ```bash
  docker buildx rm <构建器名称>
  ```

- 设置默认构建器

  ```bash
  docker buildx use <构建器名称>
  ```

- 检查 Docker Buildx 构建器的详细信息

  ```bash
  docker buildx inspect
  ```

  

# 使用脚本构建镜像并推送仓库

> 前提条件：
>
> 1. 已安装 Buildx, 安装脚本: [BUILDX安装](https://gitee.com/offends/Kubernetes/tree/main/Docker/Builder/Buildx/README.md)
>
> 2. 登录一个可推送镜像的仓库
>
>    ```bash
>    docker login <仓库地址> -u <用户名> -p<密码>
>    ```
>
> 3. 根据自己需求添加架构
>
> 4. 配置变量

```bash
export PLATFORM="linux/amd64,linux/arm/v6"
# 这里的仓库地址需要是一个可推送的镜像仓库才行,否则将推送失败
export IMAGE_NAME=<仓库地址>/<镜像名>
export IMAGE_TAG=<镜像标签>
```

开始构建

```bash
./build.sh
```
