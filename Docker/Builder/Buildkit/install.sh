#!/bin/bash

#############################################################################################
# 用途: 安装 Buildkit 脚本
# 作者: 丁辉
# 编写时间: 2023-12-05
#############################################################################################

# 加载检测脚本
source <(curl -sS https://gitee.com/offends/Linux/raw/main/File/Shell/Check_command.sh)

function CHECK(){
    # 官方下载仓库: https://github.com/moby/buildkit/releases/
    VERSION="v0.12.4"
    DIR="/usr/local/buildkit"

    URL="https://github.com/moby/buildkit/releases/download/$VERSION/buildkit-$VERSION.linux-$ARCH_TYPE_2.tar.gz"
    BIN_NAME="Buildkit"
    CHECK_BIN "$DIR/bin/buildctl"
}

# 引用 check_bin 变量不存在则安装
function CHECK_BIN_INSTALL(){
    if [ "$INSTALL_BIN" = false ]; then
        CHECK_INSTALL wget
        SEND_INFO "正在安装 $BIN_NAME,请稍后"
        CHECK_DIR "$DIR"
        CHECK_COMMAND_NULL wget $URL
        CHECK_COMMAND_NULL tar -xf buildkit-v*.linux-$ARCH_TYPE_2.tar.gz -C /usr/local/buildkit
        CHECK_COMMAND_NULL echo 'export PATH=/usr/local/buildkit/bin:$PATH' \>\> /etc/profile
        CHECK_COMMAND_NULL source /etc/profile
        CHECK_SYSTEMD_FILE
        CHECK_COMMAND_NULL systemctl daemon-reload
        CHECK_COMMAND_NULL systemctl enable --now buildkitd
        SEND_INFO "正在清理文件"
        CHECK_COMMAND_NULL rm -rf buildkit-v*.linux-$ARCH_TYPE_2.tar.gz
        SEND_INFO "$BIN_NAME 版本: $(buildkitd --version |  grep -o 'v[0-9]\+\(\.[0-9]\+\)\{2\}')"

    else 
        SEND_INFO "$BIN_NAME 已安装"
        CHECK_SYSTEMD buildkitd
        SEND_INFO "$BIN_NAME 版本: $(buildkitd --version |  grep -o 'v[0-9]\+\(\.[0-9]\+\)\{2\}')"
    fi
}

# 官方 toml 参数文档: https://docs.docker.com/build/buildkit/toml-configuration/
function CHECK_SYSTEMD_FILE(){
    CHECK_COMMAND_NULL \\cp ./buildkitd.service /usr/lib/systemd/system/buildkitd.service
    CHECK_DIR "/etc/buildkit/"
    CHECK_COMMAND_NULL \\cp ./buildkitd.toml /etc/buildkit/buildkitd.toml
}

function ALL(){
    CHECK_SYSTEMD docker
    CHECK_CPU
    CHECK
    CHECK_BIN_INSTALL
}

ALL