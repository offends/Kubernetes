#!/bin/bash

#############################################################################################
# 用途: 打包离线 Docker 安装包
# 作者: 丁辉
# 编写时间: 2023-12-08
#############################################################################################

# 加载检测脚本
source <(curl -sS https://gitee.com/offends/Linux/raw/main/File/Shell/Check_command.sh)

# 二进制文件下载地址: https://download.docker.com/linux/static/stable/
DOCKER_VERSION=${DOCKER_VERSION:-"24.0.7"}

# 根据脚本传进来的参数,执行脚本
function INTRODUCTION_ARGUMENT() {
    case $1 in
    arm64)
        ARCH_TYPE="aarch64"
    ;;
    amd64)
        ARCH_TYPE="x86_64"
    ;;
    -v | --version)
        echo "v1.0.0"
    ;;
    --help | -h)
        echo "Usage: script_name [OPTIONS] [ARGUMENTS]"
        echo ""
        echo "Description:"
        echo "  Offends"
        echo ""
        echo "Options:"
        echo "  -h, --help                  显示此帮助信息"
        echo "  -v, --version               显示当前脚本版本号"
        echo "  amd64                       打包 amd64 离线二进制文件"
        echo "  arm64                       打包 arm64 离线二进制文件"
        echo "Examples:"
        echo "  示例 1: ./offline_packager.sh amd64"
        echo "  示例 2: ./offline_packager.sh arm64"
        exit 0
    ;;
    *)
        SEND_WARN "无效参数, 请使用 --help,-h 查看"
        exit 0
    ;;
    esac
    URL="https://download.docker.com/linux/static/stable/$ARCH_TYPE/docker-$DOCKER_VERSION.tgz"
    DOWNLOAD_BINARY_FILE
}

# 下载二进制文件
function DOWNLOAD_BINARY_FILE() {
    SEND_INFO "正在下载二进制文件"
    CHECK_INSTALL wget
    CHECK_COMMAN""D_NULL wget $URL
    DIR="./Files"
    FILES_NAME=(
        "$DIR/daemon.json"
        "$DIR/docker.service"
        "$DIR/offends.conf"
    )
    FILES=${FILES_NAME[*]}
    sed -i.bak 's/source <(curl -sS https:\/\/gitee.com\/offends\/Linux\/raw\/main\/File\/Shell\/Check_command.sh)/source .\/Check_command.sh/g' install.sh
    sed -i 's/NETWORK_STATE="online"/NETWORK_STATE="offline"/' install.sh
    CHECK_COMMAND_NULL curl -sO https://gitee.com/offends/Linux/raw/main/File/Shell/Check_command.sh
    CHECK_COMMAND_NULL chmod +777 Check_command.sh
    CHECK_COMMAND_NULL tar -zcvf docker-offline.tar.gz docker-$DOCKER_VERSION.tgz $FILES install.sh Check_command.sh
    CHECK_COMMAND_NULL rm -rf docker-$DOCKER_VERSION.tgz rm -rf install.sh
    CHECK_COMMAND_NULL mv install.sh.bak install.sh
    SEND_INFO "打包完成"

}
# 执行全部函数
function ALL() {
    INTRODUCTION_ARGUMENT $@
}

ALL $@