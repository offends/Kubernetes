> 本文作者：丁辉

# Docker启动Cgroup报错

> 报错：dockerd: failed to start daemon: Devices cgroup isn‘t mounted
>
> 问题分析：此问题很可能是 Cgroup 没有挂载导致的，因为 Cgroup 没有挂载，所以无法工作

- 我们使用脚本进行处理

  执行

  ```bash
  curl -sfL https://gitee.com/offends/Kubernetes/raw/main/File/Shell/cgroup.sh | bash
  ```


# Docker重启卡死

>  Docker重启时会出现卡死的现象，导致虽 systemd 重启执行成功但 docker 未运行
>
>  问题分析：根据资料显示，在 docker 19版本重启或关闭 docker 时常出现 `containerd-shim` 残留导致此问题
>
>  解决方法：可临时清理 `containerd-shim` 进程。这种解决方法并不完美，最好的方式是升级 Docker 版本从根源解决问提。

- 停止 Docker

  ```bash
  systemctl stop docker
  ```

- 杀掉docker进程

  ```bash
  ps -ef | grep containerd-shim | awk '{print $2}' | xargs kill -9
  ```

# 无法删除 Docker 存储目录

> 删除失败原因是因为 Docker 存储目录正在被挂载使用

卸载全部挂载

```bash
mount | grep nfs | awk '{print $1}' | xargs umount -l
```

