> 本文作者：丁辉

# Docker-Compose容器安全配置

- **security_opt**

  `security_opt` 选项用于调整容器的安全配置。这个选项允许管理员覆盖或增加默认的安全设置，提供了更多的安全控制。其中一个常见的用途是 `no-new-privileges` 标志。no-new-privileges: 设置为 `true` 时，这个标志阻止容器获取任何新的权限。这意味着即使容器内的应用或用户尝试通过如 `setuid` 等方式提升权限，也会被系统阻止。这是一个防止权限提升攻击的重要安全措施。例如，如果一个容器运行的应用被攻破，攻击者将不能通过提升权限来进一步控制宿主机或其他容器。

  **示例**

  ```bash
  services:
    nginx:
      image: nginx:latest
      container_name: nginx
      security_opt:
        - no-new-privileges:true
  ```

- **cap_drop**

  `cap_drop` 选项用于删除容器的Linux能力。Linux能力是一种精细控制权限的机制，它允许将传统的root权限分解为更小的单元，每个单元控制一个特定的权限。ALL: 使用 `cap_drop: - ALL` 表示放弃所有预定义的能力。这将限制容器内进程的权限，即使它以 root 用户运行，也不能执行某些特权操作，例如修改系统文件、更改网络配置等。这种做法最大限度地减少了容器被滥用的风险，并增加了攻击者通过容器获得宿主机控制权的难度。

  **示例**

  ```bash
  services:
    nginx:
      image: nginx:latest
      container_name: nginx
      cap_drop:
        - ALL
  ```

通过使用这些选项，Docker管理员可以显著提升容器的安全性，避免容器成为攻击者突破系统安全的突破口。这些措施尤其适用于运行不信任的代码或在多租户环境中运行的容器。