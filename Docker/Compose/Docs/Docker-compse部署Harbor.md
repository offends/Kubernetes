> 本文作者：丁辉

# Docker-compse部署Harbor

[官网](https://goharbor.io/)	[包下载位置](https://github.com/goharbor/harbor/releases)

> 离线版本：harbor-offline-installer-v*.tgz
>
> 在线版本：harbor-online-installer-v*.tgz

## 安装Docker-Compose

- Centos

  ```bash
  yum install docker-compose -y
  ```

- Ubuntu

  ```bash
  apt install docker-compose -y
  ```

## 开始安装

1. 下载软件包

   > 本文以现最新版本 v2.8.3 举例

   ```bash
   wget https://ghproxy.com/https://github.com/goharbor/harbor/releases/download/v2.8.3/harbor-offline-installer-v2.8.3.tgz
   ```

2. 解压文件

   ```bash
   tar -zxvf harbor-offline-installer-v*.tgz && cd harbor && cp harbor.yml.tmpl harbor.yml
   ```

3. 更改 harbor.yml 文件

   > 配置 Tcp IP 访问

   ```bash
   vi harbor.yml
   ```

   更改如下内容

   ```yml
   hostname: harbor.store.com
   http:
     port: 9000
     
   #注释域名证书访问
   #https:
     #port: 443
     #certificate: /your/certificate/path
     #private_key: /your/private/key/path
   
   harbor_admin_password: Harbor12345
   
   data_volume: /data
   ```

4. 初始化配置

   ```bash
   ./prepare
   ```

5. 启动 harbor

   ```bash
   ./install.sh
   ```

6. 安装完成后更新 Docker 配置允许使用私有仓库

   修改 Docker 配置文件

   ```bash
   vi /etc/docker/daemon.json
   ```

   添加如下内容

   ```json
   {
     "insecure-registries": ["1.1.1.1:9000"]
   }
   ```

7. 重载 Docker

   ```bash
   systemctl reload docker
   ```

8. 登录测试

   ```bash
   docker login 1.1.1.1:9000 -uadmin -pHarbor12345
   ```

## 配置外部数据库

更改 harbor.yml 文件, 更改如下内容

```yml
external_database:
  harbor:
    host: harbor_db_host
    port: harbor_db_port
    db_name: harbor_db_name
    username: harbor_db_username
    password: harbor_db_password
    ssl_mode: disable
    max_idle_conns: 2
    max_open_conns: 0
  notary_signer:
    host: notary_signer_db_host
    port: notary_signer_db_port
    db_name: notary_signer_db_name
    username: notary_signer_db_username
    password: notary_signer_db_password
    ssl_mode: disable
  notary_server:
    host: notary_server_db_host
    port: notary_server_db_port
    db_name: notary_server_db_name
    username: notary_server_db_username
    password: notary_server_db_password
    ssl_mode: disable
    
external_redis:
  host: redis:6379
  password: 
  registry_db_index: 1
  jobservice_db_index: 2
  trivy_db_index: 5
  idle_timeout_seconds: 30
```

## 使用 trivy 镜像漏洞检测

1. 更改 harbor.yml 文件, 更改如下内容

   ```bash
   trivy:
     ignore_unfixed: false
     skip_update: true #跳过更新
     offline_scan: true #离线扫描
     security_check: vuln
     insecure: false
   ```

2. 启动 harbor 是添加 trivy 启动参数

   ```bash
   ./install.sh --with-trivy
   ```

## 离线环境使用 trivy 导入漏洞数据库

创建持久化目录(如果 harbor 已启动, 则停止后替换目录内容)

```bash
mkdir -p /data/trivy-adapter/trivy/db/
```

### 方法一

[oras官网下载地址](https://github.com/oras-project/oras/releases)

1. 下载软件

   ```bash
   wget https://ghproxy.com/https://github.com/oras-project/oras/releases/download/v1.0.1/oras_1.0.1_linux_amd64.tar.gz
   ```

2. 解压文件

   ```bash
   tar -zxvf oras_*_linux_amd64.tar.gz && mv oras-install/oras /usr/local/bin/
   ```

3. 下载数据

   ```bash
   oras pull ghcr.io/aquasecurity/trivy-db:2
   ```

4. 将数据解压到指定目录

   ```bash
   tar -xzvf db.tar.gz -C /data/trivy-adapter/trivy/db/
   ```

### 方法二

> 外网搭建 harbor, 上传 Nginx 和 Tomcat 进行检测, 获取数据目录 java-db 和 db

1. 线上环境打包书库目录

   ```bash
   cd /data/trivy-adapter/
   tar -zcvf trivy-db-offline.tar.gz trivy
   ```

2. 在离线环境将数据解压到指定目录

   ```bash
   tar -xzvf trivy-db-offline.tar.gz -C /data/trivy-adapter/trivy/db/
   ```

3. 授权目录

   ```bash
   chown -R 10000:10000 /data/trivy-adapter/trivy/db/
   ```

4. 重新启动 harbor 后完成

## Harbor配置签发Https配置私有证书

### 方法一(cfssl)

1. 首先修改 harbor.yml 文件, 配置证书

   ```yml
   hostname: harbor.store.com
   http:
     port: 80
     
   https:
     port: 443
     certificate: /data/ssl/harbor/harbor.pem
     private_key: /data/ssl/harbor/harbor-key.pem
   
   harbor_admin_password: Harbor12345
   
   data_volume: /data
   ```

2. 下载配置证书工具

   [cfssl下载地址](https://github.com/cloudflare/cfssl/releases/)

   ```bash
   wget https://ghproxy.com/https://github.com/cloudflare/cfssl/releases/download/v1.6.3/cfssl_1.6.3_linux_amd64 \  -O   /usr/local/bin/cfssl
    
   wget https://ghproxy.com/https://github.com/cloudflare/cfssl/releases/download/v1.6.3/cfssljson_1.6.3_linux_amd64 \ -O  /usr/local/bin/cfssljson
    
   wget https://ghproxy.com/https://github.com/cloudflare/cfssl/releases/download/v1.6.3/cfssl-certinfo_1.6.3_linux_amd64 \ -O  /usr/local/bin/cfssl-certinfo
    
   chmod +x  /usr/local/bin/cfssl*
   ```

3. 生成并CA配置文件

   ```json
   #cfssl print-defaults  config > ca-config.json
   cat > ca-config.json <<EOF
   {
       "signing": {
           "default": {
               "expiry": "87600h"  
           },
           "profiles": {
               "harbor": {
                   "expiry": "87600h",
                   "usages": [
                       "signing",
                       "key encipherment",
                       "server auth",
                       "client auth"
                   ]
               }
           }
       }
   }
   EOF
   ```

   > `default.expiry`：默认证书有效期（单位：h）
   > `profiles.harbor`：为服务使用该配置文件颁发证书的配置模块
   > signing：签署，表示该证书可用于签名其它证书；生成的 ca.pem 证书中 CA=TRUE
   > `key encipherment`：密钥加密
   > `profiles`：指定了不同角色的配置信息；可以定义多个 profiles，分别指定不同的过期时间、使用场景等参数；后续在签名证书时使用某个 profile
   > `server auth`：服务器身份验证；表示 client 可以用该 CA 对 server 提供的证书进行验证
   > `client auth`：客户端身份验证；表示 server 可以用该 CA 对 client 提供的证书进行验证

4. 生成并修改默认csr请求文件

   ```json
   #cfssl  print-defaults csr  > ca-csr.json
   cat > ca-csr.json <<EOF
   {
       "CN": "harbor",
       "hosts": [
       ],
       "key": {
           "algo": "rsa",
           "size": 2048
       },
       "names": [
           {
               "C": "CN",
               "ST": "Beijing",
               "L": "Beijing"
           }
       ]
   }
   EOF
   ```

   > `hosts`：包含的授权范围，不在此范围的的节点或者服务使用此证书就会报证书不匹配错误，证书如果不包含可能会出现无法连接的情况(此处是CA机构的可为空）
   > `Key`: 指定使用的加密算法，一般使用rsa非对称加密算法（algo:rsa；size:2048）
   > `CN`：Common Name，kube-apiserver 从证书中提取该字段作为请求的用户名 (User Name)；浏览器使用该字段验证网站是否合法
   > `CN`是域名，也就是你现在使用什么域名就写什么域名
   > `O`：Organization，从证书中提取该字段作为请求用户所属的组 (Group)

5. 初始化CA

   ```bash
   cfssl gencert -initca ca-csr.json | cfssljson -bare ca
   ```

   > 可以看到，当前目录下新生成了`ca.csr`、`ca-key.pem`、`ca.pem`这３个文件。 ca-key.pem、ca.pem这两个是CA相关的证书，通过这个CA来签署服务端证书。

6. 创建并修改Harbor证书请求文件

   ```bash
   #cfssl  print-defaults csr >  harbor-csr.json 
   cat > harbor-csr.json <<EOF
   {
       "CN": "1.1.1.1",
       "hosts": [
         "127.0.0.1",
         "1.1.1.1"
       ],
       "key": {
           "algo": "rsa",
           "size": 2048
       },
       "names": [
           {
               "C": "CN",
               "ST": "Beijing",
               "L": "Beijing"
           }
       ]
   }
   EOF
   ```

7. 使用请求文件根据CA配置颁发证书

   ```bash
   cfssl gencert -ca=ca.pem -ca-key=ca-key.pem \
   -config=ca-config.json \
   -profile=harbor  harbor-csr.json | cfssljson -bare  harbor
   ```

8. 拷贝证书到指定目录下

   ```bash
   cp harbor.pem harbor-key.pem  /data/ssl/harbor/
   ```

   > `-config`：指定CA证书机构的配置文件
   > `-profile`：指定使用CA配置文件中的哪个模块（此处harbor对应配置文件中的harbor）
   > `harbor.pem`：harbor服务的数字证书
   > `harbor-key`.pem：harbor服务的私钥

### 方法二(openssl)

1. 首先修改 harbor.yml 文件, 配置证书

   ```yml
   hostname: harbor.store.com
   http:
     port: 80
     
   https:
     port: 443
     certificate: /data/ssl/harbor/harbor.crt
     private_key: /data/ssl/harbor/harbor-key.key
   
   harbor_admin_password: Harbor12345
   
   data_volume: /data
   ```

2. 创建 ca.key

   ```bash
   openssl genrsa -out ca.key 4096
   ```

3. 创建 ca.crt

   ```bash
   openssl req -x509 -new -nodes -sha512 -days 3650  -subj "/C=CN/ST=Beijing/L=Beijing/O=example/OU=Personal/CN=harbor.com" -key ca.key  -out ca.crt
   ```

4. 创建 harbor.key

   ```bash
   openssl genrsa -out harbor.key 4096
   ```

5. 创建 harbor.csr

   ```bash
   openssl req -sha512 -new -subj "/C=CN/ST=Beijing/L=Beijing/O=example/OU=Personal/CN=harbor.com" -key harbor.key -out harbor.csr
   ```

6. 创建x509 v3 扩展 文件

   ```bash
   cat > v3.ext <<EOF
   authorityKeyIdentifier=keyid,issuer
   basicConstraints=CA:FALSE
   keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
   extendedKeyUsage = serverAuth
   subjectAltName = @alt_names
   
   [alt_names]
   DNS.1=harbor.com
   EOF
   ```

7. 使用 v3.ext 文件为 harbor 服务器创建证书

   ```bash
   openssl x509 -req -sha512 -days 3650 -extfile v3.ext -CA ca.crt -CAkey ca.key -CAcreateserial -in harbor.csr -out harbor.crt
   ```

### Docker配置证书验证

1. 创建目录

   ```bash
   mkdir -p /etc/docker/certs.d/harbor.com
   ```

2. 将 crt 文件转换为 cert 文件

   ```bash
   openssl x509 -inform PEM -in harbor.crt -out harbor.cert
   ```

3. 将 cert 和 key 放在对应目录下

   ```bash
   cp harbor.cert harbor.key ca.crt /etc/docker/certs.d/harbor.com/
   ```

4. 重启docker

   ```bash
   systemctl restart docker
   ```
