> 作者：丁辉

# 更新Docker-compose部署的应用

> 进入到你 docker-compose 所在的文件夹下，执行

1. 拉取最新镜像

   ```bash
   docker-compose pull
   ```

2. 使用新镜像重启容器

   ```bash
   docker-compose up -d --remove-orphans
   ```

3. 清理旧容器残留镜像

   ```bash
   docker image prune
   ```

   