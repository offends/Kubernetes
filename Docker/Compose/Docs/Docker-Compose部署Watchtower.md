> 本文作者：丁辉

# Docker-Compose部署Watchtower

[Github仓库](https://github.com/containrrr/watchtower)

## 介绍

Watchtower 是一个开源的容器监控和自动更新工具，设计用于Docker容器环境。它可以监控正在运行的容器及其使用的镜像，当发现镜像有更新时，自动拉取新镜像并重新启动容器。这种自动化管理方式有助于确保部署的应用保持最新状态，从而减少安全风险和改进功能。

## 部署

1. 创建 Docker-Compose 文件

   ```bash
   vi docker-compose.yaml
   ```

   内容如下

   ```bash
   services:      
     watchtower:
       image: containrrr/watchtower:latest
       container_name: watchtower
       volumes:
         - /var/run/docker.sock:/var/run/docker.sock
       command: --interval 14400
   ```

   **Command参数解释**

   - `--interval 14400`：设置 `watchtower` 检查更新的时间间隔为 14400 秒（即 4 小时）。`watchtower` 将每 4 小时检查一次所有运行的容器是否有可用的镜像更新，并在发现新版本时自动重新部署容器。

   - 其他参数请看此文档

     [Docker部署Watchtower管理容器更新](https://gitee.com/offends/Kubernetes/tree/main/Docker/Docs/Docker%E9%83%A8%E7%BD%B2Watchtower%E7%AE%A1%E7%90%86%E5%AE%B9%E5%99%A8%E6%9B%B4%E6%96%B0.md)

2. 启动

   ```bash
   docker-compose up -d
   ```

