> 作者：丁辉

# Docker-compose安装

## 网络安装

> 缺点: 网络安装版本一般过低,大概率为v1

- Centos

  ```bash
  yum -y install docker-compose
  ```

- Ubuntu

  ```bash
  apt -y install docker-compose
  ```

## 二进制安装

[Github下载](https://github.com/docker/compose/releases)

1. 下载

   ```
   curl -L "https://github.com/docker/compose/releases/download/v2.23.3/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
   ```

2. 配置权限

   ```bash
   chmod +x /usr/local/bin/docker-compose
   ```

3. 配置软连接

   ```bash
   ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
   ```

4. 查看结果

   ```bash
   docker-compose --version
   ```

## PIP安装

- 安装

  ```bash
  pip install -U docker-compose
  ```

- 卸载

  ```bash
  pip uninstall docker-compose
  ```

  





