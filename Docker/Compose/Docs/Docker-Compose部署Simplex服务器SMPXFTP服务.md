> 本文作者：丁辉

# Docker-Compose部署Simplex服务器SMPXFTP服务

[官网](https://simplex.chat/)	[Github安装文档](https://github.com/simplex-chat/simplex-chat/blob/stable/docs/SERVER.md)	[客户端下载](https://simplex.chat/downloads/)

|               服务器服务                |      IP      |
| :-------------------------------------: | :----------: |
| simplex-smp-server、simplex-xftp-server | 192.168.1.10 |

## 部署SMP/XFTP服务

1. 创建持久化目录

   ```bash
   mkdir -p /data/simplex/{xftp,smp}/{config,logs} && mkdir -p /data/simplex/xftp/files
   ```

2. 创建 Docker-Compose Env 文件

   ```bash
   cat << EOF >> .env
   SIMPLEX_ADDR=192.168.1.10
   XFTP_ADDR=192.168.1.10
   EOF
   ```

3. 创建 Docker-Compose 文件

   ```bash
   vi docker-compose.yaml
   ```

   内容如下

   ```bash
   version: '3'
   
   networks:
     simplex:
   
   services:
     simplex-smp-server:
       image: simplexchat/smp-server:latest
       container_name: simplex-smp-server
       restart: always
       ports:
         - "5223:5223"
       volumes:
         - /data/simplex/smp/config:/etc/opt/simplex:Z
         - /data/simplex/smp/logs:/var/opt/simplex:Z
       environment:
         - ADDR=${SIMPLEX_ADDR}
         # - PASS=""
       networks:
         - simplex
       security_opt:
         - no-new-privileges:true
       cap_drop:
         - ALL
   
     simplex-xftp-server:
       image: simplexchat/xftp-server:latest
       container_name: simplex-xftp-server
       ports:
         - "443:443"
       restart: always
       volumes:
         - /data/simplex/xftp/config:/etc/opt/simplex-xftp:Z
         - /data/simplex/xftp/logs:/var/opt/simplex-xftp:Z
         - /data/simplex/xftp/files:/srv/xftp:X
       environment:
         - ADDR=${XFTP_ADDR}
         - QUOTA=50gb
       networks:
         - simplex
       security_opt:
         - no-new-privileges:true
       cap_drop:
         - ALL
   ```

4. 启动

   ```bash
   docker-compose up -d
   ```

5. 查看日志获取链接信息

   ```bash
   docker logs -f simplex-smp-server
   ```

   ```bash
   docker logs -f simplex-xftp-server
   ```

   > 保存以 `smp://` 和 `xftp://` 开头的链接信息

6. 到客户端点击头像、网络和服务器、SMP服务器/XFTP服务器、添加服务器、填写链接信息并保存

   > 链接信息格式为：
   >
   > ```bash
   > smp://密钥=@访问地址
   > ```

**问题记录**

`simplex-xftp-server` 端口号为 443 会导致有些人的端口冲突，所以我们可以修改 Docker-Compose 文件内的对外端口比如 "5233:443"，启动后我们客户端链接时需要在IP或域名后添加端口号。如：smp://密钥=@访问地址:5233
