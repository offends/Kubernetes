#!/bin/bash

#############################################################################################
# 用途: 安装 Docker Compose
# 作者: 丁辉
# 编写时间: 2023-12-27
#############################################################################################

# 加载检测脚本
source <(curl -sS https://gitee.com/offends/Linux/raw/main/File/Shell/Check_command.sh)

# 定义变量
VERSION="v2.23.3"

# 安装 Docker Compose
# 检测某个systemd服务是否存在
function CHECK_SYSTEMD(){
    if ! command -v docker-compose >/dev/null 2>&1; then
        INSTALL_DOCKER_COMPOSE
    else
        SEND_INFO "Docker-compose 服务已安装,版本为: $(docker-compose --version | grep -oP 'v\d+\.\d+\.\d+')"
    fi
}

function INSTALL_DOCKER_COMPOSE(){
    SEND_INFO "开始安装 Docker Compose"
    curl -L "https://github.com/docker/compose/releases/download/$VERSION/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
    NULL_TRUE chmod +x /usr/local/bin/docker-compose
    NULL_TRUE ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
    SEND_INFO "Docker Compose 安装完成,版本为: $(docker-compose --version | grep -oP 'v\d+\.\d+\.\d+')"
}

CHECK_SYSTEMD