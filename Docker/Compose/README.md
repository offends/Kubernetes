> 本文作者：丁辉

# Docker-compose使用示例

**前提：**

1. 克隆代码

   ```bash
   git clone https://gitee.com/offends/Kubernetes.git
   cd Kubernetes/Docker/Compose
   ```

2. 进入示例目录

   ```bash
   cd /Yml
   ```

## 构建镜像

```bash
docker-compose -f build-compose.yml build
```

**参数解释**

| 参数         | 描述                                                      |
| ------------ | --------------------------------------------------------- |
| `build`      | 定义服务的构建方式                                        |
| `context`    | 构建上下文的路径，`.` 表示使用当前目录                    |
| `dockerfile` | 指定用于构建镜像的 Dockerfile 文件的路径                  |
| `args`       | 定义构建参数的键值对，这里的 `buildno: 1` 是一个构建参数  |
| `labels`     | 为构建的镜像添加标签，这里添加了一个名为 "offends" 的标签 |
| `target`     | 指定构建阶段的目标，这里设置为 `prod`                     |

## 安装 Gitlab

- 指定文件名启动

  ```bash
  docker-compose -f gitlab-compose.yml up -d
  ```

- 停止

  ```bash
  docker-compose -f gitlab-compose.yml down
  ```

## 示例模版演示

- ```bash
  docker-compose up -d
  ```

**YML参数解释**

[Docker从入门到实践](https://yeasy.gitbook.io/docker_practice/compose/compose_file) [菜鸟教学](https://www.runoob.com/docker/docker-compose.html)

**Docker-compose命令参数解释**

| 命令    | 描述                                              |
| ------- | ------------------------------------------------- |
| build   | 构建或重建服务                                    |
| config  | 解析、解决并渲染规范格式的Compose文件             |
| cp      | 在服务容器和本地文件系统之间复制文件/文件夹       |
| create  | 为一个服务创建容器                                |
| down    | 停止并移除容器和网络                              |
| events  | 接收来自容器的实时事件                            |
| exec    | 在运行中的容器中执行命令                          |
| images  | 列出由创建的容器使用的镜像                        |
| kill    | 强制停止服务容器                                  |
| logs    | 查看容器输出                                      |
| ls      | 列出运行中的Compose项目                           |
| pause   | 暂停服务                                          |
| port    | 打印端口绑定的公共端口                            |
| ps      | 列出容器                                          |
| pull    | 拉取服务镜像                                      |
| push    | 推送服务镜像                                      |
| restart | 重启服务容器                                      |
| rm      | 删除已停止的服务容器                              |
| run     | 在一个服务上运行一次性命令                        |
| scale   | 缩放服务                                          |
| start   | 启动服务                                          |
| stop    | 停止服务                                          |
| top     | 显示运行中的进程                                  |
| unpause | 恢复暂停的服务                                    |
| up      | 创建并启动容器                                    |
| version | 显示 Docker Compose 版本信息                      |
| wait    | 阻塞直到第一个服务容器停止                        |
| watch   | 监视服务的构建环境，当文件更新时重新构建/刷新容器 |
