> 本文作者：丁辉

# Docker安装

## 目录详情

|  文件夹名  |                  内容                  |
| :--------: | :------------------------------------: |
|    Docs    |             Docker文档目录             |
| Dockerfile |        Dockerfile示例、文档目录        |
|  Builder   |         构建镜像示例、文档目录         |
|  Compose   |         Docker-Compose文档目录         |
|   Files    | 存放各文档内部需要使用的脚本或配置文件 |
|  问题记录  |     Docker学习过程中遇到的疑难杂症     |

## 安装脚本使用

> | 支持系统  |  支持架构  |
> | :-------: | :--------: |
> | Centos7,8 | x86、arm64 |
> |  Ubuntu*  | x86、arm64 |

### 在线安装

**极简模式**

> 直接二进制安装最新版 Docker

```bash
curl -sfL https://gitee.com/offends/Kubernetes/raw/main/Docker/install.sh | bash -s all_curl
```

**多功能模式**

> 通过 clone 仓库文件进行安装

```bash
git clone https://gitee.com/offends/Kubernetes.git && cd Kubernetes/Docker
```

- 控制安装方式

  - 切换二进制安装[默认模式]

    ```bash
    export INSTALL_TYPE=binary
    ```

    | 脚本参数 |                   作用                   |        用法         |
    | :------: | :--------------------------------------: | :-----------------: |
    |   rke    | 二进制安装 Rke1 官方支持 Docker 最新版本 |  ./install.sh rke   |
    |  latest  |      二进制安装 Docker 官方最新版本      | ./install.sh latest |

  - 切换官方脚本安装

    ```bash
    export INSTALL_TYPE=official
    ```

    | 脚本参数 |          作用           |         用法         |
    | :------: | :---------------------: | :------------------: |
    | default  |    官方默认安装脚本     | ./install.sh default |
    |  aliyun  | 官方默认安装脚本-阿里源 | ./install.sh aliyun  |

  - 调试模式

    > 本状态使用于对 Docker 做特殊更改或卸载情谨慎使用

    ```bash
    export INSTALL_TYPE=privilege
    ```

    | 脚本参数  |                    作用                    |          用法          |
    | :-------: | :----------------------------------------: | :--------------------: |
    |   stop    |       强制停止 Docker,并清理残留进程       |   ./install.sh stop    |
    | uninstall | 强制卸载 Docker[正常情况下会保留存储文件]  | ./install.sh uninstall |
    |   clean   |   清理 Docker 持久化残留文件,请谨慎使用    |   ./install.sh clean   |
    |  update   | 更新 Docker 版本[只支持二进制部署环境更新] |  ./install.sh update   |


### 离线安装

> 通过 offline_packager.sh 脚本打包离线文件
>
> 可以通过传入 DOCKER_VERSION 变量指定 Docker 版本(默认版本为: 24.0.7)

- 指定版本

  ```bash
  export DOCKER_VERSION="24.0.7"
  ```

  > 清除变量
  >
  > ```bash
  > unset DOCKER_VERSION
  > ```

- 开始打包

  ```bash
  ./offline_packager.sh amd64
  ```

- 使用

  ```bash
  tar -zxvf docker-offline.tar.gz && ./install.sh
  ```

**参数**

| 参数  |          使用方法           |
| :---: | :-------------------------: |
| amd64 | ./offline_packager.sh amd64 |
| arm64 | ./offline_packager.sh arm64 |

