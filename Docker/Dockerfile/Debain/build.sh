#!/bin/bash

#############################################################################################
# 用途: 构建 Debian 系统 Docker 镜像的脚本
# 作者: 丁辉
# 编写时间: 2023-11-27
#############################################################################################

# 镜像地址
# https://docker.debian.net/

ROOTFS="https://github.com/debuerreotype/docker-debian-artifacts/raw"

VERSION="1f1e36af44a355418661956f15e39f5b04b848b6"

FILE="stable/rootfs.tar.xz"

DEBAIN=$ROOTFS/$VERSION/$FILE

# 加载检测脚本
source <(curl -sS https://gitee.com/offends/Linux/raw/main/File/Shell/Check_command.sh)

function INSTALL_WGET(){
    CHECK_INSTALL wget
}

function INSTALL(){
    SEND_INFO "正在下载资源文件,请稍等..."
    CHECK_COMMAND_NULL wget $DEBAIN
    BUILD
}

# 构建 Debian 系统
function BUILD(){
    CHECK_FILE "rootfs.tar.xz"
    SEND_INFO "正在构建 Debian 系统,请稍等..."
    CHECK_COMMAND_NULL docker import rootfs.tar.xz debian:stable
    # docker build -t debian:stable .
    CHECK_COMMAND_NULL rm -rf rootfs.tar.xz
    SEND_INFO "构建完成,镜像名称: debian:stable"
}

function ALL(){
    INSTALL_WGET
    INSTALL
}

ALL