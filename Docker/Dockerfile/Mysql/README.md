*

> 本文作者：丁辉

# Mysql 镜像构建

- 自动初始化 Mysql 数据库, 构建示例

  ```bash
  docker build -t <镜像名:标签> .
  ```

- Mysql 通过 Secrets 隐藏构建账户密码, 示例

  [Mysql-Secrets 使用](https://gitee.com/offends/Kubernetes/tree/main/Docker/Docker%E4%BD%BF%E7%94%A8%E6%96%87%E6%A1%A3/Mysql-secrets%E4%BD%BF%E7%94%A8.md)

  ```bash
  docker build -t <镜像名:标签> --file=./Dockerfile-secrets .
  ```

  
