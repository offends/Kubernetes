*

> 本文作者：丁辉

# Nginx 镜像构建

> Dockerfile示例

|     文件名      |                示例作用                 |                     构建示例                     |
| :-------------: | :-------------------------------------: | :----------------------------------------------: |
|   Dockerfile    |  示例如何通过环境变量更改Nginx配置文件  |          docker build -t 镜像名:标签 .           |
| Dockerfile-ndoe | 示例如何通过过阶段构建，构建Npm前端代码 | docker build -t 镜像名:标签 -f Dockerfile-node . |
