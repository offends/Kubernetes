*

> 本文作者：丁辉

## Drone构建参数解释

|     变量名      |                        变量值                         |      备注      |
| :-------------: | :---------------------------------------------------: | :------------: |
| DOCKER_USERNAME |                                                       |  镜像仓库账号  |
| DOCKER_PASSWORD |                                                       |  镜像仓库密码  |
|    REGISTRY     |           registry.cn-hangzhou.aliyuncs.com           |  镜像仓库地址  |
|      REPO       | registry.cn-hangzhou.aliyuncs.com/<命名空间>/<镜像名> | 镜像的仓库名称 |

