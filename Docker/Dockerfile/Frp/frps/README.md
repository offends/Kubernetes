*

> 本文作者：丁辉

# **Frps内网穿透**

> Frps为内网穿透服务端

## Docker构建

构建镜像

> 默认构建 0.53.2 版本

```bash
docker build -t registry.cn-hangzhou.aliyuncs.com/offends/frp:frps .
```

> 手动选择构建版本

```bash
docker build --build-arg VERSION_ARG=0.53.2 -t registry.cn-hangzhou.aliyuncs.com/offends/frp:frps .
```
