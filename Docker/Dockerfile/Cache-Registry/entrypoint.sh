#!/bin/sh

#############################################################################################
# 用途: 定制缓存 Registry 镜像
# 作者: 丁辉
# 编写时间: 2024-06-29
#############################################################################################

set -e

# 配置 Headers
sed -i "/headers:/a\    Access-Control-Allow-Origin: ['*']" /etc/docker/registry/config.yml
sed -i "/headers:/a\    Access-Control-Allow-Methods: ['HEAD', 'GET', 'OPTIONS', 'DELETE']" /etc/docker/registry/config.yml
sed -i "/headers:/a\    Access-Control-Expose-Headers: ['Docker-Content-Digest']" /etc/docker/registry/config.yml
 
# 检查环境变量PROXY_REMOTE_URL是否非空, 检查配置文件中变量出现的次数是否为0
if [ -n "$PROXY_REMOTE_URL" ] && [ $(grep -c "$PROXY_REMOTE_URL" "/etc/docker/registry/config.yml") -eq 0 ]; then
    echo "proxy:" >> /etc/docker/registry/config.yml
    echo "  remoteurl: $PROXY_REMOTE_URL" >> /etc/docker/registry/config.yml
    # 可以提供用户名和密码保持私密
    # echo "  username: $PROXY_USERNAME" >> /etc/docker/registry/config.yml
    # echo "  password: $PROXY_PASSWORD" >> /etc/docker/registry/config.yml
    echo "----- Enabled Proxy To Remote -----"
fi
# 判断是否开启 Registry 镜像镜像清理
if [ "$DELETE_ENABLED" = "true" ] && [ $(grep -c "delete:" /etc/docker/registry/config.yml) -eq 0 ]; then
    sed -i '/rootdirectory:/a\  delete:' /etc/docker/registry/config.yml
    sed -i '/delete:/a\    enabled: true' /etc/docker/registry/config.yml
    echo "----- Enabled Local Storage Delete -----"
fi

case "$1" in
    *.yaml|*.yml) set -- registry serve "$@" ;;
    serve|garbage-collect|help|-*) set -- registry "$@" ;;
esac

exec "$@"