> 本文作者：丁辉

# 镜像仓库代理服务

## 镜像仓库地址

|   站点名    |             URL              |                            备注                            |
| :---------: | :--------------------------: | :--------------------------------------------------------: |
|  DockerHub  | https://registry-1.docker.io | 拉取镜像需要带上 `library` (可能就我有这情况吧,没仔细深究) |
|    Quay     |       https://quay.io        |                                                            |
|     Gcr     |        https://gcr.io        |                                                            |
|    Ghcr     |       https://ghcr.io        |                                                            |
|   K8sgcr    |      https://k8s.gcr.io      |                                                            |
| Registryk8s |   https://registry.k8s.io    |                                                            |

## 已构建好的镜像

```bash
hub.offends.cn/registry-proxy:latest
```

## 启动容器

[仓库地址](https://gitee.com/offends/Kubernetes/tree/main/Docker/Dockerfile/Cache-Registry)

- Docker

  ```bash
  docker run -itd \
  --restart always \
  -p 5000:5000 \
  -v "/etc/localtime:/etc/localtime" \
  -v "/var/lib/registryproxy:/var/lib/registry" \
  -e PROXY_REMOTE_URL="" \
  --name=registryproxy \
  hub.offends.cn/registry-proxy:latest
  ```

- Docker-compose

  > 文件在本仓库当前目录下, 修改 `PROXY_REMOTE_URL` 参数后即可使用

  ```bash
  docker-compose up -d
  ```

- Kubernetes

  **查看此篇文档**
  
  [Kubernetes部署Registry镜像仓库缓存服务](https://gitee.com/offends/Kubernetes/tree/main/Docker/Dockerfile/Cache-Registry/README.md)
