*

> 本文作者：丁辉

# Drone 基础镜像构建

> Dockerfile示例

|      文件名      |          镜像功能           |                     构建示例                      |
| :--------------: | :-------------------------: | :-----------------------------------------------: |
|  Dockerfile-git  |       最小化 Git 容器       |  docker build -t 镜像名:标签 -f Dockerfile-git .  |
| Dockerfile-minio | 容器内自带 Minio 客户端命令 | docker build -t 镜像名:标签 -f Dockerfile-minio . |
|  Dockerfile-oss  |  容器内自带 oss 客户端命令  |  docker build -t 镜像名:标签 -f Dockerfile-oss .  |

