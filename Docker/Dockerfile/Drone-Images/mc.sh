#!/bin/bash

#############################################################################################
# 用途: 部署 MinIO 客户端工具 mc
# 作者: 丁辉
# 编写时间: 2024-02-14
#############################################################################################

# 判断系统架构
if [ $(arch) = "x86_64" ] || [ $(arch) = "amd64" ]; then
    ARCH_TYPE=linux-amd64
elif [ $(arch) = "aarch64" ] || [ $(arch) = "arm64" ]; then
    ARCH_TYPE=linux-arm64
else
    echo "无法识别的系统架构: $(arch)"
    exit 1
fi

# 变量定义
URL="https://dl.min.io/client/mc/release/$ARCH_TYPE"

# 下载文件
curl -so /usr/local/bin/mc https://dl.min.io/client/mc/release/linux-amd64/mc
# 添加执行权限
chmod 777 /usr/local/bin/mc
