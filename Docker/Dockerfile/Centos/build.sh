#!/bin/bash

#############################################################################################
# 用途: 构建 Centos 系统 Docker 镜像的脚本
# 作者: 丁辉
# 编写时间: 2023-11-27
#############################################################################################

# 镜像地址
# 阿里云: https://mirrors.aliyun.com/centos/
# 官方: https://www.centos.org/
# 其他: https://vault.centos.org/

VERSION="7.9.2009"

CENTOS_VERSION="7"
URL="https://mirrors.aliyun.com/centos/$VERSION/os/x86_64/Packages"
RPM_VERSION="centos-release-7-9.2009.0.el7.centos.x86_64.rpm"

CENTOS_URL="$URL/$RPM_VERSION"

# 加载检测脚本
source <(curl -sS https://gitee.com/offends/Linux/raw/main/File/Shell/Check_command.sh)

function INSTALL_WGET(){
    CHECK_INSTALL wget
}

# 初始化目录和文件
function INIT_DIR(){
    CHECK_DIR ./centos/rootfs && cd ./centos
    CHECK_COMMAND_NULL rpm --root $PWD/rootfs --initdb
    SEND_INFO "初始化目录和文件完成"
    SEND_INFO "正在获取RPM文件"
    CHECK_COMMAND_NULL wget $CENTOS_URL

    CHECK_FILE "centos-release-7-9.2009.0.el7.centos.x86_64.rpm"
    NULL_TRUE rpm -ivh --nodeps --root $PWD/rootfs --package ./$RPM_VERSION

    # #在无法获取到软件包源的情况下使用
    # SEND_INFO "正在备份 YUM 源文件"
    # CHECK_DIR /etc/yum.repos.d/Offends
    # CHECK_COMMAND_NULL \cp -r /etc/yum.repos.d/epel.repo /etc/yum.repos.d/Offends && \cp -r /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/Offends
    # # 获取需要的软件包源
    # SEND_INFO "正在获取软件包源"
    # CHECK_COMMAND_NULL wget -O /etc/yum.repos.d/CentOS-Base.repo https://mirrors.aliyun.com/repo/Centos-$CENTOS_VERSION.repo
    # CHECK_COMMAND_NULL wget -O /etc/yum.repos.d/epel.repo https://mirrors.aliyun.com/repo/epel-$CENTOS_VERSION.repo
    # # 清除缓存
    # SEND_INFO "正在清除缓存"
    # CHECK_COMMAND_NULL yum makecache
    # # 根据自己需求修改
    # CHECK_COMMAND_NULL sed -i 's@baseurl=.*@baseurl=https://mirrors.aliyun.com/centos/7.9.2009/os/x86_64/@' /etc/yum.repos.d/*.repo

    SEND_INFO "正在安装基础软件包,拉取过程较慢请稍后"
    CHECK_COMMAND_NULL yum --installroot=$PWD/rootfs install yum --nogpgcheck -y
    SEND_INFO "开始构建镜像"
    CHECK_COMMAND_NULL cd .. && docker build -t centos:$VERSION .
    SEND_INFO "构建完成,镜像名称: centos:$VERSION"

    # # 恢复 YUM 源文件
    # SEND_INFO "正在恢复 YUM 源文件"
    # CHECK_COMMAND_NULL rm -rf /etc/yum.repos.d/CentOS-Base.repo && rm -rf /etc/yum.repos.d/epel.repo
    # CHECK_COMMAND_NULL cp -r /etc/yum.repos.d/Offends/* /etc/yum.repos.d/
}


function ALL(){
    INSTALL_WGET
    INIT_DIR
}

ALL